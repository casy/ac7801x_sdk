/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

/*!
* @file ac780x_eflash.c
*
* @brief This file provides EFLASH integration functions.
*
*/

/* ===========================================  Includes  =========================================== */
#include "ac780x_eflash_reg.h"

/* ============================================  Define  ============================================ */
#define EFLSH_UNLOCK_KEY1                   0xac7811UL          /* eflash controler ulock key 1    */
#define EFLSH_UNLOCK_KEY2                   0x01234567UL        /* eflash controler ulock key 2    */
#define NUM_OF_OPTION_BYTE_REG              (48UL >> 2)         /* option byte address total count */

/* ======================================  Functions define  ======================================== */
/*!
 * @brief Unlock the eflash control
 *
 * @param[in] none
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_UnlockCtrl(void)
{
    EFLASH_StatusType statusRes = EFLASH_STATUS_SUCCESS;
    uint8_t ctrlLockBit = 0;
    uint32_t timeoutCount = 0;

    ctrlLockBit = EFLASH_GetCtrlLockBitReg();
    while (1 == ctrlLockBit)
    {
        EFLASH_SetKeyReg(EFLSH_UNLOCK_KEY1);
        EFLASH_SetKeyReg(EFLSH_UNLOCK_KEY2);

        if (100 < timeoutCount)
        {
            statusRes = EFLASH_STATUS_TIMEOUT;
            break;
        }
        ctrlLockBit = EFLASH_GetCtrlLockBitReg();
        timeoutCount++;
    }

    return statusRes;
}

/*!
 * @brief Lock the eflash control
 *
 * @param[in] none
 * @return none
 */
void EFLASH_LockCtrl(void)
{
    EFLASH_LockCtrlReg();
}

/*!
 * @brief Waiting for the end of all operation
 *
 * @param[in] timeout: Time out value
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_WaitForLastOperation(uint32_t timeout)
{
    EFLASH_StatusType statusRes = EFLASH_STATUS_SUCCESS;

    if (1 == EFLASH_GetPVDWarningReg())
    {
        statusRes = EFLASH_STATUS_PVDWARNING_ERROR;
    }
    else
    {
        while ((1 == EFLASH_GetAnyBusySTReg()) && (0 != timeout))
        {
            timeout--;
        }
        if (0 == timeout)
        {
            statusRes = EFLASH_STATUS_TIMEOUT;
        }
    }

    return statusRes;
}

/*!
 * @brief Waiting for the end of the operation
 *
 * @param[in] timeout: Time out
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_WaitEop(uint32_t timeout)
{
    EFLASH_StatusType statusRes = EFLASH_STATUS_SUCCESS;

    while ((1 != EFLASH_GetEopReg()) && (0 != timeout))
    {
        timeout--;
    }
    if (0 == timeout)
    {
        statusRes = EFLASH_STATUS_TIMEOUT;
    }

    return statusRes;
}

/*!
 * @brief Read double word from the flash specified address
 *
 * @param[in] eflashAddr: Specified address
 * @return EFLASH STATUS: eflash operation status
 */
uint32_t EFLASH_ReadDWord(uint32_t eflashAddr)
{
    return *(uint32_t*)eflashAddr;
}

/*!
 * @brief Read a certain amount of data from the flash specified address
 *
 * @param[in] readAddr: Eflash address
 * @param[in] dataBuffer: Data buffer
 * @param[in] numbToRead: Number of DWORD
 * @return none
 */
void EFLASH_Read(uint32_t readAddr, uint32_t *dataBuffer, uint32_t numbToRead)
{
    uint32_t i;

    DEVICE_ASSERT(EFLASH_BASE_ADDRESS <= readAddr);
    DEVICE_ASSERT(EFLASH_END_ADDRESS > readAddr);
    DEVICE_ASSERT(NULL != dataBuffer);

    for (i = 0; i < numbToRead; i++)
    {
        dataBuffer[i] = EFLASH_ReadDWord(readAddr);
        readAddr += 4;
    }
}

/*!
 * @brief Erase specified eflash user area address
 *
 * @param[in] pageAddress: Specified eflash addrees to be erased
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_PageErase(uint32_t pageAddress)
{
    EFLASH_StatusType statusRes = EFLASH_STATUS_SUCCESS;
    uint32_t cmdValue = 0x0;

    /* check eflash address */
    DEVICE_ASSERT(EFLASH_BASE_ADDRESS <= pageAddress);
    DEVICE_ASSERT(EFLASH_END_ADDRESS > pageAddress);
    /* Wait for last operation to be completed */
    statusRes = EFLASH_WaitForLastOperation(EFLASH_PAGE_ERASE_TIMEOUT);
    if (EFLASH_STATUS_SUCCESS != statusRes)
    {
        return statusRes;
    }
    /* Configure start address */
    EFLASH_SetStartAddressReg(pageAddress);

    /* Configure command and CMD_ST goes back to 0 */
    cmdValue = (uint32_t)EFLASH_CMD_PAGERASE;  /* Normal operation */
    EFLASH_SetCtrlReg1(cmdValue);

    /* Trigger to start */
    EFLASH_TrigCtrlCmdReg();

    /* Check whether the command is finished */
    statusRes = EFLASH_WaitForLastOperation(EFLASH_PAGE_ERASE_TIMEOUT);
    if (EFLASH_STATUS_SUCCESS == statusRes)
    {
        statusRes = EFLASH_WaitEop(EFLASH_PAGE_ERASE_TIMEOUT);
        if (EFLASH_STATUS_SUCCESS == statusRes)
        {
            /* Check error */
            if (EFLASH_GetStatusReg() & EFLASH_SR0_ERAER_Msk)
            {
                statusRes = EFLASH_STATUS_ERAER_ERROR;
            }
            else
            {
                /* Erase page success now */
                statusRes = EFLASH_STATUS_SUCCESS;
            }
        }
        else
        {
            statusRes = EFLASH_STATUS_BUSY;
        }
    }
    EFLASH_SetCtrlReg1(0x0);

    return statusRes;
}

/*!
 * @brief Mass erase elfash user area
 *
 * @param[in] none
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_MassErase(void)
{
    EFLASH_StatusType statusRes = EFLASH_STATUS_SUCCESS;
    uint32_t cmdValue = 0x0;

    /* Wait for last operation to be completed */
    statusRes = EFLASH_WaitForLastOperation(EFLASH_PAGE_ERASE_TIMEOUT);
    if (EFLASH_STATUS_SUCCESS != statusRes)
    {
        return statusRes;
    }
    /* Configure command and CMD_ST goes back to 0 */
    cmdValue = (uint32_t)EFLASH_CMD_MASSRASE;
    EFLASH_SetCtrlReg1(cmdValue);

    /* Trigger to start */
    EFLASH_TrigCtrlCmdReg();

    /* Check whether the command is finished */
    statusRes = EFLASH_WaitForLastOperation(EFLASH_PAGE_ERASE_TIMEOUT);
    if (EFLASH_STATUS_SUCCESS == statusRes)
    {
        statusRes =  EFLASH_WaitEop(EFLASH_PAGE_ERASE_TIMEOUT);
        if (EFLASH_STATUS_SUCCESS == statusRes)
        {
            /* Check error */
            if (EFLASH_GetStatusReg() & EFLASH_SR0_ERAER_Msk)
            {
                statusRes = EFLASH_STATUS_ERAER_ERROR;
            }
            else
            {
                /* Erase page success now */
                statusRes = EFLASH_STATUS_SUCCESS;
            }
        }
        else
        {
            statusRes = EFLASH_STATUS_BUSY;
        }
    }
    EFLASH_SetCtrlReg1(0x0);

    return statusRes;
}

/*!
 * @brief User page program
 *
 * @param[in] writeAddress: Specified eflash addrees to be programed
 * @param[in] dataBuffer: Point to the data to be programed
 * @param[in] dataLength: Data length to be programed
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_PageProgram(uint32_t writeAddress, uint32_t *dataBuffer, uint32_t dataLength)
{
    EFLASH_StatusType statusRes = EFLASH_STATUS_SUCCESS;
    uint32_t cmdValue = 0x0;
    uint32_t dataCount = 0x0;

    /* check eflash address */
    DEVICE_ASSERT(EFLASH_BASE_ADDRESS <= writeAddress);
    DEVICE_ASSERT(EFLASH_END_ADDRESS > writeAddress);

    DEVICE_ASSERT(NULL != dataBuffer);
    /* check program length,Max length is 512 Dword */
    if (512 < dataLength)
    {
        return EFLASH_STATUS_PARAMETER_ERROR;
    }

    /* Wait for last operation to be completed */
    statusRes = EFLASH_WaitForLastOperation(EFLASH_PAGE_ERASE_TIMEOUT);
    if (EFLASH_STATUS_SUCCESS != statusRes)
    {
        return EFLASH_STATUS_BUSY;
    }
    /* Configure start address */
    EFLASH_SetStartAddressReg(writeAddress);

    /* Configure command and CMD_ST goes back to 0 */
    cmdValue = (dataLength << EFLASH_CTRL0_PROG_LENGTH_Pos) | (uint32_t)EFLASH_CMD_PAGEPROGRAM;
    EFLASH_SetCtrlReg1(cmdValue);

    /* Trigger to start */
    EFLASH_TrigCtrlCmdReg();

    /*program data */
    for (dataCount = 0; dataCount < dataLength; dataCount++)
    {
        *(uint32_t *)writeAddress = dataBuffer[dataCount];
        writeAddress += 4;
    }

    /* Check whether the command is finished */
    statusRes = EFLASH_WaitForLastOperation(EFLASH_PAGE_ERASE_TIMEOUT);
    if (EFLASH_STATUS_SUCCESS == statusRes)
    {
        statusRes =  EFLASH_WaitEop(EFLASH_PAGE_ERASE_TIMEOUT);
        if (EFLASH_STATUS_SUCCESS == statusRes)
        {
            /* Check error */
            if (EFLASH_GetStatusReg() & EFLASH_SR0_PPADRER_Msk)
            {
                statusRes = EFLASH_STATUS_PPADRER_ERROR;
            }
            else
            {
                /* Progeam page success now */
                statusRes = EFLASH_STATUS_SUCCESS;
            }
        }
        else
        {
            statusRes = EFLASH_STATUS_BUSY;
        }
    }
    else
    {
        statusRes = EFLASH_STATUS_BUSY;
    }
    EFLASH_SetCtrlReg1(0x0);

    return statusRes;
}

/*!
 * @brief Verify whether the page erase operation is performed successfully
 *
 * @param[in] pageAddress: Specified eflash addrees to be verified
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_PageEraseVerify(uint32_t pageAddress)
{
    EFLASH_StatusType statusRes = EFLASH_STATUS_SUCCESS;
    uint32_t cmdValue = 0x0;

    /* Check the eflash address */
    DEVICE_ASSERT(EFLASH_BASE_ADDRESS <= pageAddress);
    DEVICE_ASSERT(EFLASH_END_ADDRESS > pageAddress);

    /* Wait for last operation to be completed */
    statusRes = EFLASH_WaitForLastOperation( EFLASH_PAGE_ERASE_TIMEOUT );
    if (EFLASH_STATUS_SUCCESS != statusRes)
    {
        return statusRes;
    }
    /* Configure start address */
    EFLASH_SetStartAddressReg(pageAddress);

    /* Configure command and CMD_ST goes back to 0 */
    cmdValue = (uint32_t)EFLASH_CMD_PAGERASEVERIFY;
    EFLASH_SetCtrlReg1(cmdValue);

    /* Trigger to start */
    EFLASH_TrigCtrlCmdReg();

    /* Check whether the command is finished */
    statusRes = EFLASH_WaitForLastOperation(EFLASH_PAGE_ERASE_TIMEOUT);
    if (EFLASH_STATUS_SUCCESS == statusRes)
    {
        if (1 == EFLASH_GetEopReg())
        {
            /* Check verify result */
            if (EFLASH_GetStatusReg() & EFLASH_SR0_VRER_Msk)
            {
                statusRes = EFLASH_STATUS_VRER_ERROR;
            }
        }
        else
        {
            statusRes = EFLASH_STATUS_BUSY;
        }
    }
    EFLASH_SetCtrlReg1(0x0);

    return statusRes;
}

/*!
 * @brief Verify whether the mass erase operation is performed successfully
 *
 * @param[in] none
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_MassEraseVerify(void)
{
    EFLASH_StatusType statusRes = EFLASH_STATUS_SUCCESS;
    uint32_t cmdValue = 0x0;

    /* Wait for last operation to be completed */
    statusRes = EFLASH_WaitForLastOperation( EFLASH_PAGE_ERASE_TIMEOUT );
    if (EFLASH_STATUS_SUCCESS != statusRes)
    {
        return EFLASH_STATUS_BUSY;
    }
    /* Configure command and CMD_ST goes back to 0 */
    cmdValue = (uint32_t)EFLASH_CMD_MASSRASEVERIFY;
    EFLASH_SetCtrlReg1(cmdValue);

    /* Trigger to start */
    EFLASH_TrigCtrlCmdReg();

    /* Check whether the command is finished */
    statusRes = EFLASH_WaitForLastOperation(EFLASH_PAGE_ERASE_TIMEOUT);
    if (EFLASH_STATUS_SUCCESS == statusRes)
    {
        if (1 == EFLASH_GetEopReg())
        {
            /* Check verify result */
            if (EFLASH_GetStatusReg() & EFLASH_SR0_VRER_Msk)
            {
                statusRes = EFLASH_STATUS_VRER_ERROR;
            }
        }
        else
        {
            statusRes = EFLASH_STATUS_BUSY;
        }
    }
    EFLASH_SetCtrlReg1(0x0);

    return statusRes;
}

/*!
 * @brief Option page erase
 *
 * @param[in] optPageAddress: Specified option addrees to erase
 * @return EFLASH STATUS: eflash operation status
 */
static EFLASH_StatusType EFLASH_OptionPageErase(uint32_t optPageAddress)
{
    EFLASH_StatusType statusRes = EFLASH_STATUS_SUCCESS;
    uint32_t cmdValue = 0x0;

    DEVICE_ASSERT(OPTION_BASE <= optPageAddress);
    DEVICE_ASSERT(OPTION_END > optPageAddress);

    /* Wait for last operation to be completed */
    statusRes = EFLASH_WaitForLastOperation(EFLASH_PAGE_ERASE_TIMEOUT);
    if (EFLASH_STATUS_SUCCESS == statusRes)
    {
        /* Configure start address */
        EFLASH_SetStartAddressReg(optPageAddress);

        /* Configure command and CMD_ST goes back to 0 */
        cmdValue = EFLASH_CTRL0_OPT_CMD_EN_Msk | (uint32_t)EFLASH_CMD_PROTECTERASE;
        EFLASH_SetCtrlReg1(cmdValue);

        /* Trigger to start */
        EFLASH_TrigCtrlCmdReg();

        /* Check whether the command is finished */
        statusRes = EFLASH_WaitForLastOperation(EFLASH_PAGE_ERASE_TIMEOUT);
        if (EFLASH_STATUS_SUCCESS == statusRes)
        {
            statusRes =  EFLASH_WaitEop(EFLASH_PAGE_ERASE_TIMEOUT);
            if (EFLASH_STATUS_SUCCESS == statusRes)
            {
                /* Check error */

                /* Erase option page success now */

            }
            else
            {
                statusRes = EFLASH_STATUS_BUSY;
            }
        }
    }
    else
    {
        statusRes = EFLASH_STATUS_BUSY;
    }
    EFLASH_SetCtrlReg1(0x0);

    return statusRes;
}

/*!
 * @brief Programs data to specified option page address
 *
 * @param[in] pageAddress: Specified option addrees
 * @param[in] dataBuffer: Point to the data to be programed
 * @param[in] dataLength: Data length to be programmed
 * @return EFLASH STATUS: eflash operation status
 */
static EFLASH_StatusType EFLASH_OptionPageProgram(uint32_t pageAddress, uint32_t *dataBuffer, uint32_t dataLength)
{
    EFLASH_StatusType statusRes = EFLASH_STATUS_SUCCESS;
    uint32_t cmdValue = 0x0;
    uint32_t dataCount = 0x0;

    DEVICE_ASSERT(OPTION_BASE <= pageAddress);
    DEVICE_ASSERT(OPTION_END > pageAddress);
    DEVICE_ASSERT(NULL != dataBuffer);

    /* Wait for last operation to be completed */
    statusRes = EFLASH_WaitForLastOperation(EFLASH_PAGE_ERASE_TIMEOUT);
    if (EFLASH_STATUS_SUCCESS == statusRes)
    {
        EFLASH_SetStartAddressReg(pageAddress);

        /* Configure command and CMD_ST goes back to 0 */
        cmdValue = (dataLength << EFLASH_CTRL0_PROG_LENGTH_Pos) | EFLASH_CTRL0_OPT_CMD_EN_Msk | (uint32_t)EFLASH_CMD_PROTECTROGRAM;

        EFLASH_SetCtrlReg1(cmdValue);

        /* Trigger to start */
        EFLASH_TrigCtrlCmdReg();

        /* user program data */
        for (dataCount = 0; dataCount < dataLength; dataCount++)
        {
            *(uint32_t *)pageAddress = dataBuffer[dataCount];
            pageAddress += 4;
        }

        /* Check whether the command is finished */
        statusRes = EFLASH_WaitForLastOperation(EFLASH_PAGE_ERASE_TIMEOUT);
        if (EFLASH_STATUS_SUCCESS == statusRes)
        {
            statusRes =  EFLASH_WaitEop(EFLASH_PAGE_ERASE_TIMEOUT);
            if (EFLASH_STATUS_SUCCESS == statusRes)
            {
                /* Check error */

                /* Erase page success now */
            }
            else
            {
                statusRes = EFLASH_STATUS_BUSY;
            }
        }
        else
        {
            statusRes = EFLASH_STATUS_BUSY;
        }
    }
    else
    {
        statusRes = EFLASH_STATUS_BUSY;
    }
    EFLASH_SetCtrlReg1(0x0);

    return statusRes;
}

/*!
 * @brief Set write protect
 *
 * @param[in] startPageNumber: Page to be changed write protect
 * @param[in] pageLength: write protect page amount
 * @param[in] state: enable state
                -: ENDBLE
                -: DISDBLE
 *
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_SetWriteProtect(uint32_t startPageNumber, uint32_t pageLength ,ACTION_Type state)
{
    EFLASH_StatusType statusRes = EFLASH_STATUS_SUCCESS;
    uint32_t optBuffer[NUM_OF_OPTION_BYTE_REG] = {0};
    uint32_t i;

    DEVICE_ASSERT(EFLASH_PAGE_AMOUNT > startPageNumber);

    EFLASH_Read(OPTION_BASE, optBuffer, NUM_OF_OPTION_BYTE_REG);
    statusRes = EFLASH_OptionPageErase(OPTION_BASE);
    if (EFLASH_STATUS_SUCCESS == statusRes)
    {
        for (i = 0; i < pageLength; i++)
        {
             if (ENABLE == state)
             {
                optBuffer[((startPageNumber + i)>>4) + 2] &= (~(uint32_t)(1 << ((startPageNumber + i)%16)));
             }
             else
             {
                optBuffer[((startPageNumber + i)>>4) + 2] |= (1 << ((startPageNumber + i)%16));
             }
        }
        statusRes = EFLASH_OptionPageProgram(OPTION_BASE, optBuffer, NUM_OF_OPTION_BYTE_REG);
        if (EFLASH_STATUS_SUCCESS != statusRes)
        {
            statusRes = EFLASH_STATUS_OPTER_ERROR;
        }
    }
    else
    {
        statusRes = EFLASH_STATUS_ERAER_ERROR;
    }

    return statusRes;
}

/*!
 * @brief Enable EFLASH read protection
 *
 * This API is used to set(enable) EFLASH read protection. Set(enable) read
 * protection will take effect after reset and the all EFLASH main memory(user
 * pages) will return with 0xAAAAAAAA when read by JTAG/SWD. EFLASH main memory
 * can not be erased or programed by code/JTAG/SWD when in read protection status.
 *
 * @param[in] none
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_EnableReadOut(void)
{
    EFLASH_StatusType statusRes = EFLASH_STATUS_SUCCESS;
    uint32_t optBuffer[NUM_OF_OPTION_BYTE_REG] = {0};

    /*if (1 == EFLASH_GetReadProtectReg()) */
    {
        EFLASH_Read(OPTION_BASE, optBuffer, NUM_OF_OPTION_BYTE_REG);
        statusRes = EFLASH_OptionPageErase(OPTION_BASE);
        optBuffer[0] &= 0xFFFF0000;
        optBuffer[0] = 0x5AA55AA5;
        statusRes = EFLASH_OptionPageProgram(OPTION_BASE, optBuffer, NUM_OF_OPTION_BYTE_REG);
    }

    return statusRes;
}

/*!
 * @brief Disable EFLASH read protection
 *
 * This API is used to clear(disable) EFLASH read protection. Clear(disable) read
 * protection will take effect after reset and the EFLASH main memory(user pages)
 * will be erased to 0xFFFFFFFF automatically.
 *
 * @param[in] none
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_DisableReadOut(void)
{
    EFLASH_StatusType statusRes = EFLASH_STATUS_SUCCESS;
    uint32_t optBuffer[NUM_OF_OPTION_BYTE_REG] = {0};

    /* if (1 != EFLASH_GetReadProtectReg())  //For debug */
    {
        EFLASH_Read(OPTION_BASE, optBuffer, NUM_OF_OPTION_BYTE_REG);
        statusRes = EFLASH_OptionPageErase(OPTION_BASE);
        if (EFLASH_STATUS_SUCCESS != statusRes)
        {
            statusRes = EFLASH_STATUS_ERAER_ERROR;
        }
        else
        {
            optBuffer[0] = 0xFFFFFFAC;
            statusRes = EFLASH_OptionPageProgram(OPTION_BASE, optBuffer, NUM_OF_OPTION_BYTE_REG);
            if (EFLASH_STATUS_SUCCESS != statusRes)
            {
                statusRes = EFLASH_STATUS_PPADRER_ERROR;
            }
        }
    }

    return statusRes;
}

/*!
* @brief config hardware watch.
*
* @param[in] state
                - ENABLE: enalbe hardware watchdog
                - DISABLE: disable hardware watchdog
* @return EFLASH STATUS: eflash operation status
*/
EFLASH_StatusType EFLASH_ConfigWdg(ACTION_Type state)
{
    EFLASH_StatusType statusRes = EFLASH_STATUS_SUCCESS;
    uint32_t optBuffer[NUM_OF_OPTION_BYTE_REG] = {0};

    statusRes = EFLASH_UnlockCtrl();
    if (EFLASH_STATUS_SUCCESS != statusRes)
    {
        return statusRes;
    }
    EFLASH_Read(OPTION_BASE, optBuffer, NUM_OF_OPTION_BYTE_REG);
    statusRes = EFLASH_OptionPageErase(OPTION_BASE);
    if (EFLASH_STATUS_SUCCESS == statusRes)
    {
        if (ENABLE == state)
        {
            optBuffer[1] = 0xFFFFFFCC;
        }
        else
        {
            optBuffer[1] = 0xFFFFFFFF;
        }
        statusRes = EFLASH_OptionPageProgram(OPTION_BASE, optBuffer, NUM_OF_OPTION_BYTE_REG);
        if (EFLASH_STATUS_SUCCESS != statusRes)
        {
            statusRes = EFLASH_STATUS_OPTER_ERROR;
        }
    }
    else
    {
        statusRes = EFLASH_STATUS_ERAER_ERROR;
    }
    EFLASH_LockCtrl();

    return statusRes;
}

/* =============================================  EOF  ============================================== */
