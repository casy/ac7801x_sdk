/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

/*!
* @file ac780x_ckgen.c
*
* @brief This file provides all clock generator config functions.
*
*/

/* ===========================================  Includes  =========================================== */
#include "ac780x_ckgen.h"
/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief enbale the module clock
*
* @param[in] module: CKGEN_ClockType. value can be
*                   - CLK_UART0
*                   - CLK_UART1
*                   - CLK_UART2
*                   - CLK_RESERVE3
*                   - CLK_RESERVE4
*                   - CLK_RESERVE5
*                   - CLK_SPI0
*                   - CLK_SPI1
*                   - CLK_I2C0
*                   - CLK_I2C1
*                   - CLK_PWDT0
*                   - CLK_PWM0
*                   - CLK_PWM1
*                   - CLK_RESERVE13
*                   - CLK_RESERVE14
*                   - CLK_RESERVE15
*                   - CLK_RESERVE16
*                   - CLK_RESERVE17
*                   - CLK_RESERVE18
*                   - CLK_TIMER
*                   - CLK_RTC
*                   - CLK_DMA0
*                   - CLK_RESERVE22
*                   - CLK_GPIO
*                   - CLK_RESERVE24
*                   - CLK_WDG
*                   - CLK_CRC
*                   - CLK_RESERVE27
*                   - CLK_CAN0
*                   - CLK_RESERVE29
*                   - CLK_RESERVE30
*                   - CLK_RESERVE31

*                   - CLK_RESERVE32
*                   - CLK_CTU
*                   - CLK_ADC0
*                   - CLK_ACMP0
*                   - CLK_RESERVE36
*                   - CLK_PWDT1
* @param[in] enable: 0:disable, 1: enable
* @return none
*/
void CKGEN_Enable(CKGEN_ClockType module, ACTION_Type enable)
{
    __IO uint32_t moduleValue = (uint32_t)module;

    DEVICE_ASSERT(IS_CKGEN_CLOCK_PARA(module));
    if (moduleValue < 32)
    {
        if (enable == ENABLE)
        {
            SET_BIT32(CKGEN->PERI_CLK_EN_0, (uint32_t)(1 << moduleValue));
        }
        else
        {
            CLEAR_BIT32(CKGEN->PERI_CLK_EN_0, (uint32_t)(1 << moduleValue));
        }
    }
    else if (moduleValue < (uint32_t)CLK_MODULE_NUM)
    {
        moduleValue -= 32;
        if (enable == ENABLE)
        {
            SET_BIT32(CKGEN->PERI_CLK_EN_1, (uint32_t)(1 << moduleValue));
        }
        else
        {
            CLEAR_BIT32(CKGEN->PERI_CLK_EN_1, (uint32_t)(1 << moduleValue));
        }
    }
    else
    {
        /* do nothing */
    }
}

/*!
* @brief do soft reset for the module
*
* @param[in] module: the module to do Soft Reset. value can be
*                   - SRST_UART0
*                   - SRST_UART1
*                   - SRST_UART2
*                   - SRST_RESERVE3
*                   - SRST_RESERVE4
*                   - SRST_RESERVE5
*                   - SRST_SPI0
*                   - SRST_SPI1
*                   - SRST_I2C0
*                   - SRST_I2C1
*                   - SRST_PWDT0
*                   - SRST_PWM0
*                   - SRST_PWM1
*                   - SRST_RESERVE13
*                   - SRST_RESERVE14
*                   - SRST_RESERVE15
*                   - SRST_RESERVE16
*                   - SRST_RESERVE17
*                   - SRST_RESERVE18
*                   - SRST_TIMER
*                   - SRST_RTC
*                   - SRST_DMA0
*                   - SRST_RESERVE22
*                   - SRST_GPIO
*                   - SRST_RESERVE24
*                   - SRST_WDG
*                   - SRST_CRC
*                   - SRST_RESERVE27
*                   - SRST_CAN0
*                   - SRST_RESERVE29

*                   - //PERI_SFT_RST2
*                   - SRST_RESERVE32
*                   - SRST_CTU
*                   - SRST_ADC0
*                   - SRST_ACMP0
*                   - SRST_ANA_REG
*                   - SRST_PWDT1
* @param[in] active: 1: active, 0: inactive
* @return 0 : no error, -1: not support module
*/
void CKGEN_SoftReset(CKGEN_SoftResetType module, ACTION_Type active)
{
    __IO uint32_t moduleValue = (uint32_t)module;

    DEVICE_ASSERT(IS_CKGEN_SOFT_RESET_PARA(module));
    if (moduleValue < 32)
    {
        if (active == ENABLE)
        {
            SET_BIT32(CKGEN->PERI_SFT_RST0, (uint32_t)(1 << moduleValue));
        }
        else
        {
            CLEAR_BIT32(CKGEN->PERI_SFT_RST0, (uint32_t)(1 << moduleValue));
        }
    }
    else if (moduleValue < (uint32_t)SRST_MODULE_NUM)
    {
        moduleValue -= 32;
        if (active == ENABLE)
        {
            SET_BIT32(CKGEN->PERI_SFT_RST1, (uint32_t)(1 << moduleValue));
        }
        else
        {
            CLEAR_BIT32(CKGEN->PERI_SFT_RST1, (uint32_t)(1 << moduleValue));
        }
    }
    else
    {
        /* do nothing */
    }
}


/*!
* @brief set the sysclck clock source
*
* @param[in] clockSource: set system clock source, value can be
*                        - SYSCLK_SRC_INTERNAL_OSC
*                        - SYSCLK_SRC_PLL_OUTPUT
*                        - SYSCLK_SRC_EXTERNAL_OSC
* @return none
*/
void CKGEN_SetSysclkSrc(SYSTEM_ClockSourceType clockSource)
{
    MODIFY_REG32(CKGEN->CTRL, CKGEN_CTRL_SYSCLK_SEL_Msk, CKGEN_CTRL_SYSCLK_SEL_Pos, (uint32_t)clockSource);
}

/*!
* @brief set the sysclck divider
*
* @param[in] div: system clock divider set, value can be
*                - SYSCLK_DIVIDER_1
*                - SYSCLK_DIVIDER_2
*                - SYSCLK_DIVIDER_3
*                - SYSCLK_DIVIDER_4
* @return none
*/
void CKGEN_SetSysclkDiv(SYSCLK_DividerType div)
{
    MODIFY_REG32(CKGEN->CTRL, CKGEN_CTRL_SYSCLK_DIV_Msk, CKGEN_CTRL_SYSCLK_DIV_Pos, (uint32_t)div);
}

/*!
* @brief set the APB clock divider
*
* @param[in] div: apb clock divider set, value can be
*                - APBCLK_DIVIDER_1
*                - APBCLK_DIVIDER_2
*                - APBCLK_DIVIDER_3
*                - APBCLK_DIVIDER_4
* @return none
*/
void CKGEN_SetAPBClockDivider(APBCLK_DividerType div)
{
    MODIFY_REG32(CKGEN->CTRL, CKGEN_CTRL_APBCLK_DIV_Msk, CKGEN_CTRL_APBCLK_DIV_Pos, (uint32_t)div);
}

/*!
* @brief set the PLL reference
*
* @param[in] ref: set PLL reference clock, value can be
*                - PLL_REF_INTERAL_OSC(8M)
*                - PLL_REF_EXTERNAL_OSC
* @return none
*/
void CKGEN_SetPLLReference(PLL_ReferenceType ref)
{
    MODIFY_REG32(CKGEN->CTRL, CKGEN_CTRL_PLL_REF_SEL_Msk, CKGEN_CTRL_PLL_REF_SEL_Pos, (uint32_t)ref);
}

/*!
* @brief set the PLL Previous Divider
*
* @param[in] div: set pll PREDIV, value can be
*                - PLL_PREDIV_1
*                - PLL_PREDIV_2
*                - PLL_PREDIV_4
* @return none
*/
void CKGEN_SetPllPrevDiv(uint8_t div)
{
    MODIFY_REG32(CKGEN->SYSPLL1_CFG0, CKGEN_SYSPLL1_CFG0_SYSPLL1_PREDIV_Msk, CKGEN_SYSPLL1_CFG0_SYSPLL1_PREDIV_Pos, (uint32_t)div);
}

/*!
* @brief set the PLL post Divider
*
* @param[in] div: set pll post-divider, value can be
*                - PLL_POSDIV_1
*                - PLL_POSDIV_2
*                - PLL_POSDIV_4
* @return none
*/
void CKGEN_SetPllPostDiv(uint8_t div)
{
    MODIFY_REG32(CKGEN->SYSPLL1_CFG0, CKGEN_SYSPLL1_CFG0_SYSPLL1_POSDIV_Msk, CKGEN_SYSPLL1_CFG0_SYSPLL1_POSDIV_Pos, (uint32_t)div);
}

/*!
* @brief set the PLL feedback Divider
*
* @param[in] div: set pll FBKDIV, value can be 0 to 255
* @return none
*/
void CKGEN_SetPllFeedbackDiv(uint8_t div)
{
    MODIFY_REG32(CKGEN->SYSPLL1_CFG0, CKGEN_SYSPLL1_CFG0_SYSPLL1_FBKDIV_Msk, CKGEN_SYSPLL1_CFG0_SYSPLL1_FBKDIV_Pos, (uint32_t)div);
}

/*!
* @brief set the CAN0 clock
*
* @param[in] canIndex: 0:can0
* @param[in] sel: can clock source select, value can be
*                - CAN_CLK_SEL_EXTERNAL_OSC
*                - CAN_CLK_SEL_AHB
* @return none
*/
void CKGEN_SetCANClock(uint8_t canIndex, CAN_ClockSelectType sel)
{
    MODIFY_REG32(CKGEN->CTRL, CKGEN_CTRL_CAN0_CLK_SEL_Msk, CKGEN_CTRL_CAN0_CLK_SEL_Pos, (uint32_t)sel);
}

/*!
* @brief set the CAN time clock divider (for time stamp)
*
* @param[in] canIndex: 0:can0
* @param[in] divider: CAN time clock divider, value can be
*                - 2'h0 : divider by 8
*                - 2'h1 : divider by 16
*                - 2'h2 : divider by 24
*                - 2'h3 : divider by 48
* @return none
*/
void CKGEN_SetCANTimeDivider(uint8_t canIndex, CAN_TimeClockDividerType divider)
{
    MODIFY_REG32(CKGEN->CTRL, CKGEN_CTRL_CAN0_TIMCLK_DIV_Msk, CKGEN_CTRL_CAN0_TIMCLK_DIV_Pos, (uint32_t)divider);
}

/* =============================================  EOF  ============================================== */
