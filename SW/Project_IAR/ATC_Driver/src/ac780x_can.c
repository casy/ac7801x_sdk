/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

/*!
* @file ac780x_can.c
*
* @brief This file provides CAN integration functions.
*
*/

/* ===========================================  Includes  =========================================== */
#include "ac780x_can_reg.h"

/* ============================================  Define  ============================================ */
#define MAX_CAN_WAIT_TIMES              100000UL       /* default wait times, or user define */
#define CAN_RECEIVE_FIFO_COUNT          7UL            /* CAN receive fifo count */
#define CAN_TRANSMIT_FIFO_COUNT         4UL            /* CAN transmit fifo count (1 PTB + 3 STB) */
#define CAN_IRQ_FLAG_MSK                0x00D5FF00UL   /* AN interrupt flag mask macro */

/* ===========================================  Typedef  ============================================ */
typedef struct
{
    uint32_t               interruptFlag;              /* CAN interrupt flag */
    ACTION_Type            interruptEn;                /* CAN interrupt enable */
    ACTION_Type            timeStampEn;                /* Time stamp enable */
    CAN_TransmitAmountType tsAmount;                   /* Transmit secondary all frames or one frame */
    DeviceCallback_Type    callback;                   /* CAN callback pointer */
} CAN_DeviceType;

typedef struct
{
    IRQn_Type           irq;                          /* CAN interrupt number */
    CKGEN_ClockType     clock;                        /* clock */
    CKGEN_SoftResetType reset;                        /* reset */
} CAN_InfoType;

/* ==========================================  Variables  =========================================== */
/* CAN device information */
static const CAN_InfoType s_canInfo[CAN_INSTANCE_MAX] =
{
    {CAN0_IRQn, CLK_CAN0, SRST_CAN0},
};

static CAN_DeviceType s_canDevice[CAN_INSTANCE_MAX] = {NULL};

/* ======================================  Functions define  ======================================== */
/*!
 * @brief Set call back function
 *
 * @param[in] CANx: CAN type pointer
 * @param[in] callbackFunc: Event call back function
 */
void CAN_SetCallBack(CAN_Type *CANx, DeviceCallback_Type callbackFunc)
{
    uint8_t instance = 0;

    instance = CAN_INDEX(CANx);
    DEVICE_ASSERT(CAN_INSTANCE_MAX > instance);
    if (callbackFunc)
    {
        s_canDevice[instance].callback = callbackFunc;
    }
}

/*!
 * @brief CANx interrupt handler function
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return none
 */
void CAN_HandleEvent(CAN_Type *CANx)
{
    uint8_t instance = CAN_INDEX(CANx);
    CAN_DeviceType *device = &s_canDevice[instance];

    device->interruptFlag = (CANx->CTRL1 & CAN_IRQ_FLAG_MSK);
    /* Clear flag */
    CANx->CTRL1 |= device->interruptFlag;

    /* Call back function */
    if (device->callback)
    {
        device->callback(CANx, device->interruptFlag, (uint32_t)CAN_GetKoer(CANx));
    }
}

/*!
* @brief CAN interrupt handle function
 *
 * @param[in] none
 * @return none
 */
void CAN0_Handler(void)
{
    CAN_HandleEvent(CAN0);
}

/*!
 * @brief Set CAN bitrate
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] config: Bitrate config
 * @param[in] dataBitrate: Is data bit rate  0: is not data bit rate, 1: is data bitrate
 * @return none
 */
static void CAN_SetBitrate(CAN_Type *CANx, const CAN_BitrateConfigType *config, uint8_t dataBitrate)
{
    if (!dataBitrate)
    {
        CANx->SBITRATE = (config->PRESC << CAN_BITRATE_PRESC_Pos) + (config->SJW << CAN_BITRATE_SJW_Pos) + (config->SEG_2 << CAN_BITRATE_SEG_2_Pos) + config->SEG_1;
    }
    else
    {
        CANx->FBITRATE = (config->PRESC << CAN_BITRATE_PRESC_Pos) + (config->SJW << CAN_BITRATE_SJW_Pos) + (config->SEG_2 << CAN_BITRATE_SEG_2_Pos) + config->SEG_1;
    }
}

/*!
 * @brief Get CAN error flag
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return error flag
 */
int32_t CAN_GetError(CAN_Type *CANx)
{
    int32_t errorFlag = 0;
    uint8_t instance = 0;

    instance = CAN_INDEX(CANx);

    if (s_canDevice[instance].interruptEn)
    {
        errorFlag = s_canDevice[instance].interruptFlag;
    }
    else
    {
        errorFlag = (CANx->CTRL1 & (CAN_CTRL1_EIF_Msk | CAN_CTRL1_ROIF_Msk | CAN_CTRL1_RFIF_Msk | \
                                    CAN_CTRL1_RAFIF_Msk | CAN_CTRL1_AIF_Msk | CAN_CTRL1_EPIF_Msk | CAN_CTRL1_ALIF_Msk | CAN_CTRL1_BEIF_Msk));
    }

    return errorFlag;
}

/*!
 * @brief Init CANx (GPIO, clock, bitrate, filter and interrupt setting)
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] config: CAN config
 * @return none
 */
void CAN_Init(CAN_Type *CANx, CAN_ConfigType *config)
{
    uint8_t i = 0;
    CAN_FilterControlType *filterList = NULL;
    uint8_t instance = 0;

    instance = CAN_INDEX(CANx);

    DEVICE_ASSERT(CAN_INSTANCE_MAX > instance);
    DEVICE_ASSERT(NULL != config);

    s_canDevice[instance].interruptFlag = 0;

    /* CAN and clock enable */
    if (CAN_CLKSRC_EXTERNAL_OSC == config->clockSrc)
    {
        CKGEN_SetCANClock(instance, CAN_CLK_SEL_EXTERNAL_OSC);
    }
    else
    {
        CKGEN_SetCANClock(instance, CAN_CLK_SEL_AHB);
    }
    CKGEN_Enable(s_canInfo[instance].clock, ENABLE);
    CKGEN_SoftReset(s_canInfo[instance].reset, ENABLE);

    /* Reset CAN */
    CAN_SetReset(CANx, ENABLE);
    /* Set bitrate */
    CAN_SetBitrate(CANx, config->normalBitrate, 0);
    if (config->fdModeEn)
    {
        CAN_SetFdIso(CANx, config->fdIsoEn);
        CAN_SetBitrate(CANx, config->dataBitrate, 1);

        if (config->tdcEnable)
        {
            CAN_SetTdc(CANx, config->tdcEnable);
            CAN_SetSspOffet(CANx, config->sspOffset);
        }
    }

    /* Set filters(user define or use sample setting) */
    if (config->filterList)
    {
        filterList = (CAN_FilterControlType *)config->filterList;
        for (i = 0; i < config->filterNum; i++)
        {
            if(0 != CAN_SetFilter(CANx, filterList[i].index, filterList[i].code, filterList[i].mask, filterList[i].enable))
            {

            }
        }
    }

    /* Start synchronize */
    CAN_SetReset(CANx, DISABLE);

    /* Set work mode */
    if (CAN_MODE_MONITOR == config->canMode)
    {
        CANx->CTRL0 |= CAN_CTRL0_LOM_Msk;
    }
    else if (CAN_MODE_LOOPBACK_INTERNAL == config->canMode)
    {
        CANx->CTRL0 |= CAN_CTRL0_LBMI_Msk;
    }
    else if (CAN_MODE_LOOPBACK_EXTERNAL == config->canMode)
    {
        CANx->CTRL0 |= CAN_CTRL0_LBME_Msk;
        CAN_SetSack(CANx, config->selfAckEn);
    }
    else
    {

    }

    CAN_SetAfwl(CANx, CAN_RECEIVE_FIFO_COUNT - 1);
    CAN_SetEwl(CANx, config->errorWarningLimit);

    CAN_SetTpss(CANx, config->tpss);
    CAN_SetTsss(CANx, config->tsss);

    CAN_SetTSMode(CANx, config->tsMode); /* 0->FIFO mode 1->priority decision mode */
    CAN_SetRom(CANx, config->rom); /* 0->overwrite the oldest message 1->discard new message */

    s_canDevice[instance].tsAmount = config->tsAmount;
    s_canDevice[instance].callback = config->callback;
    if (config->timeStampEn)
    {
        CKGEN_SetCANTimeDivider(instance, config->timeStampClk);
        s_canDevice[instance].timeStampEn = config->timeStampEn;
        CAN_SetTimePosition(CANx, config->timeStampPos);
        CAN_EnableTime(CANx, ENABLE);
    }

    CAN_SetIntEnable(CANx, config->interruptMask);
    /* Enable interrupt */
    if (config->interruptEn)
    {
        s_canDevice[instance].interruptEn = config->interruptEn;
        NVIC_EnableIRQ(s_canInfo[instance].irq);
    }
}

/*!
 * @brief Uninitialize CANx
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return none
 */
void CAN_DeInit(CAN_Type *CANx)
{
    uint8_t instance = 0;

    instance = CAN_INDEX(CANx);
    DEVICE_ASSERT(CAN_INSTANCE_MAX > instance);
    CAN_SetReset(CANx, ENABLE);
    /* CAN and clock disable */
    NVIC_DisableIRQ(s_canInfo[instance].irq);
    s_canDevice[instance].callback = NULL;
    NVIC_ClearPendingIRQ(s_canInfo[instance].irq);
    CKGEN_SoftReset(s_canInfo[instance].reset, DISABLE);
    CKGEN_Enable(s_canInfo[instance].clock, DISABLE);
}

/*!
 * @brief Wait transmit primary done, otherwise timeout
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return Transmit flag (0: done, 1: timeout)
 */
int32_t WaitTransmitPrimaryDone(CAN_Type *CANx)
{
    uint32_t i = 0;
    int32_t ret = 0;

    do
    {
        if (CANx->CTRL1 & CAN_CTRL1_TPIF_Msk)
        {
            CANx->CTRL1 |= CAN_CTRL1_TPIF_Msk;
            break;
        }
    } while (MAX_CAN_WAIT_TIMES > i++);

    if (MAX_CAN_WAIT_TIMES <= i)
    {
        ret = 1;
    }

    return ret;
}

/*!
 * @brief Wait transmit secondary done, otherwise timeout
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return Transmit flag (0: done, 1: timeout)
 */
int32_t WaitTransmitSecondaryDone(CAN_Type *CANx)
{
    uint32_t i = 0;
    int32_t ret = 0;

    do
    {
        if (CANx->CTRL1 & CAN_CTRL1_TSIF_Msk)
        {
            CANx->CTRL1 |= CAN_CTRL1_TSIF_Msk;
            break;
        }
    } while (MAX_CAN_WAIT_TIMES > i++);

    if (MAX_CAN_WAIT_TIMES <= i)
    {
        ret = 1;
    }

    return ret;
}

/*!
 * @brief Wait transmit done
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] type: CAN transmit type
                - CAN_TRANSMIT_PRIMARY
                - CAN_TRANSMIT_SECONDARY
 * @return Transmit flag (0: done, 1: timeout)
 */
int32_t CAN_WaitTransmissionDone(CAN_Type *CANx, CAN_TransmitBufferType type)
{
    int32_t ret = 0;
    uint8_t instance = 0;

    instance = CAN_INDEX(CANx);

    if (s_canDevice[instance].interruptEn)
    {
        /* clear flag in IRQ */
    }
    else if (type == CAN_TRANSMIT_PRIMARY)
    {
        ret = WaitTransmitPrimaryDone(CANx);
    }
    else
    {
        ret = WaitTransmitSecondaryDone(CANx);
    }

    return ret;
}

/*!
 * @brief Wait for transmitting done in primary mode; or transmission buffer not full in secondary mode, otherwise timeout
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] type: CAN transmit type
                - CAN_TRANSMIT_PRIMARY
                - CAN_TRANSMIT_SECONDARY
 * @return Transmit idle flag (0: idle, 1: timeout)
 */
int32_t CAN_WaitTransmissionIdle(CAN_Type *CANx, CAN_TransmitBufferType type)
{
    uint32_t i = 0;
    int32_t ret = 0;

    do
    {
        if (!CAN_IsTransmitting(CANx, type))
        {
            break;
        }
    } while (MAX_CAN_WAIT_TIMES > i++);

    if (MAX_CAN_WAIT_TIMES <= i)
    {
        ret = 1;
    }

    return ret;
}

/*!
 * @brief Get payload size
 *
 * @param[in] dlcValue: DLC value
 * @return ret: payload size
 */
uint8_t CAN_GetPayloadSize(uint8_t dlcValue)
{
    uint8_t ret = 0U;

    if (8U >= dlcValue)
    {
        ret = dlcValue;
    }
    else
    {
        switch (dlcValue)
        {
        case CAN_DLC_12_BYTES:
            ret = 12U;
            break;

        case CAN_DLC_16_BYTES:
            ret = 16U;
            break;

        case CAN_DLC_20_BYTES:
            ret = 20U;
            break;

        case CAN_DLC_24_BYTES:
            ret = 24U;
            break;

        case CAN_DLC_32_BYTES:
            ret = 32U;
            break;

        case CAN_DLC_48_BYTES:
            ret = 48U;
            break;

        case CAN_DLC_64_BYTES:
            ret = 64U;
            break;

        default:
            break;
        }
    }

    return ret;
}

/*!
 * @brief Set message information
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] info: CAN message information
 * @param[in] type: CAN transmit type
                - CAN_TRANSMIT_PRIMARY
                - CAN_TRANSMIT_SECONDARY
 * @return none
 */
void CAN_SetMsgInfo(CAN_Type *CANx, const CAN_MsgInfoType *info, CAN_TransmitBufferType type)
{
    uint8_t dataLength = 0, i = 0;
    uint32_t tempControl = 0, tempId = 0, tempData = 0;

    /* Select transmit buffer */
    if (CAN_TRANSMIT_PRIMARY == type)
    {
        CANx->CTRL0 &= (~CAN_CTRL0_TBSEL_Msk);
    }
    else
    {
        CANx->CTRL0 |= CAN_CTRL0_TBSEL_Msk;
    }
    /* Set message information */
    tempId = (info->ID & CAN_INFO_ID_Msk);

    /* Set TTSEN, it is the same mask between ESI and TTSEN */
    if (s_canDevice[CAN_INDEX(CANx)].timeStampEn)
    {
        tempId |= CAN_INFO_ESI_Msk;
    }
    CANx->TBUF.ID_TTSEN = tempId;

    tempControl = (uint32_t)(info->DLC & CAN_INFO_DLC_Msk);

    if (info->BRS)
    {
        tempControl |= CAN_INFO_BRS_Msk;
    }
    if (info->RTR)
    {
        tempControl |= CAN_INFO_RTR_Msk;
    }
    if (info->FDF)
    {
        tempControl &= (~CAN_INFO_RTR_Msk);
        tempControl |= CAN_INFO_FDF_Msk;
    }
    if (info->IDE)
    {
        tempControl |= CAN_INFO_IDE_Msk;
    }
    CANx->TBUF.TXCTRL = tempControl;
    if ((0 == info->RTR) && info->DLC && info->DATA)
    {
        dataLength = CAN_GetPayloadSize(info->DLC);
        if ((uint32_t)info->DATA % 4)
        {
            for (i = 0; i < dataLength; i += 4)
            {
                tempData = info->DATA[i] | (info->DATA[i + 1] << 8) |
                           (info->DATA[i + 2] << 16) | (info->DATA[i + 3] << 24);
                CANx->TBUF.DATA[i >> 2] = tempData;
            }
        }
        else
        {
            for (i = 0; i < dataLength; i += 4)
            {
                CANx->TBUF.DATA[i >> 2] = *(uint32_t *)(&info->DATA[i]);
            }
        }
    }
    /* Point to secondary next buffer */
    if (CAN_TRANSMIT_SECONDARY == type)
    {
        CANx->CTRL0 |= CAN_CTRL0_TSNEXT_Msk;
    }
}

/*!
 * @brief Set transmit amount for can secondary buffer
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] amount: CAN transmit amount
                - CAN_TRANSMIT_ALL
                - CAN_TRANSMIT_ONE
 * @return none
 */
void CAN_SetTransmitAmount(CAN_Type *CANx, CAN_TransmitAmountType amount)
{
    uint8_t instance = 0;

    instance = CAN_INDEX(CANx);

    if (CAN_TRANSMIT_AMOUNT_MAX > amount)
    {
        s_canDevice[instance].tsAmount = amount;
    }
}

/*!
 * @brief Transmit CANx message
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] info: CAN message information
 * @param[in] type: CAN transmit type
                - CAN_TRANSMIT_PRIMARY
                - CAN_TRANSMIT_SECONDARY
 * @return 0: success, -1: busy, -2: timeout, -3: send error, -4: Transmit timeout
 */
int32_t CAN_TransmitMessage(CAN_Type *CANx, const CAN_MsgInfoType *info, CAN_TransmitBufferType type)
{
    int32_t ret = 0;
    uint8_t instance = 0;

    instance = CAN_INDEX(CANx);

    if (s_canDevice[instance].interruptEn)
    {
        if (CAN_IsTransmitting(CANx, type))
        {
            ret = -1;
        }
    }
    else
    {
        if (CAN_WaitTransmissionIdle(CANx, type))
        {
            ret = -2;
        }
    }

    if (ret == 0)
    {
        CAN_SetMsgInfo(CANx, info, type);

        if (CAN_StartTransmission(CANx, type, s_canDevice[instance].tsAmount))
        {
            ret = -3;
        }
        else
        {
            if (CAN_WaitTransmissionDone(CANx, type))
            {
                ret = -4;
            }
        }
    }
    else  /* If it is busy or timeout, wait some time */
    {
    }

    return ret;
}

/*!
* @brief Receive CANx message
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] info: CAN message information
 * @return 0: success, 1: no message
 */
int32_t CAN_ReceiveMessage(CAN_Type *CANx, CAN_MsgInfoType *info)
{
    int32_t ret = 0;
    uint8_t dataLength = 0, i = 0;

    if (!CAN_IsMsgInReceiveBuf(CANx))
    {
        ret = 1;
    }
    else
    {
        /* Read message information */
        info->ID  = CANx->RBUF.ID_ESI  &  CAN_INFO_ID_Msk;
        info->ESI = CANx->RBUF.ID_ESI  >> CAN_INFO_ESI_Pos;
        info->BRS = (CANx->RBUF.RXCTRL >> CAN_INFO_BRS_Pos) & 0x01;
        info->FDF = (CANx->RBUF.RXCTRL >> CAN_INFO_FDF_Pos) & 0x01;
        info->RTR = (CANx->RBUF.RXCTRL >> CAN_INFO_RTR_Pos) & 0x01;
        info->IDE = (CANx->RBUF.RXCTRL >> CAN_INFO_IDE_Pos) & 0x01;
        info->DLC = CANx->RBUF.RXCTRL  &  CAN_INFO_DLC_Msk;

        if ((0 == info->RTR) && info->DLC && info->DATA)
        {
            dataLength = CAN_GetPayloadSize(info->DLC);
            if ((0 == info->FDF) && (8 < dataLength))
            {
                dataLength = 8;
            }
            if (((uint32_t)info->DATA % 4) || (8 > dataLength))
            {
                for (i = 0; i < dataLength; i++)
                {
                    info->DATA[i] = ((uint8_t *)CANx->RBUF.DATA)[i];
                }
            }
            else
            {
                for (i = 0; i < dataLength; i += 4)
                {
                    *(uint32_t *)(&info->DATA[i]) = CANx->RBUF.DATA[i >> 2];
                }
            }
        }
        info->RTS = CANx->RBUF.RTS[0];
        /* Receive buffer release */
        CANx->CTRL0 |= CAN_CTRL0_RREL_Msk;
    }

    return ret;
}

/*!
 * @brief Set CANx filter when RESET = 1
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] index:CAN filter id
                - (0-15)
 * @param[in] code: CAN filter code
 * @param[in] mask: CAN filter mask
 * @param[in] state: CAN filter enable state
                - ENDBLE
                - DISDBLE
 * @return 0: success, 1: reset error
 */
int32_t CAN_SetFilter(CAN_Type *CANx, uint8_t index, uint32_t code, uint32_t mask, ACTION_Type state)
{
    int32_t ret = 0;

    DEVICE_ASSERT(CAN_MAX_FILTER_NUM > index);
    if (0 == (CANx->CTRL0 & CAN_CTRL0_RESET_Msk))  /* Should set RESET = 1 first */
    {
        ret = 1;
    }
    else
    {
        CAN_SetAcfEn(CANx, index, state);
        if (state)
        {
            CAN_SetAcfIndex(CANx, index);
            CAN_SetAcfCode(CANx, code);
            CAN_SetAcfMask(CANx, mask);
        }
    }

    return ret;
}

/* =============================================  EOF  ============================================== */
