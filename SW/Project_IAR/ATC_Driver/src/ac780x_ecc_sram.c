/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

/*!
* @file ac780x_ecc_sram.c
*
* @brief This file provides ecc_sram integration functions.
*
*/

/* ===========================================  Includes  =========================================== */
#include "ac780x_ecc_sram_reg.h"

/* ==========================================  Variables  =========================================== */

/* ECC_SRAM callback pointer*/
static DeviceCallback_Type s_ecc_SramCallback = NULL;

/* ======================================  Functions define  ======================================== */
/*!
* @brief ECC_SRAM initialize
*
* @param[in] config: ECC_SRAM configuration pointer
* @return none
*/
void ECC_SRAM_Init(const ECC_SRAM_ConfigType *config)
{
    DEVICE_ASSERT(NULL != config);

    /* Register callback function */
    s_ecc_SramCallback = config->callBack;

    /* Set ECC_SRAM interrupt */
    ECC_SRAM_SetInterrupt(config->interruptEn);
    if (ENABLE == config->interruptEn)
    {
        NVIC_EnableIRQ(ECC_SRAM_IRQn);
    }
    else
    {
        NVIC_DisableIRQ(ECC_SRAM_IRQn);
    }

    if (ENABLE == config->resetEn)
    {
        ECC_SRAM_Enable2BitErrReset(ENABLE);
    }
    else
    {
        ECC_SRAM_Enable2BitErrReset(DISABLE);
    }
}

/*!
* @brief ECC_SRAM De-initialize
*
* @param[in] none
* @return none
*/
void ECC_SRAM_DeInit()
{
    s_ecc_SramCallback = NULL;
    NVIC_DisableIRQ(ECC_SRAM_IRQn);
}

/*!
 * @brief Set call back function
 *
 * @param[in] callbackFunc: Event call back function
 */
void ECC_SRAM_SetCallBack(DeviceCallback_Type callbackFunc)
{
    if (NULL != callbackFunc)
    {
        s_ecc_SramCallback = callbackFunc;
    }
}

/*!
* @brief ECC_SRAM err interrupt handler
*
* @param[in] none
* @return none
*/
static void ECC_SRAM_CommonISR()
{
    uint32_t wpara = 0;

    /* store device status */
    wpara = ECC_SRAM_GetInterruptFlag();

    /* clear flag*/
    ECC_SRAM_ClearInterrupFlag();
    if (NULL != s_ecc_SramCallback)
    {
        /* callback */
        s_ecc_SramCallback(ECC_SRAM, wpara, 0);
    }
}

/*!
* @brief ECC_SRAM interrupt handler
*
* @param[in] none
* @return none
*/
void ECC_SRAM_IRQHandler()
{
   ECC_SRAM_CommonISR();
}

/* =============================================  EOF  ============================================== */
