/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

/*!
* @file ac780x_acmp.c
*
* @brief This file provides analog comparator module integration functions.
*
*/

/* ===========================================  Includes  =========================================== */
#include "ac780x_acmp_reg.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */
/* ACMP related info */
static const IRQn_Type s_acmpIRQ[ACMP_INSTANCE_MAX] =
{ACMP0_IRQn};

/* ACMP callback pointer */
static DeviceCallback_Type s_acmpCallback[ACMP_INSTANCE_MAX] = {NULL};

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief ACMP initialize.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] config: pointer to configuration structure
* @return none
*/
void ACMP_Init(ACMP_Type *ACMPx, const ACMP_ConfigType *config)
{
    uint8_t instance = 0;

    instance = ACMP_INDEX(ACMPx);

    DEVICE_ASSERT(ACMP_INSTANCE_MAX > instance);
    DEVICE_ASSERT(NULL != config);

    /* Enbale acmp clock */
    CKGEN_Enable(CLK_ACMP0, ENABLE);
    CKGEN_SoftReset(SRST_ACMP0, ENABLE);

    /* Set dac output */
    ACMP_SetDacReference(ACMPx, config->dacRef);
    ACMP_SetDacOutput(ACMPx, config->dacValue, config->dacEn);

    /* Set input pin */
    ACMP_SetPositiveInputPin(ACMPx, config->positivePin);
    ACMP_SetNegativeInputPin(ACMPx, config->negativePin);

    /* Set output pin */
    ACMP_SetOutputToPin(ACMPx, config->acmpOutEn);

    /* Set hysteresis voltage */
    ACMP_SetHysteresisVoltage(ACMPx, config->hysterisisVol);

    /* Set polling mode */
    if (ACMP_POSITIVE_POLLING == config->pollingModeType)
    {
        ACMP_SetPositivePollingMode(ACMPx, ENABLE);
        ACMP_SetNegativePollingMode(ACMPx, DISABLE);
    }
    else if (ACMP_NEGATIVE_POLLING == config->pollingModeType)
    {
        ACMP_SetPositivePollingMode(ACMPx, DISABLE);
        ACMP_SetNegativePollingMode(ACMPx, ENABLE);
    }
    else
    {
        ACMP_SetPositivePollingMode(ACMPx, DISABLE);
        ACMP_SetNegativePollingMode(ACMPx, DISABLE);
    }

    ACMP_SetPollingModeSequence(ACMPx, config->pollingSequence);
    ACMP_SetPollingModeFreqDiv(ACMPx, config->pollingDiv);

    /* Set hall output */
    ACMP_SetHallOutput(ACMPx, config->hallAOutputCh, config->hallBOutputCh, config->hallCOutputCh, config->hallOutEn);

    /* Set default lpf 1000KHz */
    ACMP_SetLowPassFilter(ACMP_LPF_1000KHZ);

    /* Enable acmp module*/
    ACMP_Enable(ACMPx, ENABLE);

    /* Set interrupt type*/
    ACMP_SetTriggerInterruptEdgeMode(ACMPx, config->edgeType);

    /* Register callback function */
    s_acmpCallback[instance] = config->callBack;

    /* Clear status */
    ACMPx->SR = 0xFFFF;
    ACMP_SetInterrupt(ACMPx, config->interruptEn);
    if (ENABLE == config->interruptEn)
    {
        NVIC_EnableIRQ(s_acmpIRQ[instance]);
    }
    else
    {
        NVIC_DisableIRQ(s_acmpIRQ[instance]);
    }
}

/*!
* @brief ACMP De-initialize.
*
* @param[in] none
* @return none
*/
void ACMP_DeInit(ACMP_Type *ACMPx)
{
    uint8_t instance = 0;

    instance = ACMP_INDEX(ACMPx);
    DEVICE_ASSERT(ACMP_INSTANCE_MAX > instance);

    s_acmpCallback[instance] = NULL;
    NVIC_DisableIRQ(s_acmpIRQ[instance]);
    NVIC_ClearPendingIRQ(s_acmpIRQ[instance]);
    CKGEN_SoftReset(SRST_ACMP0, DISABLE);
    CKGEN_Enable(CLK_ACMP0, DISABLE);
}

/*!
* @brief Set acmp callback function.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] func: callback function
* @return none
*/
void ACMP_SetCallback(ACMP_Type *ACMPx, const DeviceCallback_Type func)
{
    uint8_t instance = 0;

    instance = ACMP_INDEX(ACMPx);

    DEVICE_ASSERT(ACMP_INSTANCE_MAX > instance);
    s_acmpCallback[instance] = func;
}

/*!
* @brief ACMP common interrupt service routine.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @return none
*/
static void ACMP_CommonISR(ACMP_Type *ACMPx)
{
    uint8_t instance = 0;
    uint32_t wpara = 0;

    instance = ACMP_INDEX(ACMPx);

    /* store device status */
    wpara = ACMPx->SR;

    /* clear status */
    ACMPx->SR = wpara;

    if (NULL != s_acmpCallback[instance])
    {
        /* callback */
        s_acmpCallback[instance](ACMPx, wpara, 0);
    }
}

/*!
* @brief ACMP0 interrupt request handler.
*
* @param[in] none
* @return none
*/
void ACMP0_IRQHandler(void)
{
    ACMP_CommonISR(ACMP0);
}

/* =============================================  EOF  ============================================== */
