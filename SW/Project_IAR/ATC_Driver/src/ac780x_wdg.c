/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

/*!
* @file ac780x_wdg.c
*
* @brief This file provides wdg integration functions.
*
*/

/* ===========================================  Includes  =========================================== */
#include "ac780x_wdg_reg.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */
/* WDG callback pointer */
static DeviceCallback_Type s_wdgCallback = (DeviceCallback_Type)NULL;

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief Initialize WDG module
*
* @param[in] config: WDG configuration pointer
* @return none
*/
void WDG_Init(const WDG_ConfigType *config)
{
    uint32_t CS0 = WDG->CS0, CS1 = WDG->CS1;

    DEVICE_ASSERT(NULL != config);

    /* enable wdg system module */
    CKGEN_Enable(CLK_WDG, ENABLE);
    CKGEN_SoftReset(SRST_WDG, ENABLE);

    CS1 &= ~WDG_CS1_FLG_Msk;
    /* config wdg clocksource */
    CS1 &= ~WDG_CS1_CLK_Msk;
    CS1 |= (((uint32_t)config->clockSource) << WDG_CS1_CLK_Pos) & WDG_CS1_CLK_Msk;

    /* config wdg update */
    if (DISABLE == config->updateEn)
    {
        CS0 &= ~WDG_CS0_UPDATE_Msk;
    }
    else
    {
        CS0 |= WDG_CS0_UPDATE_Msk;
    }

    /* config wdg enable */
    if (DISABLE == config->WDGEn)
    {
        CS0 &= ~WDG_CS0_EN_Msk;
    }
    else
    {
        CS0 |= WDG_CS0_EN_Msk;
    }

    /* config wdg psr enable */
    if (DISABLE == config->prescalerEn)
    {
        CS1 &= ~WDG_CS1_PRES_Msk;
    }
    else if (ENABLE == config->prescalerEn)
    {
        CS1 |= WDG_CS1_PRES_Msk;
    }
    /* config wdg win enable */
    if (DISABLE == config->windowEn)
    {
        CS1 &= ~WDG_CS1_WIN_Msk;
    }
    else if (ENABLE == config->windowEn)
    {
        CS1 |= WDG_CS1_WIN_Msk;
    }
    /* config wdg int enable */
    if (DISABLE == config->interruptEn)
    {
        CS0 &= ~WDG_CS0_INT_Msk;
    }
    else if (ENABLE == config->interruptEn)
    {
        CS0 |= WDG_CS0_INT_Msk;
    }

    /* register user callback function */
    s_wdgCallback = config->callBack;

    /* enable wdg interrupt */
    if (DISABLE == config->interruptEn)
    {
        NVIC_DisableIRQ(WDG_IRQn);
        NVIC_ClearPendingIRQ(WDG_IRQn);
    }
    else if (ENABLE == config->interruptEn)
    {
        NVIC_EnableIRQ(WDG_IRQn);
    }

    DisableInterrupts;
    WDG_Unlock();
    WDG_SetTimeOutVal(config->timeoutValue);
    WDG_SetWindowVal(config->windowValue);
    WDG_SetCS1(CS1);
    WDG_SetCS0(CS0);
    EnableInterrupts;
}

/*!
* @brief WDG De-initialize
*
* @param[in] none
* @return none
*/
void WDG_DeInit(void)
{
    s_wdgCallback = (DeviceCallback_Type)NULL;
    NVIC_DisableIRQ(WDG_IRQn);
    NVIC_ClearPendingIRQ(WDG_IRQn);
    CKGEN_SoftReset(SRST_WDG, DISABLE);
    CKGEN_Enable(CLK_WDG, DISABLE);
}

/*!
* @brief set WDG Callback function
*
* @param[in] callback: call back func
* @return none
*/
void WDG_SetCallback(DeviceCallback_Type callback)
{
    s_wdgCallback = callback;
}

/*!
* @brief WDG interrupt request handler
*
* @param[in] none
* @return none
*/
void WDG_IRQHandler(void)
{
    /* excute callback function */
    if (NULL != s_wdgCallback)
    {
        s_wdgCallback(WDG, 0, 0);
    }
}

/* =============================================  EOF  ============================================== */
