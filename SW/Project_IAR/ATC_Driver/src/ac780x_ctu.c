/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

/*!
* @file ac780x_ctu.c
*
* @brief This file provides module-to-module interconnections module integration functions.
*
*/

/* ===========================================  Includes  =========================================== */
#include "ac780x_ctu_reg.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief CTU initialize
*
* @param[in] config: pointer to configuration structure
* @return none
*/
void CTU_Init(const CTU_ConfigType *config)
{
    CKGEN_Enable(CLK_CTU, ENABLE);
    CKGEN_SoftReset(SRST_CTU, ENABLE);
    CTU_SetUart0RxFilter(config->uart0RxFilterEn);
    CTU_SetRtcCaptureByPwm1Ch1(config->rtcCaptureEn);
    CTU_SetAcmpOutputCaptureByPwm1Ch0(config->acmpCaptureEn);
    CTU_SetUart0RxCapture(config->uart0RxCaptureEn);
    CTU_SetUart0TxModulation(config->uartTxModulateEn);
    CTU_SetClockPrescaler(config->clkPsc);
    CTU_SetAdcRegularTriggerSource(config->adcRegularTriggerSource);
    CTU_SetDelay0Time(config->delay0Time);
    CTU_SetAdcInjectTriggerSource(config->adcInjectTriggerSource);
    CTU_SetDelay1Time(config->delay1Time);
    CTU_SetPwdtIn3InputSource(0, config->pwdt0In3Source);
    CTU_SetPwdtIn3InputSource(1, config->pwdt1In3Source);
}

/*!
* @brief CTU De-initialize
*
* @param[in] none
* @return none
*/
void CTU_DeInit(void)
{
    CKGEN_SoftReset(SRST_CTU, DISABLE);
    CKGEN_Enable(CLK_CTU, DISABLE);
}

/* =============================================  EOF  ============================================== */
