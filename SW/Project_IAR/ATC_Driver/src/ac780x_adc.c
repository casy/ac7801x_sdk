/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

/*!
* @file ac780x_adc.c
*
* @brief This file provides analog to digital converter module integration functions.
*
*/

/* ===========================================  Includes  =========================================== */
#include "ac780x_adc_reg.h"
#include "ac780x_dma.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */
/* ADC related info */
static const IRQn_Type s_adcIRQ[ADC_INSTANCE_MAX] = {ADC0_IRQn};

/* ADC callback pointer */
static DeviceCallback_Type s_adcCallback[ADC_INSTANCE_MAX] = {NULL};

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief ADC initialize.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] config: pointer to configuration structure
* @return none
*/
void ADC_Init(ADC_Type *ADCx, const ADC_ConfigType *config)
{
    uint8_t instance = 0;

    instance = ADC_INDEX(ADCx);

    DEVICE_ASSERT(ADC_INSTANCE_MAX > instance);
    DEVICE_ASSERT(NULL != config);
    DEVICE_ASSERT(ADC_REGULAR_SEQUENCE_LEGNTH >= config->regularSequenceLength);
    DEVICE_ASSERT(ADC_INJECT_SEQUENCE_LEGNTH >= config->injectSequenceLength);

    /* Enbale adc clock */
    CKGEN_Enable(CLK_ADC0, ENABLE);
    CKGEN_SoftReset(SRST_ADC0, ENABLE);

    /* Set clock prescaler */
    ADC_SetClockPrescaler(ADCx, config->clkPsc);

    /* Set adc mode */
    ADC_SetScanMode(ADCx, config->scanModeEn);
    ADC_SetContinuousMode(ADCx, config->continousModeEn);
    ADC_SetRegularDiscontinuousMode(ADCx, config->regularDiscontinousModeEn);
    ADC_SetInjectDiscontinuousMode(ADCx, config->injectDiscontinousModeEn);
    ADC_SetInjectAutoMode(ADCx, config->injectAutoModeEn);
    ADC_SetIntervalMode(ADCx, config->intervalModeEn);
    ADC_SetRegularDiscontinuousNum(ADCx, config->regularDiscontinousNum);
    ADC_SetRegularTriggerSource(ADCx, config->regularTriggerMode);
    ADC_SetInjectTriggerSource(ADCx, config->injectTriggerMode);
    ADC_SetDataAlign(ADCx, config->dataAlign);

    /* Set adc sequence length */
    if (0 == config->regularSequenceLength)
    {
        ADC_SetRegularLength(ADCx, 0);
    }
    else
    {
        ADC_SetRegularLength(ADCx, config->regularSequenceLength - 1);
    }

    if (0 == config->injectSequenceLength)
    {
        ADC_SetInjectLength(ADCx, 0);
    }
    else
    {
        ADC_SetInjectLength(ADCx, config->injectSequenceLength - 1);
    }

    /* Register callback function */
    s_adcCallback[instance] = config->callBack;

    /* Set adc interrupt */
    ADC_SetEOCInterrupt(ADCx, config->EOCInterruptEn);
    ADC_SetIEOCInterrupt(ADCx, config->IEOCInterruptEn);
    if (ENABLE == config->interruptEn)
    {
        NVIC_EnableIRQ(s_adcIRQ[instance]);
    }
    else
    {
        NVIC_DisableIRQ(s_adcIRQ[instance]);
    }

    /* Set adc dma */
    ADC_SetDMA(ADCx, config->regularDMAEn);

    /* Set power */
    ADC_SetPowerMode(ADCx, config->powerMode);
}

/*!
* @brief ADC De-initialize.
*
* @param[in] ADCx: adc module
                - ADC0
* @return none
*/
void ADC_DeInit(ADC_Type *ADCx)
{
    uint8_t instance = 0;

    instance = ADC_INDEX(ADCx);
    DEVICE_ASSERT(ADC_INSTANCE_MAX > instance);

    NVIC_DisableIRQ(s_adcIRQ[instance]);
    NVIC_ClearPendingIRQ(s_adcIRQ[instance]);
    CKGEN_SoftReset(SRST_ADC0, DISABLE);
    CKGEN_Enable(CLK_ADC0, DISABLE);
}

/*!
* @brief Set adc regular group channel.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] channel: adc channel
                - ADC_CH_0
                - ADC_CH_1
                - ADC_CH_2
                - ADC_CH_3
                - ADC_CH_4
                - ADC_CH_5
                - ADC_CH_6
                - ADC_CH_7
                - ADC_CH_8
                - ADC_CH_9
                - ADC_CH_10
                - ADC_CH_11
                - ADC_CH_BANDGAP
                - ADC_CH_TSENSOR
* @param[in] spt: sample time
                - ADC_SPT_CLK_9
                - ADC_SPT_CLK_7
                - ADC_SPT_CLK_15
                - ADC_SPT_CLK_33
                - ADC_SPT_CLK_64
                - ADC_SPT_CLK_140
                - ADC_SPT_CLK_215
                - ADC_SPT_CLK_5
* @param[in] seq: regular group convert sequence
                - 0 ~ 11
* @return none
*/
void ADC_SetRegularGroupChannel(ADC_Type *ADCx, ADC_ChannelType channel, ADC_SampleTimeType spt, uint8_t seq)
{
    DEVICE_ASSERT(ADC_INSTANCE_MAX > ADC_INDEX(ADCx));
    DEVICE_ASSERT(ADC_REGULAR_SEQUENCE_LEGNTH > channel);
    DEVICE_ASSERT(ADC_SPT_CLK_MAX > spt);
    DEVICE_ASSERT(ADC_CHANNEL_MAX > seq);

    /* Set adc channel sample time */
    ADC_SetChannelSampleTime(ADCx, channel, spt);

    /* Set regular convert sequence */
    ADC_SetRegularSequence(ADCx, channel, seq);
}

/*!
* @brief Set adc injected group channel.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] channel: adc channel
                - ADC_CH_0
                - ADC_CH_1
                - ADC_CH_2
                - ADC_CH_3
                - ADC_CH_4
                - ADC_CH_5
                - ADC_CH_6
                - ADC_CH_7
                - ADC_CH_8
                - ADC_CH_9
                - ADC_CH_10
                - ADC_CH_11
                - ADC_CH_BANDGAP
                - ADC_CH_TSENSOR
* @param[in] spt: sample time
                - ADC_SPT_CLK_9
                - ADC_SPT_CLK_7
                - ADC_SPT_CLK_15
                - ADC_SPT_CLK_33
                - ADC_SPT_CLK_64
                - ADC_SPT_CLK_140
                - ADC_SPT_CLK_215
                - ADC_SPT_CLK_5
* @param[in] seq: injected group convert sequence
                - 0 ~ 3
* @return none
*/
void ADC_SetInjectGroupChannel(ADC_Type *ADCx, ADC_ChannelType channel, ADC_SampleTimeType spt, uint8_t seq)
{
    DEVICE_ASSERT(ADC_INSTANCE_MAX > ADC_INDEX(ADCx));
    DEVICE_ASSERT(ADC_CH_MAX > channel);
    DEVICE_ASSERT(ADC_INJECT_SEQUENCE_LEGNTH > seq);
    DEVICE_ASSERT(ADC_SPT_CLK_MAX > spt);

    /* Set adc channel sample time */
    ADC_SetChannelSampleTime(ADCx, channel, spt);

    /* Set injected convert sequence */
    ADC_SetInjectSequence(ADCx, channel, seq);
}

/*!
* @brief Set adc analog monitor.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] config: pointer to configuration structure
* @return none
*/
void ADC_SetAnalogMonitor(ADC_Type *ADCx, ADC_AMOConfigType *config)
{
    DEVICE_ASSERT(ADC_INSTANCE_MAX > ADC_INDEX(ADCx));
    DEVICE_ASSERT(NULL != config);

    /* Set analog monitor mode */
    ADC_SetAMOTriggerMode(ADCx, config->AMOTriggerMode);
    ADC_SetAMOInterrupt(ADCx, config->AMOInterruptEn);
    ADC_SetAMORegularMode(ADCx, config->AMORegularEn);
    ADC_SetAMOInjectMode(ADCx, config->AMOInjectEn);
    ADC_SetAMOSingleChannelMode(ADCx, config->AMOSingleModeEn);

    ADC_SetAMOSingleChannel(ADCx, config->AMOSingleChannel);
    ADC_SetAMOThreshold(ADCx, config->AMOHighThreshold, config->AMOLowThreshold);
    ADC_SetAMOOffset(ADCx, config->AMOHighOffset, config->AMOLowOffset);
}

/*!
* @brief Set adc receive dma.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] rxBuffer: point to receive buffer
* @param[in] transferNum: Transfer data numbers, 0~32767, bit15 should be 0
* @param[in] callback: point to DMA callback function
* @return none
*/
void ADC_SetReceiveDMA(ADC_Type *ADCx, DMA_ChannelType *DMAx, uint16_t *rxBuffer,
                    uint16_t transferNum, DeviceCallback_Type callback)
{
    DMA_ConfigType dmaConfig;

    DEVICE_ASSERT(ADC_INSTANCE_MAX > ADC_INDEX(ADCx));
    DEVICE_ASSERT(IS_DMA_PERIPH(DMAx));
    DEVICE_ASSERT(IS_DMA_TRANSFER_NUM(transferNum));

    dmaConfig.memStartAddr = (uint32_t)rxBuffer;
    dmaConfig.memEndAddr = dmaConfig.memStartAddr + (transferNum * 2);
    dmaConfig.periphStartAddr = (uint32_t)(&(ADCx->RDR));
    dmaConfig.channelEn = ENABLE;
    dmaConfig.finishInterruptEn = ENABLE;
    dmaConfig.halfFinishInterruptEn = DISABLE;
    dmaConfig.errorInterruptEn = ENABLE;
    dmaConfig.channelPriority = DMA_PRIORITY_VERY_HIGH;
    dmaConfig.circular = ENABLE;
    dmaConfig.direction = DMA_READ_FROM_PERIPH;
    dmaConfig.MEM2MEM = DISABLE;
    dmaConfig.memByteMode = DMA_MEM_BYTE_MODE_1TIME;
    dmaConfig.memIncrement = ENABLE;
    dmaConfig.periphIncrement = DISABLE;
    dmaConfig.memSize = DMA_MEM_SIZE_16BIT;
    dmaConfig.periphSize = DMA_PERIPH_SIZE_16BIT;
    dmaConfig.periphSelect = DMA_PEPIRH_ADC0;
    dmaConfig.transferNum = transferNum;
    dmaConfig.callBack = callback;
    DMA_Init(DMAx, &dmaConfig);
}

/*!
* @brief Set adc callback function.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] func: callback function
* @return none
*/
void ADC_SetCallback(ADC_Type *ADCx, const DeviceCallback_Type func)
{
    uint8_t instance = 0;

    instance = ADC_INDEX(ADCx);

    DEVICE_ASSERT(ADC_INSTANCE_MAX > instance);
    s_adcCallback[instance] = func;
}

/*!
* @brief ADC common interrupt service routine
*
* @param[in] ADCx: adc module
                - ADC0
* @return none
*/
static void ADC_CommonISR(ADC_Type *ADCx)
{
    uint8_t instance = 0;
    uint32_t wpara = 0;

    instance = ADC_INDEX(ADCx);

    /* store device status */
    wpara = ADCx->STR;

    /* clear device status */
    ADCx->STR = ~wpara;

    if (NULL != s_adcCallback[instance])
    {
        /* callback */
        s_adcCallback[instance](ADCx, wpara, 0);
    }
}

/*!
* @brief ADC0 interrupt request handler
*
* @param[in] none
* @return none
*/
void ADC0_IRQHandler(void)
{
    ADC_CommonISR(ADC0);
}

/* =============================================  EOF  ============================================== */
