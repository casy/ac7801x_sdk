/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

/*!
* @file ac780x_dma.c
*
* @brief This file provides dma integration functions.
*
*/

/* ===========================================  Includes  =========================================== */
#include "ac780x_dma_reg.h"


/* ============================================  Define  ============================================ */


/* ===========================================  Typedef  ============================================ */


/* ==========================================  Variables  =========================================== */
const DMA_InfoType g_dmaChannel[] =
{
    {DMA0_CHANNEL0,  DMA0_CHANNEL0_IRQn},  /* DMA0_CHANNEL0 */
    {DMA0_CHANNEL1,  DMA0_CHANNEL1_IRQn},  /* DMA0_CHANNEL1 */
    {DMA0_CHANNEL2,  DMA0_CHANNEL2_IRQn},  /* DMA0_CHANNEL2 */
    {DMA0_CHANNEL3,  DMA0_CHANNEL3_IRQn},  /* DMA0_CHANNEL3 */
};

DeviceCallback_Type g_dmaCallbackArray[DMA0_CHANNEL_INDEX_MAX] = {(DeviceCallback_Type)NULL};


/* ====================================  Functions declaration  ===================================== */


/* ======================================  Functions define  ======================================== */
/*!
* @brief Initialize DMA channel
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] config:DMA config type pointer which contains the configuration
*                   information for the specified DMA Channel.
* @return none
*/
void DMA_Init(DMA_ChannelType *DMAx, const DMA_ConfigType *config)
{
    uint8_t  dmaIndex = (uint8_t)DMA0_CHANNEL_INDEX(DMAx); /* dmaIndex in range: 0~3 */

    DEVICE_ASSERT(IS_DMA_PERIPH(DMAx));
    DEVICE_ASSERT(IS_DMA_PERIPH_ID(config->periphSelect));
    DEVICE_ASSERT(IS_DMA_DIR(config->direction));
    DEVICE_ASSERT(IS_DMA_M2M_STATE(config->MEM2MEM));
    DEVICE_ASSERT(IS_DMA_MEM_INC_STATE(config->memIncrement));
    DEVICE_ASSERT(IS_DMA_PERIPH_INC_STATE(config->periphIncrement));
    DEVICE_ASSERT(IS_DMA_PERIPH_SIZE(config->periphSize));
    DEVICE_ASSERT(IS_DMA_MEM_SIZE(config->memSize));
    DEVICE_ASSERT(IS_DMA_TRANSFER_NUM(config->transferNum));
    DEVICE_ASSERT(IS_DMA_MEM_BYTE_MODE(config->memByteMode));
    DEVICE_ASSERT(IS_DMA_CIRCULAR_STATE(config->circular));
    DEVICE_ASSERT(IS_DMA_PRIORITY(config->channelPriority));

    CKGEN_Enable(CLK_DMA0, ENABLE);       /* Enable DMA0 APB clock */
    CKGEN_SoftReset(SRST_DMA0, ENABLE);   /* Inactive DMA0_APB reset */

    g_dmaCallbackArray[dmaIndex] = config->callBack;

    DMA_ChannelHardRst(DMAx);   /* Reset DMA channel */
    DMA_ChannelMemStartAddr(DMAx, config->memStartAddr);
    DMA_ChannelMemEndAddr(DMAx, config->memEndAddr);
    DMA_ChannelPeriphAddr(DMAx, config->periphStartAddr);

    if ((config->finishInterruptEn) || (config->halfFinishInterruptEn) || (config->errorInterruptEn))
    {
        NVIC_EnableIRQ(g_dmaChannel[dmaIndex].dmaIRQn);
    }
    else
    {
        /* do nothing */
    }

    DMA_SetFinishInterrupt(DMAx, config->finishInterruptEn);
    DMA_SetHalfFinishInterrupt(DMAx, config->halfFinishInterruptEn);
    DMA_SetTransferErrorInterrupt(DMAx, config->errorInterruptEn);

    DMA_ChannelPriority(DMAx, config->channelPriority);
    DMA_ChannelCircular(DMAx, config->circular);
    DMA_ChannelDir(DMAx, config->direction);
    DMA_ChannelM2M(DMAx, config->MEM2MEM);
    DMA_ChannelMemByteMode(DMAx, config->memByteMode);
    DMA_ChannelPeriphSel(DMAx, config->periphSelect);
    DMA_ChannelMemIncrement(DMAx, config->memIncrement);
    DMA_ChannelPeriphIncrement(DMAx, config->periphIncrement);
    DMA_ChannelMemSize(DMAx, config->memSize);
    DMA_ChannelPeriphSize(DMAx, config->periphSize);
    DMA_ChannelSetLength(DMAx, config->transferNum);

    DMA_SetChannel(DMAx, config->channelEn);
}

/*!
* @brief Uninitialize DMA channel
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return none
*/
void DMA_DeInit(DMA_ChannelType *DMAx)
{
    DMA_ChannelIndexType dmaIndex = DMA0_CHANNEL_INDEX(DMAx); /* dmaIndex in range[1:12] */

    DEVICE_ASSERT(IS_DMA_PERIPH(DMAx));

    CKGEN_Enable(CLK_DMA0, ENABLE);       /* Enable DMA0 APB clock, DMA0 all channels share the CLK_DMA0 */
    CKGEN_SoftReset(SRST_DMA0, ENABLE);   /* Active DMA0_APB reset */

    NVIC_DisableIRQ(g_dmaChannel[dmaIndex].dmaIRQn);
    NVIC_ClearPendingIRQ(g_dmaChannel[dmaIndex].dmaIRQn);
    DMA_ChannelHardRst(DMAx);

    g_dmaCallbackArray[dmaIndex] = NULL;
}

/*!
* @brief Get DMA channel transmission length
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return DMA channel transfer length
*/
uint32_t DMA_GetTransmissionLength(DMA_ChannelType *DMAx)
{
    DEVICE_ASSERT(IS_DMA_PERIPH(DMAx));

    return (DMAx->CHAN_LENGTH & DMA_CHANNEL_CHAN_LENGTH_CHAN_LENGTH_Msk);
}

/*!
* @brief Get DMA channel transfered data num
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return DMA channel transfered num
*/
uint32_t DMA_GetTransferedDataNum(DMA_ChannelType *DMAx)
{
    DEVICE_ASSERT(IS_DMA_PERIPH(DMAx));

    return (DMAx->DATA_TRANS_NUM & DMA_CHANNEL_DATA_TRANS_NUM_DATA_TRANS_NUM_Msk);
}

/*!
* @brief Enable/Disable DMA channel
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] state: Enable/Disable DMA channel
*                 - ENABLE
*                 - DISABLE
* @return none
*/
void DMA_SetChannel(DMA_ChannelType *DMAx, ACTION_Type state)
{
    DEVICE_ASSERT(IS_DMA_PERIPH(DMAx));

    MODIFY_REG32(DMAx->CHAN_ENABLE, DMA_CHANNEL_CHAN_ENABLE_CHAN_ENABLE_Msk,  DMA_CHANNEL_CHAN_ENABLE_CHAN_ENABLE_Pos, state);
}

/*!
* @brief Flush DMA channel data
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return none
*/
void DMA_ChannelFlush(DMA_ChannelType *DMAx)
{
    DEVICE_ASSERT(IS_DMA_PERIPH(DMAx));

    DMAx->RST |= DMA_CHANNEL_RST_FLUSH_Msk;
}

/*!
* @brief Get the number of the data left in the DMA channel's internal FIFO
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return DMA Channel FIFO left data number
*/
uint32_t DMA_GetInterFIFODataLeftNum(DMA_ChannelType *DMAx)
{
    DEVICE_ASSERT(IS_DMA_PERIPH(DMAx));

    return (DMAx->FIFO_LEFT_NUM & DMA_CHANNEL_FIFO_LEFT_NUM_FIFO_LEFT_NUM_Msk);
}

/*!
* @brief Clear DMA Finish, Half Finish and Error Flags
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return DMA channel's interrupt flags
*/
uint32_t DMA_ClearFlag(DMA_ChannelType *DMAx)
{
    uint32_t tmpDMAStatus = DMAx->STATUS;

    DEVICE_ASSERT(IS_DMA_PERIPH(DMAx));

    if (tmpDMAStatus & DMA_CHANNEL_STATUS_TRANS_ERROR_Msk)
    {
        DMA_ChannelHardRst(DMAx);      /* When AHB error, reset DMA channel */
    }
    DMAx->STATUS &= ~tmpDMAStatus;     /* Write 0 to clear */

    return tmpDMAStatus;
}

/*!
* @brief Set DAM event callback function
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] callback:DMAx interrupt application functon which will be called by DMAx_IRQHandler()
* @return none
*/
void DMA_SetCallback(DMA_ChannelType *DMAx, const DeviceCallback_Type callback )
{
    uint8_t  dmaIndex = (uint8_t)DMA0_CHANNEL_INDEX(DMAx);

    DEVICE_ASSERT(IS_DMA_PERIPH(DMAx));

    g_dmaCallbackArray[dmaIndex] = callback;
}

/*!
* @brief DMA0_Channel0 Interrupt Handler CallBack Function
*
* @param[in] none
* @return none
*/
void DMA0_Channel0_IRQHandler(void)
{
    uint32_t dmaChannelStatus = DMA_ClearFlag(DMA0_CHANNEL0);

    if (g_dmaCallbackArray[DMA0_CHANNEL0_INDEX])
    {
        g_dmaCallbackArray[DMA0_CHANNEL0_INDEX](DMA0_CHANNEL0, dmaChannelStatus, 0);
    }
}

/*!
* @brief DMA0_Channel0 Interrupt Handler CallBack Function
*
* @param[in] none
* @return none
*/
void DMA0_Channel1_IRQHandler(void)
{
    uint32_t dmaChannelStatus = DMA_ClearFlag(DMA0_CHANNEL1);

    if (g_dmaCallbackArray[DMA0_CHANNEL1_INDEX])
    {
        g_dmaCallbackArray[DMA0_CHANNEL1_INDEX](DMA0_CHANNEL1, dmaChannelStatus, 0);
    }
}

/*!
* @brief DMA0_Channel2 Interrupt Handler CallBack Function
*
* @param[in] none
* @return none
*/
void DMA0_Channel2_IRQHandler(void)
{
    uint32_t dmaChannelStatus = DMA_ClearFlag(DMA0_CHANNEL2);

    if (g_dmaCallbackArray[DMA0_CHANNEL2_INDEX])
    {
        g_dmaCallbackArray[DMA0_CHANNEL2_INDEX](DMA0_CHANNEL2, dmaChannelStatus, 0);
    }
}

/*!
* @brief DMA0_Channel3 Interrupt Handler CallBack Function
*
* @param[in] none
* @return none
*/
void DMA0_Channel3_IRQHandler(void)
{
    uint32_t dmaChannelStatus = DMA_ClearFlag(DMA0_CHANNEL3);

    if (g_dmaCallbackArray[DMA0_CHANNEL3_INDEX])
    {
        g_dmaCallbackArray[DMA0_CHANNEL3_INDEX](DMA0_CHANNEL3, dmaChannelStatus, 0);
    }
}


/* =============================================  EOF  ============================================== */
