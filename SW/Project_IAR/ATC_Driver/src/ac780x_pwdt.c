/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

/*!
* @file ac780x_pwdt.c
*
* @brief This file provides Pulse width measurement module integration functions.
*
*/

/* ===========================================  Includes  =========================================== */
#include "ac780x_pwdt_reg.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */
/* This structure holds the clock/reset/irq info */
typedef struct
{
    CKGEN_ClockType clock;      /* clock */
    CKGEN_SoftResetType reset;  /* reset */
    IRQn_Type irq;              /* irq */
} PWDT_InfoType;

/* ==========================================  Variables  =========================================== */
/* PWDT related info */
static const PWDT_InfoType s_pwdtInfo[PWDT_INSTANCE_MAX] =
{
    {CLK_PWDT0, SRST_PWDT0, PWDT0_IRQn},
    {CLK_PWDT1, SRST_PWDT1, PWDT1_IRQn},
};

/* PWDT callback pointer */
static DeviceCallback_Type s_pwdtCallback[PWDT_INSTANCE_MAX] = {NULL};

/* ====================================  Functions declaration  ===================================== */
void PWDT_SetClockPrescaler(PWDT_Type *PWDTx, PWDT_ClkPrescalerType psc);

/* ======================================  Functions define  ======================================== */
/*!
* @brief PWDT initialize.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] config: pointer to configuration structure
* @return none
*/
void PWDT_Init(PWDT_Type *PWDTx, const PWDT_ConfigType *config)
{
    uint8_t instance = 0;

    instance = PWDT_INDEX(PWDTx);

    DEVICE_ASSERT(PWDT_INSTANCE_MAX > instance);
    DEVICE_ASSERT(NULL != config);

    /* Enbale pwdt clock */
    CKGEN_Enable(s_pwdtInfo[instance].clock, ENABLE);
    CKGEN_SoftReset(s_pwdtInfo[instance].reset, ENABLE);

    /* Set clock prescaler */
    PWDT_SetClockPrescaler(PWDTx, config->clkPsc);

    /* Set pwdt clock prescaler */
    PWDT_SetEdgeType(PWDTx, config->edgeType);

    /* Set pwdt input pin */
    PWDT_SetInputChannel(PWDTx, config->channel);

    /* Register callback function */
    s_pwdtCallback[instance] = config->callBack;

    /* Set overflow interrupt */
    PWDT_SetOverflowInterrupt(PWDTx, config->overflowInterruptEn);

    /* Set data ready interrupt */
    PWDT_SetReadyInterrupt(PWDTx, config->readyInterruptEn);

    /* Set pwdt module interrupt */
    PWDT_SetInterrupt(PWDTx, config->interruptEn);
    if (ENABLE == config->interruptEn)
    {
        NVIC_EnableIRQ(s_pwdtInfo[instance].irq);
    }
    else
    {
        NVIC_DisableIRQ(s_pwdtInfo[instance].irq);
    }

    /* Set comparator mode */
    PWDT_SetCompMode(PWDTx, config->cmpModeEn);

    /* Set hall mode */
    PWDT_SetHallMode(PWDTx, config->hallModeEn);

    /* Set filter */
    PWDT_SetFilter(PWDTx, config->filterEn, config->filterPsc, config->filterValue);

    /* Set timer period value in general timer mode */
    PWDT_SetTimerPeriodValue(PWDTx, config->periodValue);

    /* Set pwdt mode */
    if (PWDT_MEASURE_MODE == config->mode)
    {
        PWDT_SetMeasureMode(PWDTx, ENABLE);
        PWDT_SetTimerMode(PWDTx, DISABLE);
    }
    else if (PWDT_TIMER_MODE == config->mode)
    {
        PWDT_SetTimerMode(PWDTx, ENABLE);
        PWDT_SetMeasureMode(PWDTx, DISABLE);
    }
    else
    {
        PWDT_SetMeasureMode(PWDTx, DISABLE);
        PWDT_SetTimerMode(PWDTx, DISABLE);
    }
}

/*!
* @brief PWDT De-initialize.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @return none
*/
void PWDT_DeInit(PWDT_Type *PWDTx)
{
    uint8_t instance = 0;

    instance = PWDT_INDEX(PWDTx);
    DEVICE_ASSERT(PWDT_INSTANCE_MAX > instance);

    NVIC_DisableIRQ(s_pwdtInfo[instance].irq);
    NVIC_ClearPendingIRQ(s_pwdtInfo[instance].irq);
    CKGEN_SoftReset(s_pwdtInfo[instance].reset, DISABLE);
    CKGEN_Enable(s_pwdtInfo[instance].clock, DISABLE);
}

/*!
* @brief Set pwdt clock prescaler.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] psc: prescaler divide
                - PWDT_CLK_PRESCALER_1
                - PWDT_CLK_PRESCALER_2
                - PWDT_CLK_PRESCALER_4
                - PWDT_CLK_PRESCALER_8
                - PWDT_CLK_PRESCALER_16
                - PWDT_CLK_PRESCALER_32
                - PWDT_CLK_PRESCALER_64
                - PWDT_CLK_PRESCALER_128
                - PWDT_CLK_PRESCALER_256
                - PWDT_CLK_PRESCALER_512
* @return none
*/
void PWDT_SetClockPrescaler(PWDT_Type *PWDTx, PWDT_ClkPrescalerType psc)
{
    if (PWDT_CLK_PRESCALER_256 > psc)
    {
        PWDT_SetClockPrescaler0(PWDTx, psc);
        PWDT_SetClockPrescaler1(PWDTx, PWDT_CLK_PRESCALER_1);
    }
    else
    {
        PWDT_SetClockPrescaler0(PWDTx, PWDT_CLK_PRESCALER_128);
        if (PWDT_CLK_PRESCALER_256 == psc)
        {
            PWDT_SetClockPrescaler1(PWDTx, PWDT_CLK_PRESCALER_2);
        }
        else
        {
            PWDT_SetClockPrescaler1(PWDTx, PWDT_CLK_PRESCALER_4);
        }
    }
}

/*!
* @brief Set pwdt callback function.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] func: callback function
* @return none
*/
void PWDT_SetCallback(PWDT_Type *PWDTx, const DeviceCallback_Type func)
{
    uint8_t instance = 0;

    instance = PWDT_INDEX(PWDTx);

    DEVICE_ASSERT(PWDT_INSTANCE_MAX > instance);
    s_pwdtCallback[instance] = func;
}

/*!
* @brief PWDT common interrupt service routine.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @return none
*/
static void PWDT_CommonISR(PWDT_Type *PWDTx)
{
    uint8_t instance = 0;
    uint32_t wpara = 0;

    instance = PWDT_INDEX(PWDTx);

    /* store device status */
    wpara = PWDTx->INIT0 & (PWDT_INIT0_OVF_Msk | PWDT_INIT0_RDYF_Msk);

    if (wpara & PWDT_INIT0_OVF_Msk)
    {
        /* clear device status*/
        PWDT_ClearOverflowFlag(PWDTx);
    }

    if (wpara & PWDT_INIT0_RDYF_Msk)
    {
        if (NULL != s_pwdtCallback[instance])
        {
            /* callback */
            s_pwdtCallback[instance](PWDTx, wpara, 0);
        }
        /* In order to buffer mechanism work, need to read data before clear flag*/
        PWDT_ClearReadyFlag(PWDTx);
    }
    else /* just occur overflow */
    {
        if (NULL != s_pwdtCallback[instance])
        {
            /* callback */
            s_pwdtCallback[instance](PWDTx, wpara, 0);
        }
    }
}


/*!
* @brief PWDT0 interrupt request handler.
*
* @param[in] none
* @return none
*/
void PWDT0_IRQHandler(void)
{
    PWDT_CommonISR(PWDT0);
}

/*!
* @brief PWDT1 interrupt request handler.
*
* @param[in] none
* @return none
*/
void PWDT1_IRQHandler(void)
{
    PWDT_CommonISR(PWDT1);
}

/* =============================================  EOF  ============================================== */
