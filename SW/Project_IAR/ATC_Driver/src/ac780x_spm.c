/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

/*!
* @file ac780x_spm.c
*
* @brief This file provides system power manage module integration functions.
*
*/

/* ===========================================  Includes  =========================================== */
#include "ac780x_spm.h"

/* ============================================  Define  ============================================ */
#define REG_PORLPVD_CFG0_ADDR               0x40008880UL
#define LOW_POWER_CFG_MASK                  0x180000UL
#define LOW_POWER_CFG_START_BIT             19UL
#define LOW_POWER_CFG_VALUE                 0x3UL

#define REG_XOSC_CFG0_ADDR                  0x400088B0UL
#define XOSC_CFG0_OK_BYPASS_MASK            0x10000000UL
#define XOSC_CFG0_OK_BYPASS_POS             28UL

#define REG_SYSPLL1_CFG0_ADDR               0x40008890UL
#define SYSPLL1_CFG0_PLL_MONREF_EN_MASK     0x00000200UL
#define SYSPLL1_CFG0_PLL_MONREF_EN_POS      9UL

#define REG_SYSPLL1_CFG1_ADDR               0x40008894UL
#define SYSPLL1_CFG1_PLL_DETECTOR_EN_MASK   0x00000100UL
#define SYSPLL1_CFG1_PLL_DETECTOR_EN_POS    8UL
#define SYSPLL1_CFG1_PLL_LD_DLY_EN_MASK     0x0000000EUL
#define SYSPLL1_CFG1_PLL_LD_DLY_EN_POS      1UL

#define REG_DEBUG_MODE_ADDR                 0x40008050UL
#define DEBUG_MODE_EN_MASK                  0x78010001

#define REG_DEBUG_MGR_CFG1_ADDR             0x40008058UL
#define DEBUG_MGR_CFG1_MASK                 0x01UL
#define DEBUG_MGR_CFG1_POS                  0UL
#define DEBUG_LPOSC_STRUP_MASK              0x00080000UL
#define DEBUG_LPOSC_STRUP_POS               19UL

#define SPM_MGR_CFG0_MASK                   0x08UL
/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */
/* for SPM Event call back function */
static DeviceCallback_Type s_spmCallback = 0;
/* for PVD Event call back function */
static DeviceCallback_Type s_pvdCallback = 0;
/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief set the low power mode
*
*
* @param[in] mode: stop or sleep mode, value can be
*                 - LOW_POWER_MODE_STOP
*                 - LOW_POWER_MODE_STANDBY
* @return none
*/
void SPM_SetLowPowerMode(SPM_PowerModeType mode)
{
    DEVICE_ASSERT(IS_POWER_MODE_PARA(mode));

    WRITE_MEM32(REG_DEBUG_MODE_ADDR,DEBUG_MODE_EN_MASK);
    MODIFY_MEM32(REG_DEBUG_MGR_CFG1_ADDR,DEBUG_LPOSC_STRUP_MASK,DEBUG_LPOSC_STRUP_POS,0UL);

    MODIFY_REG32(SPM->PWR_MGR_CFG0, SPM_PWR_MGR_CFG0_SLEEP_MODE_Msk, SPM_PWR_MGR_CFG0_SLEEP_MODE_Pos, (uint32_t)mode);
    SET_BIT32(SPM->PWR_MGR_CFG0, SPM_PWR_MGR_CFG0_PWR_EN_Msk);
    MODIFY_MEM32(REG_PORLPVD_CFG0_ADDR, LOW_POWER_CFG_MASK, LOW_POWER_CFG_START_BIT, LOW_POWER_CFG_VALUE);
    CLEAR_BIT32(SPM->PWR_MGR_CFG0, SPM_MGR_CFG0_MASK);
    CLEAR_BIT32(CKGEN->PERI_SFT_RST0, CKGEN_PERI_SFT_RST0_SRST_UART0_Msk | \
                                      CKGEN_PERI_SFT_RST0_SRST_UART1_Msk | \
                                      CKGEN_PERI_SFT_RST0_SRST_UART2_Msk | \
                                      CKGEN_PERI_SFT_RST0_SRST_SPI0_Msk  | \
                                      CKGEN_PERI_SFT_RST0_SRST_SPI1_Msk  | \
                                      CKGEN_PERI_SFT_RST0_SRST_I2C0_Msk  | \
                                      CKGEN_PERI_SFT_RST0_SRST_I2C1_Msk  | \
                                      CKGEN_PERI_SFT_RST0_SRST_DMA0_Msk  | \
                                      CKGEN_PERI_SFT_RST0_SRST_CAN0_Msk);
    CLEAR_BIT32(CKGEN->PERI_SFT_RST1, CKGEN_PERI_SFT_RST1_SRST_ADC0_Msk  | \
                                      CKGEN_PERI_SFT_RST1_SRST_ACMP0_Msk);
    CLEAR_BIT32(CKGEN->PERI_CLK_EN_0, CKGEN_PERI_CLK_EN_0_UART0_EN_Msk   | \
                                      CKGEN_PERI_CLK_EN_0_UART1_EN_Msk   | \
                                      CKGEN_PERI_CLK_EN_0_UART2_EN_Msk   | \
                                      CKGEN_PERI_CLK_EN_0_SPI0_EN_Msk    | \
                                      CKGEN_PERI_CLK_EN_0_SPI1_EN_Msk    | \
                                      CKGEN_PERI_CLK_EN_0_I2C0_EN_Msk    | \
                                      CKGEN_PERI_CLK_EN_0_I2C1_EN_Msk    | \
                                      CKGEN_PERI_CLK_EN_0_DMA0_EN_Msk    | \
                                      CKGEN_PERI_CLK_EN_0_CAN0_EN_Msk);
    CLEAR_BIT32(CKGEN->PERI_CLK_EN_1, CKGEN_PERI_CLK_EN_1_ADC0_EN_Msk    | \
                                      CKGEN_PERI_CLK_EN_1_ACMP0_EN_Msk);

    NVIC_EnableIRQ(SPM_IRQn);
}

/*!
* @brief enable pll detector function
*
* @param[in] enable: 1: enable, 0 disable
* @return none
*/
void EnablePLLDetectorFunction(ACTION_Type enable)
{
    MODIFY_MEM32(REG_SYSPLL1_CFG1_ADDR, SYSPLL1_CFG1_PLL_DETECTOR_EN_MASK, SYSPLL1_CFG1_PLL_DETECTOR_EN_POS, (uint32_t)enable);
    MODIFY_MEM32(REG_SYSPLL1_CFG1_ADDR, SYSPLL1_CFG1_PLL_LD_DLY_EN_MASK, SYSPLL1_CFG1_PLL_LD_DLY_EN_POS, 6UL);
    MODIFY_MEM32(REG_SYSPLL1_CFG0_ADDR, SYSPLL1_CFG0_PLL_MONREF_EN_MASK, SYSPLL1_CFG0_PLL_MONREF_EN_POS, (uint32_t)enable);
}

/*!
* @brief enable fast boot mode
*
* @param[in] enable: 1: enable, 0 disable
* @return none
*/
void SPM_EnableFastBoot(ACTION_Type enable)
{
    MODIFY_REG32(SPM->PWR_MGR_CFG0, SPM_PWR_MGR_CFG0_EN_FAST_BOOT_Msk, SPM_PWR_MGR_CFG0_EN_FAST_BOOT_Pos, (uint32_t)enable);
}

/*!
* @brief enable PVD
*
* @param[in] enable: 1: enable, 0 disable
* @return none
*/
void SPM_EnablePVD(ACTION_Type enable)
{
    MODIFY_REG32(SPM->PWR_MGR_CFG0, SPM_PWR_MGR_CFG0_EN_PVD_Msk, SPM_PWR_MGR_CFG0_EN_PVD_Pos, (uint32_t)enable);
}

/*!
* @brief enable LVD
*
* @param[in] enable: 1: enable, 0 disable
* @return none
*/
void SPM_EnableLVD(ACTION_Type enable)
{
    MODIFY_REG32(SPM->PWR_MGR_CFG0, SPM_PWR_MGR_CFG0_EN_LVD_Msk, SPM_PWR_MGR_CFG0_EN_LVD_Pos, (uint32_t)enable);
}


/*!
* @brief enable XOSC bypass mode
*
* @param[in] enable: 1: enable, 0 disable
* @return none
*/
void SPM_EnableXOSCBypassMode(ACTION_Type enable)
{
    MODIFY_REG32(SPM->PWR_MGR_CFG1, SPM_PWR_MGR_CFG1_XOSC_HSEBYP_Msk, SPM_PWR_MGR_CFG1_XOSC_HSEBYP_Pos, (uint32_t)enable);
}

/*!
* @brief set XOSC ok bypass
*
* @param[in] enable: 1: enable, 0 disable
* @return none
*/
void SPM_XOSCOKBypassSet(ACTION_Type enable)
{
    MODIFY_MEM32(REG_XOSC_CFG0_ADDR, XOSC_CFG0_OK_BYPASS_MASK, XOSC_CFG0_OK_BYPASS_POS, (uint32_t)enable);
}

/*!
* @brief enable XOSC and wait the XOSC status is OK
*
* @param[in] enable: 0:disable, 1: enable
* @return SUCCESS:xosc set success
*           ERROR:xosc set fail
*/
ERROR_Type SPM_EnableXOSC(ACTION_Type enable)
{
    __IO uint32_t status = 0;
    uint32_t timeout = SPM_TIMEOUT_VALUE;
    ERROR_Type ret = SUCCESS;

    if (enable == ENABLE)
    {
        SPM_XOSCOKBypassSet(DISABLE);
        SET_BIT32(SPM->PWR_MGR_CFG1, SPM_PWR_MGR_CFG1_XOSC_HSEON_Msk);
        do
        {
            status = READ_BIT32(SPM->PWR_MGR_CFG1, SPM_PWR_MGR_CFG1_XOSC_RDY_Msk);
            timeout--;
        } while ((!status) && (timeout != 0));
    }
    else
    {
        CLEAR_BIT32(SPM->PWR_MGR_CFG1, SPM_PWR_MGR_CFG1_XOSC_HSEON_Msk);
        do
        {
            status = READ_BIT32(SPM->PWR_MGR_CFG1, SPM_PWR_MGR_CFG1_XOSC_RDY_Msk);
            timeout--;
        } while ((status) && (timeout != 0));
    }

    if (timeout == 0)
    {
        /* check if xosc enable fail */
        if (enable == ENABLE)
        {
            CLEAR_BIT32(SPM->PWR_MGR_CFG1, SPM_PWR_MGR_CFG1_XOSC_HSEON_Msk);
            WRITE_MEM32(REG_DEBUG_MODE_ADDR, DEBUG_MODE_EN_MASK);
            MODIFY_MEM32(REG_DEBUG_MGR_CFG1_ADDR, DEBUG_MGR_CFG1_MASK, DEBUG_MGR_CFG1_POS, 0);
            SPM_XOSCOKBypassSet(ENABLE);
        }

        ret = ERROR;
    }

    return ret;
}

/*!
* @brief enable PLL and wait for the status is OK
*
* @param[in] enable: 0:disable, 1: enable
* @return SUCCESS:pll set success
*           ERROR:pll set fail
*/
ERROR_Type SPM_EnablePLL(ACTION_Type enable)
{
    __IO uint32_t status = 0;
    uint32_t timeout = SPM_TIMEOUT_VALUE;
    ERROR_Type ret = SUCCESS;

    if (enable == ENABLE)
    {
        SET_BIT32(SPM->PWR_MGR_CFG1, SPM_PWR_MGR_CFG1_SYSPLL_ON_Msk);
        do
        {
            status = READ_BIT32(SPM->PWR_MGR_CFG1, SPM_PWR_MGR_CFG1_SYSPLL_RDY_Msk);
            timeout--;
        } while ((!status) && (timeout != 0));
    }
    else
    {
        CLEAR_BIT32(SPM->PWR_MGR_CFG1, SPM_PWR_MGR_CFG1_SYSPLL_ON_Msk);
        do
        {
            status = READ_BIT32(SPM->PWR_MGR_CFG1, SPM_PWR_MGR_CFG1_SYSPLL_RDY_Msk);
            timeout--;
        } while ((status) && (timeout != 0));
    }

    if (timeout == 0)
    {
        ret = ERROR;
    }

    return ret;
}

/*!
* @brief enable CAN lowpass filter
*
* @param[in] canIndex: value can be 0:CAN0
* @param[in]   enable: 0:disable, 1: enable
* @return none
*/
void SPM_EnableCANLowpassFilter(uint8_t canIndex, ACTION_Type enable)
{
    if (enable == ENABLE)
    {
        SET_BIT32(SPM->PWR_MGR_CFG0, SPM_PWR_MGR_CFG0_EN_CAN0_FILTER_Msk);
    }
    else
    {
        CLEAR_BIT32(SPM->PWR_MGR_CFG0, SPM_PWR_MGR_CFG0_EN_CAN0_FILTER_Msk);
    }
}

/*!
* @brief Config Module Wakeup Function
*
* @param[in] module: module flag in SPM, value can be
*                   - SPM_MODULE_ACMP0
*                   - SPM_MODULE_I2C0
*                   - SPM_MODULE_I2C1
*                   - SPM_MODULE_SPI0
*                   - SPM_MODULE_SPI1
*                   - SPM_MODULE_CAN0
*                   - SPM_MODULE_UART0
*                   - SPM_MODULE_UART1
*                   - SPM_MODULE_UART2
*                   - SPM_MODULE_RTC
*                   - SPM_MODULE_ADC0
*                   - SPM_MODULE_GPIO
*                   - SPM_MODULE_NMI
*                   - SPM_MODULE_PVD
*                   - SPM_MODULE_SPM_OVER_COUNT
* @param[in]  enable: 0:disable, 1: enable
* @return 0: success,  -1: error
*/
void SPM_EnableModuleWakeup(SPM_ModuleType module, ACTION_Type enable)
{
    DEVICE_ASSERT(IS_SPM_MODULE_PARA(module));

    if (enable == ENABLE)
    {
        SPM->EN_PERIPH_WAKEUP |= ((uint32_t)1 << ((uint32_t)module));
    }
    else
    {
        SPM->EN_PERIPH_WAKEUP &= ~((uint32_t)1 << ((uint32_t)module));
    }
}

/*!
* @brief Enable which module the SPM wait the ack when do stop
*
* @param[in] module: module flag in SPM, value can be
*                   - SLEEP_ACK_ACMP0
*                   - SLEEP_ACK_I2C0
*                   - SLEEP_ACK_I2C1
*                   - SLEEP_ACK_SPI0
*                   - SLEEP_ACK_SPI1
*                   - SLEEP_ACK_CAN0
*                   - SLEEP_ACK_UART0
*                   - SLEEP_ACK_UART1
*                   - SLEEP_ACK_UART2
*                   - SLEEP_ACK_DMA0
*                   - SLEEP_ACK_ADC0
*                   - SLEEP_ACK_EFLASH
* @param[in]  enable: 0:disable, 1: enable
* @return 0: success,  -1: error
*/
void SPM_EnableModuleSleepACK(SPM_SleepACKType module, ACTION_Type enable)
{
    DEVICE_ASSERT(IS_SLEEP_ACK_PARA(module));

    if (enable == ENABLE)
    {
        SPM->EN_PERIPH_SLEEP_ACK |= ((uint32_t)1 << ((uint32_t)module));
    }
    else
    {
        SPM->EN_PERIPH_SLEEP_ACK &= ~((uint32_t)1 << ((uint32_t)module));
    }
}

/*!
* @brief get sleep ack status
*
* @param[in]:none
* @return sleep ack status value
*/
uint32_t SPM_GetModuleSleepACKStatus(void)
{
    __IO uint32_t status = SPM->PERIPH_SLEEP_ACK_STATUS;

    return status;
}

/*!
* @brief get the wakeup source flag
*
* @param[in]:none
* @return the wakeup source flag
*/
uint32_t SPM_GetModuleWakeupSourceFlag(void)
{
    __IO uint32_t flag = SPM->WAKEUP_IRQ_STATUS;

    return flag;
}

/*!
* @brief clear the wakeup source flag
*
* @param[in]:none
* @return none
*/
void SPM_ClearWakeupSourceFlag(void)
{
    __IO uint32_t flag = SPM->WAKEUP_IRQ_STATUS;

    SPM->WAKEUP_IRQ_STATUS = flag;
}

/*!
* @brief set SPM IRQHandler callback function
*
* @param[in] eventFunc: the pointer of the call back function, which will be called in SPM_IRQHandler
* @return none
*/
void SPM_SetCallback(const DeviceCallback_Type eventFunc)
{
    s_spmCallback = eventFunc;
}

/*!
* @brief SPM IRQHandler
*
* @param[in]:none
* @return none
*/
void SPM_IRQHandler(void)
{
    __IO uint32_t wakeupFlag = SPM_GetModuleWakeupSourceFlag();

    SPM->WAKEUP_IRQ_STATUS = wakeupFlag;

    if (s_spmCallback)
    {
        s_spmCallback((void *)SPM, wakeupFlag, SPM->PERIPH_SLEEP_ACK_STATUS);
    }
}

/*!
* @brief set PVD Handler callback function
*
* @param[in] eventFunc: the pointer of the call back function, which will be called in PVD_IRQHandler
* @return none
*/
void PVD_SetCallback(const DeviceCallback_Type eventFunc)
{
    s_pvdCallback = eventFunc;
}

/*!
* @brief PVD Handler
*
* @param[in]:none
* @return none
*/
void PVD_IRQHandler(void)
{
    __IO uint32_t wakeupFlag = SPM_GetModuleWakeupSourceFlag();

    SPM->WAKEUP_IRQ_STATUS = wakeupFlag;

    if (s_pvdCallback)
    {
        s_pvdCallback((void *)SPM, wakeupFlag, 0);
    }
}

/*!
* @brief override the NMI_Handler
*
* @param[in] none
* @return none
*/
void NMI_Handler(void)
{
    uint32_t wakeupStatus = SPM_GetModuleWakeupSourceFlag();

    /* check if nmi_b pad triggle nmi interrupt */
    if (wakeupStatus & SPM_WAKEUP_IRQ_STATUS_NMI_Msk)
    {
        SET_BIT32(SPM->WAKEUP_IRQ_STATUS, SPM_WAKEUP_IRQ_STATUS_NMI_Msk);  /* write 1 clear NMI IRQ flag */
    }

    /* check if xosc loss triggle nmi interrupt */
    if (READ_BIT32(CKGEN->RESET_STATUS, CKGEN_RESET_STATUS_XOSC_LOSS_STATUS_Msk))
    {
        /* clear all reset status to prevent always run in nmi_handler */
        MODIFY_REG32(CKGEN->RESET_STATUS, CKGEN_RESET_STATUS_CLR_RESET_STATUS_Msk, CKGEN_RESET_STATUS_CLR_RESET_STATUS_Pos, 1);  /* clear all reset status */
        // add change clock source here
        CKGEN_SetSysclkSrc(SYSCLK_SRC_INTERNAL_OSC); /* set internal osc as sysclk src */
        CKGEN_SetPLLReference(PLL_REF_INTERAL_OSC);  /* change pll source to hsi 8m */
        /* set system clock divider */
        CKGEN_SetSysclkDiv(SYSCLK_DIV);
        CKGEN_SetAPBClockDivider(APBCLK_DIV);
        if(SPM_EnablePLL(ENABLE) == SUCCESS)
        {
            CKGEN_SetSysclkSrc(SYSCLK_SRC_PLL_OUTPUT);   /* change system clock to pll output */
        }
    }

    /* check if pll unlock detector triggle nmi interrupt */
    if (READ_BIT32(CKGEN->RESET_STATUS, CKGEN_RESET_STATUS_PLL_UNLOCK_RST_STATUS_Msk))
    {
        /* clear all reset status to prevent always run in nmi_handler */
        MODIFY_REG32(CKGEN->RESET_STATUS, CKGEN_RESET_STATUS_CLR_RESET_STATUS_Msk, CKGEN_RESET_STATUS_CLR_RESET_STATUS_Pos, 1);  /* clear all reset status */
        // add change clock source here
        CKGEN_SetSysclkSrc(SYSCLK_SRC_INTERNAL_OSC);  /* set internal osc as sysclk src, sysclk = 8m */
        CKGEN_SetPLLReference(PLL_REF_INTERAL_OSC);  /* change pll source to hsi 8m */
        /* check if pll disable success? */
        if(SPM_EnablePLL(DISABLE) == SUCCESS)
        {
            mdelay(100);
            /* check if pll enable success? */
            if (SPM_EnablePLL(ENABLE) == SUCCESS)
            {
                CKGEN_SetSysclkSrc(SYSCLK_SRC_PLL_OUTPUT); /* change system clock to pll output */
            }
        }
    }
}

/* =============================================  EOF  ============================================== */
