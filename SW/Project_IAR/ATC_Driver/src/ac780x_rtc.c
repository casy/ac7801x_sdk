/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

/*!
* @file ac780x_rtc.c
*
* @brief This file provides rtc integration functions.
*
*/

/* ===========================================  Includes  =========================================== */
#include "ac780x_rtc_reg.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */
/* RTC callback pointer */
static DeviceCallback_Type s_rtcCallback = (DeviceCallback_Type)NULL;

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief Initialize RTC module
*
* @param[in] config: RTC configuration pointer
* @return none
*/
void RTC_Init(const RTC_ConfigType *config)
{
    DEVICE_ASSERT(NULL != config);

    /* enable rtc system module */
    CKGEN_Enable(CLK_RTC, ENABLE);
    CKGEN_SoftReset(SRST_RTC, ENABLE);

    /* config RTC clock source */
    RTC_SetClkSource(config->clockSource);

    /* config RTC output */
    RTC_SetRTCOutput(config->rtcOutEn);

    /* config modulo value */
    RTC_SetPeriod(config->periodValue);

    /* register user callback function */
    s_rtcCallback = config->callBack;

    /* clear int flag */
    RTC_ClearRTIF();
    RTC_ClearRPIF();

    /* config RTC int */
    RTC_SetRTIE(config->rtcInterruptEn);

    /* config RTC prescaler int */
    RTC_SetRPIE(config->psrInterruptEn);

    /* config rtc nvic interrupt */
    if ((ENABLE == config->rtcInterruptEn) || (ENABLE == config->psrInterruptEn))
    {
        NVIC_EnableIRQ(RTC_IRQn);
    }
    else
    {
        NVIC_DisableIRQ(RTC_IRQn);
        NVIC_ClearPendingIRQ(RTC_IRQn);
    }

    /* make sure that lastly config prescaler */
    RTC_SetPrescaler(config->psrValue);
}

/*!
* @brief RTC De-initialize
*
* @param[in] none
* @return none
*/
void RTC_DeInit(void)
{
    s_rtcCallback = (DeviceCallback_Type)NULL;
    NVIC_DisableIRQ(RTC_IRQn);
    NVIC_ClearPendingIRQ(RTC_IRQn);
    CKGEN_SoftReset(SRST_RTC, DISABLE);
    CKGEN_Enable(CLK_RTC, DISABLE);
}

/*!
* @brief set RTC Callback function
*
* @param[in] callback: call back func
* @return none
*/
void RTC_SetCallback(DeviceCallback_Type callback)
{
    s_rtcCallback = callback;
}

/*!
* @brief RTC interrupt request handler
*
* @param[in] none
* @return none
*/
void RTC_IRQHandler(void)
{
    uint32_t wpara = 0;

    /* store device status */
    wpara = RTC_GetCtrlStatusReg();

    /* clear interrupt flag */
    if (wpara & RTC_SC_RTIF_Msk)
    {
        RTC_ClearRTIF();
    }
    if (wpara & RTC_SC_RPIF_Msk)
    {
        RTC_ClearRPIF();
    }

    /* excute callback func */
    if (NULL != s_rtcCallback)
    {
        s_rtcCallback(RTC, wpara, 0);
    }
}

/* =============================================  EOF  ============================================== */
