/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_WDG_REG_H
#define _AC780X_WDG_REG_H

/*!
* @file ac780x_wdg_reg.h
*
* @brief WDG access register inline function definition.
*
*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ===========================================  Includes  =========================================== */
#include "ac780x_wdg.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief set WDG clock source
*
* @param[in] clkSource:
*    0: WDG_CLOCK_APB
*    1: WDG_CLOCK_LSI
*    2: WDG_CLOCK_HSI
*    3: WDG_CLOCK_HSE
* @return none
*/
__STATIC_INLINE void WDG_SetClkSource(WDG_ClockSourceType clkSource)
{
    uint32_t tmp = WDG->CS1 & (~(WDG_CS1_FLG_Msk | WDG_CS1_CLK_Msk));

    tmp |= (((uint32_t)clkSource) << WDG_CS1_CLK_Pos) & WDG_CS1_CLK_Msk;
    WDG->CS1 = tmp;
}

/*!
* @brief set WDG counter timeout value
*
* @param[in] tovalValue: 32bit WDG timeout value
* @return none
*/
__STATIC_INLINE void WDG_SetTimeOutVal(uint32_t tovalValue)
{
    WDG->TOVAL = tovalValue;
}

/*!
* @brief get WDG counter timeout value
*
* @param[in] none
* @return 32bit WDG timeout value
*/
__STATIC_INLINE uint32_t WDG_GetTimeOutVal(void)
{
    return (WDG->TOVAL);
}

/*!
* @brief set WDG counter window value
*
* @param[in] winValue: 32 bit WDG window value
* @return none
*/
__STATIC_INLINE void WDG_SetWindowVal(uint32_t winValue)
{
    WDG->WIN = winValue;
}

/*!
* @brief get WDG counter window value
*
* @param[in] none
* @return 32bit WDG window value
*/
__STATIC_INLINE uint32_t WDG_GetWindowVal(void)
{
    return (WDG->WIN);
}

/*!
* @brief set WDG CS0 reg
*
* @param[in] value: WDG CS0 reg value
* @return none
*/
__STATIC_INLINE void WDG_SetCS0(uint32_t value)
{
    WDG->CS0 = value;
}

/*!
* @brief set WDG CS1 reg
*
* @param[in] value: WDG CS1 reg value
* @return none
*/
__STATIC_INLINE void WDG_SetCS1(uint32_t value)
{
    WDG->CS1 = value;
}

/*!
* @brief enable WDG counting
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void WDG_Enable(void)
{
    WDG->CS0 |= WDG_CS0_EN_Msk;
}

/*!
* @brief disable WDG counting
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void WDG_Disable(void)
{
    WDG->CS0 &= ~WDG_CS0_EN_Msk;
}

/*!
* @brief enable WDG update, allow changing WDG register
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void WDG_EnableUpdate(void)
{
    WDG->CS0 |= WDG_CS0_UPDATE_Msk;
}

/*!
* @brief disable WDG update, not allow changing WDG register
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void WDG_DisableUpdate(void)
{
    WDG->CS0 &= ~WDG_CS0_UPDATE_Msk;
}

/*!
* @brief enable WDG timeout interrupt,
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void WDG_EnableInterrupt(void)
{
    WDG->CS0 |= WDG_CS0_INT_Msk;
}

/*!
* @brief disable WDG timeout interrupt
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void WDG_DisableInterrupt(void)
{
    WDG->CS0 &= ~WDG_CS0_INT_Msk;
}

/*!
* @brief enable WDG window mode
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void WDG_EnableWindow(void)
{
    uint32_t tmp = WDG->CS1 & (~WDG_CS1_FLG_Msk);

    tmp |= WDG_CS1_WIN_Msk;
    WDG->CS1 = tmp;
}

/*!
* @brief disable WDG window mode
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void WDG_DisableWindow(void)
{
    uint32_t tmp = WDG->CS1 & (~WDG_CS1_FLG_Msk);

    tmp &= ~WDG_CS1_WIN_Msk;
    WDG->CS1 = tmp;
}

/*!
* @brief enable WDG fixed 256 prescaler
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void WDG_EnablePrescaler(void)
{
    uint32_t tmp = WDG->CS1 & (~WDG_CS1_FLG_Msk);

    tmp |= WDG_CS1_PRES_Msk;
    WDG->CS1 = tmp;
}

/*!
* @brief disable WDG fixed 256 prescaler
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void WDG_DisablePrescaler(void)
{
    uint32_t tmp = WDG->CS1 & (~WDG_CS1_FLG_Msk);

    tmp &= ~WDG_CS1_PRES_Msk;
    WDG->CS1 = tmp;
}

/*!
* @brief get WDG timeout interrupt flag
*
* @param[in] none
* @return WDG interrupt flag
*/
__STATIC_INLINE uint32_t WDG_GetInterruptFlag(void)
{
    return (WDG->CS1 & WDG_CS1_FLG_Msk);
}

/*!
* @brief clear WDG timeout interrupt flag
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void WDG_ClearInterruptFlag(void)
{
    WDG->CS1 |= WDG_CS1_FLG_Msk;
}

/*!
* @brief get WDG timeout counter counting value
*
* @param[in] none
* @return WDG cnt
*/
__STATIC_INLINE uint32_t WDG_GetTimeoutCnt(void)
{
    return (WDG->CNT);
}

/*!
* @brief unlock WDG, prepare to config WDG register
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void WDG_Unlock(void)
{
    WDG->CNT = 0xE064D987;
    WDG->CNT = 0x868A8478;
}

/*!
* @brief feed WDG, trig timeout counter to 0
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void WDG_Feed(void)
{
    DisableInterrupts;
    WDG->CNT = 0x7908AD15;
    WDG->CNT = 0x5AD5A879;
    EnableInterrupts;
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _AC780X_WDG_REG_H */

/* =============================================  EOF  ============================================== */
