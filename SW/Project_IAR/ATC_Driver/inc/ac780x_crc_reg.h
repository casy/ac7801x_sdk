/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_CRC_REG_H
#define _AC780X_CRC_REG_H

/*!
* @file ac780x_crc_reg.h
*
* @brief CRC access register inline function definition.
*
*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ===========================================  Includes  =========================================== */
#include "ac780x_crc.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief config CRC CTRL register
*
* @param[in] value: config value
* @return none
*/
__STATIC_INLINE void CRC_SetCTRLReg(uint32_t value)
{
    CRC->CTRL = value;
}

/*!
* @brief Set CRC Poly register
*
* @param[in] value: poly value
* @return none
*/
__STATIC_INLINE void CRC_SetPolyReg(uint32_t value)
{
    CRC->POLY = value;
}

/*!
* @brief Get CRC Poly register
*
* @param[in] none
* @return poly
*/
__STATIC_INLINE uint32_t CRC_GetPolyReg(void)
{
    return (CRC->POLY);
}

/*!
* @brief Set CRC Data register
*
* @param[in] value: data value
* @return none
*/
__STATIC_INLINE void CRC_SetDataReg(uint32_t value)
{
    CRC->DATAn.DATA32 = value;
}

/*!
* @brief Get CRC Data register
*
* @param[in] none
* @return data value
*/
__STATIC_INLINE uint32_t CRC_GetDataReg(void)
{
    return (CRC->DATAn.DATA32);
}

/*!
* @brief Set CRC Data Low 16bit register
*
* @param[in] value: data value
* @return none
*/
__STATIC_INLINE void CRC_SetDataLReg(uint16_t value)
{
    CRC->DATAn.DATA16.L = value;
}

/*!
* @brief Set CRC Data Low 8bit register
*
* @param[in] value: data value
* @return none
*/
__STATIC_INLINE void CRC_SetDataLLReg(uint8_t value)
{
    CRC->DATAn.DATA8.LL = value;
}

/*!
* @brief select crc protocol mode between crc16 or crc32
*
* @param[in] type: crc check mode select
* @return none
*/
__STATIC_INLINE void CRC_SetProtocolType(CRC_ProtocolType type)
{
    MODIFY_REG32(CRC->CTRL, CRC_CTRL_TCRC_Msk, CRC_CTRL_TCRC_Pos, type);
}

/*!
* @brief get crc protocol mode between crc16 or crc32
*
* @param[in] none
* @return crc protocol mode
*/
__STATIC_INLINE CRC_ProtocolType CRC_GetProtocolType(void)
{
    return ((CRC_ProtocolType)((CRC->CTRL & CRC_CTRL_TCRC_Msk) >> CRC_CTRL_TCRC_Pos));
}

/*!
* @brief select seed or data mode
*
* @param[in] mode: mode select
* @return none
*/
__STATIC_INLINE void CRC_SetSeedOrDataMode(CRC_DataType mode)
{
    MODIFY_REG32(CRC->CTRL, CRC_CTRL_WAS_Msk, CRC_CTRL_WAS_Pos, mode);
}

/*!
* @brief set CRC write transpose mode
*
* @param[in] transType: write transpose mode
* @return none
*/
__STATIC_INLINE void CRC_SetWriteTranspose(CRC_WriteTransposeType transType)
{
    MODIFY_REG32(CRC->CTRL, CRC_CTRL_TOTW_Msk, CRC_CTRL_TOTW_Pos, transType);
}

/*!
* @brief get CRC write transpose mode
*
* @param[in] none
* @return write transpose mode
*/
__STATIC_INLINE CRC_WriteTransposeType CRC_GetWriteTranspose(void)
{
    return ((CRC_WriteTransposeType)((CRC->CTRL & CRC_CTRL_TOTW_Msk) >> CRC_CTRL_TOTW_Pos));
}

/*!
* @brief set CRC read transpose mode
*
* @param[in] transType: read transpose mode
* @return none
*/
__STATIC_INLINE void CRC_SetReadTranspose(CRC_ReadTransposeType transType)
{
    MODIFY_REG32(CRC->CTRL, CRC_CTRL_TOTR_Msk, CRC_CTRL_TOTR_Pos, transType);
}

/*!
* @brief get CRC read transpose mode
*
* @param[in] none
* @return read transpose mode
*/
__STATIC_INLINE CRC_ReadTransposeType CRC_GetReadTranspose(void)
{
    return ((CRC_ReadTransposeType)((CRC->CTRL & CRC_CTRL_TOTR_Msk) >> CRC_CTRL_TOTR_Pos));
}

/*!
* @brief set CRC result xor mode
*
* @param[in] enable: read transpose mode
                - ENABLE: read crc result with xor
                - DISABLE: read crc result without xor
* @return none
*/
__STATIC_INLINE void CRC_SetResultXorMode(ACTION_Type enable)
{
    MODIFY_REG32(CRC->CTRL, CRC_CTRL_FXOR_Msk, CRC_CTRL_FXOR_Pos, enable);
}

/*!
* @brief get CRC read xor mode
*
* @param[in] none
* @return read transpose mode
*/
__STATIC_INLINE ACTION_Type CRC_GetFXorMode(void)
{
    return ((CRC->CTRL & CRC_CTRL_FXOR_Msk) ? ENABLE : DISABLE);
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _AC780X_CRC_REG_H */

/* =============================================  EOF  ============================================== */
