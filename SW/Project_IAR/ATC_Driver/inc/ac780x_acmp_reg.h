/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_ACMP_REG_H
#define _AC780X_ACMP_REG_H
/*!
* @file ac780x_acmp_reg.h
*
* @brief Analog comparator module access register inline function definition.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x_acmp.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief Enable/disable acmp module.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ACMP_Enable(ACMP_Type *ACMPx, ACTION_Type state)
{
    MODIFY_REG32(ACMPx->CR0, ACMP_CR0_EN_Msk, ACMP_CR0_EN_Pos, state);
}

/*!
* @brief Set acmp hysteresis voltage.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] mV: hysteresis voltage
                - ACMP_HYSTERISIS_20MV
                - ACMP_HYSTERISIS_40MV
* @return none
*/
__STATIC_INLINE void ACMP_SetHysteresisVoltage(ACMP_Type *ACMPx, ACMP_HysterisisVolType mV)
{
    MODIFY_REG32(ACMP_ANACFG->CFG0, ACMP_ANACFG_CFG0_HYST_Msk, ACMP_ANACFG_CFG0_HYST_Pos, mV);
}

/*!
* @brief Set low-pass filter.
*
* @param[in] freq: low pass filter frequence
                - ACMP_LPF_200KHZ
                - ACMP_LPF_500KHZ
                - ACMP_LPF_750KHZ
                - ACMP_LPF_1000KHZ
* @return none
*/
__STATIC_INLINE void ACMP_SetLowPassFilter(ACMP_LowPassFilterType freq)
{
    MODIFY_REG32(ACMP_ANACFG->CFG0, ACMP_ANACFG_CFG0_LPF_Msk, ACMP_ANACFG_CFG0_LPF_Pos, freq);
}

/*!
* @brief Set acmp sensitivity modes of the interrupt trigger.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] mode: edge mode
                - ACMP_EDGE_TRIGGER_FALLING
                - ACMP_RISING_EDGE_TRIGGER
                - ACMP_FALLING_RISING_EDGE_TRIGGER
* @return none
*/
__STATIC_INLINE void ACMP_SetTriggerInterruptEdgeMode(ACMP_Type *ACMPx, ACMP_EdgeTrigType mode)
{
    MODIFY_REG32(ACMPx->CR0, ACMP_CR0_MOD_Msk, ACMP_CR0_MOD_Pos, mode);
}

/*!
* @brief Set acmp hall output.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] hallACh: which polling channel is the hallA output
                - ACMP_EXTERNAL_CH0
                - ACMP_EXTERNAL_CH1
                - ACMP_EXTERNAL_CH2
                - ACMP_EXTERNAL_CH3
                - ACMP_EXTERNAL_CH4
                - ACMP_EXTERNAL_CH5
                - ACMP_EXTERNAL_CH6
                - ACMP_DAC_OUTPUT_CH7
* @param[in] hallBCh: which polling channel is the hallB output
* @param[in] hallCCh: which polling channel is the hallC output
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ACMP_SetHallOutput(ACMP_Type *ACMPx, ACMP_ChannelType hallACh, ACMP_ChannelType hallBCh, ACMP_ChannelType hallCCh, ACTION_Type state)
{
    MODIFY_REG32(ACMPx->OPA, ACMP_OPA_OPASEL_Msk, ACMP_OPA_OPASEL_Pos, hallACh);
    MODIFY_REG32(ACMPx->OPB, ACMP_OPB_OPBSEL_Msk, ACMP_OPB_OPBSEL_Pos, hallBCh);
    MODIFY_REG32(ACMPx->OPC, ACMP_OPC_OPCSEL_Msk, ACMP_OPC_OPCSEL_Pos, hallCCh);
    MODIFY_REG32(ACMPx->CR0, ACMP_CR0_OPE_Msk, ACMP_CR0_OPE_Pos, state);
}

/*!
* @brief Set compare result output to the pin.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ACMP_SetOutputToPin(ACMP_Type *ACMPx, ACTION_Type state)
{
    MODIFY_REG32(ACMPx->CR0, ACMP_CR0_OUTEN_Msk, ACMP_CR0_OUTEN_Pos, state);
}

/*!
* @brief Set acmp interrupt.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ACMP_SetInterrupt(ACMP_Type *ACMPx, ACTION_Type state)
{
    MODIFY_REG32(ACMPx->CR0, ACMP_CR0_IE_Msk, ACMP_CR0_IE_Pos, state);
}

/*!
* @brief Set acmp positive input pin
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] channel: input channel
                - ACMP_EXTERNAL_CH0
                - ACMP_EXTERNAL_CH1
                - ACMP_EXTERNAL_CH2
                - ACMP_EXTERNAL_CH3
                - ACMP_EXTERNAL_CH4
                - ACMP_EXTERNAL_CH5
                - ACMP_EXTERNAL_CH6
                - ACMP_DAC_OUTPUT_CH7
* @return none
*/
__STATIC_INLINE void ACMP_SetPositiveInputPin(ACMP_Type *ACMPx, ACMP_ChannelType channel)
{
    MODIFY_REG32(ACMPx->CR1, ACMP_CR1_PSEL_Msk, ACMP_CR1_PSEL_Pos, channel);
}

/*!
* @brief Set acmp negative input pin
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] channel: input channel
                - ACMP_EXTERNAL_CH0
                - ACMP_EXTERNAL_CH1
                - ACMP_EXTERNAL_CH2
                - ACMP_EXTERNAL_CH3
                - ACMP_EXTERNAL_CH4
                - ACMP_EXTERNAL_CH5
                - ACMP_EXTERNAL_CH6
                - ACMP_DAC_OUTPUT_CH7
* @return none
*/
__STATIC_INLINE void ACMP_SetNegativeInputPin(ACMP_Type *ACMPx, ACMP_ChannelType channel)
{
    MODIFY_REG32(ACMPx->CR1, ACMP_CR1_NSEL_Msk, ACMP_CR1_NSEL_Pos, channel);
}

/*!
* @brief Set dac value.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] value: dac output value
                - 0~63
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ACMP_SetDacOutput(ACMP_Type *ACMPx, uint8_t value, ACTION_Type state)
{
    MODIFY_REG32(ACMPx->CR2, ACMP_CR2_DACVAL_Msk, ACMP_CR2_DACVAL_Pos, value);
    MODIFY_REG32(ACMPx->CR2, ACMP_CR2_DACEN_Msk, ACMP_CR2_DACEN_Pos, state);
}

/*!
* @brief Set dac reference voltage.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] ref: Dac reference voltage
                - ACMP_DAC_BANDGAP
                - ACMP_DAC_VDD
* @return none
*/
__STATIC_INLINE void ACMP_SetDacReference(ACMP_Type *ACMPx, ACMP_DACReferenceType ref)
{
    MODIFY_REG32(ACMPx->DACSR, ACMP_DACSR_DACREF_Msk, ACMP_DACSR_DACREF_Pos, ref);
}

/*!
* @brief Set positive input polling mode
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ACMP_SetPositivePollingMode(ACMP_Type *ACMPx, ACTION_Type state)
{
    MODIFY_REG32(ACMPx->CR3, ACMP_CR3_PSPLEN_Msk, ACMP_CR3_PSPLEN_Pos, state);
}

/*!
* @brief Set negative input polling mode
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ACMP_SetNegativePollingMode(ACMP_Type *ACMPx, ACTION_Type state)
{
    MODIFY_REG32(ACMPx->CR3, ACMP_CR3_NSPLEN_Msk, ACMP_CR3_NSPLEN_Pos, state);
}

/*!
* @brief Set polling mode sequence.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] seq: sequence
                - 0~0xFF
* @return none
*/
__STATIC_INLINE void ACMP_SetPollingModeSequence(ACMP_Type *ACMPx, uint8_t seq)
{
    MODIFY_REG32(ACMPx->CR4, ACMP_CR4_PLSEQ_Msk, ACMP_CR4_PLSEQ_Pos, seq);
}

/*!
* @brief Get acmp data output value in polling mode.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @return polling mode data output value
*/
#define ACMP_GetPollingModeOutputValue(ACMPx)   READ_BIT32(ACMPx->DR, ACMP_DR_POLLING_O_Msk)

/*!
* @brief Get acmp data output value in normal mode.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @return normal mode data output value
*/
#define ACMP_GetOutputValue(ACMPx)   READ_BIT32(ACMPx->DR, ACMP_DR_O_Msk)

/*!
* @brief Get acmp status in polling mode.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @return polling mode status flag
*/
#define ACMP_GetPollingModeStatus(ACMPx)   READ_BIT32(ACMPx->SR, ACMP_SR_POLLING_F_Msk)

/*!
* @brief Get acmp wakeup status in normal mode.
*
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @return wakeup status flag
*/
#define ACMP_GetWakeupStatus(ACMPx)   READ_BIT32(ACMPx->SR, ACMP_SR_WPF_Msk)

/*!
* @brief Get acmp status in normal mode.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @return normal mode status flag
*/
#define ACMP_GetStatus(ACMPx)   READ_BIT32(ACMPx->SR, ACMP_SR_F_Msk)

/*!
* @brief Clear acmp status in polling mode.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @return none
*/
#define ACMP_ClearPollingModeStatus(ACMPx)      WRITE_REG32(ACMPx->SR, ACMP_SR_POLLING_F_Msk)

/*!
* @brief Clear acmp wakeup status in normal mode.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @return none
*/
#define ACMP_ClearWakeupStatus(ACMPx)           WRITE_REG32(ACMPx->SR, ACMP_SR_WPF_Msk)

/*!
* @brief Clear acmp status in normal mode.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @return none
*/
#define ACMP_ClearStatus(ACMPx)                 WRITE_REG32(ACMPx->SR, ACMP_SR_F_Msk)

/*!
* @brief Set acmp polling mode switch frequency divider.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] div: divider
                - ACMP_CLK_DIVIDE_256
                - ACMP_CLK_DIVIDE_100
                - ACMP_CLK_DIVIDE_70
                - ACMP_CLK_DIVIDE_50
* @return none
*/
__STATIC_INLINE void ACMP_SetPollingModeFreqDiv(ACMP_Type *ACMPx, ACMP_PollingModeClkDivType div)
{
    MODIFY_REG32(ACMPx->FD, ACMP_FD_PLFD_Msk, ACMP_FD_PLFD_Pos, div);
}

#ifdef __cplusplus
}
#endif

#endif /* _AC780X_ACMP_REG_H */

/* =============================================  EOF  ============================================== */
