/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_PWM_REG_H
#define _AC780X_PWM_REG_H
/*!
* @file ac780x_pwm_reg.h
*
* @brief Pulse width modulation module access register inline function definition.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x_pwm.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief Set pwm clock source.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] source: clock source
                - PWM_CLK_SOURCE_NONE: No clock selected,in effect disables the PWM counter
                - PWM_CLK_SOURCE_APB: Bus clock
                - PWM_CLK_SOURCE_HSI: internel RC clock
* @return none
*/
__STATIC_INLINE void PWM_SetClockSource(PWM_Type *PWMx, PWM_ClkSourceType source)
{
    MODIFY_REG32(PWMx->INIT, PWM_INIT_CLKSRC_Msk, PWM_INIT_CLKSRC_Pos, source);
}

/*!
* @brief Set pwm clock prescaler.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] psc: clock prescaler
                - 0 ~ 65535
* @return none
*/
__STATIC_INLINE void PWM_SetClockPrescaler(PWM_Type *PWMx, uint16_t psc)
{
    MODIFY_REG32(PWMx->INIT, PWM_INIT_CLKPSC_Msk, PWM_INIT_CLKPSC_Pos, psc);
}

/*!
* @brief Set counting mode.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] mode: counting mode
                - PWM_UP_COUNT: Up counting mode
                - PWM_UP_DOWN_COUNT: Up-down counting mode
* @return none
*/
__STATIC_INLINE void PWM_SetCountMode(PWM_Type *PWMx, PWM_CountModeType mode)
{
    MODIFY_REG32(PWMx->INIT, PWM_INIT_CNTMODE_Msk, PWM_INIT_CNTMODE_Pos, mode);
}

/*!
* @brief Set overflow interrupt.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetOverflowInterrupt(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->INIT, PWM_INIT_CNTOIE_Msk, PWM_INIT_CNTOIE_Pos, state);
}

/*!
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return none
*/
#define PWM_GetOverflowInterrupt(PWMx)           READ_BIT32(PWMx->INIT, PWM_INIT_CNTOIE_Msk)

/*!
* @brief Get overflow flag.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return none
*/
#define PWM_GetOverflowFlag(PWMx)           READ_BIT32(PWMx->INIT, PWM_INIT_CNTOF_Msk)

/*!
* @brief Clear overflow flag.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return none
*/
#define PWM_ClearOverflowFlag(PWMx)         CLEAR_BIT32(PWMx->INIT, PWM_INIT_CNTOF_Msk)

/*!
* @brief Set counter value.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] value: counter value
                - 0 ~ 0xffff
* @return none
*/
#define PWM_SetCounter(PWMx, value)             (PWMx->CNT = value)

/*!
* @brief Get counter value.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return counter value
*/
#define PWM_GetCounter(PWMx)                    (PWMx->CNT)

/*!
* @brief Set counter initial value.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] value: initial counting value
                - 0 ~ 0xffff
* @return none
*/
#define PWM_SetCounterInitValue(PWMx, value)    (PWMx->CNTIN = value)

/*!
* @brief Get counter initial value.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return counter initial value
*/
#define PWM_GetCounterInitValue(PWMx)           (PWMx->CNTIN)

/*!
* @brief Set max counting value.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] value: max counting value
                - 0 ~ 0xffff
* @return none
*/
#define PWM_SetMaxCountValue(PWMx, value)       (PWMx->MCVR = value)

/*!
* @brief Get max counting value.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return max counting value
*/
#define PWM_GetMaxCountValue(PWMx)              (PWMx->MCVR)

/*!
* @brief Set channel edge level.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                - PWM_CH_1
                - PWM_CH_2
                - PWM_CH_3
                - PWM_CH_4
                - PWM_CH_5
                - PWM_CH_6
                - PWM_CH_7
* @param[in] edgeLevel: ELSR1:ELSR0
                - 00, 01, 10, 11
* @return none
*/
__STATIC_INLINE void PWM_SetChannelELSR(PWM_Type *PWMx, PWM_ChannelType channel, uint8_t edgeLevel)
{
    MODIFY_REG32(PWMx->CHANNELS[channel].CHSCR, PWM_CHSCR_ELSR0_Msk | PWM_CHSCR_ELSR1_Msk, PWM_CHSCR_ELSR0_Pos, edgeLevel & 0x3UL);
}

/*!
* @brief Set channel mode.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                ...
                - PWM_CH_7
* @param[in] mode: MSR1:MSR0
                - 00, 01, 10, 11
* @return none
*/
__STATIC_INLINE void PWM_SetChannelMSR(PWM_Type *PWMx, PWM_ChannelType channel, uint8_t mode)
{
    MODIFY_REG32(PWMx->CHANNELS[channel].CHSCR, PWM_CHSCR_MSR0_Msk | PWM_CHSCR_MSR1_Msk, PWM_CHSCR_MSR0_Pos, mode & 0x3UL);
}

/*!
* @brief Set channel match point direction.
*
* only works in combine mode & up-down counting mode
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                ...
                - PWM_CH_7
* @param[in] dir: match point direction
                - PWM_MATCH_DIR_UP: up counting direction
                - PWM_MATCH_DIR_DOWN: down counting direction
* @return none
*/
__STATIC_INLINE void PWM_SetChannelMatchDir(PWM_Type *PWMx, PWM_ChannelType channel, PWM_MatchPointDirType dir)
{
    MODIFY_REG32(PWMx->CHANNELS[channel].CHSCR, PWM_CHSCR_DIR_Msk, PWM_CHSCR_DIR_Pos, dir);
}

/*!
* @brief Set the channel interrupt.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                ...
                - PWM_CH_7
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetChannelInterrupt(PWM_Type *PWMx, PWM_ChannelType channel, ACTION_Type state)
{
    MODIFY_REG32(PWMx->CHANNELS[channel].CHSCR, PWM_CHSCR_CHIE_Msk, PWM_CHSCR_CHIE_Pos, state);
}

/*!
* @brief Get the channel flag.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                ...
                - PWM_CH_7
* @return channel Flag
*/
#define PWM_GetChannelFlag(PWMx, channel)           READ_BIT32(PWMx->CHANNELS[channel].CHSCR, PWM_CHSCR_CHIF_Msk)

/*!
* @brief Clear the channel flag.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                ...
                - PWM_CH_7
* @return none
*/
#define PWM_ClearChannelFlag(PWMx, channel)         CLEAR_BIT32(PWMx->CHANNELS[channel].CHSCR, PWM_CHSCR_CHIF_Msk)

/*!
* @brief Get all channel flag.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return all channel Flag
*/
#define PWM_GetAllChannelFlag(PWMx)                 READ_BIT32(PWMx->STR, PWM_STR_CHSF_Msk)

/*!
* @brief Clear all channel flag.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] mask: mask bit
                - 0 ~ 0xff
* @return none
*/
#define PWM_ClearAllChannelFlag(PWMx, mask)         CLEAR_BIT32(PWMx->STR, mask)

/*!
* @brief Set channel counting value.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                ...
                - PWM_CH_7
* @param[in] value: counting value
                - 0 ~ 0xffff
* @return none
*/
#define PWM_SetChannelCountValue(PWMx, channel, value)      (PWMx->CHANNELS[channel].CHV = value)

/*!
* @brief Get channel counting value.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                ...
                - PWM_CH_7
* @return channel counting value
*/
#define PWM_GetChannelCountValue(PWMx, channel)             (PWMx->CHANNELS[channel].CHV)

/*!
* @brief Enable global synchronizing function.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetSync(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->FUNCSEL, PWM_FUNCSEL_PWMSYNCEN_Msk, PWM_FUNCSEL_PWMSYNCEN_Pos, state);
}

/*!
* @brief Set initial channel output function.
*
* One-time effect,manual clear is not required
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetInitChannelOutput(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->FUNCSEL, PWM_FUNCSEL_INIT_Msk, PWM_FUNCSEL_INIT_Pos, state);
}

/*!
* @brief Set the write protection.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetWriteProtection(PWM_Type *PWMx, ACTION_Type state)
{
    if (state)
    {
        MODIFY_REG32(PWMx->FDSR, PWM_FDSR_WPEN_Msk, PWM_FDSR_WPEN_Pos, 0x1UL);
    }
    else
    {
        MODIFY_REG32(PWMx->FUNCSEL, PWM_FUNCSEL_WPDIS_Msk, PWM_FUNCSEL_WPDIS_Pos, 0x1UL);
    }
}

/*!
* @brief Set synchronizing type.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] type: sycn type
                - 0: No restrictions. Software and hardware triggers can be used by MCVR, CHnV, OMSR, and
                     PWM counter synchronization
                - 1: Software trigger can only be used by MCVR and CHnV synchronization, and hardware triggers
                     can only be used by OMSR and PWM counter synchronization
* @return none
*/
__STATIC_INLINE void PWM_SetSyncType(PWM_Type *PWMx, uint8_t type)
{
    MODIFY_REG32(PWMx->FUNCSEL, PWM_FUNCSEL_PWMSYNC_Msk, PWM_FUNCSEL_PWMSYNC_Pos, type);
}

/*!
* @brief Set fault control mode.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] mode: fault mode
                - PWM_FAULT_CTRL_NONE
                - PWM_FAULT_CTRL_MANUAL_EVEN
                - PWM_FAULT_CTRL_MANUAL_ALL
                - PWM_FAULT_CTRL_AUTO
* @return none
*/
__STATIC_INLINE void PWM_SetFaultMode(PWM_Type *PWMx, PWM_FaultCtrlModeType mode)
{
    MODIFY_REG32(PWMx->FUNCSEL, PWM_FUNCSEL_FAULTMODE_Msk, PWM_FUNCSEL_FAULTMODE_Pos, mode);
}

/*!
* @brief Set fault interrupt.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetFaultInterrupt(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->FUNCSEL, PWM_FUNCSEL_FAULTIE_Msk, PWM_FUNCSEL_FAULTIE_Pos, state);
}

/*!
* @brief Get fault interrupt.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return none
*/
#define PWM_GetFaultInterrupt(PWMx)           READ_BIT32(PWMx->FUNCSEL, PWM_FUNCSEL_FAULTIE_Msk)

/*!
* @brief Set maximum loading point mode.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetMaxLoadingPoint(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->SYNC, PWM_SYNC_MAXSYNCP_Msk, PWM_SYNC_MAXSYNCP_Pos, state);
}

/*!
* @brief Set minimum loading point mode.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetMinLoadingPoint(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->SYNC, PWM_SYNC_MINSYNCP_Msk, PWM_SYNC_MINSYNCP_Pos, state);
}

/*!
* @brief Set hardware synchronization trigger source.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] num: hardware synchronization trigger num
                - 0 ~ 2
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetHardwareTriggerSyncSrc(PWM_Type *PWMx, uint8_t num, ACTION_Type state)
{
    DEVICE_ASSERT(3U < num);
    MODIFY_REG32(PWMx->SYNC, PWM_SYNC_TRIG0_Msk << num, PWM_SYNC_TRIG0_Pos << num, state);
}

/*!
* @brief Software trigger synchronization.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetSoftwareTriggerSync(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->SYNC, PWM_SYNC_SWSYNC_Msk, PWM_SYNC_SWSYNC_Pos, state);
}

/*!
* @brief Set channel output init value.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                ...
                - PWM_CH_7
* @param[in] level: initialization level
                - PWM_LOW_LEVEL
                - PWM_HIGH_LEVEL
* @return none
*/
__STATIC_INLINE void PWM_SetChannelOutputInitLevel(PWM_Type *PWMx, PWM_ChannelType channel, PWM_OutputLevelType level)
{
    MODIFY_REG32(PWMx->OUTINIT, PWM_OUTINIT_CH0OIV_Msk << channel, channel, level);
}

/*!
* @brief Set channel output mask.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                ...
                - PWM_CH_7
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
#define PWM_SetChannelOutputMask(PWMx, channel, state)      MODIFY_REG32(PWMx->OMCR, PWM_OMCR_CH0OMEN_Msk << channel, channel, state)

/*!
* @brief Set all channel output mask by mask value.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] mask: channel mask
                - 0 ~ 0xff
* @return none
*/
#define PWM_SetAllChannelOutputMask(PWMx, mask)             MODIFY_REG32(PWMx->OMCR, PWM_OMCR_CHOMEN_Msk, PWM_OMCR_CH0OMEN_Pos, mask);

/*!
* @brief Set channel pair output combine.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] pairChannel: pair channel number
                - 0 ~ 3
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetPairChannelCombine(PWM_Type *PWMx, uint8_t pairChannel, ACTION_Type state)
{
    MODIFY_REG32(PWMx->MODESEL, PWM_MODESEL_PAIR0COMBINEN_Msk << (pairChannel * PWM_MODESEL_PAIR_CONFIG_WIDTH), \
                 (pairChannel * PWM_MODESEL_PAIR_CONFIG_WIDTH), state);
}

/*!
* @brief Set channel pair output complement.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] pairChannel: pair channel number
                - 0 ~ 3
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetPairChannelComplement(PWM_Type *PWMx, uint8_t pairChannel, ACTION_Type state)
{
    MODIFY_REG32(PWMx->MODESEL, PWM_MODESEL_PAIR0COMPEN_Msk << (pairChannel * PWM_MODESEL_PAIR_CONFIG_WIDTH), \
                 PWM_MODESEL_PAIR0COMPEN_Pos << (pairChannel * PWM_MODESEL_PAIR_CONFIG_WIDTH), state);
}

/*!
* @brief Set channel pair dual edge capture mode.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] pairChannel: pair channel number
                - 0 ~ 3
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetPairChannelDualEdgeCapture(PWM_Type *PWMx, uint8_t pairChannel, ACTION_Type state)
{
    MODIFY_REG32(PWMx->MODESEL, PWM_MODESEL_PAIR0DECAPEN_Msk << (pairChannel * PWM_MODESEL_PAIR_CONFIG_WIDTH), \
                 PWM_MODESEL_PAIR0DECAPEN_Pos << (pairChannel * PWM_MODESEL_PAIR_CONFIG_WIDTH), state);
}

/*!
* @brief Set channel pair dual edge capture.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] pairChannel: pair channel number
                - 0 ~ 3
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetPairChannelDecap(PWM_Type *PWMx, uint8_t pairChannel, ACTION_Type state)
{
    MODIFY_REG32(PWMx->MODESEL, PWM_MODESEL_PAIR0DECAP_Msk << (pairChannel * PWM_MODESEL_PAIR_CONFIG_WIDTH), \
                 PWM_MODESEL_PAIR0DECAP_Pos << (pairChannel * PWM_MODESEL_PAIR_CONFIG_WIDTH), state);
}

/*!
* @brief Set channel pair deadtime insertion.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] pairChannel: pair channel number
                - 0 ~ 3
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetPairChannelDeadtime(PWM_Type *PWMx, uint8_t pairChannel, ACTION_Type state)
{
    MODIFY_REG32(PWMx->MODESEL, PWM_MODESEL_PAIR0DTEN_Msk << (pairChannel * PWM_MODESEL_PAIR_CONFIG_WIDTH), \
                 PWM_MODESEL_PAIR0DTEN_Pos << (pairChannel * PWM_MODESEL_PAIR_CONFIG_WIDTH), state);
}

/*!
* @brief Set channel pair counter synchronization.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] pairChannel: pair channel number
                - 0 ~ 3
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetPairChannelCounterSync(PWM_Type *PWMx, uint8_t pairChannel, ACTION_Type state)
{
    MODIFY_REG32(PWMx->MODESEL, PWM_MODESEL_PAIR0SYNCEN_Msk << (pairChannel * PWM_MODESEL_PAIR_CONFIG_WIDTH), \
                 PWM_MODESEL_PAIR0SYNCEN_Pos << (pairChannel * PWM_MODESEL_PAIR_CONFIG_WIDTH), state);
}

/*!
* @brief Set channel pair fault control.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] pairChannel: pair channel number
                - 0 ~ 3
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetPairChannelFaultControl(PWM_Type *PWMx, uint8_t pairChannel, ACTION_Type state)
{
    MODIFY_REG32(PWMx->MODESEL, PWM_MODESEL_PAIR0FAULTEN_Msk << (pairChannel * PWM_MODESEL_PAIR_CONFIG_WIDTH), \
                 PWM_MODESEL_PAIR0FAULTEN_Pos << (pairChannel * PWM_MODESEL_PAIR_CONFIG_WIDTH), state);
}

/*!
* @brief Set channel inverting control.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] pairChannel: pair channel number
                - 0 ~ 3
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetPairChannelInvert(PWM_Type *PWMx, uint8_t pairChannel, ACTION_Type state)
{
    MODIFY_REG32(PWMx->INVCR, PWM_INVCR_PAIR0INVEN_Msk << pairChannel, pairChannel, state);
}

/*!
* @brief Set dead time count.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] psc: prescaler divider
                - PWM_DEADTIME_DIVID_1
                - PWM_DEADTIME_DIVID_4
                - PWM_DEADTIME_DIVID_16
* @param[in] value: inserted value
                - 0 ~ 63
* @return none
*/
__STATIC_INLINE void PWM_SetDeadtime(PWM_Type *PWMx, PWM_DeadtimePscType psc, uint8_t value)
{
    MODIFY_REG32(PWMx->DTSET, PWM_DTSET_DTPSC_Msk, PWM_DTSET_DTPSC_Pos, psc);
    MODIFY_REG32(PWMx->DTSET, PWM_DTSET_DTVAL_Msk, PWM_DTSET_DTVAL_Pos, value);
}

/*!
* @brief Set the channel trigger source.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                ...
                - PWM_CH_7
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetChannelTrigger(PWM_Type *PWMx, PWM_ChannelType channel, ACTION_Type state)
{
    MODIFY_REG32(PWMx->EXTTRIG, PWM_EXTTRIG_CH0TRIG_Msk << channel, channel, state);
}

/*!
* @brief Set the initialization trigger source.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetInitTrigger(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->EXTTRIG, PWM_EXTTRIG_INITTRIGEN_Msk, PWM_EXTTRIG_INITTRIGEN_Pos, state);
}

/*!
* @brief Get the channel trigger flag.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return external trigger flag
*/
__STATIC_INLINE uint32_t PWM_GetChannelTriggerFlag(PWM_Type *PWMx)
{
    return READ_BIT32(PWMx->EXTTRIG, PWM_EXTTRIG_TRIGF_Msk);
}

/*!
* @brief Clear channel trigger flag.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return none
*/
__STATIC_INLINE void PWM_ClearChannelTriggerFlag(PWM_Type *PWMx)
{
    CLEAR_BIT32(PWMx->EXTTRIG, PWM_EXTTRIG_TRIGF_Msk);
}

/*!
* @brief Set channel polarity.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                ...
                - PWM_CH_7
* @param[in] polarity: output polarity
                - PWM_OUTPUT_POLARITY_ACTIVE_HIGH: active low
                - PWM_OUTPUT_POLARITY_ACTIVE_LOW: active high
* @return none
*/
__STATIC_INLINE void PWM_SetChannelPolarity(PWM_Type *PWMx, PWM_ChannelType channel, PWM_OutputPolarityActiveType polarity)
{
    MODIFY_REG32(PWMx->CHOPOLCR, PWM_CHOPOLCR_CH0POL_Msk << channel, channel, polarity);
}

/*!
* @brief Get main fault detection flag.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return main fault detection flag
*/
#define PWM_GetFaultFlag(PWMx)                      READ_BIT32(PWMx->FDSR, PWM_FDSR_FAULTDF_Msk)

/*!
* @brief Clear main fault detection flag.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return main fault detection flag
*/
#define PWM_ClearFaultFlag(PWMx)                    CLEAR_BIT32(PWMx->FDSR, PWM_FDSR_FAULTDF_Msk);

/*!
* @brief Get fault channel detection flag.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: fault input channel
                - PWM_FAULT_CH_0
                - PWM_FAULT_CH_1
                - PWM_FAULT_CH_2
* @return fault channel detection flag
*/
#define PWM_GetFaultChannelFlag(PWMx, channel)      READ_BIT32(PWMx->FDSR, (PWM_FDSR_FAULTDF0_Msk << channel))

/*!
* @brief Clear fault channel detection flag.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: fault input channel
                - PWM_FAULT_CH_0
                - PWM_FAULT_CH_1
                - PWM_FAULT_CH_2
* @return none
*/
#define PWM_ClearFaultChannelFlag(PWMx, channel)    CLEAR_BIT32(PWMx->FDSR, (PWM_FDSR_FAULTDF0_Msk << channel))

/*!
* @brief Get fault input status flag.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return fault input status flag
*/
#define PWM_GetFaultInputStatusFlag(PWMx)           READ_BIT32(PWMx->FDSR, PWM_FDSR_FAULTIN_Msk)

/*!
* @brief Set channel input filter value.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                - PWM_CH_1
                ...
                - PWM_CH_3
* @param[in] value: filter value
                - 0 ~ 255
* @return none
*/
__STATIC_INLINE void PWM_SetChannelInputFilter(PWM_Type *PWMx, PWM_ChannelType channel, uint8_t value)
{
    DEVICE_ASSERT(PWM_CH_4 > channel);
    MODIFY_REG32(PWMx->CAPFILTER, PWM_CAPFILTER_CH0CAPFVAL_Msk << (channel * PWM_CAPFILTER_CHCAPFVAL_WIDTH) , \
                 (channel * PWM_CAPFILTER_CHCAPFVAL_WIDTH), value);
}

/*!
* @brief Set fault input activity.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: fault input channel
                - PWM_FAULT_CH_0
                - PWM_FAULT_CH_1
                - PWM_FAULT_CH_2
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetFaultInput(PWM_Type *PWMx, PWM_FaultChannelType channel, ACTION_Type state)
{
    MODIFY_REG32(PWMx->FFAFER, PWM_FFAFER_FER0EN_Msk << channel, channel, state);
}

/*!
* @brief Set fault input filter.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: fault input channel
                - PWM_FAULT_CH_0
                - PWM_FAULT_CH_1
                - PWM_FAULT_CH_2
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetFaultInputFilter(PWM_Type *PWMx, PWM_FaultChannelType channel, ACTION_Type state)
{
    MODIFY_REG32(PWMx->FFAFER, PWM_FFAFER_FF0EN_Msk << channel, PWM_FFAFER_FF0EN_Pos << channel, state);
}

/*!
* @brief Set fault input filter value.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] value: fault filter value
                - 0 ~ 255
* @return none
*/
__STATIC_INLINE void PWM_SetFaultInputFilterValue(PWM_Type *PWMx, uint8_t value)
{
    MODIFY_REG32(PWMx->FFAFER, PWM_FFAFER_FFVAL_Msk, PWM_FFAFER_FFVAL_Pos, value);
}

/*!
* @brief Set fault input polarity.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: fault input channel
                - PWM_FAULT_CH_0
                - PWM_FAULT_CH_1
                - PWM_FAULT_CH_2
* @param[in] polarity: output polarity
                - PWM_INPUT_POLARITY_ACTIVE_HIGH: fault input polarity is active high
                - PWM_INPUT_POLARITY_ACTIVE_LOW: fault input polarity is active low
* @return none
*/
__STATIC_INLINE void PWM_SetFaultInputPolarity(PWM_Type *PWMx, PWM_FaultChannelType channel, PWM_FaultInputPolarityActiveType polarity)
{
    MODIFY_REG32(PWMx->FLTPOL, PWM_FLTPOL_FLT0POL_Msk << channel, channel, polarity);
}

/*!
* @brief Set quadrature decoder.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetQuadDecode(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->QDI, PWM_QDI_QDIEN_Msk, PWM_QDI_QDIEN_Pos, state);
}

/*!
* @brief Get the timer overflow direction.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return quad time overflow direction flag
*/
#define PWM_GetQuadOverflowDir(PWMx)            READ_BIT32(PWMx->QDI, PWM_QDI_CNTOFDIR_Msk)

/*!
* @brief Get the timer counting direction.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return counting direction
*/
#define PWM_GetQuadCountDir(PWMx)               READ_BIT32(PWMx->QDI, PWM_QDI_QUADIR_Msk)

/*!
* @brief Set quadrature encoding mode.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] mode: quadrature encoding mode
                - PWM_QUAD_PHASE_ENCODE: Phase A and Phase B encoding mode
                - PWM_QUAD_COUNT_DIR: Count and direction encoding mode
* @return none
*/
__STATIC_INLINE void PWM_SetQuadEncodeMode(PWM_Type *PWMx, PWM_QuadDecodeModeType mode)
{
    MODIFY_REG32(PWMx->QDI, PWM_QDI_QUADMODE_Msk, PWM_QDI_QUADMODE_Pos, mode);
}

/*!
* @brief Set quadrature phase A polarity.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] polarity: quadrature input polarity
                - PWM_QUAD_PHASE_NORMAL: Normal polarity
                - PWM_QUAD_PHASE_INVERT: Inverted polarity
* @return none
*/
__STATIC_INLINE void PWM_SetQuadPhaseAPolarity(PWM_Type *PWMx, PWM_QuadPhasePolarityType polarity)
{
    MODIFY_REG32(PWMx->QDI, PWM_QDI_PHAPOL_Msk, PWM_QDI_PHAPOL_Pos, polarity);
}

/*!
* @brief Set quadrature phase B polarity.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] polarity: quadrature input polarity
                - PWM_QUAD_PHASE_NORMAL: Normal polarity
                - PWM_QUAD_PHASE_INVERT: Inverted polarity
* @return none
*/
__STATIC_INLINE void PWM_SetQuadPhaseBPolarity(PWM_Type *PWMx, PWM_QuadPhasePolarityType polarity)
{
    MODIFY_REG32(PWMx->QDI, PWM_QDI_PHBPOL_Msk, PWM_QDI_PHBPOL_Pos, polarity);
}

/*!
* @brief Set CNTOF frequency.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] freq: CNTOF overflow frequency
                - 0 ~ 127
* @return none
*/
__STATIC_INLINE void PWM_SetCNTOFFrequence(PWM_Type *PWMx, uint8_t freq)
{
    MODIFY_REG32(PWMx->CONF, PWM_CONF_CNTOFNUM_Msk, PWM_CONF_CNTOFNUM_Pos, freq);
}

/**
* @brief Set the capture event prescale.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                ...
                - PWM_CH_7
* @param[in] psc: capture event prescale
                - PWM_EVENT_PSC_1
                - PWM_EVENT_PSC_2
                - PWM_EVENT_PSC_4
                - PWM_EVENT_PSC_8
* @return none
*/
__STATIC_INLINE void PWM_SetCaptureEventPrescaler(PWM_Type *PWMx, PWM_ChannelType channel, PMW_InputEventPscType psc)
{
    MODIFY_REG32(PWMx->CONF, PWM_CONF_EVENT0PSC_Msk << (channel * PWM_CONF_EVENTPSC_WIDTH), \
                 PWM_CONF_EVENT0PSC_Pos << (channel * PWM_CONF_EVENTPSC_WIDTH), psc);
}

/*!
* @brief Set global time base.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetGlobalTimeBase(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->CONF, PWM_CONF_GTBEEN_Msk, PWM_CONF_GTBEEN_Pos, state);
}

/*!
* @brief Set global time base.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetGlobalTimeBaseOutput(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->CONF, PWM_CONF_GTBEOUT_Msk, PWM_CONF_GTBEOUT_Pos, state);
}

/*!
* @brief Set hardware trigger auto clear.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetHwTriggerSyncAutoClear(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_HWTRIGMODESEL_Msk, PWM_SYNCONF_HWTRIGMODESEL_Pos, state);
}

/*!
* @brief Set synchronization mode.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] mode: synchronization selection
                - PWM_SYNC_MODE_LEGACY: Legacy synchronization
                - PWM_SYNC_MODE_ENHANCED: Enhanced synchronization
* @return none
*/
__STATIC_INLINE void PWM_SetSyncMode(PWM_Type *PWMx, PWM_SyncModeType mode)
{
    MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_SYNCMODE_Msk, PWM_SYNCONF_SYNCMODE_Pos, mode);
}

/*!
* @brief Set the OMCR register updated time.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetOMCRSync(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->SYNC, PWM_SYNC_OMSYNCP_Msk, PWM_SYNC_OMSYNCP_Pos, state);
}

/*!
* @brief Set the CNTIN register updated time.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetCNTINSync(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_CNTINC_Msk, PWM_SYNCONF_CNTINC_Pos, state);
}

/*!
* @brief Set the INVCR register updated time.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetINVCRSync(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_INVC_Msk, PWM_SYNCONF_INVC_Pos, state);
}

/*!
* @brief Set the CHOSWCR register updated time.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetCHOSWCRSync(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_SWOC_Msk, PWM_SYNCONF_SWOC_Pos, state);
}

/*!
* @brief Set the CHOPOLCR register updated time.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetCHOPOLCRSync(PWM_Type *PWMx, ACTION_Type state)
{
    MODIFY_REG32(PWMx->SYNC, PWM_SYNC_SYNCPOL_Msk, PWM_SYNC_SYNCPOL_Pos, state);
}

/*!
* @brief Set CNT register tirgger mode.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] mode: tirgger mode
                - PWM_SYNC_TRIGGER_SOFTWARE: software trigger
                - PWM_SYNC_TRIGGER_HARDWARE: hardware trigger
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetCNTSyncTriggerMode(PWM_Type *PWMx, PWM_SyncTriggerModeType mode, ACTION_Type state)
{
    if (PWM_SYNC_TRIGGER_SOFTWARE == mode)
    {
        MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_CNTVSWSYNC_Msk, PWM_SYNCONF_CNTVSWSYNC_Pos, state);
    }
    else
    {
        MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_CNTVHWSYNC_Msk, PWM_SYNCONF_CNTVHWSYNC_Pos, state);
    }
}

/*!
* @brief Set MCVR/CNTIN/CHV register tirgger mode.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] mode: tirgger mode
                - PWM_SYNC_TRIGGER_SOFTWARE: software trigger
                - PWM_SYNC_TRIGGER_HARDWARE: hardware trigger
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetMCVRSyncTriggerMode(PWM_Type *PWMx, PWM_SyncTriggerModeType mode, ACTION_Type state)
{
    if (PWM_SYNC_TRIGGER_SOFTWARE == mode)
    {
        MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_PWMSVSWSYNC_Msk, PWM_SYNCONF_PWMSVSWSYNC_Pos, state);
    }
    else
    {
        MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_PWMSVHWSYNC_Msk, PWM_SYNCONF_PWMSVHWSYNC_Pos, state);
    }
}

/*!
* @brief Set OMCR register tirgger mode.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] mode: tirgger mode
                - PWM_SYNC_TRIGGER_SOFTWARE: software trigger
                - PWM_SYNC_TRIGGER_HARDWARE: hardware trigger
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetOMCRSyncTriggerMode(PWM_Type *PWMx, PWM_SyncTriggerModeType mode, ACTION_Type state)
{
    if (PWM_SYNC_TRIGGER_SOFTWARE == mode)
    {
        MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_OMVSWSYNC_Msk, PWM_SYNCONF_OMVSWSYNC_Pos, state);
    }
    else
    {
        MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_OMVHWSYNC_Msk, PWM_SYNCONF_OMVHWSYNC_Pos, state);
    }
}

/*!
* @brief Set INVCR register tirgger mode.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] mode: tirgger mode
                - PWM_SYNC_TRIGGER_SOFTWARE: software trigger
                - PWM_SYNC_TRIGGER_HARDWARE: hardware trigger
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetINVCRSyncTriggerMode(PWM_Type *PWMx, PWM_SyncTriggerModeType mode, ACTION_Type state)
{
    if (PWM_SYNC_TRIGGER_SOFTWARE == mode)
    {
        MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_INVSWSYNC_Msk, PWM_SYNCONF_INVSWSYNC_Pos, state);
    }
    else
    {
        MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_INVHWSYNC_Msk, PWM_SYNCONF_INVHWSYNC_Pos, state);
    }
}

/*!
* @brief Set CHOSWCR register tirgger mode.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] mode: tirgger mode
                - PWM_SYNC_TRIGGER_SOFTWARE: software trigger
                - PWM_SYNC_TRIGGER_HARDWARE: hardware trigger
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetCHOSWCRSyncTriggerMode(PWM_Type *PWMx, PWM_SyncTriggerModeType mode, ACTION_Type state)
{
    if (PWM_SYNC_TRIGGER_SOFTWARE == mode)
    {
        MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_SWVSWSYNC_Msk, PWM_SYNCONF_SWVSWSYNC_Pos, state);
    }
    else
    {
        MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_SWVHWSYNC_Msk, PWM_SYNCONF_SWVHWSYNC_Pos, state);
    }
}

/*!
* @brief Set CHOPOLCR register tirgger mode.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] mode: tirgger mode
                - PWM_SYNC_TRIGGER_SOFTWARE: software trigger
                - PWM_SYNC_TRIGGER_HARDWARE: hardware trigger
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SetCHOPOLCRSyncTriggerMode(PWM_Type *PWMx, PWM_SyncTriggerModeType mode, ACTION_Type state)
{
    if (PWM_SYNC_TRIGGER_SOFTWARE == mode)
    {
        MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_SWPOL_Msk, PWM_SYNCONF_SWPOL_Pos, state);
    }
    else
    {
        MODIFY_REG32(PWMx->SYNCONF, PWM_SYNCONF_HWPOL_Msk, PWM_SYNCONF_HWPOL_Pos, state);
    }
}

/*!
* @brief Set channel software control function.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                ...
                - PWM_CH_7
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
#define PWM_SetChannelSoftControl(PWMx, channel, state)     MODIFY_REG32(PWMx->CHOSWCR, PWM_CHOSWCR_CH0SWEN_Msk << channel, channel, state)

/*!
* @brief Set all channel software control function.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] mask: channel mask
                - 0 ~ 0xff
* @return none
*/
#define PWM_SetAllChannelSoftControl(PWMx, mask)            MODIFY_REG32(PWMx->CHOSWCR, PWM_CHOSWCR_CHSWEN_Msk, PWM_CHOSWCR_CH0SWEN_Pos, mask)

/*!
* @brief software control channel output level.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] channel: pwm channel number
                - PWM_CH_0
                ...
                - PWM_CH_7
* @param[in] level: output level
                - PWM_LOW_LEVEL: output low level
                - PWM_HIGH_LEVEL: output high level
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWM_SoftControlChannelLevel(PWM_Type *PWMx, PWM_ChannelType channel, PWM_OutputLevelType level, ACTION_Type state)
{
    MODIFY_REG32(PWMx->CHOSWCR, PWM_CHOSWCR_CH0SWCV_Msk << channel, PWM_CHOSWCR_CH0SWCV_Pos << channel, level);
    PWM_SetChannelSoftControl(PWMx, channel, state);
}

/*!
* @brief software control all channel output level.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] mask: channel mask
                - 0 ~ 0xff
* @param[in] value: output value
                - 0 ~ 0xff
* @return none
*/
__STATIC_INLINE void PWM_SoftControlAllChannelLevel(PWM_Type *PWMx, uint8_t mask, uint8_t value)
{
    MODIFY_REG32(PWMx->CHOSWCR, PWM_CHOSWCR_CHSWCV_Msk, PWM_CHOSWCR_CH0SWCV_Pos, value);
    PWM_SetAllChannelSoftControl(PWMx, mask);
}

#ifdef __cplusplus
}
#endif

#endif /* _AC780X_PWM_REG_H */

/* =============================================  EOF  ============================================== */
