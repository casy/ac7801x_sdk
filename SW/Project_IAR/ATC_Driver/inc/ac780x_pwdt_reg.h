/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_PWDT_REG_H
#define _AC780X_PWDT_REG_H
/*!
* @file ac780x_pwdt_reg.h
*
* @brief Pulse width measurement module access register inline function definition.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x_pwdt.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief Set pwdt clock pre-prescaler 0.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] psc: prescaler divide
                - PWDT_CLK_PRESCALER_1
                - PWDT_CLK_PRESCALER_2
                - PWDT_CLK_PRESCALER_4
                - PWDT_CLK_PRESCALER_8
                - PWDT_CLK_PRESCALER_16
                - PWDT_CLK_PRESCALER_32
                - PWDT_CLK_PRESCALER_64
                - PWDT_CLK_PRESCALER_128
* @return none
*/
__STATIC_INLINE void PWDT_SetClockPrescaler0(PWDT_Type *PWDTx, PWDT_ClkPrescalerType psc)
{
    DEVICE_ASSERT(PWDT_CLK_PRESCALER_256 > psc);
    MODIFY_REG32(PWDTx->INIT0, PWDT_INIT0_PSC0_Msk, PWDT_INIT0_PSC0_Pos, psc);
}

/*!
* @brief Set pwdt clock post-prescaler 1.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] psc: prescaler divide
                - PWDT_CLK_PRESCALER_1
                - PWDT_CLK_PRESCALER_2
                - PWDT_CLK_PRESCALER_4
* @return none
*/
__STATIC_INLINE void PWDT_SetClockPrescaler1(PWDT_Type *PWDTx, PWDT_ClkPrescalerType psc)
{
    DEVICE_ASSERT(PWDT_CLK_PRESCALER_8 > psc);
    MODIFY_REG32(PWDTx->INIT0, PWDT_INIT0_PSC1_Msk, PWDT_INIT0_PSC1_Pos, psc);
}

/*!
* @brief Set pwdt count overflow interrupt.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWDT_SetOverflowInterrupt(PWDT_Type *PWDTx, ACTION_Type state)
{
    MODIFY_REG32(PWDTx->INIT0, PWDT_INIT0_OVIE_Msk, PWDT_INIT0_OVIE_Pos, state);
}

/*!
* @brief Set pwdt data ready interrupt.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWDT_SetReadyInterrupt(PWDT_Type *PWDTx, ACTION_Type state)
{
    MODIFY_REG32(PWDTx->INIT0, PWDT_INIT0_PRDYIE_Msk, PWDT_INIT0_PRDYIE_Pos, state);
}

/*!
* @brief Set pwdt module interrupt.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWDT_SetInterrupt(PWDT_Type *PWDTx, ACTION_Type state)
{
    MODIFY_REG32(PWDTx->INIT0, PWDT_INIT0_IE_Msk, PWDT_INIT0_IE_Pos, state);
}

/*!
* @brief Set pwdt edge trigger type.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] edge: edge type
                - PWDT_FALLING_START_CAPTURE_FALLING
                - PWDT_RISING_START_CAPTURE_ALL
                - PWDT_FALLING_START_CAPTURE_ALL
                - PWDT_RISING_START_CAPTURE_RISING
* @return none
*/
__STATIC_INLINE void PWDT_SetEdgeType(PWDT_Type *PWDTx, PWDT_EdgeType edge)
{
    MODIFY_REG32(PWDTx->INIT0, PWDT_INIT0_EDGE_Msk, PWDT_INIT0_EDGE_Pos, edge);
}

/*!
* @brief Set pwdt pin.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] channel: input channel
                - PWDT_INPUT_CH_0
                - PWDT_INPUT_CH_1
                - PWDT_INPUT_CH_2
                - PWDT_INPUT_CH_3
* @return none
*/
__STATIC_INLINE void PWDT_SetInputChannel(PWDT_Type *PWDTx, PWDT_InputChannelType channel)
{
    MODIFY_REG32(PWDTx->INIT0, PWDT_INIT0_PINSEL_Msk, PWDT_INIT0_PINSEL_Pos, channel);
}

/*!
* @brief Get pwdt overflow flag.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @return overflow flag
*/
#define PWDT_GetOverflowFlag(PWDTx)         READ_BIT32(PWDTx->INIT0, PWDT_INIT0_OVF_Msk)

/*!
* @brief Clear pwdt overflow flag.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @return none
*/
#define PWDT_ClearOverflowFlag(PWDTx)       CLEAR_BIT32(PWDTx->INIT0, PWDT_INIT0_OVF_Msk)

/*!
* @brief Get pwdt data ready flag.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @return data ready flag
*/
#define PWDT_GetReadyFlag(PWDTx)            READ_BIT32(PWDTx->INIT0, PWDT_INIT0_RDYF_Msk)

/*!
* @brief Clear pwdt data ready flag.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @return none
*/
#define PWDT_ClearReadyFlag(PWDTx)          CLEAR_BIT32(PWDTx->INIT0, PWDT_INIT0_RDYF_Msk)

/*!
* @brief Get pwdt positive pulse width value.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @return Positive pulse width value
*/
#define PWDT_GetPositivePulseWidth(PWDTx)   (READ_BIT32(PWDTx->INIT0, PWDT_INIT0_PPW_Msk) >> PWDT_INIT0_PPW_Pos)

/*!
* @brief Get pwdt negative pulse width value.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @return negative pulse width value
*/
#define PWDT_GetNegativePulseWidth(PWDTx)   (READ_BIT32(PWDTx->NPW, PWDT_NPW_NPW_Msk) >> PWDT_NPW_NPW_Pos)

/*!
* @brief Get pwdt counting value.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @return counting value
*/
#define PWDT_GetCurrentCount(PWDTx)         (READ_BIT32(PWDTx->NPW, PWDT_NPW_PWDTC_Msk) >> PWDT_NPW_PWDTC_Pos)

/*!
* @brief Set comparator output as input mode.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWDT_SetCompMode(PWDT_Type *PWDTx, ACTION_Type state)
{
    MODIFY_REG32(PWDTx->INIT1, PWDT_INIT1_CMPEN_Msk, PWDT_INIT1_CMPEN_Pos, state);
}

/*!
* @brief Set pwdt hall mode.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWDT_SetHallMode(PWDT_Type *PWDTx, ACTION_Type state)
{
    MODIFY_REG32(PWDTx->INIT1, PWDT_INIT1_HALLEN_Msk, PWDT_INIT1_HALLEN_Pos, state);
}

/*!
* @brief Get pwdt hall status.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @return hall status
*/
#define PWDT_GetHallStatus(PWDTx)         (READ_BIT32(PWDTx->INIT1, PWDT_INIT1_HALLSTATUS_Msk) >> PWDT_INIT1_HALLSTATUS_Pos)

/*!
* @brief Set filter value.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @param[in] psc: filer prescaler divide
                - PWDT_FILTER_PRESCALER_1
                - PWDT_FILTER_PRESCALER_2
                - PWDT_FILTER_PRESCALER_4
                - PWDT_FILTER_PRESCALER_8
                - PWDT_FILTER_PRESCALER_16
                - PWDT_FILTER_PRESCALER_32
                - PWDT_FILTER_PRESCALER_64
                - PWDT_FILTER_PRESCALER_128
                - PWDT_FILTER_PRESCALER_256
                - PWDT_FILTER_PRESCALER_512
                - PWDT_FILTER_PRESCALER_1024
                - PWDT_FILTER_PRESCALER_2048
                - PWDT_FILTER_PRESCALER_4096
* @param[in] value: filer Value
                - 0~15
* @return none
*/
__STATIC_INLINE void PWDT_SetFilter(PWDT_Type *PWDTx, ACTION_Type state, PWDT_FilterPrescalerType psc, uint8_t value)
{
    MODIFY_REG32(PWDTx->INIT1, PWDT_INIT1_FILTPSC_Msk, PWDT_INIT1_FILTPSC_Pos, psc);
    MODIFY_REG32(PWDTx->INIT1, PWDT_INIT1_FILTVAL_Msk, PWDT_INIT1_FILTVAL_Pos, value);
    MODIFY_REG32(PWDTx->INIT1, PWDT_INIT1_FILTEN_Msk, PWDT_INIT1_FILTEN_Pos, state);
}

/*!
* @brief Set pwdt measure mode.
*
* pwdt measure mode enable prior to timer mode, so when timer mode
* is going to be enabled, pwdten must be disabled
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWDT_SetMeasureMode(PWDT_Type *PWDTx, ACTION_Type state)
{
    MODIFY_REG32(PWDTx->INIT0, PWDT_INIT0_PWDTEN_Msk, PWDT_INIT0_PWDTEN_Pos, state);
}

/*!
* @brief Set pwdt timer mode.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void PWDT_SetTimerMode(PWDT_Type *PWDTx, ACTION_Type state)
{
    MODIFY_REG32(PWDTx->INIT1, PWDT_INIT1_TIMEN_Msk, PWDT_INIT1_TIMEN_Pos, state);
}

/*!
* @brief Set pwdt timer value.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] value: period value
                - 0~0xffff
* @return none
*/
__STATIC_INLINE void PWDT_SetTimerPeriodValue(PWDT_Type *PWDTx, uint16_t value)
{
    MODIFY_REG32(PWDTx->INIT1, PWDT_INIT1_TIMLDVAL_Msk, PWDT_INIT1_TIMLDVAL_Pos, value);
}


#ifdef __cplusplus
}
#endif

#endif /* _AC780X_PWDT_REG_H */

/* =============================================  EOF  ============================================== */
