/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_UART_H
#define _AC780X_UART_H
/*!
* @file ac780x_uart.h
*
* @brief This file provides uart integration functions interface.
*
*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* ===========================================  Includes  =========================================== */
#include "ac780x_dma.h"


/* ============================================  Define  ============================================ */
#define UART_GUARDCNT_DEF           (0x10)

/*!
* @brief UART instance index macro.
*/
#define UART_INDEX(UARTx)    ((UART_IndexType)(((uint32_t)(UARTx) - UART0_BASE) >> 12))

/*!< Check function parameter: Is UART peripheral or not */
#define IS_UART_PERIPH(periph) (((periph) == UART0) || \
                                ((periph) == UART1) || \
                                ((periph) == UART2))

/*!< Check function parameter: Is UART baudrate or not?  sampleCnt_div: UARTn_SMP_CNT register division */
#define IS_UART_BAUDRATE(baudrate, sampleCnt_div) (((baudrate) > 0) && ((baudrate) <= (APB_BUS_FREQ/sampleCnt_div)))

/* ===========================================  Typedef  ============================================ */
typedef enum {
    UART0_INDEX = 0U,   /*!< UART0 index */
    UART1_INDEX,        /*!< UART1 index */
    UART2_INDEX,        /*!< UART2 index */
    UART_INDEX_MAX
}UART_IndexType;
/*!< Check function parameter: Is UART index or not */
#define IS_UART_INDEX(index) (((index) == UART0_INDEX) || \
                              ((index) == UART1_INDEX) || \
                              ((index) == UART2_INDEX))

typedef enum {
    UART_WORD_LEN_5BIT = 5U,   /*!< word length is 5 bits */
    UART_WORD_LEN_6BIT,        /*!< word length is 6 bits */
    UART_WORD_LEN_7BIT,        /*!< word length is 7 bits */
    UART_WORD_LEN_8BIT,        /*!< word length is 8 bits */
    UART_WORD_LEN_9BIT         /*!< word length is 9 bits */
}UART_WordLenType;
/*!< Check function parameter: Is UART word length or not */
#define IS_UART_WORD_LEN(wordLength) (((wordLength) == UART_WORD_LEN_5BIT) || \
                                      ((wordLength) == UART_WORD_LEN_6BIT) || \
                                      ((wordLength) == UART_WORD_LEN_7BIT) || \
                                      ((wordLength) == UART_WORD_LEN_8BIT) || \
                                      ((wordLength) == UART_WORD_LEN_9BIT))

typedef enum {
    UART_STOP_1BIT = 0U,      /*!< stop bits number: 1 bit */
    UART_STOP_2BIT            /*!< stop bits number: 2 bit */
}UART_StopBitType;
/*!< Check function parameter: Is UART stop bits or not */
#define IS_UART_STOPBITS(stopBits) (((stopBits) == UART_STOP_1BIT) || \
                                    ((stopBits) == UART_STOP_2BIT))

typedef enum {
    UART_PARI_NO = 0,        /*!< No parity */
    UART_PARI_ODD,           /*!< odd parity */
    UART_PARI_EVEN           /*!< even parity */
}UART_ParityType;
/*!< Check function parameter: Is UART parity or not */
#define IS_UART_PARITY(parity) (((parity) == UART_PARI_NO) || \
                                ((parity) == UART_PARI_ODD) || \
                                ((parity) == UART_PARI_EVEN))

typedef enum {
    UART_RTSCTS_NONE = 0,    /*!< RTS CTS: all disable */
    UART_RTS_ONLY,           /*!< RTS noly */
    UART_CTS_ONLY,           /*!< CTS noly */
    UART_RTSCTS_ALL         /*!< RTS CTS: all Enable */
}UART_RtsCtsType;

typedef enum {
    UART_DMA_TXRX_NONE = 0, /*!< UART DMA TX and RX: all disable */
    UART_DMA_RX_EN,         /*!< UART DMA RX enable */
    UART_DMA_TX_EN,         /*!< UART DMA TX enable */
    UART_DMA_TXRX_EN        /*!< UART DMA TX and RX: all enable */
}UART_DmaEnType;
/*!< Check function parameter: Is UART dma enable type or not */
#define IS_UART_DMAEN(dmaEn) (((dmaEn) == UART_DMA_TXRX_NONE) || \
                              ((dmaEn) == UART_DMA_RX_EN) || \
                              ((dmaEn) == UART_DMA_TX_EN) || \
                              ((dmaEn) == UART_DMA_TXRX_EN))

typedef enum {
    UART_SMP_CNT0 = 0,     /*!< SAM_CNT0: based on 16*baud_pulse, baud_rate = APB CLOCK/16/{DLH, DLL} */
    UART_SMP_CNT1,         /*!< SAM_CNT1: based on 8*baud_pulse, baud_rate = APB CLOCK/8/{DLH, DLL} */
    UART_SMP_CNT2,         /*!< SAM_CNT2: based on 4*baud_pulse, baud_rate = APB CLOCK/4/{DLH, DLL} */
    UART_SMP_CNT3          /*!< SAM_CNT3: based on sampe_count*baud_pulse, baud_rate = APB CLOCK/16/{DLM, DLL} */
}UART_SampleCntType;
/*!< Check function parameter: Is UART sample count or not */
#define IS_UART_SAMPLE_COUNT(sampleCnt) (((sampleCnt) == UART_SMP_CNT0) || \
                                         ((sampleCnt) == UART_SMP_CNT1) || \
                                         ((sampleCnt) == UART_SMP_CNT2) || \
                                         ((sampleCnt) == UART_SMP_CNT3))

typedef struct {
    uint32_t baudrate;               /*!< uart baudrate */
    UART_WordLenType dataBits;       /*!< 5~9: select word length, 5~9 bits */
    UART_StopBitType stopBits;       /*!< 1: 1bit stop; 2: 2bits stop */
    UART_ParityType  parity;         /*!< 0: no parity; 1: odd; 2: even */
    ACTION_Type fifoByteEn;          /*!< 0: Disable; 1: Enable */
    UART_DmaEnType dmaEn;            /*!< 0: disable DMA;  1: DMA_RX Enable; 2: DMA_TX enable; 3: DMATX & DMARX enable */
    UART_SampleCntType sampleCnt;    /*!< sample counter */
    DeviceCallback_Type callBack;    /*!< IRQ Callback pointer */
}UART_ConfigType;


/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */
/*!
* @brief UART initialize
*
* @param[in] UARTx: UART type pointer, x value can be 0 to 2
*                         - UART0
*                         - UART1
*                         - UART2
* @param[in] config: uart setting type pointer which contains the configuration
*                         information for the specified UART.
* @return none
*/
void UART_Init(UART_Type *UARTx, const UART_ConfigType *config);

/*!
* @brief UART uninitialize
*
* @param[in] UARTx: UART type pointer, x value can be 0 to 2
*                         - UART0
*                         - UART1
*                         - UART2
* @return none
*/
void UART_DeInit(UART_Type *UARTx);

/*!
* @brief Set UART baudrate
*
* @param[in] UARTx: UART type pointer, x value can be 0 to 2
*                         - UART0
*                         - UART1
*                         - UART2
* @param[in] baudrate: UART baudrate
* @param[in] sampleCounter: Division factor for bus clock, value can be
*                         - UART_SMP_CNT0: baud_rate = APB CLOCK/16/{DLH, DLL}
*                         - UART_SMP_CNT1: baud_rate = APB CLOCK/8/{DLH, DLL}
*                         - UART_SMP_CNT2: baud_rate = APB CLOCK/4/{DLH, DLL}
*                         - UART_SMP_CNT3: baud_rate = APB CLOCK/16/{DLM, DLL}
* @return none
*/
void UART_SetBaudrate(UART_Type *UARTx, uint32_t baudrate, UART_SampleCntType sampleCounter);

/*!
* @brief Get UART type pointer
*
* @param[in] uartIndex: UART index, value can be 0 to 2
*                         - UART0_INDEX
*                         - UART1_INDEX
*                         - UART2_INDEX
* @return UART type pointer
*                         - UART0
*                         - UART1
*                         - UART2
*/
UART_Type* UART_GetUARTCtrl(UART_IndexType uartIndex);

/*!
* @brief UART send data by normal mode
*
* @param[in] UARTx: UART type pointer, x value can be 0 to 2
*                         - UART0
*                         - UART1
*                         - UART2
* @return Function return state:
*                         -SUCCESS
*                         -ERROR: timeout
*/
ERROR_Type UART_SendData(UART_Type *UARTx, uint16_t data);

/*!
* @brief  UART receive data by normal mode
*
* @param[in] UARTx: UART type pointer, x value can be 0 to 2
*                         - UART0
*                         - UART1
*                         - UART2
* @return Get uart data
*/
uint16_t UART_ReceiveData(UART_Type *UARTx);

/*!
* @brief UART DMA transmit data
*
* @param[in] UARTx: UART type pointer, x value can be 0 to 2
*                 - UART0
*                 - UART1
*                 - UART2
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] txBuffer: point to the send data
* @param[in] length: transfmit data length by byte
*                    If UART is 8 bits, length must be 0~32767, because DMA CHAN_LENGTH is 0~32767
*                    If UART is 9 bits, length must be multiple of 2, because DMA CHAN_LENGTH is length/2,
*                    otherwise the last data cannot be transmit
* @param[in] callback: point to DMA callback function
* @return Function return state: SUCCESS/ERROR
*/
ERROR_Type UART_TransmitDMA(UART_Type *UARTx, DMA_ChannelType *DMAx, \
                                      uint8_t* txBuffer, uint16_t length, DeviceCallback_Type callback);

/*!
* @brief  UART DMA receive data
*
* @param[in] UARTx: UART type pointer, x value can be 0 to 2
*                 - UART0
*                 - UART1
*                 - UART2
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] rxBuffer: point to the receive buffer
* @param[in] length: Receive data length by byte
*                    If UART is 8 bits, length must be 0~32767, because DMA CHAN_LENGTH is 0~32767
*                    If UART is 9 bits, length must be multiple of 2, because DMA CHAN_LENGTH is length/2,
*                    otherwise the last data cannot be receive
* @param[in] callback: point to DMA callback function
* @return Function return state: SUCCESS/ERROR
*/
ERROR_Type UART_ReceiveDMA(UART_Type *UARTx, DMA_ChannelType *DMAx, \
                                    uint8_t* rxBuffer, uint16_t length, DeviceCallback_Type callback);

/*!
* @brief Set UART Parity
*
* @param[in] UARTx: UART type pointer, x value can be 0 to 2
*                         - UART0
*                         - UART1
*                         - UART2
* @param[in] parity: UART parity set
*                         - UART_PARI_NO: no parity
*                         - UART_PARI_ODD: ODD parity
*                         - UART_PARI_EVEN: EVEN parity
* @return none
*/
void UART_SetParity(UART_Type *UARTx, UART_ParityType parity);

/*!
* @brief Set UART RTS CTS
*
* @param[in] UARTx: UART type pointer, x value should be 0, only UART0 has CTS/RTS function
*                         - UART0
* @param[in] RTSCTS: UART rtsCts, value can be 0 to 3
*                         -UART_RTSCTS_NONE��NO RTS or CTS
*                         -UART_RTS_ONLY: RTS only
*                         -UART_CTS_ONLY: CTS only
*                         -UART_RTSCTS_ALL: enable RTS and CTS
* @return none
*/
void UART_SetCTSRTS(UART_Type *UARTx, UART_RtsCtsType RTSCTS);

/*!
* @brief Set UART call back
*
* @param[in] UARTx: UART type pointer, x value can be 0 to 2
*                         - UART0
*                         - UART1
*                         - UART2
* @param[in] Callback: UART Callback function which will be called in UARTx_IRQHandler.
* @return none
*/
void UART_SetCallback(UART_Type *UARTx, const DeviceCallback_Type callback);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _AC780X_UART_H */

/* =============================================  EOF  ============================================== */
