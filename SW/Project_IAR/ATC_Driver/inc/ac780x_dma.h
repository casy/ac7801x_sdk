/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_DMA_H
#define _AC780X_DMA_H
/*!
* @file ac780x_dma.h
*
* @brief This file provides dma integration functions interface.
*
*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ===========================================  Includes  =========================================== */
#include "ac780x.h"


/* ============================================  Define  ============================================ */
/*!
* @brief DMA Channel instance index macro, DMAx should be DMA0_CHANNEL0~DMA0_CHANNEL3
*/
#define DMA0_CHANNEL_INDEX(DMAx)    ((DMA_ChannelIndexType)(((uint32_t)(DMAx) - DMA0_CHANNEL0_BASE) >> 6))

/*!
* @brief DMA Channel IRQ number macro, DMAx should be DMA0_CHANNEL0~DMA0_CHANNEL3
*/
#define DMA0_CHANNEL_IRQ_NUM(DMAx)  ((IRQn_Type)((uint32_t)DMA0_CHANNEL_INDEX(DMAx) + (uint32_t)DMA0_CHANNEL0_IRQn))

/*!< Check function parameter is DMA peripheral or not */
#define IS_DMA_PERIPH(periph) (((periph) == DMA0_CHANNEL0) || \
                               ((periph) == DMA0_CHANNEL1) || \
                               ((periph) == DMA0_CHANNEL2) || \
                               ((periph) == DMA0_CHANNEL3))

/*!< Check function parameter: Is DMA Memory to Memory or not */
#define IS_DMA_M2M_STATE(state) (((state) == ENABLE) || ((state) == DISABLE))

/*!< Check function parameter: Is DMA Memory increase or not */
#define IS_DMA_MEM_INC_STATE(state) (((state) == ENABLE) || ((state) == DISABLE))

/*!< Check function parameter: Is DMA Peripheral increase or not */
#define IS_DMA_PERIPH_INC_STATE(state) (((state) == ENABLE) || ((state) == DISABLE))

/*!< Check function parameter: Is DMA transfer number or not */
#define IS_DMA_TRANSFER_NUM(number) (((number) >= 1U) && ((number) <= 32767U))

/*!< Check function parameter: Is circular mode or not */
#define IS_DMA_CIRCULAR_STATE(circular) (((circular) == ENABLE) || ((circular) == DISABLE))


/* ===========================================  Typedef  ============================================ */
/*!
* @brief DMA Channel index enumeration.
*/
typedef enum
{
    DMA0_CHANNEL0_INDEX = 0,       /*!< 0: DMA channel0 index */
    DMA0_CHANNEL1_INDEX,           /*!< 1: DMA channel1 index */
    DMA0_CHANNEL2_INDEX,           /*!< 2: DMA channel2 index */
    DMA0_CHANNEL3_INDEX,           /*!< 3: DMA channel3 index */
    DMA0_CHANNEL_INDEX_MAX
}DMA_ChannelIndexType;
/*!< Check function parameter is DMA channel index or not */
#define IS_DMA_CHAN_INDEX(index) (((index) == DMA0_CHANNEL0_INDEX) || \
                                  ((index) == DMA0_CHANNEL1_INDEX) || \
                                  ((index) == DMA0_CHANNEL2_INDEX) || \
                                  ((index) == DMA0_CHANNEL3_INDEX))

/*!
* @brief DMA peripheral select ID type enumeration.
*/
typedef enum
{
    DMA_PEPIRH_UART0_TX = 0,   /*!< 0: UART0_TX as DMA peripheral */
    DMA_PEPIRH_UART0_RX,       /*!< 1: UART0_RX as DMA peripheral */
    DMA_PEPIRH_UART1_TX,       /*!< 2: UART1_TX as DMA peripheral */
    DMA_PEPIRH_UART1_RX,       /*!< 3: UART1_RX as DMA peripheral */
    DMA_PEPIRH_UART2_TX,       /*!< 4: UART2_TX as DMA peripheral */
    DMA_PEPIRH_UART2_RX,       /*!< 5: UART2_RX as DMA peripheral */
    DMA_PEPIRH_SPI0_TX,        /*!< 6: SPI0_TX as DMA peripheral */
    DMA_PEPIRH_SPI0_RX,        /*!< 7: SPI0_RX as DMA peripheral */
    DMA_PEPIRH_SPI1_TX,        /*!< 8: SPI1_TX as DMA peripheral */
    DMA_PEPIRH_SPI1_RX,        /*!< 9: SPI1_RX as DMA peripheral */
    DMA_PEPIRH_I2C0_TX,        /*!< 10: I2C0_TX as DMA peripheral */
    DMA_PEPIRH_I2C0_RX,        /*!< 11: I2C0_RX as DMA peripheral */
    DMA_PEPIRH_I2C1_TX,        /*!< 12: I2C1_TX as DMA peripheral */
    DMA_PEPIRH_I2C1_RX,        /*!< 13: I2C1_RX as DMA peripheral */
    DMA_PEPIRH_ADC0,           /*!< 14: ADC0 as DMA peripheral */
}DMA_PeriphType;
/*!< Check function parameter is DMA peripheral select id or not */
#define IS_DMA_PERIPH_ID(id) (((id) == DMA_PEPIRH_UART0_TX) || \
                              ((id) == DMA_PEPIRH_UART0_RX) || \
                              ((id) == DMA_PEPIRH_UART1_TX) || \
                              ((id) == DMA_PEPIRH_UART1_RX) || \
                              ((id) == DMA_PEPIRH_UART2_TX) || \
                              ((id) == DMA_PEPIRH_UART2_RX) || \
                              ((id) == DMA_PEPIRH_SPI0_TX) || \
                              ((id) == DMA_PEPIRH_SPI0_RX) || \
                              ((id) == DMA_PEPIRH_SPI1_TX) || \
                              ((id) == DMA_PEPIRH_SPI1_RX) || \
                              ((id) == DMA_PEPIRH_I2C0_TX) || \
                              ((id) == DMA_PEPIRH_I2C0_RX) || \
                              ((id) == DMA_PEPIRH_I2C1_TX) || \
                              ((id) == DMA_PEPIRH_I2C1_RX) || \
                              ((id) == DMA_PEPIRH_ADC0))

/*!
* @brief DMA peripheral select ID type enumeration.
*/
typedef enum
{
    DMA_READ_FROM_PERIPH = 0,   /*!< 0: DMA read from peripheral */
    DMA_READ_FROM_MEM,          /*!< 1: DMA read from memory */
}DMA_DirType;
/*!< Check function parameter: Is DMA direction or not */
#define IS_DMA_DIR(dir) (((dir) == DMA_READ_FROM_PERIPH) || \
                         ((dir) == DMA_READ_FROM_MEM))

/*!
* @brief DMA peripheral size enumeration.
*/
typedef enum
{
    DMA_PERIPH_SIZE_8BIT = 0,       /*!< 0: DMA peripheral size 8 bits */
    DMA_PERIPH_SIZE_16BIT,          /*!< 1: DMA peripheral size 16 bits */
    DMA_PERIPH_SIZE_32BIT,          /*!< 2: DMA peripheral size 32 bits */
}DMA_PeriphSizeType;
/*!< Check function parameter: Is Peripheral Size or not */
#define IS_DMA_PERIPH_SIZE(size) (((size) == DMA_PERIPH_SIZE_8BIT) || \
                                  ((size) == DMA_PERIPH_SIZE_16BIT) || \
                                  ((size) == DMA_PERIPH_SIZE_32BIT))

/*!
* @brief DMA memory size enumeration.
*/
typedef enum
{
    DMA_MEM_SIZE_8BIT = 0,       /*!< 0: DMA memory size 8 bits */
    DMA_MEM_SIZE_16BIT,          /*!< 1: DMA memory size 16 bits */
    DMA_MEM_SIZE_32BIT,          /*!< 2: DMA memory size 32 bits */
}DMA_MemSizeType;
/*!< Check function parameter: Is Memory Size or not */
#define IS_DMA_MEM_SIZE(size) (((size) == DMA_MEM_SIZE_8BIT) || \
                               ((size) == DMA_MEM_SIZE_16BIT) || \
                               ((size) == DMA_MEM_SIZE_32BIT))

/*!
* @brief DMA memory byte mode enumeration.
*/
typedef enum
{
    DMA_MEM_BYTE_MODE_1TIME = 0U,      /*!< 0: DMA memory word is divided to transfer in 1 time, 32 bits each time */
    DMA_MEM_BYTE_MODE_2TIME = 1U,      /*!< 1: DMA memory word is divided to transfer in 2 times, 16 bits each time */
    DMA_MEM_BYTE_MODE_4TIME = 3U       /*!< 3: DMA memory word is divided to transfer in 4 times, 8 bits each time */
}DMA_MemByteModeType;
/*!< Check function parameter: Is Memory Byte Mode transfer times or not */
#define IS_DMA_MEM_BYTE_MODE(transferTimes) (((transferTimes) == DMA_MEM_BYTE_MODE_1TIME) || \
                                             ((transferTimes) == DMA_MEM_BYTE_MODE_2TIME) || \
                                             ((transferTimes) == DMA_MEM_BYTE_MODE_4TIME))

/*!
* @brief DMA priority enumeration.
*/
typedef enum
{
    DMA_PRIORITY_LOW = 0,         /*!< 0: DMA priority low */
    DMA_PRIORITY_MEDIUM,          /*!< 1: DMA priority medium */
    DMA_PRIORITY_HIGH,            /*!< 2: DMA priority high */
    DMA_PRIORITY_VERY_HIGH,        /*!< 3: DMA priority very high */
}DMA_PriorityType;
/*!< Check function parameter: Is Peripheral increase or not */
#define IS_DMA_PRIORITY(priority) (((priority) == DMA_PRIORITY_LOW) || \
                                   ((priority) == DMA_PRIORITY_MEDIUM) || \
                                   ((priority) == DMA_PRIORITY_HIGH) || \
                                   ((priority) == DMA_PRIORITY_VERY_HIGH))

/*!
* @brief DMA config type struct
*/
typedef struct
{
    uint32_t memStartAddr;             /*!< memory start address */
    uint32_t memEndAddr;               /*!< memory end address */
    uint32_t periphStartAddr;          /*!< peripheral address in memory */
    uint16_t transferNum;              /*!< transfer total transfer numbers: 0~32767, bit 15 should be 0 */
    DMA_PeriphType periphSelect;       /*!< periph work id sel */
    DMA_MemSizeType memSize;           /*!< 2:32-bit, right now only support 32-bit */
    DMA_PeriphSizeType periphSize;     /*!< 0:8-bit; 1:16-bit; 2:32-bit; 3:reserved, if PERIPH_INCREMENT is fixed, it will be 32-bit */
    ACTION_Type memIncrement;          /*!< 0:MEM address is fixed; 1: MEM address increment */
    ACTION_Type periphIncrement;       /*!< 0:Peripheral address is fixed; 1: Peripheral address increment */
    DMA_DirType direction;             /*!< 0: DMA read from peripheral; 1: DMA read from memory */
    ACTION_Type MEM2MEM;               /*!< 0:DMA mode between memory and peripheral device; 1: DMA mode between memory and memory */
    ACTION_Type circular;              /*!< 0:normal mode 1:enable circular mode */
    DMA_MemByteModeType memByteMode;   /*!< memory word is divided to transfer in 1/2/4 times */
    DMA_PriorityType channelPriority;  /*!< 0:low; 1:medium; 2:high; 3: very high */
    ACTION_Type finishInterruptEn;     /*!< finish interrupt:  0:disable 1:enable */
    ACTION_Type halfFinishInterruptEn; /*!< half finish interrupt:  0:disable 1:enable */
    ACTION_Type errorInterruptEn;      /*!< error interrupt:  0:disable 1:enable */
    ACTION_Type channelEn;             /*!< DMA channel: 0:disable 1:enable */
    DeviceCallback_Type callBack;      /*!< IRQ Callback pointer */
}DMA_ConfigType, *DMA_ConfigPtr;

/*!
* @brief DMA IRQ information type struct
*/
typedef struct
{
    DMA_ChannelType* channelNum;    /*!< DMA channel number */
    IRQn_Type dmaIRQn;              /*!< DMA IRQ number */
}DMA_InfoType;


/* ==========================================  Variables  =========================================== */
extern const DMA_InfoType g_dmaChannel[];


/* ====================================  Functions declaration  ===================================== */
/*!
* @brief Initialize DMA channel
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] config:DMA config type pointer which contains the configuration
*                   information for the specified DMA Channel.
* @return none
*/
void DMA_Init(DMA_ChannelType *DMAx, const DMA_ConfigType *config);

/*!
* @brief Uninitialize DMA channel
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return none
*/
void DMA_DeInit(DMA_ChannelType *DMAx);

/*!
* @brief Enable/Disable DMA channel
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] state: Enable/Disable DMA channel
*                 - ENABLE
*                 - DISABLE
* @return none
*/
void DMA_SetChannel(DMA_ChannelType *DMAx, ACTION_Type state);

/*!
* @brief Get DMA channel transmission length
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return DMA channel transfer length
*/
uint32_t DMA_GetTransmissionLength(DMA_ChannelType *DMAx);

/*!
* @brief Get DMA channel transfered data num
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return DMA channel transfered num
*/
uint32_t DMA_GetTransferedDataNum(DMA_ChannelType *DMAx);

/*!
* @brief Get the number of the data left in the DMA channel's internal FIFO
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return DMA Channel FIFO left data number
*/
uint32_t DMA_GetInterFIFODataLeftNum(DMA_ChannelType *DMAx);

/*!
* @brief Flush DMA channel data
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return none
*/
void DMA_ChannelFlush(DMA_ChannelType *DMAx);

/*!
* @brief Set DAM event callback function
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] callback:DMAx interrupt application functon which will be called by DMAx_IRQHandler()
* @return none
*/
void DMA_SetCallback(DMA_ChannelType *DMAx, const DeviceCallback_Type callback);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _AC780X_DMA_H */

/* =============================================  EOF  ============================================== */
