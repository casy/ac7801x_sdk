/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_EFLASH_REG_H
#define _AC780X_EFLASH_REG_H
/*!
* @file ac780x_eflash_reg.h
*
* @brief EFLASH access register inline function definition.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x_eflash.h"

/* ======================================  Functions define  ======================================== */
/*!
 * @brief Set eflash key sequence register
 *
 * @param[in] keyValue: unlock eflash controler key value
 * @return none
 */
__STATIC_INLINE void EFLASH_SetKeyReg(uint32_t keyValue)
{
   EFLASH->KEY = keyValue;
}

/*!
 * @brief Get protect status
 *
 * @param[in] none
 * @return 0: not in read protection, 1: in read protection
 */
__STATIC_INLINE uint8_t EFLASH_GetReadProtectReg(void)
{
    return (EFLASH->INFO & EFLASH_INFO_RD_PRT_Msk);
}

/*!
 * @brief Get the can eflash controler lock bit
 *
 * param[in] none
 * @return 0: eFlash control is unlocked, 1: eFlash control is locked
 */
__STATIC_INLINE uint16_t EFLASH_GetCtrlLockBitReg(void)
{
   return (EFLASH->INFO & EFLASH_INFO_LOCK_Msk) >> EFLASH_INFO_LOCK_Pos;
}

/*!
 * @brief Lock eflash controler
 *
 * @param[in] none
 * @return none
 */
__STATIC_INLINE void EFLASH_LockCtrlReg(void)
{
    EFLASH->INFO |= EFLASH_INFO_LOCK_Msk;
}

/*!
 * @brief Set program or erase start address
 *
 * @param[in] startAddress: start address
 * @return none
 */
__STATIC_INLINE void EFLASH_SetStartAddressReg(uint32_t startAddress)
{
    EFLASH->ADR_CMD = (0x000FFFFF & startAddress);
}

 /*!
 * @brief Set eflash conrtol register 1
 *
 * @param[in] ctrl1Value: Command
 * @return none
 */
__STATIC_INLINE void EFLASH_SetCtrlReg1(uint32_t ctrl1Value)
{
    EFLASH->CTRL0 = ctrl1Value;
}


/*!
 * @brief Set eflash command
 *
 * @param[in] cmdValue: Command
 * @return none
 */
__STATIC_INLINE void EFLASH_SetCtrlCmdReg(uint8_t cmdValue)
{
    MODIFY_REG32(EFLASH->CTRL0, EFLASH_CTRL0_CMD_CTL_Msk, EFLASH_CTRL0_CMD_CTL_Pos, cmdValue);
}

 /*!
 * @brief Trigger the command to start
 *
 * @param[in] none
 * @return none
 */
__STATIC_INLINE void EFLASH_TrigCtrlCmdReg(void)
{
    EFLASH->CTRL0 |= EFLASH_CTRL0_CMD_ST_Msk;
}

/*!
 * @brief Hardfault  interrupt enable
 *
 * @param[in] none
 * @return none
 */
__STATIC_INLINE void EFLASH_EnableCtrlHardfault(void)
{
    EFLASH->CTRL0 |= EFLASH_CTRL0_HDFEN_Msk;
}

/*!
 * @brief Hardfault  interrupt Disable
 *
 * @param[in] none
 * @return none
 */
__STATIC_INLINE void EFLASH_DisableCtrlHardfault(void)
{
    EFLASH->CTRL0 &= (~EFLASH_CTRL0_HDFEN_Msk);
}

/*!
 * @brief Get status that indicate whether command operation is finished
 *
 * @param[in] none
 * @return 0: not finished,1: finished
 */
__STATIC_INLINE uint8_t EFLASH_GetEopReg(void)
{
    return ((EFLASH->SR0 & EFLASH_SR0_EOP_Msk) >> EFLASH_SR0_EOP_Pos);
}

/*!
* @brief clear indicate whether command operation is finished status
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void EFLASH_ClearEopReg(void)
{
    EFLASH->SR0 |= EFLASH_SR0_EOP_Msk;
}

 /*!
 * @brief Clear the operation violates the write protection rules flag
 *
 * @param  none
 * @return none
 */
__STATIC_INLINE void EFLASH_ClearWrerReg(void)
{
    EFLASH->SR0 |= EFLASH_SR0_WRER_Msk;
}
 /*!
 * @brief Clear the operation violates the read protection rules flag
 *
 * @param  none
 * @return none
 */
__STATIC_INLINE void EFLASH_ClearRderReg(void)
{
    EFLASH->SR0 |= EFLASH_SR0_RDER_Msk;
}
 /*!
* @brief  Get status that indicate whether any of the command operations is in process
 *
 * @param[in]  none
 * @return
    0: all operations are not in process
    1: at least one operation in process
 */
__STATIC_INLINE uint8_t EFLASH_GetAnyBusySTReg(void)
{
    return ((EFLASH->SR0 & EFLASH_SR0_CMD_BSY_Msk) >> EFLASH_SR0_CMD_BSY_Pos);
}

 /*!
 * @brief  Get status that indicate whether pvd warning is generated
 *
 * @param[in]  none
 * @return
    0: no pvd warning
    1: pvd warning is generated
 */
__STATIC_INLINE uint8_t EFLASH_GetPVDWarningReg(void)
{
    return ((EFLASH->CTRL2 & EFLASH_CTRL2_PVD_WRN_Msk) >> EFLASH_CTRL2_PVD_WRN_Pos);
}

 /*!
 * @brief Get the error status for  main memory
 *
 * @param[in] none
 * @return status value
 */
__STATIC_INLINE uint32_t EFLASH_GetStatusReg(void)
{
    return (EFLASH->SR0);
}

 /*!
* @brief fore the program command operation to be finished
 *
 * @param  none
 * @return none
 */
__STATIC_INLINE void EFLASH_FlushOperation(void)
{
    EFLASH->SR0 |= EFLASH_SR0_FLUSH_Msk;
}

 /*!
 * @brief Set controler register 2
 *
 * @param[in] ctrlValue: Controler register 2 value
 * @return none
 */
__STATIC_INLINE void EFLASH_SetCtrl2Reg(uint32_t ctrlValue )
{
    EFLASH->CTRL1 = ctrlValue;
}

 /*!
 * @brief Set controler register 2
 *
 * @param[in] none
 * @return none
 */
__STATIC_INLINE uint32_t EFLASH_GetCtrl2Reg(void)
{
    return (EFLASH->CTRL1);
}

 /*!
 * @brief Get Write protect register 1 value
 *
 * @param[in] none
 * @return Write protect register 1 value
 */
__STATIC_INLINE uint32_t EFLASH_GetWriteProReg1(void)
{
    return (EFLASH->WPRT_EN0);
}

 /*!
 * @brief Get Write protect register 2 value
 *
 * @param[in] none
 * @return Write protect register 2 value
 */
__STATIC_INLINE uint32_t EFLASH_GetWriteProReg2(void)
{
    return (EFLASH->WPRT_EN1);
}

#ifdef __cplusplus
}
#endif

#endif /* _AC780X_EFLASH_REG_H */

/* =============================================  EOF  ============================================== */
