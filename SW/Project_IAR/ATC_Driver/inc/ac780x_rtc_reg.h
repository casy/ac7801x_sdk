/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_RTC_REG_H
#define _AC780X_RTC_REG_H

/*!
* @file ac780x_rtc_reg.h
*
* @brief RTC access register inline function definition.
*
*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ===========================================  Includes  =========================================== */
#include "ac780x_rtc.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief get RTC control and state register
*
* @return control and status register value
*/
__STATIC_INLINE uint32_t RTC_GetCtrlStatusReg(void)
{
    return (RTC->SC);
}

/*!
* @brief set RTC clock source
*
* @param[in] clkSource:
*    0: RTC_CLOCK_APB
*    1: RTC_CLOCK_LSI
*    2: RTC_CLOCK_HSE
*    3: RTC_CLOCK_LSE
* @return none
*/
__STATIC_INLINE void RTC_SetClkSource(RTC_ClockSourceType clkSource)
{
    uint32_t tmp = RTC->SC & (~(RTC_SC_RTIF_Msk | RTC_SC_RPIF_Msk | RTC_SC_RTCLKS_Msk));

    tmp |= (((uint32_t)clkSource) << RTC_SC_RTCLKS_Pos) & RTC_SC_RTCLKS_Msk;
    RTC->SC = tmp;
}

/*!
* @brief set RTC prescaler value
*
* @param[in] psrValue: 20bit RTC prescaler value
* @return none
*/
__STATIC_INLINE void RTC_SetPrescaler(uint32_t psrValue)
{
    RTC->PS = psrValue;
}

/*!
* @brief get RTC prescaler value
*
* @param[in] none
* @return RTC prescaler value
*/
__STATIC_INLINE uint32_t RTC_GetPrescaler(void)
{
    return (RTC->PS);
}

/*!
* @brief set RTC timeout value
*
* @param[in] 32 bit RTC timeout value
* @return none
*/
__STATIC_INLINE void RTC_SetPeriod(uint32_t timeoutValue)
{
    RTC->MOD = timeoutValue;
}

/*!
* @brief get RTC modulo value
*
* @param[in] none
* @return RTC modulo value
*/
__STATIC_INLINE uint32_t RTC_GetPeriod(void)
{
    return (RTC->MOD);
}

/*!
* @brief get RTC prescaler value
*
* @param[in] none
* @return RTC cnt value
*/
__STATIC_INLINE uint32_t RTC_GetPrescalerCnt(void)
{
    return (RTC->PSCNT);
}

/*!
* @brief get RTC cnt value
*
* @param[in] none
* @return RTC cnt value
*/
__STATIC_INLINE uint32_t RTC_GetPeriodCnt(void)
{
    return (RTC->CNT);
}

/*!
* @brief enable RTC interrupt
*
* @param[in] enable: enable RTC interrupt
* @return none
*/
__STATIC_INLINE void RTC_SetRTIE(ACTION_Type enable)
{
    uint32_t tmp = RTC->SC & (~(RTC_SC_RTIF_Msk | RTC_SC_RPIF_Msk));

    tmp &= ~RTC_SC_RTIE_Msk;
    if (ENABLE == enable)
    {
        tmp |= RTC_SC_RTIE_Msk;
    }
    RTC->SC = tmp;
}

/*!
* @brief enable RTC prescaler interrupt
*
* @param[in] enable: enable RTC prescaler interrupt
* @return none
*/
__STATIC_INLINE void RTC_SetRPIE(ACTION_Type enable)
{
    uint32_t tmp = RTC->SC & (~(RTC_SC_RTIF_Msk | RTC_SC_RPIF_Msk));

    tmp &= ~RTC_SC_RPIE_Msk;
    if (ENABLE == enable)
    {
        tmp |= RTC_SC_RPIE_Msk;
    }
    RTC->SC = tmp;
}

/*!
* @brief enable RTC output
*
* @param[in] enable: enable RTC output
* @return none
*/
__STATIC_INLINE void RTC_SetRTCOutput(ACTION_Type enable)
{
    uint32_t tmp = RTC->SC & (~(RTC_SC_RTIF_Msk | RTC_SC_RPIF_Msk));

    tmp &= ~RTC_SC_RTCO_Msk;
    if (ENABLE == enable)
    {
        tmp |= RTC_SC_RTCO_Msk;
    }
    RTC->SC = tmp;
}

/*!
* @brief get rtc interrupt flag
*
* @param[in] none
* @return rtc interrupt flag
*/
__STATIC_INLINE uint32_t RTC_GetRTIF(void)
{
    return (RTC->SC & RTC_SC_RTIF_Msk);
}

/*!
* @brief clear rtc interrupt flag
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void RTC_ClearRTIF(void)
{
    uint32_t tmp = RTC->SC & (~RTC_SC_RPIF_Msk);

    tmp |= RTC_SC_RTIF_Msk;
    RTC->SC = tmp;
}

/*!
* @brief get rtc prescaler interrupt flag
*
* @param[in] none
* @return rtc prescaler interrupt flag
*/
__STATIC_INLINE uint32_t RTC_GetRPIF(void)
{
    return (RTC->SC & RTC_SC_RPIF_Msk);
}

/*!
* @brief clear rtc prescaler interrupt flag
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void RTC_ClearRPIF(void)
{
    uint32_t tmp = RTC->SC & (~RTC_SC_RTIF_Msk);

    tmp |= RTC_SC_RPIF_Msk;
    RTC->SC = tmp;
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _AC780X_RTC_REG_H */

/* =============================================  EOF  ============================================== */
