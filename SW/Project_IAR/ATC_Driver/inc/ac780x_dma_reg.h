/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_DMA_REG_H
#define _AC780X_DMA_REG_H
/*!
* @file ac780x_dma_reg.h
*
* @brief Dma access register inline function definition.
*
*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* ===========================================  Includes  =========================================== */
#include "ac780x_dma.h"


/* ============================================  Define  ============================================ */

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief DMA Top warm reset
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void DMA_TopWarmRst(void)
{
    DMA0_TOP_RST->TOP_RST |= DMA_TOP_RST_WARM_RST_Msk;
}

/*!
* @brief DMA_Top Hard reset
*
* @param[in] none
* @return    none
*/
__STATIC_INLINE void DMA_TopHardRst(void)
{
    DMA0_TOP_RST->TOP_RST |= DMA_TOP_RST_HARD_RST_Msk;
    DMA0_TOP_RST->TOP_RST &= ~DMA_TOP_RST_HARD_RST_Msk;
}

/*!
* @brief Check current DMA finish flag
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return 0:Data transfer not finished, 1:Data transfer finished
*/
__STATIC_INLINE uint32_t DMA_IsFinish(DMA_ChannelType *DMAx)
{
    return ((DMAx->STATUS & DMA_CHANNEL_STATUS_FINISH_Msk) >> DMA_CHANNEL_STATUS_FINISH_Pos);
}

/*!
* @brief Check current DMA half finish flag
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return 0:Half of data not finished, 1:Half of data finished
*/
__STATIC_INLINE uint32_t DMA_IsHalfFinish(DMA_ChannelType *DMAx)
{
    return ((DMAx->STATUS & DMA_CHANNEL_STATUS_HALF_FINISH_Msk) >> DMA_CHANNEL_STATUS_HALF_FINISH_Pos);
}

/*!
* @brief Check current DMA transfer error flag
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return 0:Error not happened, 1:Error happened
*/
__STATIC_INLINE uint32_t DMA_IsTransferError(DMA_ChannelType *DMAx)
{
    return ((DMAx->STATUS & DMA_CHANNEL_STATUS_TRANS_ERROR_Msk) >> DMA_CHANNEL_STATUS_TRANS_ERROR_Pos);
}

/*!
* @brief Enable/Disable DMA Finish interrupt
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] state: enable/disable DMA Finish interrupt
*                  - ENABLE
*                  - DISABLE
* @return none
*/
__STATIC_INLINE void DMA_SetFinishInterrupt(DMA_ChannelType *DMAx, ACTION_Type state)
{
    MODIFY_REG32(DMAx->INTEN, DMA_CHANNEL_INTEN_FINISH_INTERRUPT_ENABLE_Msk, DMA_CHANNEL_INTEN_FINISH_INTERRUPT_ENABLE_Pos, state);
}

/*!
* @brief Enable DMA Half Finish interrupt
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] state: enable/disable DMA Half Finish interrupt
*                  - ENABLE
*                  - DISABLE
* @return none
*/
__STATIC_INLINE void DMA_SetHalfFinishInterrupt(DMA_ChannelType *DMAx, ACTION_Type state)
{
    MODIFY_REG32(DMAx->INTEN, DMA_CHANNEL_INTEN_HALF_FINISH_INTERRUPT_ENABLE_Msk, DMA_CHANNEL_INTEN_HALF_FINISH_INTERRUPT_ENABLE_Pos, state);
}

/*!
* @brief Enable DMA Transfer Error interrupt
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] state: enable/disable DMA Transfer Error interrupt
*                  - ENABLE
*                  - DISABLE
* @return none
*/
__STATIC_INLINE void DMA_SetTransferErrorInterrupt(DMA_ChannelType *DMAx, ACTION_Type state)
{
    MODIFY_REG32(DMAx->INTEN, DMA_CHANNEL_INTEN_TRANS_ERROR_INTERRUPT_ENABLE_Msk, DMA_CHANNEL_INTEN_TRANS_ERROR_INTERRUPT_ENABLE_Pos, state);
}

/*!
* @brief DMA channel warm reset
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return none
*/
__STATIC_INLINE void DMA_ChannelWarmRst(DMA_ChannelType *DMAx)
{
    DMAx->RST |= DMA_CHANNEL_RST_WARM_RST_Msk;
}

/*!
* @brief DMA channel hard reset
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @return none
*/
__STATIC_INLINE void DMA_ChannelHardRst(DMA_ChannelType *DMAx)
{
    DMAx->RST |= DMA_CHANNEL_RST_HARD_RST_Msk;
    DMAx->RST &= ~DMA_CHANNEL_RST_HARD_RST_Msk;
}

/*!
* @brief DMA channel transfer pause/resume
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] state: enable/disable DMA paulse
*                  - ENABLE: DMA channel transfer pause
*                  - DISABLE: DMA channel transfer resume
* @return none
*/
__STATIC_INLINE void DMA_SetChannelPause(DMA_ChannelType *DMAx, ACTION_Type state)
{
    MODIFY_REG32(DMAx->STOP, DMA_CHANNEL_STOP_STOP_Msk, DMA_CHANNEL_STOP_STOP_Pos, state);
}

/*!
* @brief Set DMA channel transfer between MEM and MEM or Peripheral
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] state: Enable/Disable DMA Memory 2 Memory.
*                   -ENABLE: transfer between memory and memory
*                   -DISABLE: transfer between memory and periph
* @return none
*/
__STATIC_INLINE void DMA_ChannelM2M(DMA_ChannelType *DMAx, ACTION_Type state)
{
    MODIFY_REG32(DMAx->CONFIG, DMA_CHANNEL_CONFIG_MEM2MEM_Msk,  DMA_CHANNEL_CONFIG_MEM2MEM_Pos, state);
}

/*!
* @brief Set DMA channel priority
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] channelPriority:DMA Priority
*                   -DMA_PRIORITY_LOW
*                   -DMA_PRIORITY_MEDIUM
*                   -DMA_PRIORITY_HIGH
*                   -DMA_PRIORITY_VERY_HIGH
* @return none
*/
__STATIC_INLINE void DMA_ChannelPriority(DMA_ChannelType *DMAx, DMA_PriorityType channelPriority)
{
    MODIFY_REG32(DMAx->CONFIG, DMA_CHANNEL_CONFIG_CHAN_PRIORITY_Msk, DMA_CHANNEL_CONFIG_CHAN_PRIORITY_Pos, channelPriority);
}

/*!
* @brief Set DMA channel Memory size
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] memSize:DMA Memory size
*                   -DMA_MEM_SIZE_8BIT
*                   -DMA_MEM_SIZE_16BIT
*                   -DMA_MEM_SIZE_32BIT
* @return none
*/
__STATIC_INLINE void DMA_ChannelMemSize(DMA_ChannelType *DMAx, DMA_MemSizeType memSize)
{
    MODIFY_REG32(DMAx->CONFIG, DMA_CHANNEL_CONFIG_MEM_SIZE_Msk, DMA_CHANNEL_CONFIG_MEM_SIZE_Pos, memSize);
}

/*!
* @brief Set DMA channel Peripheral size
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] periphSize:DMA Periph size
*                   -DMA_PERIPH_SIZE_8BIT
*                   -DMA_PERIPH_SIZE_16BIT
*                   -DMA_PERIPH_SIZE_32BIT
* @return none
*/
__STATIC_INLINE void DMA_ChannelPeriphSize(DMA_ChannelType *DMAx, DMA_PeriphSizeType periphSize)
{
    MODIFY_REG32(DMAx->CONFIG, DMA_CHANNEL_CONFIG_PERIPH_SIZE_Msk,  DMA_CHANNEL_CONFIG_PERIPH_SIZE_Pos, periphSize);
}

/*!
* @brief Enable/Disable DMA Channel Memory increment
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] state: enable/disable DMA Memory increment
*                  - ENABLE: memory address increment
*                  - DISABLE: memory address fixed
* @return none
*/
__STATIC_INLINE void DMA_ChannelMemIncrement(DMA_ChannelType *DMAx, ACTION_Type state)
{
    MODIFY_REG32(DMAx->CONFIG, DMA_CHANNEL_CONFIG_MEM_INCREMENT_Msk, DMA_CHANNEL_CONFIG_MEM_INCREMENT_Pos, state);
}

/*!
* @brief Enable/Disable DMA Channel Peripheral increment
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] state: enable/disable DMA Periph increment
*                  - ENABLE: periph address increment
*                  - DISABLE: periph address fixed
* @return    none
*/
__STATIC_INLINE void DMA_ChannelPeriphIncrement(DMA_ChannelType *DMAx, ACTION_Type state)
{
    MODIFY_REG32(DMAx->CONFIG, DMA_CHANNEL_CONFIG_PERIPH_INCREMENT_Msk,  DMA_CHANNEL_CONFIG_PERIPH_INCREMENT_Pos, state);
}

/*!
* @brief Enable/Disable DMA channel circular
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] state: enable/disable DMA channel circular
*                  - ENABLE: enable circular mode
*                  - DISABLE: disable circular mode
* @return none
*/
__STATIC_INLINE void DMA_ChannelCircular(DMA_ChannelType *DMAx, ACTION_Type state)
{
    MODIFY_REG32(DMAx->CONFIG, DMA_CHANNEL_CONFIG_CHAN_CIRCULAR_Msk,  DMA_CHANNEL_CONFIG_CHAN_CIRCULAR_Pos, state);
}

/*!
* @brief Set DMA channel direction
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] channelDir:DMA channel direction,value can be
*                   -DMA_READ_FROM_PERIPH
*                   -DMA_READ_FROM_MEM
* @return none
*/
__STATIC_INLINE void DMA_ChannelDir(DMA_ChannelType *DMAx, DMA_DirType channelDir)
{
    MODIFY_REG32(DMAx->CONFIG, DMA_CHANNEL_CONFIG_CHAN_DIR_Msk,  DMA_CHANNEL_CONFIG_CHAN_DIR_Pos, channelDir);
}

/*!
* @brief Set DMA channel memory byte mode
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] memByteMode:DMA channel mem byte mode,indicate memory word split
*            transfer number,value can be
*                   -DMA_MEM_BYTE_MODE_1TIME: DMA memory word is divided to transfer in 1 time, 32 bits each time
*                   -DMA_MEM_BYTE_MODE_2TIME: DMA memory word is divided to transfer in 2 times, 16 bits each time
*                   -DMA_MEM_BYTE_MODE_4TIME: DMA memory word is divided to transfer in 4 times, 8 bits each time
* @return none
*/
__STATIC_INLINE void DMA_ChannelMemByteMode(DMA_ChannelType *DMAx, DMA_MemByteModeType memByteMode)
{
    MODIFY_REG32(DMAx->CONFIG, DMA_CHANNEL_CONFIG_MEM_BYTE_MODE_Msk,  DMA_CHANNEL_CONFIG_MEM_BYTE_MODE_Pos, memByteMode);
}

/*!
* @brief  Set DMA channel Periph Type
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] priphID:DMA channel periph select
*                   -DMA_PEPIRH_UART0_TX
*                   -DMA_PEPIRH_UART0_RX
*                   -DMA_PEPIRH_UART1_TX
*                   -DMA_PEPIRH_UART1_RX
*                   -DMA_PEPIRH_UART2_TX
*                   -DMA_PEPIRH_UART2_RX
*                   -DMA_PEPIRH_SPI0_TX
*                   -DMA_PEPIRH_SPI0_RX
*                   -DMA_PEPIRH_SPI1_TX
*                   -DMA_PEPIRH_SPI1_RX
*                   -DMA_PEPIRH_I2C0_TX
*                   -DMA_PEPIRH_I2C0_RX
*                   -DMA_PEPIRH_I2C1_TX
*                   -DMA_PEPIRH_I2C1_RX
*                   -DMA_PEPIRH_ADC0
* @return none
*/
__STATIC_INLINE void DMA_ChannelPeriphSel(DMA_ChannelType *DMAx, DMA_PeriphType priphID)
{
    MODIFY_REG32(DMAx->CONFIG, DMA_CHANNEL_CONFIG_PERIPH_SEL_Msk,  DMA_CHANNEL_CONFIG_PERIPH_SEL_Pos, priphID);
}

/*!
* @brief Set DMA channel transfer length
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] channelLength:DMA channel transfer length(0~32767, bit 15 should be 0)
* @return none
*/
__STATIC_INLINE void DMA_ChannelSetLength(DMA_ChannelType *DMAx, uint16_t channelLength)
{
    MODIFY_REG32(DMAx->CHAN_LENGTH, DMA_CHANNEL_CHAN_LENGTH_CHAN_LENGTH_Msk,  DMA_CHANNEL_CHAN_LENGTH_CHAN_LENGTH_Pos, channelLength);
}

/*!
* @brief Set DMA memory start address
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] memStartAddr:DMA memory start address, 32bit
* @return none
*/
__STATIC_INLINE void DMA_ChannelMemStartAddr(DMA_ChannelType *DMAx, uint32_t memStartAddr)
{
    DMAx->MEM_START_ADDR = memStartAddr;
}

/*!
* @brief Set DMA memory end address
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] memEndAddr:DMA memory end address, 32bit
* @return none
*/
__STATIC_INLINE void DMA_ChannelMemEndAddr(DMA_ChannelType *DMAx, uint32_t memEndAddr)
{
    DMAx->MEM_END_ADDR = memEndAddr;
}

/*!
* @brief Set DMA peripheral address
*
* @param[in] DMAx:DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] periphAddr:DMA peripheral address, 32bit
* @return none
*/
__STATIC_INLINE void DMA_ChannelPeriphAddr(DMA_ChannelType *DMAx, uint32_t periphAddr)
{
    DMAx->PERIPH_ADDR = periphAddr;
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _AC780X_DMA_REG_H */

/* =============================================  EOF  ============================================== */
