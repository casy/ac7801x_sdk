/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef  _AC780X_ACMP_H
#define  _AC780X_ACMP_H
/*!
* @file ac780x_acmp.h
*
* @brief This file provides analog comparator module integration functions interfaces.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x.h"

/* ============================================  Define  ============================================ */
/*!
* @brief ACMP instance index macro.
*/
#define ACMP_INDEX(ACMPx)    ((uint8_t)(((uint32_t)(ACMPx) - ACMP0_BASE)))

/* ===========================================  Typedef  ============================================ */
/*!
* @brief ACMP channel type enumeration.
*/
typedef enum
{
    ACMP_EXTERNAL_CH0 = 0,  /*!< External channel 0 */
    ACMP_EXTERNAL_CH1,      /*!< External channel 1 */
    ACMP_EXTERNAL_CH2,      /*!< External channel 2 */
    ACMP_EXTERNAL_CH3,      /*!< External channel 3 */
    ACMP_EXTERNAL_CH4,      /*!< External channel 4 */
    ACMP_EXTERNAL_CH5,      /*!< External channel 5 */
    ACMP_EXTERNAL_CH6,      /*!< External channel 6 */
    ACMP_DAC_OUTPUT_CH7,    /*!< DAC output channel 7 */
    ACMP_CHANNEL_MAX,       /*!< Invalid channel */
} ACMP_ChannelType;

/*!
* @brief ACMP sensitivity edge modes of enumeration.
*/
typedef enum
{
    ACMP_EDGE_TRIGGER_FALLING = 0,          /*!< Trigger interrupt on falling edge */
    ACMP_EDGE_TRIGGER_RISING = 1,           /*!< Trigger interrupt on rising edge */
    ACMP_EDGE_TRIGGER_FALLING_RISING = 3,   /*!< Trigger interrupt on falling/rising edge */
    ACMP_EDGE_TYPE_MAX                      /*!< Invalid trigger edge type  */
} ACMP_EdgeTrigType;

/*!
* @brief ACMP hysterisis voltage enumeration.
*/
typedef enum
{
    ACMP_HYSTERISIS_20MV = 0,   /*!< 20mv hysterisis voltage */
    ACMP_HYSTERISIS_40MV,       /*!< 40mv hysterisis voltage */
} ACMP_HysterisisVolType;

/*!
* @brief ACMP low-pass filter enumeration.
*/
typedef enum
{
    ACMP_LPF_200KHZ = 0,        /*!< 200KHz filter frequence */
    ACMP_LPF_500KHZ = 1,        /*!< 500KHz filter frequence */
    ACMP_LPF_750KHZ = 2,        /*!< 750KHz filter frequence */
    ACMP_LPF_1000KHZ = 3,       /*!< 1000KHz filter frequence */
    ACMP_LPF_MAX,               /*!< Invalid filter frequence */
} ACMP_LowPassFilterType;

/*!
* @brief ACMP DAC Reference voltage source enumeration.
*/
typedef enum
{
    ACMP_DAC_BANDGAP = 0,   /*!< Bandgap as reference source */
    ACMP_DAC_VDD,           /*!< Vdd as reference source */
} ACMP_DACReferenceType;

/*!
* @brief ACMP polling mode enumeration.
*/
typedef enum
{
    ACMP_NONE_POLLING = 0,      /*!< It's not polling mode */
    ACMP_POSITIVE_POLLING,      /*!< Positive polling */
    ACMP_NEGATIVE_POLLING,      /*!< Negative polling */
} ACMP_PollingModeType;

/*!
* @brief ACMP polling mode frequency divider enumeration.
*/
typedef enum
{
    ACMP_CLK_DIVIDE_256 = 0,        /*!< Clock/256 */
    ACMP_CLK_DIVIDE_100,            /*!< Clock/100 */
    ACMP_CLK_DIVIDE_70,             /*!< Clock/70 */
    ACMP_CLK_DIVIDE_50,             /*!< Clock/50 */
    ACMP_CLK_DIVIDE_MAX,            /*!< Invalid divid ratio */
} ACMP_PollingModeClkDivType;

/*!
* @brief ACMP power mode enumeration.
*/
typedef enum
{
    ACMP_LOW_POWER = 0,     /*!< Low power mode */
    ACMP_NORMAL_POWER ,     /*!< Normal power mode */
} ACMP_PowerModeType;

/*!
* @brief ACMP configuration structure.
*/
typedef struct
{
    /* General parameters */
    ACMP_ChannelType positivePin;           /*!< Positive input pin */
    ACMP_ChannelType negativePin;           /*!< Negative input pin */
    ACMP_HysterisisVolType hysterisisVol;   /*!< Hysterisis voltage */
    ACMP_EdgeTrigType edgeType;             /*!< Sensitivity modes of the interrupt trigger */
    ACTION_Type acmpOutEn;                  /*!< compare result output pin */
    ACTION_Type interruptEn;                /*!< Enable/disable interrupt */
    DeviceCallback_Type callBack;           /*!< Callback pointer */
    ACTION_Type acmpEn;                     /*!< Enable/disable ACMP module */
    /* Dac parameters */
    ACMP_DACReferenceType dacRef;           /*!< DAC reference select */
    uint8_t dacValue;                       /*!< 6 bit DAC value */
    ACTION_Type dacEn;                      /*!< Enable/disable DAC */
    /* Polling mode parameters */
    ACMP_PollingModeType pollingModeType;   /*!< Polling mode type*/
    uint8_t pollingSequence;                /*!< Polling sequence */
    ACMP_PollingModeClkDivType pollingDiv;  /*!< Polling mode frequency divider */
    ACMP_ChannelType hallAOutputCh;         /*!< HallA output channel */
    ACMP_ChannelType hallBOutputCh;         /*!< HallB output channel */
    ACMP_ChannelType hallCOutputCh;         /*!< HallC output channel */
    ACTION_Type hallOutEn;                  /*!< Enable/disable Hall Output */
} ACMP_ConfigType;

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */
/*!
* @brief ACMP initialize.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] config: pointer to configuration structure
* @return none
*/
void ACMP_Init(ACMP_Type *ACMPx, const ACMP_ConfigType *config);

/*!
* @brief ACMP De-initialize.
*
* @param[in] none
* @return none
*/
void ACMP_DeInit(ACMP_Type *ACMPx);

/*!
* @brief Set acmp callback function.
*
* @param[in] ACMPx: acmp module
                - ACMP0
* @param[in] func: callback function
* @return none
*/
void ACMP_SetCallback(ACMP_Type *ACMPx, const DeviceCallback_Type func);

#ifdef __cplusplus
}
#endif

#endif /* _AC780X_ACMP_H */

/* =============================================  EOF  ============================================== */
