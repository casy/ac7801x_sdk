/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780x_CTU_H
#define _AC780x_CTU_H
/*!
* @file ac780x_ctu.h
*
* @brief This file provides module-to-module interconnections module integration functions interfaces.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */
/*!
* @brief CTU clock prescaler enumeration.
*/
typedef enum
{
    CTU_CLK_PRESCALER_1 = 0,    /*!< Clock presalcer divide 1 */
    CTU_CLK_PRESCALER_2,        /*!< Clock presalcer divide 2 */
    CTU_CLK_PRESCALER_4,        /*!< Clock presalcer divide 4 */
    CTU_CLK_PRESCALER_8,        /*!< Clock presalcer divide 8 */
    CTU_CLK_PRESCALER_16,       /*!< Clock presalcer divide 16 */
    CTU_CLK_PRESCALER_32,       /*!< Clock presalcer divide 32 */
    CTU_CLK_PRESCALER_64,       /*!< Clock presalcer divide 64 */
    CTU_CLK_PRESCALER_128,      /*!< Clock presalcer divide 128 */
    CTU_CLK_PRESCALER_MAX       /*!< Invalid clock presalcer */
} CTU_ClkPrescalerType;

/*!
* @brief CTU adc hardware trigger source enumeration.
*/
typedef enum
{
    CTU_TRIGGER_ADC_RTC_OVERFLOW = 0,   /*!< RTC overflow as the ADC hardware trigger */
    CTU_TRIGGER_ADC_PWM0_INIT,          /*!< PWM0 initialization trigger as the ADC hardware trigger */
    CTU_TRIGGER_ADC_PWM0_MATCH,         /*!< PWM0 match trigger as the ADC hardware trigger */
    CTU_TRIGGER_ADC_PWM1_INIT,          /*!< PWM1 initialization trigger as the ADC hardware trigger */
    CTU_TRIGGER_ADC_PWM1_MATCH,         /*!< PWM1 match trigger as the ADC hardware trigger */
    CTU_TRIGGER_ADC_TIMER_CH0_OVERFLOW, /*!< Timer channel 0 overflow as the ADC hardware trigger */
    CTU_TRIGGER_ADC_TIMER_CH1_OVERFLOW, /*!< Timer channel 1 overflow as the ADC hardware trigger */
    CTU_TRIGGER_ADC_ACMP0_OUTPUT,       /*!< Acmp 0 output as the ADC hardware trigger */
} CTU_AdcTriggerSourceType;

/*!
* @brief CTU pwdt int3 input source enumeration.
*/
typedef enum
{
    CTU_PWDT_IN3_SOURCE_UART0_RX = 0,    /*!< PWDT in3 input for uart0 rx */
    CTU_PWDT_IN3_SOURCE_UART1_RX,        /*!< PWDT in3 input for uart1 rx */
    CTU_PWDT_IN3_SOURCE_UART2_RX,        /*!< PWDT in3 input for uart2 rx */
    CTU_PWDT_IN3_SOURCE_ACMP0_OUT,       /*!< PWDT in3 input for acmp0 output */
} CTU_PwdtIn3InputSourceType;

/*!
* @brief CTU configuration structure.
*/
typedef struct
{
    ACTION_Type uart0RxFilterEn;                        /*!< UART0 RxD Filter */
    ACTION_Type rtcCaptureEn;                           /*!< Real-Time Counter Capture by PWM1 Channel 1 */
    ACTION_Type acmpCaptureEn;                          /*!< Analog Comparator output to PWM1 Channel 0 Input Capture */
    ACTION_Type uart0RxCaptureEn;                       /*!< UART0 RX to PWM0 Channel 1 capture */
    ACTION_Type uartTxModulateEn;                       /*!< Use PWM0 Channel0 to Modulate UART0 TX signal */
    CTU_ClkPrescalerType clkPsc;                        /*!< Clock prescaler */
    CTU_AdcTriggerSourceType adcRegularTriggerSource;   /*!< ADC Hardware Trigger Source for Regular Group */
    uint8_t delay0Time;                                 /*!< Trigger Delay Cnt for Regular Group */
    CTU_AdcTriggerSourceType adcInjectTriggerSource;    /*!< ADC Hardware Trigger Source for Injected Group */
    uint8_t delay1Time;                                 /*!< Trigger Delay Cnt for Injected Group */
    CTU_PwdtIn3InputSourceType pwdt0In3Source;          /*!< Pwdt0 in3 input source */
    CTU_PwdtIn3InputSourceType pwdt1In3Source;          /*!< Pwdt1 in3 input source */
} CTU_ConfigType;

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */
/*!
* @brief CTU initialize
*
* @param[in] config: pointer to configuration structure
* @return none
*/
void CTU_Init(const CTU_ConfigType *config);

/*!
* @brief CTU De-initialize
*
* @param[in] none
* @return none
*/
void CTU_DeInit(void);

#ifdef __cplusplus
}
#endif

#endif /* _AC780x_CTU_H */

/* =============================================  EOF  ============================================== */
