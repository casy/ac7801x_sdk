/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_WDG_H
#define _AC780X_WDG_H

/*!
* @file ac780x_wdg.h
*
* @brief This file provides wdg integration functions interface.
*
*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ============================================  Includes  ============================================ */
#include "ac780x.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */
/*!
* @brief WDG clock source enumeration
*/
typedef enum
{
    WDG_CLOCK_APB = 0x00U,          /*!< WDG APB clock source */
    WDG_CLOCK_LSI,                  /*!< WDG internal 32k clock source */
    WDG_CLOCK_HSI,                  /*!< WDG internal 8m clock source */
    WDG_CLOCK_HSE                   /*!< WDG external XOSC clock source */
} WDG_ClockSourceType;

/*!
* @brief WDG configuration structure
*
* This structure holds the configuration settings for the WDG
*/
typedef struct
{
    uint32_t timeoutValue;             /*!< WDG timeout value */
    uint32_t windowValue;              /*!< WDG windows value */
    WDG_ClockSourceType clockSource;    /*!< WDG clock source */
    ACTION_Type updateEn;               /*!< Enable/disable WDG update */
    ACTION_Type WDGEn;                  /*!< Enable/disable WDG module */
    ACTION_Type prescalerEn;            /*!< Enable/disable WDG fixed 256 prescaler */
    ACTION_Type windowEn;               /*!< Enable/disable WDG window mode */
    ACTION_Type interruptEn;            /*!< Enable/disable WDG interrupt */
    DeviceCallback_Type callBack;       /*!< WDG Callback function */
} WDG_ConfigType;

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */
/*!
* @brief Initialize WDG module
*
* @param[in] config: WDG configuration pointer
* @return none
*/
void WDG_Init(const WDG_ConfigType *config);

/*!
* @brief WDG De-initialize
*
* @param[in] none
* @return none
*/
void WDG_DeInit(void);

/*!
* @brief set callback func
*
* @param[in] callback: callback func
* @return none
*/
void WDG_SetCallback(DeviceCallback_Type callback);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _AC780X_WDG_H */

/* =============================================  EOF  ============================================== */
