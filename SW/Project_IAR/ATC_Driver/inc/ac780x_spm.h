/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_SPM_H
#define _AC780X_SPM_H
/*!
* @file ac780x_spm.h
*
* @brief This file provides system power manage module integration functions interfaces.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x.h"

/* ============================================  Define  ============================================ */
#define SPM_TIMEOUT_VALUE    10000UL    /* for xosc and pll timeout wait, 8.32ms with system clock 48M */

/* ===========================================  Typedef  ============================================ */

/*!
* @brief config the power mode enumeration.
*/
typedef enum
{
    LOW_POWER_MODE_STOP      = 1,
    LOW_POWER_MODE_STANDBY   = 3
}SPM_PowerModeType;

#define IS_POWER_MODE_PARA(MODE) (((MODE) == LOW_POWER_MODE_STOP) || \
                                  ((MODE) == LOW_POWER_MODE_STANDBY))

/*!
* @brief periph sleep ack index in SPM enumeration.
*/
typedef enum
{
    SLEEP_ACK_ACMP0 = 0,
    SLEEP_ACK_RESERVE1,
    SLEEP_ACK_I2C0,
    SLEEP_ACK_I2C1,
    SLEEP_ACK_SPI0,
    SLEEP_ACK_SPI1,
    SLEEP_ACK_RESERVE6,
    SLEEP_ACK_CAN0,
    SLEEP_ACK_RESERVE8,
    SLEEP_ACK_UART0,
    SLEEP_ACK_UART1,
    SLEEP_ACK_UART2,
    SLEEP_ACK_RESERVE12,
    SLEEP_ACK_RESERVE13,
    SLEEP_ACK_RESERVE14,
    SLEEP_ACK_DMA0,
    SLEEP_ACK_ADC0,
    SLEEP_ACK_RESERVE17,
    SLEEP_ACK_EFLASH,
    SLEEP_ACK_NUM
}SPM_SleepACKType;

#define IS_SLEEP_ACK_PARA(MODULE) (((MODULE) == SLEEP_ACK_ACMP0)  || \
                                    ((MODULE) == SLEEP_ACK_I2C0)   || \
                                    ((MODULE) == SLEEP_ACK_I2C1)   || \
                                    ((MODULE) == SLEEP_ACK_SPI0)   || \
                                    ((MODULE) == SLEEP_ACK_SPI1)   || \
                                    ((MODULE) == SLEEP_ACK_CAN0)   || \
                                    ((MODULE) == SLEEP_ACK_UART0)  || \
                                    ((MODULE) == SLEEP_ACK_UART1)  || \
                                    ((MODULE) == SLEEP_ACK_UART2)  || \
                                    ((MODULE) == SLEEP_ACK_DMA0)   || \
                                    ((MODULE) == SLEEP_ACK_ADC0)   || \
                                    ((MODULE) == SLEEP_ACK_EFLASH) )

/*!
* @brief the module's index in SPM enumeration.
*/
typedef enum
{
    SPM_MODULE_ACMP0 = 0,
    SPM_MODULE_RESERVE1,
    SPM_MODULE_I2C0,
    SPM_MODULE_I2C1,
    SPM_MODULE_SPI0,
    SPM_MODULE_SPI1,
    SPM_MODULE_RESERVE6,
    SPM_MODULE_CAN0,
    SPM_MODULE_RESERVE8,
    SPM_MODULE_UART0,
    SPM_MODULE_UART1,
    SPM_MODULE_UART2,
    SPM_MODULE_RESERVE12,
    SPM_MODULE_RESERVE13,
    SPM_MODULE_RESERVE14,
    SPM_MODULE_RTC,
    SPM_MODULE_ADC0,
    SPM_MODULE_GPIO,
    SPM_MODULE_NMI,
    SPM_MODULE_PVD,
    SPM_MODULE_SPM_OVER_COUNT,
    SPM_MODULE_NUM
}SPM_ModuleType;

#define IS_SPM_MODULE_PARA(MODULE) (((MODULE) == SPM_MODULE_ACMP0)  || \
                                    ((MODULE) == SPM_MODULE_I2C0)  || \
                                    ((MODULE) == SPM_MODULE_I2C1)  || \
                                    ((MODULE) == SPM_MODULE_SPI0)  || \
                                    ((MODULE) == SPM_MODULE_SPI1)  || \
                                    ((MODULE) == SPM_MODULE_CAN0)   || \
                                    ((MODULE) == SPM_MODULE_UART0) || \
                                    ((MODULE) == SPM_MODULE_UART1) || \
                                    ((MODULE) == SPM_MODULE_UART2) || \
                                    ((MODULE) == SPM_MODULE_RTC)   || \
                                    ((MODULE) == SPM_MODULE_ADC0)   || \
                                    ((MODULE) == SPM_MODULE_GPIO)  || \
                                    ((MODULE) == SPM_MODULE_NMI)   || \
                                    ((MODULE) == SPM_MODULE_PVD)   || \
                                    ((MODULE) == SPM_MODULE_SPM_OVER_COUNT))

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */

/*
1) The clocks of all modules are already after wake up and can operate normally.
2) When the wake-up source of  UART, CAN, I2C, SPI, LIN generate wake-up signal, enter the IRQ of SPM firstly,then return to the next command of _WFI
3) When the wakeup source of GPIO,RTC��ADC,LVD,ACMP��T-Sensor generates wakeup signals��enter the IRQ of the module, and then return to the next command of _WFI.��Each module can ignore the interrupt of SPM��
*/
/*!
* @brief enable pll detector function
*
* @param[in] enable: 1: enable, 0 disable
* @return none
*/
void EnablePLLDetectorFunction(ACTION_Type enable);
/*!
* @brief enable fast boot mode
*
* @param[in] enable: 1: enable, 0 disable
* @return none
*/
void SPM_EnableFastBoot(ACTION_Type enable);
/*!
* @brief enable LVD
*
* @param[in] enable: 1: enable, 0 disable
* @return none
*/
void SPM_EnableLVD(ACTION_Type enable);
/*!
* @brief enable PVD
*
* @param[in] enable: 1: enable, 0 disable
* @return none
*/
void SPM_EnablePVD(ACTION_Type enable);
/*!
* @brief set SPM IRQHandler callback function
*
* @param[in] eventFunc: the pointer of the call back function, which will be called in SPM_IRQHandler
* @return none
*/
void SPM_SetCallback(DeviceCallback_Type eventFunc);
/*!
* @brief set PVD Handler callback function
*
* @param[in] eventFunc: the pointer of the call back function, which will be called in PVD_IRQHandler
* @return none
*/
void PVD_SetCallback(const DeviceCallback_Type eventFunc);
/*!
* @brief set the low power mode
*
*
* @param[in] mode: stop or sleep mode, value can be
*                 - LOW_POWER_MODE_STOP
*                 - LOW_POWER_MODE_STANDBY
* @return none
*/
void SPM_SetLowPowerMode(SPM_PowerModeType mode);
/*!
* @brief enable XOSC and wait the XOSC status is OK
*
* @param[in] enable: 0:disable, 1: enable
* @return SUCCESS:xosc set success
*           ERROR:xosc set fail
*/
ERROR_Type SPM_EnableXOSC(ACTION_Type enable);
/*!
* @brief enable XOSC bypass mode
*
* @param[in] enable: 1: enable, 0 disable
* @return none
*/
void SPM_EnableXOSCBypassMode(ACTION_Type enable);
/*!
* @brief set XOSC ok bypass
*
* @param[in] enable: 1: enable, 0 disable
* @return none
*/
void SPM_XOSCOKBypassSet(ACTION_Type enable);
/*!
* @brief enable PLL and wait for the status is OK
*
* @param[in] enable: 0:disable, 1: enable
* @return SUCCESS:pll set success
*           ERROR:pll set fail
*/
ERROR_Type SPM_EnablePLL(ACTION_Type enable);
/*!
* @brief enable CAN0 lowpass filter
*
* @param[in] canIndex: value can be 0:CAN0
* @param[in]   enable: 0:disable, 1: enable
* @return none
*/
void SPM_EnableCANLowpassFilter(uint8_t canIndex, ACTION_Type enable);
/*!
* @brief Config Module Wakeup Function
*
* @param[in] module: module flag in SPM, value can be
*                   - SPM_MODULE_ACMP0
*                   - SPM_MODULE_I2C0
*                   - SPM_MODULE_I2C1
*                   - SPM_MODULE_SPI0
*                   - SPM_MODULE_SPI1
*                   - SPM_MODULE_CAN0
*                   - SPM_MODULE_UART0
*                   - SPM_MODULE_UART1
*                   - SPM_MODULE_UART2
*                   - SPM_MODULE_RTC
*                   - SPM_MODULE_ADC0
*                   - SPM_MODULE_GPIO
*                   - SPM_MODULE_NMI
*                   - SPM_MODULE_PVD
*                   - SPM_MODULE_SPM_OVER_COUNT
* @param[in]  enable: 0:disable, 1: enable
* @return 0: success,  -1: error
*/
void SPM_EnableModuleWakeup(SPM_ModuleType module, ACTION_Type enable);
/*!
* @brief Enable which module the SPM wait the ack when do stop
*
* @param[in] module: module flag in SPM, value can be
*                   - SLEEP_ACK_ACMP0
*                   - SLEEP_ACK_I2C0
*                   - SLEEP_ACK_I2C1
*                   - SLEEP_ACK_SPI0
*                   - SLEEP_ACK_SPI1
*                   - SLEEP_ACK_CAN0
*                   - SLEEP_ACK_UART0
*                   - SLEEP_ACK_UART1
*                   - SLEEP_ACK_UART2
*                   - SLEEP_ACK_DMA0
*                   - SLEEP_ACK_ADC0
*                   - SLEEP_ACK_EFLASH
* @param[in]  enable: 0:disable, 1: enable
* @return 0: success,  -1: error
*/
void SPM_EnableModuleSleepACK(SPM_SleepACKType module, ACTION_Type enable);
/*!
* @brief get sleep ack status
*
* @param[in]:none
* @return sleep ack status value
*/
uint32_t SPM_GetModuleSleepACKStatus(void);
/*!
* @brief get the wakeup source flag
*
* @param[in]:none
* @return the wakeup source flag
*/
uint32_t SPM_GetModuleWakeupSourceFlag(void);
/*!
* @brief clear the wakeup source flag
*
* @param[in]:none
* @return none
*/
void SPM_ClearWakeupSourceFlag(void);

#ifdef __cplusplus
}
#endif

#endif /* _AC780X_SPM_H */

/* =============================================  EOF  ============================================== */
