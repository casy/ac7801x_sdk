/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_I2C_REG_H
#define _AC780X_I2C_REG_H

/*!
* @file ac780x_i2c_reg.h
*
* @brief I2C access register inline function definition.
*
*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ===========================================  Includes  =========================================== */
#include "ac780x_i2c.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief Set i2c module enable
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetModuleEnable(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL0, I2C_CTRL0_IICEN_Msk, I2C_CTRL0_IICEN_Pos, state);
}

/*!
* @brief Set i2c master/slave
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] mode: i2c mode
                - I2C_SLAVE
                - I2C_MASTER
* @return none
*/
__STATIC_INLINE void I2C_SetMSTR(I2C_Type *I2Cx, I2C_ModeType mode)
{
    MODIFY_REG32(I2Cx->CTRL0, I2C_CTRL0_MSTR_Msk, I2C_CTRL0_MSTR_Pos, mode);
}

/*!
* @brief Set i2c interrupt
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetInterrupt(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL0, I2C_CTRL0_IICIE_Msk, I2C_CTRL0_IICIE_Pos, state);
}

/*!
* @brief Get i2c interrupt enable status
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: the i2c interrupt enable status
*/
__STATIC_INLINE uint32_t I2C_IsInterruptEnable(I2C_Type *I2Cx)
{
    return (I2Cx->CTRL0 & I2C_CTRL0_IICIE_Msk);
}

/*!
* @brief Set i2c SCL sample step cnt
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] sampleCnt: sample cnt
             stepCnt: step cnt
                - sample_width = (sampleCnt + 1) * APB period
                - half_pulse_width = (stepCnt + 1) * sample_width
* @return none
*/
__STATIC_INLINE void I2C_SetSampleStep(I2C_Type *I2Cx, uint8_t sampleCnt, uint8_t stepCnt)
{
    MODIFY_REG32(I2Cx->SAMPLE_CNT, I2C_SAMPLE_CNT_SAMPLE_CNT_Msk, I2C_SAMPLE_CNT_SAMPLE_CNT_Pos, sampleCnt);
    MODIFY_REG32(I2Cx->STEP_CNT, I2C_STEP_CNT_STEP_CNT_Msk, I2C_STEP_CNT_STEP_CNT_Pos, stepCnt);
}

/*!
* @brief Set NACK interrupt
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetNackInterrupt(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL2, I2C_CTRL2_NACKIE_Msk, I2C_CTRL2_NACKIE_Pos, state);
}

/*!
* @brief Get NACK interrupt bit
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return NACK interrupt bit
*/
__STATIC_INLINE uint32_t I2C_IsNackIntEnable(I2C_Type *I2Cx)
{
    return (I2Cx->CTRL2 & I2C_CTRL2_NACKIE_Msk);
}

/*!
* @brief Set start stop interrupt
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetSSInterrupt(I2C_Type *I2Cx, ACTION_Type state)
{
    uint32_t tmp = I2Cx->DGLCFG & (~(I2C_DGLCFG_STARTF_Msk | I2C_DGLCFG_STOPF_Msk));

    tmp &= ~I2C_DGLCFG_SSIE_Msk;
    if (ENABLE == state)
    {
        tmp |= I2C_DGLCFG_SSIE_Msk;
    }
    I2Cx->DGLCFG = tmp;
}

/*!
* @brief Get the ss int enable
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: the ss int enable flag
*/
__STATIC_INLINE uint32_t I2C_IsSSIntEnable(I2C_Type *I2Cx)
{
    return (I2Cx->DGLCFG & I2C_DGLCFG_SSIE_Msk);
}

/*!
* @brief Set dma tx
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetDMATx(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL3, I2C_CTRL3_DMATXEN_Msk, I2C_CTRL3_DMATXEN_Pos, state);
}

/*!
* @brief Set dma rx
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetDMARx(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL3, I2C_CTRL3_DMARXEN_Msk, I2C_CTRL3_DMARXEN_Pos, state);
}

/*!
* @brief Set deglitch DGL cnt
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] DGLCnt: deglitch cnt
* @return none
*/
__STATIC_INLINE void I2C_SetDGLCnt(I2C_Type *I2Cx, uint8_t DGLCnt)
{
    uint32_t tmp = I2Cx->DGLCFG & (~(I2C_DGLCFG_STARTF_Msk | I2C_DGLCFG_STOPF_Msk));

    tmp &= ~I2C_DGLCFG_DGL_CNT_Msk;
    tmp |= (DGLCnt << I2C_DGLCFG_DGL_CNT_Pos) & I2C_DGLCFG_DGL_CNT_Msk;
    I2Cx->DGLCFG = tmp;
}

/*!
* @brief Set wakeup enable
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetWakeup(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL0, I2C_CTRL0_WUEN_Msk, I2C_CTRL0_WUEN_Pos, state);
}

/*!
* @brief Set sync enable
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetSYNC(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL1, I2C_CTRL1_SYNCEN_Msk, I2C_CTRL1_SYNCEN_Pos, state);
}

/*!
* @brief Set arbitration enable
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetARB(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL1, I2C_CTRL1_ARBEN_Msk, I2C_CTRL1_ARBEN_Pos, state);
}

/*!
* @brief Set stretch enable
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetStretch(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL1, I2C_CTRL1_STREN_Msk, I2C_CTRL1_STREN_Pos, state);
}

/*!
* @brief Get stretch enable bit
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return stretch enable bit
*/
__STATIC_INLINE uint32_t I2C_IsStretch(I2C_Type *I2Cx)
{
    return (I2Cx->CTRL1 & I2C_CTRL1_STREN_Msk);
}

/*!
* @brief Set slave tx buff empty interrupt
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetTxEInterrupt(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL2, I2C_CTRL2_TXEMIE_Msk, I2C_CTRL2_TXEMIE_Pos, state);
}

/*!
* @brief Set slave rx buff full interrupt
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetRxFInterrupt(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL2, I2C_CTRL2_RXFIE_Msk, I2C_CTRL2_RXFIE_Pos, state);
}

/*!
* @brief Set slave tx under flow interrupt
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetTxUFInterrupt(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL2, I2C_CTRL2_TXUFIE_Msk, I2C_CTRL2_TXUFIE_Pos, state);
}

/*!
* @brief Set slave rx over flow interrupt
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetRxOFInterrupt(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL2, I2C_CTRL2_RXOFIE_Msk, I2C_CTRL2_RXOFIE_Pos, state);
}

/*!
* @brief Get slave tx under flow bit
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return tx under flow bit
*/
__STATIC_INLINE uint32_t I2C_IsTxUF(I2C_Type *I2Cx)
{
    return (I2Cx->STATUS1 & I2C_STATUS1_TXUF_Msk);
}

/*!
* @brief Get slave rx over flow bit
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return rx over flow bit
*/
__STATIC_INLINE uint32_t I2C_IsRxOF(I2C_Type *I2Cx)
{
    return (I2Cx->STATUS1 & I2C_STATUS1_RXOF_Msk);
}

/*!
* @brief Set slave address
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] slaveAddr: slave 7~10bit address value
* @return none
*/
__STATIC_INLINE void I2C_SetSlaveAddr(I2C_Type *I2Cx, uint16_t slaveAddr)
{
    MODIFY_REG32(I2Cx->ADDR0, I2C_ADDR0_AD_Msk, I2C_ADDR0_AD_Pos, slaveAddr & 0x7F);
    MODIFY_REG32(I2Cx->ADDR1, I2C_ADDR1_AD_Msk, I2C_ADDR1_AD_Pos, slaveAddr >> 7U);
}

/*!
* @brief Set slave range address
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] rangeAddr: slave range address value
* @return none
*/
__STATIC_INLINE void I2C_SetSlaveRangeAddr(I2C_Type *I2Cx, uint8_t rangeAddr)
{
    MODIFY_REG32(I2Cx->ADDR1, I2C_ADDR1_RAD_Msk, I2C_ADDR1_RAD_Pos, rangeAddr);
}

/*!
* @brief Set slave address extention
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetADEXT(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL1, I2C_CTRL1_ADEXT_Msk, I2C_CTRL1_ADEXT_Pos, state);
}

/*!
* @brief Set slave range address
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetRAD(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->ADDR1, I2C_ADDR1_RMEN_Msk, I2C_ADDR1_RMEN_Pos, state);
}

/*!
* @brief Set slave monitor function
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetMNT(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL2, I2C_CTRL2_MNTEN_Msk, I2C_CTRL2_MNTEN_Pos, state);
}

/*!
* @brief Set slave general call
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void I2C_SetGCA(I2C_Type *I2Cx, ACTION_Type state)
{
    MODIFY_REG32(I2Cx->CTRL1, I2C_CTRL1_GCAEN_Msk, I2C_CTRL1_GCAEN_Pos, state);
}

/*!
* @brief Set transfer tx direction
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return none
*/
__STATIC_INLINE void I2C_TxEn(I2C_Type *I2Cx)
{
    I2Cx->CTRL0 |= I2C_CTRL0_TX_Msk;
}

/*!
* @brief Set transfer rx direction
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return none
*/
__STATIC_INLINE void I2C_RxEn(I2C_Type *I2Cx)
{
    I2Cx->CTRL0 &= ~I2C_CTRL0_TX_Msk;
}

/*!
* @brief Set transfer ack
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return none
*/
__STATIC_INLINE void I2C_SendAck(I2C_Type *I2Cx)
{
    I2Cx->CTRL0 &= ~I2C_CTRL0_TACK_Msk;
}

/*!
* @brief Set transfer nack
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return none
*/
__STATIC_INLINE void I2C_SendNack(I2C_Type *I2Cx)
{
    I2Cx->CTRL0 |= I2C_CTRL0_TACK_Msk;
}

/*!
* @brief Write data register
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] data: the data to write
* @return none
*/
__STATIC_INLINE void I2C_WriteDataReg(I2C_Type *I2Cx, uint8_t data)
{
    I2Cx->DATA = data;
}

/*!
* @brief Read data register
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: the data read out
*/
__STATIC_INLINE uint8_t I2C_ReadDataReg(I2C_Type *I2Cx)
{
    return (I2Cx->DATA);
}

/*!
* @brief Dump read data register
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: none
*/
__STATIC_INLINE void I2C_DumpReadDataReg(I2C_Type *I2Cx)
{
    uint32_t read = (I2Cx->DATA);
}

/*!
* @brief Enable transfer start
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return none
*/
__STATIC_INLINE void I2C_SendStart(I2C_Type *I2Cx)
{
    I2Cx->STARTSTOP |= I2C_STARTSTOP_START_Msk;
}

/*!
* @brief Enable transfer stop
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return none
*/
__STATIC_INLINE void I2C_SendStop(I2C_Type *I2Cx)
{
    I2Cx->STARTSTOP |= I2C_STARTSTOP_STOP_Msk;
}

/*!
* @brief Clear the start flag bit
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return none
*/
__STATIC_INLINE void I2C_ClearStartFlag(I2C_Type *I2Cx)
{
    uint32_t tmp = I2Cx->DGLCFG & (~I2C_DGLCFG_STOPF_Msk);

    tmp |= I2C_DGLCFG_STARTF_Msk;
    I2Cx->DGLCFG = tmp;
}


/*!
* @brief Clear the stop flag bit
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return none
*/
__STATIC_INLINE void I2C_ClearStopFlag(I2C_Type *I2Cx)
{
    uint32_t tmp = I2Cx->DGLCFG & (~I2C_DGLCFG_STARTF_Msk);

    tmp |= I2C_DGLCFG_STOPF_Msk;
    I2Cx->DGLCFG = tmp;
}

/*!
* @brief Get the slave extend address status
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: slave extend address enabling bit
*/
__STATIC_INLINE uint32_t I2C_IsADEXT(I2C_Type *I2Cx)
{
    return (I2Cx->CTRL1 & I2C_CTRL1_ADEXT_Msk);
}

/*!
* @brief Get the transfer direction
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: transfer direction flag
*/
__STATIC_INLINE uint32_t I2C_IsTx(I2C_Type *I2Cx)
{
    return (I2Cx->CTRL0 & I2C_CTRL0_TX_Msk);
}

/*!
* @brief Get the BND status
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: BND flag
*/
__STATIC_INLINE uint32_t I2C_IsBND(I2C_Type *I2Cx)
{
    return (I2Cx->STATUS0 & I2C_STATUS0_BND_Msk);
}

/*!
* @brief Get the NACK status
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: NACK flag
*/
__STATIC_INLINE uint32_t I2C_IsNack(I2C_Type *I2Cx)
{
    return (I2Cx->STATUS0 & I2C_STATUS0_RACK_Msk);
}


/*!
* @brief Get the BUS busy status
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: busy flag
*/
__STATIC_INLINE uint32_t I2C_IsBusy(I2C_Type *I2Cx)
{
    return (I2Cx->STATUS0 & I2C_STATUS0_BUSY_Msk);
}

/*!
* @brief Get the core ready status
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: ready flag
*/
__STATIC_INLINE uint32_t I2C_IsReady(I2C_Type *I2Cx)
{
    return (I2Cx->STATUS0 & I2C_STATUS0_READY_Msk);
}

/*!
* @brief Get the device role
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: wheather is master or not
*/
__STATIC_INLINE uint32_t I2C_IsMaster(I2C_Type *I2Cx)
{
    return (I2Cx->CTRL0 & I2C_CTRL0_MSTR_Msk);
}

/*!
* @brief Get the start flag
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: the start flag
*/
__STATIC_INLINE uint32_t I2C_IsStart(I2C_Type *I2Cx)
{
    return (I2Cx->DGLCFG & I2C_DGLCFG_STARTF_Msk);
}

/*!
* @brief Get the stop flag
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: the stop flag
*/
__STATIC_INLINE uint32_t I2C_IsStop(I2C_Type *I2Cx)
{
    return (I2Cx->DGLCFG & I2C_DGLCFG_STOPF_Msk);
}

/*!
* @brief Get status0 register
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: status0 register
*/
__STATIC_INLINE uint32_t I2C_GetStatus0(I2C_Type *I2Cx)
{
    return (I2Cx->STATUS0);
}

/*!
* @brief Get status1 register
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: status1 register
*/
__STATIC_INLINE uint32_t I2C_GetStatus1(I2C_Type *I2Cx)
{
    return (I2Cx->STATUS1);
}

/*!
* @brief Clear the status1 mask bit
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] clearMsk: the mask bit to clear
* @return none
*/
__STATIC_INLINE void I2C_ClearStatus0(I2C_Type *I2Cx, uint32_t clearMsk)
{
    I2Cx->STATUS0 = clearMsk;
}

/*!
* @brief Clear the status2 mask bit
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @param[in] clearMsk: the mask bit to clear
* @return none
*/
__STATIC_INLINE void I2C_ClearStatus1(I2C_Type *I2Cx, uint32_t clearMsk)
{
    I2Cx->STATUS1 = clearMsk;
}

/*!
* @brief Get whether the Tx is DMA or not
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: DMATX status
*/
__STATIC_INLINE uint32_t I2C_IsDMATxEnable(I2C_Type *I2Cx)
{
    return (I2Cx->CTRL3 & I2C_CTRL3_DMATXEN_Msk);
}

/*!
* @brief Get whether the Rx is DMA or not
*
* @param[in] I2Cx: i2c module
                - I2C0
                - I2C1
* @return: DMARX status
*/
__STATIC_INLINE uint32_t I2C_IsDMARxEnable(I2C_Type *I2Cx)
{
    return (I2Cx->CTRL3 & I2C_CTRL3_DMARXEN_Msk);
}


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _AC780X_I2C_REG_H */

/* =============================================  EOF  ============================================== */
