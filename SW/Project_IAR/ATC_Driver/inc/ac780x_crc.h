/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_CRC_H
#define _AC780X_CRC_H

/*!
* @file ac780x_crc.h
*
* @brief This file provides crc integration functions interface.
*
*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ============================================  Includes  ============================================ */
#include "ac780x.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */
/*!
* @brief CRC protocol enum
*/
typedef enum
{
    CRC_PROTOCOL_16BIT = 0x00U,     /*!< CRC Protocol 16bit mode */
    CRC_PROTOCOL_32BIT              /*!< CRC Protocol 32bit mode */
} CRC_ProtocolType;

/*!
* @brief CRC data type enum
*/
typedef enum
{
    CRC_DATA_IS_DATA = 0x00U,       /*!< CRC DATA register is data */
    CRC_DATA_IS_SEED                /*!< CRC DATA register is seed */
} CRC_DataType;

/*!
* @brief CRC write transpose enum
*/
typedef enum
{
    CRC_WRITE_TRANSPOSE_NONE = 0x00U,   /*!< CRC write in without tranpose */
    CRC_WRITE_TRANSPOSE_BITS,           /*!< CRC write in with bits transpose */
    CRC_WRITE_TRANSPOSE_BITS_BYTES,     /*!< CRC write in with bits and bytes transpose */
    CRC_WRITE_TRANSPOSE_BYTES           /*!< CRC write in with bytes transpose */
} CRC_WriteTransposeType;

/*!
* @brief CRC read transpose enumeration
*/
typedef enum
{
    CRC_READ_TRANSPOSE_NONE = 0x00U,    /*!< CRC read out without tranpose */
    CRC_READ_TRANSPOSE_BITS,            /*!< CRC read out with bits transpose */
    CRC_READ_TRANSPOSE_BITS_BYTES,      /*!< CRC read out with bits and bytes transpose */
    CRC_READ_TRANSPOSE_BYTES            /*!< CRC read out with bytes transpose */
} CRC_ReadTransposeType;

/*!
* @brief CRC write num once enumeration
*/
typedef enum
{
    CRC_WRITE_1_BYTE_ONCE = 0x00U,    /*!< CRC write 1 byte once */
    CRC_WRITE_2_BYTE_ONCE,            /*!< CRC write 2 byte once */
    CRC_WRITE_4_BYTE_ONCE,            /*!< CRC write 4 byte once */
} CRC_WriteBytesNumOnceType;

/*!
* @brief CRC configuration structure
*
* This structure holds the configuration settings for the crc
*/
typedef struct
{
    CRC_ProtocolType crcProtocolType;           /*!< CRC 16/32 protocol type */
    CRC_WriteTransposeType writeTransposeType;  /*!< CRC write in transpose type */
    CRC_ReadTransposeType readTransposeType;    /*!< CRC read out transpose type */
    ACTION_Type finalXOR;                       /*!< Enable/disable result XOR */
    CRC_WriteBytesNumOnceType writeBytesNumOnce;/*!< CRC write bytes num once select */
    uint32_t poly;                              /*!< CRC polynomial */
} CRC_ConfigType;

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */
/*!
* @brief Initialize CRC module
*
* @param[in] config: CRC configuration pointer
* @return none
*/
void CRC_Init(const CRC_ConfigType *config);

/*!
* @brief CRC De-initialize
*
* @param[in] none
* @return none
*/
void CRC_DeInit(void);

/*!
* @brief excute CRC check
*
* @param[in] type: select crc-16 or crc-32 protocol
*            seed: load crc seed value
*            msg: pointer to the data array
*            sizeBytes: number of the data array
* @return result
*/
uint32_t CRC_Check(uint32_t seed, const uint8_t *msg, uint32_t sizeBytes);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _AC780X_CRC_H */

/* =============================================  EOF  ============================================== */
