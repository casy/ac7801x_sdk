/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_SPI_H
#define _AC780X_SPI_H
/*!
* @file ac780x_spi.h
*
* @brief This file provides spi integration functions interface.
*
*/

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ===========================================  Includes  =========================================== */
#include "ac780x.h"


/* ============================================  Define  ============================================ */
/*!< SPI use interrput to transmit or receive data or not */
#define SPI_USE_INTERRUPT_TRANSMIT

/*!< SPI interrput transmit status */
#define SPI_STATUS_NONE                (0x00U)
#define SPI_STATUS_RX_OVERFLOW_MASK    (0x01U)  /* BIT 0 */
#define SPI_STATUS_TX_UNDERFLOW_MASK   (0x02U)  /* BIT 1 */
#define SPI_STATUS_MODE_FAULT_MASK     (0x04U)  /* BIT 2 */
#define SPI_STATUS_RX_FINISH_MASK      (0x08U)  /* BIT 3 */
#define SPI_STATUS_TX_FINISH_MASK      (0x10U)  /* BIT 4 */

/*!
* @brief SPI instance index macro, SPIx should be SPI0/SPI1
*/
#define SPI_INDEX(SPIx)    ((SPI_IndexType)(((uint32_t)(SPIx) - SPI0_BASE) >> 12))

/*!< Check function parameter is SPI peripheral or not */
#define IS_SPI_PERIPH(SPIx) (((SPIx) == SPI0) || ((SPIx) == SPI1))


/* ===========================================  Typedef  ============================================ */
/*!
* @brief SPI module index enumeration.
*/
typedef enum
{
    SPI0_INDEX = 0,       /*!< 0: SPI0 index */
    SPI1_INDEX,           /*!< 1: SPI1 index */
    SPI_INDEX_MAX
}SPI_IndexType;

typedef enum
{
    SPI_SLAVE = 0,     /*!< SPI mode: slave */
    SPI_MASTER = 1,    /*!< SPI mode: master */
}SPI_ModeType;
/*!< SPI mode parameter check */
#define IS_SPI_MODE(mode) (((mode) == SPI_SLAVE) ||  \
                           ((mode) == SPI_MASTER))

typedef enum
{
    SPI_CPOL_LOW = 0U,     /*!< SPI CPOL: clock is low when SPI idle */
    SPI_CPOL_HIGH = 1U,    /*!< SPI CPOL: clock is high when SPI idle */
}SPI_CpolType;
/*!< SPI cpol parameter check */
#define IS_SPI_CPOL(cpol) (((cpol) == SPI_CPOL_LOW) || \
                           ((cpol) == SPI_CPOL_HIGH))

typedef enum
{
    SPI_CPHA_1EDGE = 0U,  /*!< SPI CPHA: latched data on the first edge */
    SPI_CPHA_2EDGE = 1U,  /*!< SPI CPHA: latched data on the second edge */
}SPI_CphaType;
/*!< SPI cpha parameter check */
#define IS_SPI_CPHA(cpha) (((cpha) == SPI_CPHA_1EDGE) || \
                           ((cpha) == SPI_CPHA_2EDGE))

typedef enum
{
    SPI_FRAME_SIZE_4BITS = 3U,
    SPI_FRAME_SIZE_5BITS = 4U,
    SPI_FRAME_SIZE_6BITS = 5U,
    SPI_FRAME_SIZE_7BITS = 6U,
    SPI_FRAME_SIZE_8BITS = 7U,
    SPI_FRAME_SIZE_9BITS = 8U,
    SPI_FRAME_SIZE_10BITS = 9U,
    SPI_FRAME_SIZE_11BITS = 10U,
    SPI_FRAME_SIZE_12BITS = 11U,
    SPI_FRAME_SIZE_13BITS = 12U,
    SPI_FRAME_SIZE_14BITS = 13U,
    SPI_FRAME_SIZE_15BITS = 14U,
    SPI_FRAME_SIZE_16BITS = 15U,
}SPI_FrameSizeType;
/*!< SPI frame size parameter check */
#define IS_SPI_FRAMESIZE(frameSize) ((frameSize) <= SPI_FRAME_SIZE_16BITS)

/*!< refer to SPI configuration register */
typedef struct
{
    uint8_t sckHigh;              /*!< SCK high time */
    uint8_t sckLow;               /*!< SCK low time */
    uint8_t csHold;               /*!< CS hold time */
    uint8_t csSetup;              /*!< CS setup time */
    uint8_t csIdle;               /*!< CS Idle Time */
    SPI_ModeType mode;            /*!< SPI mode : 0:Slave 1:Master  */
    SPI_CpolType cpol;            /*!< 0: SCK is Low when idle  1:SCK is High when idle */
    SPI_CphaType cpha;            /*!< 0:1Edge 1:2Edge */
    SPI_FrameSizeType frmSize;    /*!< Frame Size select: 4~16 bits */
    ACTION_Type txMsbFirstEn;     /*!< Enable/Disable TX MSB First */
    ACTION_Type rxMsbFirstEn;     /*!< Enable/Disable RX MSB First */
    ACTION_Type csOutputEn;       /*!< CS Hardware Output Enable */
    ACTION_Type continuousCSEn;   /*!< CS Continuous Output Enable */
    ACTION_Type dmaTxEn;          /*!< Enable/Disable SPI DMA TX */
    ACTION_Type dmaRxEn;          /*!< Enable/Disable SPI DMA RX */
    ACTION_Type modeFaultEn;             /*!< Enable/Disable SPI mode fault detect */
    ACTION_Type modeFaultInterruptEn;    /*!< Enable/Disable SPI mode fault interrupt */
    ACTION_Type txUFInterruptEn;         /*!< Enable/Disable SPI tx under flow interrupt */
    ACTION_Type rxOFInterruptEn;         /*!< Enable/Disable SPI rx over flow interrupt */
    ACTION_Type interruptEn;      /*!< Enable/Disable SPI module interrupt */
    ACTION_Type wakeUpEn;
    ACTION_Type spiEn;            /*!< Enable/Disable SPI module */
    DeviceCallback_Type callBack;   /*!< IRQ Callback pointer */
} SPI_ConfigType, *SPI_ConfigPtr;

typedef struct
{
    SPI_Type *SPIx;                       /*!< SPIx Type Pointer */
    IRQn_Type SPIx_IRQn;                  /*!< SPIx IRQ Number */
    CKGEN_ClockType CKGEN_SPIx;           /*!< SPIx Clock Enable Data */
    CKGEN_SoftResetType SRST_SPIx;        /*!< SPIx Module Reset Data */
}SPI_InfoType;


/* ==========================================  Variables  =========================================== */


/* ====================================  Functions declaration  ===================================== */
/*!
* @brief Initialize SPI module
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
* @param[in] config: SPI configuration structure pointer which contains the configuration
*                    information for the specified SPI.
*/
void SPI_Init(SPI_Type *SPIx, const SPI_ConfigType *config);

/*!
* @brief Uninitialize SPI module
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
*/
void SPI_DeInit(SPI_Type *SPIx);

/*!
* @brief SPI transmission,reception by polling
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
* @param[in] rxBuffer: point to the receive data
* @param[in] txBuffer: point to the send data
* @param[in] length: transfer data length
* @return Function status
*            0: ERROR, length is 0 or rdbuff is NULL or txBuffer is NULL
*            1: SUCCESS
*/
ERROR_Type SPI_TransmitReceivePoll(SPI_Type *SPIx, uint8_t *rxBuffer, const uint8_t *txBuffer, uint32_t length);

/*!
* @brief SPI transfer by polling, ignore receive (txOnly mode)
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
* @param[in] txBuffer: point to the send data
* @param[in] length: transfer data length
* @return Function status
*            0: ERROR, length is 0 or txBuffer is NULL
*            1: SUCCESS
*/
ERROR_Type SPI_TransmitPoll(SPI_Type *SPIx, const uint8_t *txBuffer, uint32_t length);

/*!
* @brief SPI read by polling, ignore write (rxOnly mode)
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
* @param[in] rxBuffer: point to the receive data
* @param[in] Length: transfer data length
* @return Function status
*            0: ERROR, length is 0 or txBuffer is NULL
*            1: SUCCESS
*/
ERROR_Type SPI_ReceivePoll(SPI_Type *SPIx, uint8_t *rxBuffer, uint32_t length);

/*!
* @brief SPI transmission,reception by DMA
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
* @param[in] txDMAx: tx DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] rxDMAx: rx DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] rxBuffer: point to the receive data (Buffer must be 4-byte alignment)
* @param[in] txBuffer: point to the send data (Buffer must be 4-byte alignment)
* @param[in] length: transfer data(uint8_t) length
*                    If spi frameSize <= 8 bits, length must be 0~32767, because DMA CHAN_LENGTH is 0~32767
*                    If spi frameSize > 8 bits, length must be multiple of 2, because DMA CHAN_LENGTH is length/2,
*                    otherwise the last byte cannot be transmit or receive
* @param[in] callback: set dma interrupt callback function
* @return Function status
*            0: ERROR, length is 0 or rdbuff is NULL or txBuffer is NULL
*            1: SUCCESS
*/
ERROR_Type SPI_TransmitReceiveDMA(SPI_Type *SPIx, DMA_ChannelType *txDMAx, DMA_ChannelType *rxDMAx,  \
                                                uint8_t *rxBuffer, const uint8_t *txBuffer, uint16_t length,  \
                                                DeviceCallback_Type callback);

/*!
* @brief SPI transmit by DMA, ignore receive (txOnly mode)
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
* @param[in] txDMAx: tx DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] txBuffer: point to the send data (Buffer must be 4-byte alignment)
* @param[in] length: transfer data length
*                    If spi frameSize <= 8 bits, length must be 0~32767, because DMA CHAN_LENGTH is 0~32767
*                    If spi frameSize > 8 bits, length must be multiple of 2, because DMA CHAN_LENGTH is length/2,
*                    otherwise the last byte cannot be transmit or receive
* @param[in] callback: set dma interrupt callback function
* @return Function status
*            0: ERROR, length is 0 or txBuffer is NULL
*            1: SUCCESS
*/
ERROR_Type SPI_TransmitDMA(SPI_Type *SPIx, DMA_ChannelType *txDMAx, const uint8_t *txBuffer, uint16_t length, DeviceCallback_Type callback);

/*!
* @brief SPI read by DMA, ignore write (rxOnly mode).
*        This function must be used by spi slave mode, master mode cannnot used.
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
* @param[in] rxDMAx: rx DMA Channel type pointer,
*                 -DMA0_CHANNEL0
*                 -DMA0_CHANNEL1
*                 -DMA0_CHANNEL2
*                 -DMA0_CHANNEL3
* @param[in] rxBuffer: point to the receive data (Buffer must be 4-byte alignment)
* @param[in] length: transfer data length
*                    If spi frameSize <= 8 bits, length must be 0~32767, because DMA CHAN_LENGTH is 0~32767
*                    If spi frameSize > 8 bits, length must be multiple of 2, because DMA CHAN_LENGTH is length/2,
*                    otherwise the last byte cannot be transmit or receive
* @param[in] callback: set dma interrupt callback function
* @return Function status
*            0: ERROR, length is 0 or txBuffer is NULL
*            1: SUCCESS
*/
ERROR_Type SPI_ReceiveDMA(SPI_Type *SPIx, DMA_ChannelType *rxDMAx, uint8_t *rxBuffer, uint16_t length, DeviceCallback_Type callback);

#ifdef SPI_USE_INTERRUPT_TRANSMIT
/*!
* @brief SPI transmission, reception by interrupt
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
* @param[in] rxBuffer: point to the receive data
* @param[in] txBuffer: point to the send data
* @param[in] length: transfer data length
* @return Function status
*            0: ERROR, length is 0 or rdbuff is NULL or txBuffer is NULL
*            1: SUCCESS
*/
ERROR_Type SPI_TransmitReceiveInt(SPI_Type *SPIx, uint8_t *rxBuffer, const uint8_t *txBuffer,uint32_t length);

/*!
* @brief SPI transmission by interrupt, ignore reception (txOnly mode)
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
* @param[in] txBuffer: point to the send data
* @param[in] length: transfer data length
* @return Function status
*            0: ERROR, length is 0 or txBuffer is NULL
*            1: SUCCESS
*/
ERROR_Type SPI_TransmitInt(SPI_Type *SPIx, const uint8_t *txBuffer,uint32_t length);

/*!
* @brief SPI read by interrupt, ignore write (rxOnly mode)
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
* @param[in] rxBuffer: point to the receive data
* @param[in] length: transfer data length
* @return Function status
*            0: ERROR, length is 0 or txBuffer is NULL
*            1: SUCCESS
*/
ERROR_Type SPI_ReceiveInt(SPI_Type *SPIx, uint8_t *rxBuffer,uint32_t length);

/*!
* @brief Get SPI interrupt transfer status
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
* @return SPI transfer status
*               BIT0: SPI_STATUS_RX_OVERFLOW_MASK  0: No Rx over flow  1: Occour Rx over flow
*               BIT1: SPI_STATUS_TX_UNDERFLOW_MASK 0: No Tx under flow 1: Occour Tx under flow
*               BIT2: SPI_STATUS_MODE_FAULT_MASK   0: No Mode fault    1: Occour Mode fault
*               BIT3: SPI_STATUS_RX_FINISH_MASK    0: Rx by interrupt is not finished    1: Rx by interrupt is finished
*               BIT4: SPI_STATUS_TX_FINISH_MASK    0: Tx by interrupt is not finished    1: Tx by interrupt is finished
*/
uint32_t SPI_GetTransmitReceiveStatus(SPI_Type *SPIx );

/*!
* @brief Reset SPI interrupt transfer status
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
* @return none
*/
void SPI_ResetTransmitReceiveStatus(SPI_Type *SPIx );

/*!
* @brief Wait SPI receive(interrupt mode) finish
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
* @return Function status
*            0: ERROR, timeout
*            1: SUCCESS
*/
ERROR_Type SPI_WaitReceiveFinish(SPI_Type *SPIx);

#endif /* SPI_USE_INTERRUPT_TRANSMIT */

/*!
* @brief Release SPI Master CS to stop continuous selection of Slave
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
* @return none
*/
void SPI_MasterReleaseCS(SPI_Type *SPIx);

/*!
* @brief Set SPI callback function
*
* @param[in] SPIx: SPI type pointer,x can be 0 to 1
* @param[in] callback: SPI callback type function pointer,which will be called in SPIx_IRQHandler
* @return 0: success, other: error value
*/
void SPI_SetCallback(SPI_Type *SPIx, const DeviceCallback_Type callback);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _AC780X_SPI_H */

/* =============================================  EOF  ============================================== */
