/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780x_CTU_REG_H
#define _AC780x_CTU_REG_H
/*!
* @file ac780x_ctu_reg.h
*
* @brief module-to-module interconnections module access register inline function definition.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x_ctu.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief Set Uart0 rx input signal filter.
*
* @param[in] state: enabling state
                - ENABLE: Uart0 rx input signal is filtered by ACMP0, then injected to Uart0
                - DISABLE: Uart0 rx input signal is connected to UART module directly
* @return none
*/
__STATIC_INLINE void CTU_SetUart0RxFilter(ACTION_Type state)
{
    MODIFY_REG32(CTU->CONFIG0, CTU_CONFIG0_RXDFE_Msk, CTU_CONFIG0_RXDFE_Pos, state);
}

/*!
* @brief Set real-time counter(RTC) overflow to be captured by PWM1 channel 1.
*
* @param[in] state: enabling state
                - ENABLE: RTC overflow is connected to PWM1 channel 1
                - DISABLE: RTC overflow is not connected to PWM1 channel 1
* @return none
*/
__STATIC_INLINE void CTU_SetRtcCaptureByPwm1Ch1(ACTION_Type state)
{
    MODIFY_REG32(CTU->CONFIG0, CTU_CONFIG0_RTCC_Msk, CTU_CONFIG0_RTCC_Pos, state);
}

/*!
* @brief Set connects the output of ACMP0 to PWM1 channel 0.
*
* @param[in] state: enabling state
                - ENABLE: ACMP0 output is connected to PWM1 channel 0
                - DISABLE: ACMP0 output is not connected to PWM1 channel 0
* @return none
*/
__STATIC_INLINE void CTU_SetAcmpOutputCaptureByPwm1Ch0(ACTION_Type state)
{
    MODIFY_REG32(CTU->CONFIG0, CTU_CONFIG0_ACIC_Msk, CTU_CONFIG0_ACIC_Pos, state);
}

/*!
* @brief Set connects the output of ACMP0 to PWM1 channel 0.
*
* @param[in] state: enabling state
                - ENABLE: UART0_RX input signal is connected to the UART0 module and PWM0 channel 1
                - DISABLE: UART0_RX input signal is connected to the UART0 module only
* @return none
*/
__STATIC_INLINE void CTU_SetUart0RxCapture(ACTION_Type state)
{
    MODIFY_REG32(CTU->CONFIG0, CTU_CONFIG0_RXDCE_Msk, CTU_CONFIG0_RXDCE_Pos, state);
}

/*!
* @brief Set generates PWM synchronization trigger to the PWM module.
*
* @param[in] state: enabling state
                - ENABLE: generates PWM synchronization trigger
                - DISABLE: no synchronization triggered
* @return none
*/
__STATIC_INLINE void CTU_SetSoftwareTriggerPwmSync(ACTION_Type state)
{
    MODIFY_REG32(CTU->CONFIG0, CTU_CONFIG0_PWMSYNC_Msk, CTU_CONFIG0_PWMSYNC_Pos, state);
}

/*!
* @brief Set the UART0_TX output modulated by PWMM0 channel 0.
*
* @param[in] state: enabling state
                - ENABLE: UART0_TX output is modulated by PWM0 channel 0 before mapped to pinout
                - DISABLE: UART0_TX output is connected to pinout directly
* @return none
*/
__STATIC_INLINE void CTU_SetUart0TxModulation(ACTION_Type state)
{
    MODIFY_REG32(CTU->CONFIG0, CTU_CONFIG0_TXDME_Msk, CTU_CONFIG0_TXDME_Pos, state);
}

/*!
* @brief Set clock prescaler.
*
* @param[in] psc: prescaler divide
                - CTU_CLK_PRESCALER_1
                - CTU_CLK_PRESCALER_2
                - CTU_CLK_PRESCALER_4
                - CTU_CLK_PRESCALER_8
                - CTU_CLK_PRESCALER_16
                - CTU_CLK_PRESCALER_32
                - CTU_CLK_PRESCALER_64
                - CTU_CLK_PRESCALER_128
* @return none
*/
__STATIC_INLINE void CTU_SetClockPrescaler(CTU_ClkPrescalerType psc)
{
    MODIFY_REG32(CTU->CONFIG0, CTU_CONFIG0_PSC_Msk, CTU_CONFIG0_PSC_Pos, psc);
}

/*!
* @brief Set adc regular group trigger source.
*
* @param[in] source: trigger source
                - CTU_TRIGGER_ADC_RTC_OVERFLOW
                - CTU_TRIGGER_ADC_PWM0_INIT
                - CTU_TRIGGER_ADC_PWM0_MATCH
                - CTU_TRIGGER_ADC_PWM1_INIT
                - CTU_TRIGGER_ADC_PWM1_MATCH
                - CTU_TRIGGER_ADC_TIMER_CH0_OVERFLOW
                - CTU_TRIGGER_ADC_TIMER_CH1_OVERFLOW
                - CTU_TRIGGER_ADC_ACMP0_OUTPUT
* @return none
*/
__STATIC_INLINE void CTU_SetAdcRegularTriggerSource(CTU_AdcTriggerSourceType source)
{
    MODIFY_REG32(CTU->CONFIG0, CTU_CONFIG0_ADHWT0_Msk, CTU_CONFIG0_ADHWT0_Pos, source);
}

/*!
* @brief Get delay unit 0 active flag.
*
* delay unit 0 use for pwm trigger adc regular delay time
*
* @param[in] none
* @return none
*/
__STATIC_INLINE uint32_t CTU_GetDelay0ActiveFlag(void)
{
    return READ_BIT32(CTU->CONFIG0, CTU_CONFIG0_DLYACT0_Msk);
}

/*!
* @brief Set delay unit 0 time.
*
* delay unit 0 use for pwm trigger adc regular group delay time
*
* @param[in] delay: delay time
                - 0 ~ 255
* @return none
*/
__STATIC_INLINE void CTU_SetDelay0Time(uint8_t delay)
{
    MODIFY_REG32(CTU->CONFIG0, CTU_CONFIG0_DELAY0_Msk, CTU_CONFIG0_DELAY0_Pos, delay);
}

/*!
* @brief Set adc injected group trigger source.
*
* @param[in] source: trigger source
                - CTU_TRIGGER_ADC_RTC_OVERFLOW
                - CTU_TRIGGER_ADC_PWM0_INIT
                - CTU_TRIGGER_ADC_PWM0_MATCH
                - CTU_TRIGGER_ADC_PWM1_INIT
                - CTU_TRIGGER_ADC_PWM1_MATCH
                - CTU_TRIGGER_ADC_TIMER_CH0_OVERFLOW
                - CTU_TRIGGER_ADC_TIMER_CH1_OVERFLOW
                - CTU_TRIGGER_ADC_ACMP0_OUTPUT
* @return none
*/
__STATIC_INLINE void CTU_SetAdcInjectTriggerSource(CTU_AdcTriggerSourceType source)
{
    MODIFY_REG32(CTU->CONFIG1, CTU_CONFIG1_ADHWT1_Msk, CTU_CONFIG1_ADHWT1_Pos, source);
}

/**
* @brief select pwdt_in3 input signal source
*
* @param[in] pwdtNum: pwdt num
                0: config pwdt0 in3 input signal
                1: config pwdt1 in3 input signal
* @param[in] source: pwdt_in3 input source
                - CTU_PWDT_IN3_SOURCE_UART0_RX
                - CTU_PWDT_IN3_SOURCE_UART1_RX
                - CTU_PWDT_IN3_SOURCE_UART2_RX
                - CTU_PWDT_IN3_SOURCE_ACMP0_OUT
* @return none
*/
__STATIC_INLINE void CTU_SetPwdtIn3InputSource(uint8_t pwdtNum, CTU_PwdtIn3InputSourceType source)
{
    if (0U == pwdtNum)
    {
        MODIFY_REG32(CTU->CONFIG1, CTU_CONFIG1_PWDT0IN3S_Msk, CTU_CONFIG1_PWDT0IN3S_Pos, source);
    }
    else
    {
        MODIFY_REG32(CTU->CONFIG1, CTU_CONFIG1_PWDT1IN3S_Msk, CTU_CONFIG1_PWDT1IN3S_Pos, source);
    }
}

/*!
* @brief Get delay unit 1 active flag.
*
* delay unit 1 use for pwm trigger adc regular delay time
*
* @param[in] none
* @return none
*/
__STATIC_INLINE uint32_t CTU_GetDelay1ActiveFlag(void)
{
    return READ_BIT32(CTU->CONFIG1, CTU_CONFIG1_DLYACT1_Msk);
}

/*!
* @brief Set delay unit 1 time.
*
* delay unit 1 use for pwm trigger adc injected group delay time
*
* @param[in] delay: delay time
                - 0 ~ 255
* @return none
*/
__STATIC_INLINE void CTU_SetDelay1Time(uint8_t delay)
{
    MODIFY_REG32(CTU->CONFIG1, CTU_CONFIG1_DELAY1_Msk, CTU_CONFIG1_DELAY1_Pos, delay);
}

#ifdef __cplusplus
}
#endif

#endif /* _AC780x_CTU_REG_H */

/* =============================================  EOF  ============================================== */
