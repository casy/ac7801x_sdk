/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_ADC_REG_H
#define _AC780X_ADC_REG_H
/*!
* @file ac780x_adc_reg.h
*
* @brief Analog to digital converter module access register inline function definition.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x_adc.h"

/* ============================================  Define  ============================================ */
#define ADC_AUXADC_CFG0_SEL1_Pos             (8UL)      /*!< RG_MCU_AUXADC_INR_SEL_1 (Bit 8) */
#define ADC_AUXADC_CFG0_SEL1_Msk             (0x100UL)  /*!< RG_MCU_AUXADC_INR_SEL_1 (Bitfield-Mask: 0x01) */
#define ADC_AUXADC_CFG1_SEL0_Pos             (16UL)     /*!< RG_MCU_AUXADC_INR_SEL_0 (Bit 16) */
#define ADC_AUXADC_CFG1_SEL0_Msk             (0x10000UL)/*!< RG_MCU_AUXADC_INR_SEL_0 (Bitfield-Mask: 0x01) */

#define ADC_EACH_REG_SPT_NUM    (10UL)
#define ADC_EACH_REG_RSQ_NUM    (6UL)

/* ===========================================  Typedef  ============================================ */
/*!< ADC AUXADC CFG Structure 0*/
typedef struct
{
    __IOM uint32_t CFG0;     /*!< config high-bit 00:No filter  01:8.77MHz  10:2.9MHz 11:1.2MHz */
    __IOM uint32_t CFG1;     /*!< config low-bit  00:No filter  01:8.77MHz  10:2.9MHz 11:1.2MHz */
} ADC_AUXADC_CFG_Type;

#define ADC_AUXADC_CFG_BASE         (0x40008840UL)
#define ADC_AUXADC_CFG              ((ADC_AUXADC_CFG_Type*) ADC_AUXADC_CFG_BASE)

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief Set adc power mode.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] mode: power mode
                - ADC_POWER_ON
                - ADC_POWER_DOWN
* @return none
*/
__STATIC_INLINE void ADC_SetPowerMode(ADC_Type *ADCx, ADC_PowerModeType mode)
{
    MODIFY_REG32(ADCx->CTRL1, ADC_CTRL1_ADON_Msk, ADC_CTRL1_ADON_Pos, mode);
}

/*!
* @brief Set adc clock prescaler.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] psc: prescaler value
                - ADC_CLK_PRESCALER_1
                - ADC_CLK_PRESCALER_2
                - ADC_CLK_PRESCALER_3
                - ADC_CLK_PRESCALER_4
                - ADC_CLK_PRESCALER_5
                - ADC_CLK_PRESCALER_6
                - ADC_CLK_PRESCALER_7
                - ADC_CLK_PRESCALER_8
                - ADC_CLK_PRESCALER_9
                - ADC_CLK_PRESCALER_10
                - ADC_CLK_PRESCALER_11
                - ADC_CLK_PRESCALER_12
                - ADC_CLK_PRESCALER_13
                - ADC_CLK_PRESCALER_14
                - ADC_CLK_PRESCALER_15
                - ADC_CLK_PRESCALER_16
* @return none
*/
__STATIC_INLINE void ADC_SetClockPrescaler(ADC_Type *ADCx, ADC_ClkPrescalerType psc)
{
    MODIFY_REG32(ADCx->CTRL1, ADC_CTRL1_PSC_Msk, ADC_CTRL1_PSC_Pos, psc);
}

/*!
* @brief Set channel sample time.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] channel: adc channel
                - ADC_CH_0
                - ADC_CH_1
                - ADC_CH_2
                - ADC_CH_3
                - ADC_CH_4
                - ADC_CH_5
                - ADC_CH_6
                - ADC_CH_7
                - ADC_CH_8
                - ADC_CH_9
                - ADC_CH_10
                - ADC_CH_11
                - ADC_CH_BANDGAP
                - ADC_CH_TSENSOR
* @param[in] time: sample time
                - ADC_SPT_CLK_9
                - ADC_SPT_CLK_7
                - ADC_SPT_CLK_15
                - ADC_SPT_CLK_33
                - ADC_SPT_CLK_64
                - ADC_SPT_CLK_140
                - ADC_SPT_CLK_215
                - ADC_SPT_CLK_5
* @return none
*/
__STATIC_INLINE void ADC_SetChannelSampleTime(ADC_Type *ADCx, ADC_ChannelType channel, ADC_SampleTimeType time)
{
    if (ADC_EACH_REG_SPT_NUM > channel)
    {
        MODIFY_REG32(ADCx->SPT1, ADC_SPT_Msk << (channel * ADC_SPT_Width), (channel * ADC_SPT_Width), time);
    }
    else
    {
        MODIFY_REG32(ADCx->SPT0, ADC_SPT_Msk << ((channel - ADC_EACH_REG_SPT_NUM) * ADC_SPT_Width), ((channel - ADC_EACH_REG_SPT_NUM) * ADC_SPT_Width), time);
    }
}

/*!
* @brief Set adc data alignment format.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] align: alignment format
                - ADC_DATA_ALIGN_RIGHT
                - ADC_DATA_ALIGN_LEFT
* @return none
*/
__STATIC_INLINE void ADC_SetDataAlign(ADC_Type *ADCx, ADC_DataAlignType align)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_ALIGN_Msk, ADC_CTRL0_ALIGN_Pos, align);
}

/*!
* @brief Set scan convert mode.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ADC_SetScanMode(ADC_Type *ADCx, ACTION_Type state)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_SCAN_Msk, ADC_CTRL0_SCAN_Pos, state);
}

/*!
* @brief Set continuous convert mode.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ADC_SetContinuousMode(ADC_Type *ADCx, ACTION_Type state)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_CONT_Msk, ADC_CTRL0_CONT_Pos, state);
}

/*!
* @brief Set discontinuous mode number for regular group.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] num: sub group number
                - 0~7
* @return none
*/
__STATIC_INLINE void ADC_SetRegularDiscontinuousNum(ADC_Type* ADCx, uint8_t num)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_DISCNUM_Msk, ADC_CTRL0_DISCNUM_Pos, num);
}

/*!
* @brief Set discontinuous mode for regular group.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ADC_SetRegularDiscontinuousMode(ADC_Type *ADCx, ACTION_Type state)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_DISCEN_Msk, ADC_CTRL0_DISCEN_Pos, state);
}

/*!
* @brief Start software trigger for regular group convert.
*
* @param[in] ADCx: adc module
                - ADC0
* @return none
*/
__STATIC_INLINE void ADC_SoftwareStartRegularConvert(ADC_Type *ADCx)
{
    SET_BIT32(ADCx->CTRL0, ADC_CTRL0_SWSTART_Msk);
}

/*!
* @brief Set regular group trigger source.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] source: trigger source
                - ADC_TRIGGER_EXTERNAL
                - ADC_TRIGGER_INTERNAL
* @return none
*/
__STATIC_INLINE void ADC_SetRegularTriggerSource(ADC_Type *ADCx, ADC_TriggerConvertType source)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_EXTTRIG_Msk, ADC_CTRL0_EXTTRIG_Pos, source);
}

/*!
* @brief Set EOC Interrupt.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ADC_SetEOCInterrupt(ADC_Type *ADCx, ACTION_Type state)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_EOCIE_Msk, ADC_CTRL0_EOCIE_Pos, state);
}

/*!
* @brief Set regular group length.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] length: regular group convert length
                - 0 ~ ADC_REGULAR_SEQUENCE_LEGNTH-1
* @return none
*/
__STATIC_INLINE void ADC_SetRegularLength(ADC_Type *ADCx, uint8_t length)
{
    MODIFY_REG32(ADCx->RSQR0, ADC_RSQR0_RSQL_Msk, ADC_RSQR0_RSQL_Pos, length);
}

/*!
* @brief Set regular group sequence.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] channel: need to covert regular group channel
                - ADC_CH_0
                - ADC_CH_1
                ...
                - ADC_CH_TSENSOR
* @param[in] seq: regular group sequence
                - 0 ~ 11
* @return none
*/
__STATIC_INLINE void ADC_SetRegularSequence(ADC_Type *ADCx, ADC_ChannelType channel, uint8_t seq)
{
    if (ADC_EACH_REG_RSQ_NUM  > seq)
    {
        MODIFY_REG32(ADCx->RSQR2, ADC_SQ_Msk << (seq * ADC_SQ_Width), (seq * ADC_SQ_Width), channel);
    }
    else
    {
        MODIFY_REG32(ADCx->RSQR1, ADC_SQ_Msk << ((seq - ADC_EACH_REG_RSQ_NUM) * ADC_SQ_Width), ((seq - ADC_EACH_REG_RSQ_NUM) * ADC_SQ_Width), channel);
    }
}

/*!
* @brief Get regular group data.
*
* @param[in] ADCx: adc module
                - ADC0
* @return regular group data
*/
#define ADC_GetRegularData(ADCx)    (ADCx->RDR)

/*!
* @brief Enable DMA mode.
*
* This function just use for regular group
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ADC_SetDMA(ADC_Type *ADCx, ACTION_Type state)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_DMAEN_Msk, ADC_CTRL0_DMAEN_Pos, state);
}

/*!
* @brief Start software trigger for injected group convert.
*
* @param[in] ADCx: adc module
                - ADC0
* @return none
*/
__STATIC_INLINE void ADC_SoftwareStartInjectConvert(ADC_Type *ADCx)
{
    SET_BIT32(ADCx->CTRL0, ADC_CTRL0_ISWSTART_Msk);
}

/*!
* @brief Set injected group trigger source.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] source: trigger source
                - ADC_TRIGGER_EXTERNAL
                - ADC_TRIGGER_INTERNAL
* @return none
*/
__STATIC_INLINE void ADC_SetInjectTriggerSource(ADC_Type *ADCx, ADC_TriggerConvertType source)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_IEXTTRIG_Msk, ADC_CTRL0_IEXTTRIG_Pos, source);
}

/*!
* @brief Set IEOC Interrupt.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ADC_SetIEOCInterrupt(ADC_Type *ADCx, ACTION_Type state)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_IEOCIE_Msk, ADC_CTRL0_IEOCIE_Pos, state);
}

/*!
* @brief Set injected group length.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] length: injected group convert length
                - 0 ~ ADC_INJECT_SEQUENCE_LEGNTH-1
* @return none
*/
__STATIC_INLINE void ADC_SetInjectLength(ADC_Type *ADCx, uint8_t length)
{
    MODIFY_REG32(ADCx->ISQR, ADC_ISQR_ISQL_Msk, ADC_ISQR_ISQL_Pos, length);
}

/*!
* @brief Set injected group sequence.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] channel: need to covert regular group channel
                - ADC_CH_0
                - ADC_CH_1
                ...
                - ADC_CH_TSENSOR
* @param[in] seq: injected group sequence
                - 0 ~ 3
* @return none
*/
__STATIC_INLINE void ADC_SetInjectSequence(ADC_Type *ADCx, ADC_ChannelType channel, uint8_t seq)
{
    MODIFY_REG32(ADCx->ISQR, ADC_SQ_Msk << (seq * ADC_SQ_Width), (seq * ADC_SQ_Width), channel);
}

/*!
* @brief Set injected group offset value.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] num: injected group channel number
                - 0 ~ 3
* @param[in] offset: Offset Value
                - 0 ~ 0xfff
* @return none
*/
__STATIC_INLINE void ADC_SetInjectOffset(ADC_Type *ADCx, uint8_t num, uint32_t offset)
{
    MODIFY_REG32(ADCx->IOFR[num], ADC_IOFR_IOFR_Msk, ADC_IOFR_IOFR_Pos, offset);
}

/*!
* @brief Get injected group data.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] num: injected group channel number
                - 0 ~ 3
* @return injected data
*/
#define ADC_GetInjectData(ADCx, num)    (ADCx->IDR[num])

/*!
* @brief Set ADC automatic injected group convert after regular complete.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ADC_SetInjectAutoMode(ADC_Type *ADCx, ACTION_Type state)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_IAUTO_Msk, ADC_CTRL0_IAUTO_Pos, state);
}

/*!
* @brief Set ADC interval mode.
*
* Can only be configured in mode3/5, other modes do not work
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ADC_SetIntervalMode(ADC_Type *ADCx, ACTION_Type state)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_INTERVAL_Msk, ADC_CTRL0_INTERVAL_Pos, state);
}

/*!
* @brief Set discontinuous mode for injected group.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ADC_SetInjectDiscontinuousMode(ADC_Type *ADCx, ACTION_Type state)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_IDISCEN_Msk, ADC_CTRL0_IDISCEN_Pos, state);
}

/*!
* @brief Set AMO Interrupt.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ADC_SetAMOInterrupt(ADC_Type *ADCx, ACTION_Type state)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_AMOIE_Msk, ADC_CTRL0_AMOIE_Pos, state);
}

/*!
* @brief Set analog monitor single channel.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] channel: analog monitor channel
                - ADC_CH_0
                - ADC_CH_1
                ...
                - ADC_CH_TSENSOR
* @return none
*/
__STATIC_INLINE void ADC_SetAMOSingleChannel(ADC_Type *ADCx, ADC_ChannelType channel)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_AMOCH_Msk, ADC_CTRL0_AMOCH_Pos, channel);
}

/*!
* @brief Set analog monitor single channel mode.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ADC_SetAMOSingleChannelMode(ADC_Type *ADCx, ACTION_Type state)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_AMOSGL_Msk, ADC_CTRL0_AMOSGL_Pos, state);
}

/*!
* @brief Set regular group analog monitor mode.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ADC_SetAMORegularMode(ADC_Type *ADCx, ACTION_Type state)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_AMOEN_Msk, ADC_CTRL0_AMOEN_Pos, state);
}

/*!
* @brief Set injected group analog monitor mode.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ADC_SetAMOInjectMode(ADC_Type *ADCx, ACTION_Type state)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_IAMOEN_Msk, ADC_CTRL0_IAMOEN_Pos, state);
}

/*!
* @brief Set analog monitor trigger type.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] mode: amo trigger mode
                - ADC_AMO_TRIGGER_LEVEL
                - ADC_AMO_TRIGGER_EDGE
* @return none
*/
__STATIC_INLINE void ADC_SetAMOTriggerMode(ADC_Type *ADCx, ADC_AMOTriggerModeType mode)
{
    MODIFY_REG32(ADCx->CTRL0, ADC_CTRL0_AMOMODE_Msk, ADC_CTRL0_AMOMODE_Pos, mode);
}

/*!
* @brief Set analog monitor threshold.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] highValue: high threshold value
                - 0~0xfff
* @param[in] lowValue: low threshold value
                - 0~0xfff
* @return none
*/
__STATIC_INLINE void ADC_SetAMOThreshold(ADC_Type *ADCx, uint16_t highValue, uint16_t lowValue)
{
    MODIFY_REG32(ADCx->AMOHR, ADC_AMOHR_AMOHT_Msk, ADC_AMOHR_AMOHT_Pos, highValue);
    MODIFY_REG32(ADCx->AMOLR, ADC_AMOLR_AMOLT_Msk, ADC_AMOLR_AMOLT_Pos, lowValue);
}

/*!
* @brief Set analog monitor offset.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] highOffset: high offset value
                - 0~0xfff
* @param[in] lowOffset: low offset value
                - 0~0xfff
* @return none
*/
__STATIC_INLINE void ADC_SetAMOOffset(ADC_Type *ADCx, uint16_t highOffset, uint16_t lowOffset)
{
    MODIFY_REG32(ADCx->AMOHR, ADC_AMOHR_AMOHO_Msk, ADC_AMOHR_AMOHO_Pos, highOffset);
    MODIFY_REG32(ADCx->AMOLR, ADC_AMOLR_AMOLO_Msk, ADC_AMOLR_AMOLO_Pos, lowOffset);
}

/*!
* @brief Get AMO flag.
*
* @param[in] ADCx: adc module
                - ADC0
* @return AMO flag
*/
#define ADC_GetAMOFlag(ADCx)            READ_BIT32(ADCx->STR, ADC_STR_AMO_Msk)

/*!
* @brief Clear AMO flag.
*
* @param[in] ADCx: adc module
                - ADC0
* @return none
*/
#define ADC_ClearAMOFlag(ADCx)          CLEAR_BIT32(ADCx->STR, ADC_STR_AMO_Msk)

/*!
* @brief Get EOC flag.
*
* @param[in] ADCx: adc module
                - ADC0
* @return EOC flag
*/
#define ADC_GetEOCFlag(ADCx)            READ_BIT32(ADCx->STR, ADC_STR_EOC_Msk)

/*!
* @brief Clear EOC flag.
*
* @param[in] ADCx: adc module
                - ADC0
* @return none
*/
#define ADC_ClearEOCFlag(ADCx)          CLEAR_BIT32(ADCx->STR, ADC_STR_EOC_Msk)

/*!
* @brief Get IEOC flag.
*
* @param[in] ADCx: adc module
                - ADC0
* @return IEOC flag
*/
#define ADC_GetIEOCFlag(ADCx)           READ_BIT32(ADCx->STR, ADC_STR_IEOC_Msk)

/*!
* @brief Clear IEOC flag.
*
* @param[in] ADCx: adc module
                - ADC0
* @return none
*/
#define ADC_ClearIEOCFlag(ADCx)         CLEAR_BIT32(ADCx->STR, ADC_STR_IEOC_Msk)

/*!
* @brief Get idle flag.
*
* @param[in] ADCx: adc module
                - ADC0
* @return idle flag
*/
#define ADC_GetIdleFlag(ADCx)           READ_BIT32(ADCx->STR, ADC_STR_IDLE_Msk)

/*!
* @brief Get normal analog monitor flag.
*
* @param[in] ADCx: adc module
                - ADC0
* @return NAMO flag
*/
#define ADC_GetNAMOFlag(ADCx)           READ_BIT32(ADCx->STR, ADC_STR_NAMO_Msk)

/*!
* @brief Clear normal analog monitor flag.
*
* @param[in] ADCx: adc module
                - ADC0
* @return none
*/
#define ADC_ClearNAMOFlag(ADCx)         CLEAR_BIT32(ADCx->STR, ADC_STR_NAMO_Msk)

/*!
* @brief Get abnormal analog monitor flag.
*
* @param[in] ADCx: adc module
                - ADC0
* @return AAMO flag
*/
#define ADC_GetAAMOFlag(ADCx)           READ_BIT32(ADCx->STR, ADC_STR_AAMO_Msk)

/*!
* @brief Clear abnormal analog monitor flag.
*
* @param[in] ADCx: adc module
                - ADC0
* @return none
*/
#define ADC_ClearAAMOFlag(ADCx)         CLEAR_BIT32(ADCx->STR, ADC_STR_AAMO_Msk)

/*!
* @brief Set Calibration function.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] state: enabling state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ADC_SetCalibration(ADC_Type *ADCx, ACTION_Type state)
{
    MODIFY_REG32(ADCx->CTRL1, ADC_CTRL1_CALEN_Msk, ADC_CTRL1_CALEN_Pos, state);
}

/*!
* @brief Set Low pass filter frequency.
*
* @param[in] ADCx: adc module
                - ADC0
* @param[in] channel: analog monitor channel
                - ADC_CH_0
                - ADC_CH_1
                ...
                - ADC_CH_11
* @param[in] freq: low pass filter frequence
                - ADC_LPF_NONE
                - ADC_LPF_8770KHZ
                - ADC_LPF_2900KHZ
                - ADC_LPF_1200KHZ

* @return none
*/
__STATIC_INLINE void ADC_SetLowPassFilter(ADC_Type *ADCx, ADC_ChannelType channel, ADC_LowPassFilterType freq)
{
    DEVICE_ASSERT(ADC_CH_BANDGAP > channel);
    MODIFY_REG32(ADC_AUXADC_CFG->CFG0, ADC_AUXADC_CFG0_SEL1_Msk << channel, ADC_AUXADC_CFG0_SEL1_Pos << channel, ((freq & 0x02U) >> 1U));
    MODIFY_REG32(ADC_AUXADC_CFG->CFG1, ADC_AUXADC_CFG1_SEL0_Msk << channel, ADC_AUXADC_CFG1_SEL0_Pos << channel, freq & 0x01U);
}

#ifdef __cplusplus
}
#endif

#endif /* _AC780X_ADC_REG_H */

/* =============================================  EOF  ============================================== */
