/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_PWDT_H
#define _AC780X_PWDT_H
/*!
* @file ac780x_pwdt.h
*
* @brief This file provides Pulse width measurement module integration functions interfaces.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x.h"

/* ============================================  Define  ============================================ */
/*!
* @brief PWDT instance index macro.
*/
#define PWDT_INDEX(PWDTx)    ((uint8_t)(((uint32_t)(PWDTx) - PWDT0_BASE) >> 11) )

/* ===========================================  Typedef  ============================================ */
/*!
* @brief PWDT mode enumeration.
*/
typedef enum
{
    PWDT_NONE_MODE = 0,     /*!< none mode */
    PWDT_MEASURE_MODE,      /*!< Measure mode */
    PWDT_TIMER_MODE,        /*!< Timer mode */
} PWDT_ModeType;

/*!
* @brief PWDT clock prescaler enumeration.
*/
typedef enum
{
    PWDT_CLK_PRESCALER_1 = 0,   /*!< Clock presalcer divide 1 */
    PWDT_CLK_PRESCALER_2,       /*!< Clock presalcer divide 2 */
    PWDT_CLK_PRESCALER_4,       /*!< Clock presalcer divide 4 */
    PWDT_CLK_PRESCALER_8,       /*!< Clock presalcer divide 8 */
    PWDT_CLK_PRESCALER_16,      /*!< Clock presalcer divide 16 */
    PWDT_CLK_PRESCALER_32,      /*!< Clock presalcer divide 32 */
    PWDT_CLK_PRESCALER_64,      /*!< Clock presalcer divide 64 */
    PWDT_CLK_PRESCALER_128,     /*!< Clock presalcer divide 128 */
    PWDT_CLK_PRESCALER_256,     /*!< Clock presalcer divide 256 */
    PWDT_CLK_PRESCALER_512,     /*!< Clock presalcer divide 512 */
    PWDT_CLK_PRESCALER_MAX      /*!< Invalid clock presalcer */
} PWDT_ClkPrescalerType;

/*!
* @brief PWDT measure edge type enumeration.
*/
typedef enum
{
    PWDT_FALLING_START_CAPTURE_FALLING = 0, /*!< First falling-edge start to measure, all falling-edge capture */
    PWDT_RISING_START_CAPTURE_ALL,          /*!< First rising-edge start to measure, all edge capture */
    PWDT_FALLING_START_CAPTURE_ALL,         /*!< First falling-edge start to measure, all edge capture */
    PWDT_RISING_START_CAPTURE_RISING,       /*!< First rising-edge start to measure, all rising-edge capture */
} PWDT_EdgeType;

/*!
* @brief PWDT measure input channel enumeration.
*/
typedef enum
{
    PWDT_INPUT_CH_0 = 0,    /*!< pwdt_in0 as input channel */
    PWDT_INPUT_CH_1,        /*!< pwdt_in1 as input channel */
    PWDT_INPUT_CH_2,        /*!< pwdt_in2 as input channel */
    PWDT_INPUT_CH_3,        /*!< pwdt_in3 as input channel */
    PWDT_INPUT_CH_MAX,      /*!< Invalid pwdt input channel */
} PWDT_InputChannelType;

/*!
* @brief PWDT filter prescaler enumeration.
*/
typedef enum
{
    PWDT_FILTER_PRESCALER_1 = 0,    /*!< Filter presalcer divide 1 */
    PWDT_FILTER_PRESCALER_2,        /*!< Filter presalcer divide 2 */
    PWDT_FILTER_PRESCALER_4,        /*!< Filter presalcer divide 4 */
    PWDT_FILTER_PRESCALER_8,        /*!< Filter presalcer divide 8 */
    PWDT_FILTER_PRESCALER_16,       /*!< Filter presalcer divide 16 */
    PWDT_FILTER_PRESCALER_32,       /*!< Filter presalcer divide 32 */
    PWDT_FILTER_PRESCALER_64,       /*!< Filter presalcer divide 64 */
    PWDT_FILTER_PRESCALER_128,      /*!< Filter presalcer divide 128 */
    PWDT_FILTER_PRESCALER_256,      /*!< Filter presalcer divide 256 */
    PWDT_FILTER_PRESCALER_512,      /*!< Filter presalcer divide 512 */
    PWDT_FILTER_PRESCALER_1024,     /*!< Filter presalcer divide 1024 */
    PWDT_FILTER_PRESCALER_2048,     /*!< Filter presalcer divide 2048 */
    PWDT_FILTER_PRESCALER_4096,     /*!< Filter presalcer divide 4096 */
    PWDT_FILTER_PRESCALER_MAX,      /*!< Invalid filter presalcer */
} PWDT_FilterPrescalerType;

/*!
* @brief PWDT configuration structure.
*/
typedef struct
{
    /* General parameters */
    PWDT_ClkPrescalerType clkPsc;               /*!< Clock prescaler */
    PWDT_EdgeType edgeType;                     /*!< Measure edge type */
    PWDT_InputChannelType channel;              /*!< Select measure input channel */
    ACTION_Type overflowInterruptEn;            /*!< Overflow interrupt enable/disable */
    ACTION_Type readyInterruptEn;               /*!< Data ready interrupt enable/disable */
    ACTION_Type interruptEn;                    /*!< PWDT module interrupt enable/disable */
    DeviceCallback_Type callBack;               /*!< Callback pointer */
    ACTION_Type cmpModeEn;                      /*!< Comparator mode enable/disable */
    ACTION_Type hallModeEn;                     /*!< Hall mode enable/disable */
    PWDT_ModeType mode;                         /*!< PWDT working mode */
    /* Filter parameters */
    PWDT_FilterPrescalerType filterPsc;         /*!< Filter prescaler */
    uint8_t filterValue;                        /*!< Filter value */
    ACTION_Type filterEn;                       /*!< Filter enable/disable */
    /* Timer parameters */
    uint16_t periodValue;                       /*!< Timer period value */
} PWDT_ConfigType;

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */
/*!
* @brief PWDT initialize.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] config: pointer to configuration structure
* @return none
*/
void PWDT_Init(PWDT_Type *PWDTx, const PWDT_ConfigType *config);

/*!
* @brief PWDT De-initialize.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @return none
*/
void PWDT_DeInit(PWDT_Type *PWDTx);

/*!
* @brief Set pwdt clock prescaler.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] psc: prescaler divide
                - PWDT_CLK_PRESCALER_1
                - PWDT_CLK_PRESCALER_2
                - PWDT_CLK_PRESCALER_4
                - PWDT_CLK_PRESCALER_8
                - PWDT_CLK_PRESCALER_16
                - PWDT_CLK_PRESCALER_32
                - PWDT_CLK_PRESCALER_64
                - PWDT_CLK_PRESCALER_128
                - PWDT_CLK_PRESCALER_256
                - PWDT_CLK_PRESCALER_512
* @return none
*/
void PWDT_SetClockPrescaler(PWDT_Type *PWDTx, PWDT_ClkPrescalerType psc);

/*!
* @brief Set pwdt callback function.
*
* @param[in] PWDTx: pwdt module
                - PWDT0
                - PWDT1
* @param[in] func: callback function
* @return none
*/
void PWDT_SetCallback(PWDT_Type *PWDTx, const DeviceCallback_Type func);

#ifdef __cplusplus
}
#endif

#endif /* _AC780X_PWDT_H */

/* =============================================  EOF  ============================================== */
