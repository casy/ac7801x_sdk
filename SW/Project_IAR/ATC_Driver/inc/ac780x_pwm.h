/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_PWM_H
#define _AC780X_PWM_H
/*!
* @file ac780x_pwm.h
*
* @brief This file provides pulse width modulation module integration functions interfaces.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x.h"

/* ============================================  Define  ============================================ */
/*!
* @brief PWM instance index macro.
*/
#define PWM_INDEX(PWMx)    ((uint8_t)(((uint32_t)(PWMx) - PWM0_BASE) >> 12) )

/* @brief Number of pair channels */
#define PWM_PAIR_CHANNEL_NUM            (4UL)

/* The maximum of counter */
#define PWM_MAX_COUNTER_VALUE           (0xFFFFUL)


/* ===========================================  Typedef  ============================================ */
/*!
* @brief PWM channel enumeration.
*/
typedef enum
{
    PWM_CH_0 = 0,       /*!< PWM channel 0 */
    PWM_CH_1,           /*!< PWM channel 1 */
    PWM_CH_2,           /*!< PWM channel 2 */
    PWM_CH_3,           /*!< PWM channel 3 */
    PWM_CH_4,           /*!< PWM channel 4 */
    PWM_CH_5,           /*!< PWM channel 5 */
    PWM_CH_6,           /*!< PWM channel 6 */
    PWM_CH_7,           /*!< PWM channel 7 */
    PWM_CH_MAX,         /*!< Invalid channel */
} PWM_ChannelType;

/*******************************************************************************
* PWM_MODE_MODULATION: Pulse width mudulation mode type definition
*******************************************************************************/
/*!
 * @brief PWM output level enumeration.
 */
typedef enum
{
    PWM_LOW_LEVEL = 0,      /*!< LOW LEVEL */
    PWM_HIGH_LEVEL,         /*!< HIGH LEVEL */
} PWM_OutputLevelType;

/*!
 * @brief PWM channel combine enumeration.
 */
typedef enum
{
    PWM_INDEPENDENT_MODE = 0,   /*!< Independent channel mode */
    PWM_COMBINE_MODE,           /*!< Combine channel mode */
} PWM_ChannelCombineType;

/*!
 * @brief PWM channel output level mode enumeration.
 */
typedef enum
{
    PWM_HIGH_TRUE = 2,     /*!< High true */
    PWM_LOW_TRUE,          /*!< Low true */
} PWM_OutputLevelModeType;

/*!
 * @brief PWM channel match point counting direction in combine mode enumeration.
 */
typedef enum
{
    PWM_MATCH_DIR_DOWN = 0,     /*!< channel match point in down counting direction */
    PWM_MATCH_DIR_UP,           /*!< channel match point in up counting direction */
} PWM_MatchPointDirType;

/*!
 * @brief PWM counting mode enumeration.
 */
typedef enum
{
    PWM_UP_COUNT = 0,       /*!< Up counting mode */
    PWM_UP_DOWN_COUNT,      /*!< Up-down counting mode */
} PWM_CountModeType;

/*!
* @brief PWM prescaler factor for the dead-time insertion enumeration.
*/
typedef enum
{
    PWM_DEADTIME_DIVID_1 = 1,   /*!< Divide by 1 */
    PWM_DEADTIME_DIVID_4,       /*!< Divide by 4 */
    PWM_DEADTIME_DIVID_16,      /*!< Divide by 16 */
} PWM_DeadtimePscType;

/*!
* @brief PWM channel output polarity active enumeration.
*/
typedef enum
{
    PWM_OUTPUT_POLARITY_ACTIVE_HIGH = 0,   /*!< The channel output polarity is active high */
    PWM_OUTPUT_POLARITY_ACTIVE_LOW,        /*!< The channel output polarity is active low */
} PWM_OutputPolarityActiveType;

/*!
* @brief PWM fault input channel enumeration.
*/
typedef enum
{
    PWM_FAULT_CH_0 = 0,       /*!< PWM fault input channel 0 */
    PWM_FAULT_CH_1,           /*!< PWM fault input channel 1 */
    PWM_FAULT_CH_2,           /*!< PWM fault input channel 2 */
    PWM_FAULT_CH_MAX,         /*!< Invalid fault input channel */
} PWM_FaultChannelType;

/*!
* @brief PWM fault channel input polarity active enumeration.
*/
typedef enum
{
    PWM_INPUT_POLARITY_ACTIVE_HIGH = 0,   /*!< The fault channel polarity is active high */
    PWM_INPUT_POLARITY_ACTIVE_LOW,        /*!< The fault channel polarity is active low */
} PWM_FaultInputPolarityActiveType;

/*!
* @brief PWM channel fault control mode enumeration.
*/
typedef enum
{
    PWM_FAULT_CTRL_NONE  = 0,       /*!< No Fault control */
    PWM_FAULT_CTRL_MANUAL_EVEN,     /*!< Fault control is enabled for even channels and manual fault clearing */
    PWM_FAULT_CTRL_MANUAL_ALL,      /*!< Fault control is enabled for all channels and manual fault clearing */
    PWM_FAULT_CTRL_AUTO,            /*!< Fault control is enabled for all channels and automatic fault clearing */
} PWM_FaultCtrlModeType;

/*!
* @brief PWM Fault channel configuration structure.
*/
typedef struct
{
    ACTION_Type faultInputEn;               /*!< Fault input channel state */
    ACTION_Type faultFilterEn;              /*!< Fault channel filter state */
    PWM_FaultInputPolarityActiveType faultPolarity;   /*!< Fault channel input polarity active */
} PWM_FaultChConfigType;

/*!
* @brief PWM Fault configuration structure.
*/
typedef struct
{
    PWM_FaultCtrlModeType mode;                                         /*!< Fault mode */
    PWM_FaultChConfigType channelConfig[PWM_FAULT_CH_MAX];              /*!< Fault input channels configuration */
    uint8_t filterValue;                                                /*!< Fault filter value */
    ACTION_Type faultCtrlOutputEn[PWM_PAIR_CHANNEL_NUM];                /*!< Fault control channel output state */
    ACTION_Type interruptEn;                                            /*!< Enable PWM fault interrupt */
} PWM_FaultConfig;

/*!
* @brief PWM sync mode enumeration.
*/
typedef enum
{
    PWM_SYNC_MODE_LEGACY = 0,   /*!< Legacy synchronization */
    PWM_SYNC_MODE_ENHANCED,     /*!< Enhanced synchronization */
} PWM_SyncModeType;

/*!
* @brief PWM sync trigger mode enumeration.
*/
typedef enum
{
    PWM_SYNC_TRIGGER_SOFTWARE = 0,  /*!< Software trigger synchronization */
    PWM_SYNC_TRIGGER_HARDWARE,      /*!< Hardware trigger synchronization */
} PWM_SyncTriggerModeType;

/*!
* @brief PWM sync configuration structure.
* Please don't use software and hardware trigger simultaneously
*/
typedef struct
{
    PWM_SyncTriggerModeType syncTriggerMode;/*!< Synchronization trigger mode */
    ACTION_Type hardwareSync0En;            /*!< Enable/disable hardware sync trigger source 0 */
    ACTION_Type hardwareSync1En;            /*!< Enable/disable hardware sync trigger source 1 */
    ACTION_Type hardwareSync2En;            /*!< Enable/disable hardware sync trigger source 2 */
    ACTION_Type autoClearHWTriggerEn;       /*!< Available only for hardware trigger */
    ACTION_Type counterInitSyncEn;          /*!< Enable/disable CNTIN sync */
    ACTION_Type outCtrlSyncEn;              /*!< Enable/disable CHOSWCR sync */
    ACTION_Type inverterSyncEn;             /*!< Enable/disable INVCR sync */
    ACTION_Type outmaskSyncEn;              /*!< Enable/disable OMCR sync */
    ACTION_Type polaritySyncEn;             /*!< Enable/disable CHOPOLCR sync */
    ACTION_Type chValueSyncEn[PWM_PAIR_CHANNEL_NUM];/*!< Enable/disable dual channel CHV sync */
    ACTION_Type maxLoadingPointEn;          /*!< Enable/disable maximum loading point */
    ACTION_Type minLoadingPointEn;          /*!< Enable/disable minimum loading point */
    ACTION_Type syncEn;                     /*!< Enable/disable Synchronization mode */
} PWM_SyncConfigType;

/*!
* @brief PWM independent channel configuration structure.
*/
typedef struct
{
    PWM_ChannelType channel;                /*!< Independent channel number */
    uint16_t chValue;                       /*!< Channel value */
    PWM_OutputLevelModeType levelMode;      /*!< Output level mode */
    PWM_OutputPolarityActiveType polarity;  /*!< Polarity of the channel output */
    ACTION_Type interruptEn;                /*!< Enable/disable channel interrupt */
    PWM_OutputLevelType initLevel;          /*!< Channel init output level */
    ACTION_Type triggerEn;                  /*!< Enable/disable channel trigger */
} PWM_IndependentChConfig;

/*!
* @brief PWM combine channel configuration structure.
*/
typedef struct
{
    PWM_ChannelType pairChannel;            /*!< pair of channel number (PWM_CH_0/2/4/6) */
    uint16_t ch1stValue;                    /*!< Channel n channel value */
    uint16_t ch2ndValue;                    /*!< Channel n+1 channel value */
    PWM_OutputLevelModeType levelMode;      /*!< output level mode */
    ACTION_Type deadtimeEn;                 /*!< Enable/disable dead-time insertion */
    ACTION_Type complementEn;               /*!< Enable/disable complementary output */
    PWM_MatchPointDirType ch1stMatchDir;    /*!< Channel n Match point direction, It only active on the up-down counting mode */
    PWM_MatchPointDirType ch2ndMatchDir;    /*!< Channel n+1 Match point direction, It only active on the up-down counting mode */
    PWM_OutputPolarityActiveType ch1stPolarity;/*!< Channel n the polarity of the channel output */
    PWM_OutputPolarityActiveType ch2ndPolarity;/*!< Channel n+1 the polarity of the channel output */
    ACTION_Type ch1stInterruptEn;           /*!< Enable/disable channel n interrupt */
    ACTION_Type ch2ndInterruptEn;           /*!< Enable/disable channel n+1 interrupt */
    PWM_OutputLevelType ch1stInitLevel;     /*!< Channel n init output level */
    PWM_OutputLevelType ch2ndInitLevel;     /*!< Channel n+1 init output level */
    ACTION_Type ch1stTriggerEn;             /*!< Enable/disable channel n trigger */
    ACTION_Type ch2ndTriggerEn;             /*!< Enable/disable channel n+1 trigger */
} PWM_CombineChConfig;

/*!
* @brief PWM Modulation configuration structure.
*/
typedef struct
{
    PWM_CountModeType countMode;                    /*!< Counting mode */
    uint8_t independentChannelNum;                  /*!< Number of independent channels (0~7) */
    uint8_t combineChannelNum;                      /*!< Number of combine channels (0~3) */
    PWM_IndependentChConfig *independentChConfig;   /*!< Configuration for independent channels */
    PWM_CombineChConfig *combineChConfig;           /*!< Configuration for combined channels */
    uint8_t deadtime;                               /*!< Dead time value  */
    PWM_DeadtimePscType deadtimePsc;                /*!< Dead time prescaler value */
    ACTION_Type initChOutputEn;                     /*!< Enable/disable Initial channel output */
    ACTION_Type initTriggerEn;                      /*!< Enable/disable Initial trigger */
} PWM_ModulationConfigType;

/*******************************************************************************
* PWM_MODE_OUTPUT_COMPARE: PWM output compare mode type definition
*******************************************************************************/
/*!
* @brief PWM output compare mode enumeration.
*/
typedef enum
{
    PWM_NONE_OUTPUT = 0,    /*!< No output */
    PWM_TOGGLE_OUTPUT,      /*!< Toggle output */
    PWM_CLEAR_OUTPUT,       /*!< Clear output */
    PWM_SET_OUTPUT,         /*!< Set output */
} PWM_OutputCompareModeType;

/*!
* @brief PWM output compare channel configuration structure.
*/
typedef struct
{
    PWM_ChannelType channel;                /*!< Channel number */
    uint16_t comparedValue;                 /*!< Compared value */
    PWM_OutputCompareModeType mode;         /*!< Output compare mode */
    ACTION_Type interruptEn;                /*!< Enable/disable channel interrupt */
    PWM_OutputLevelType initLevel;          /*!< Channel init output level */
    ACTION_Type triggerEn;                  /*!< Enable/disable channel trigger */
} PWM_OutputCompareChConfigType;

/*!
* @brief PWM output compare configuration structure.
*/
typedef struct
{
    uint8_t channelNum;                             /*!< Number of output compare channels */
    PWM_OutputCompareChConfigType *channelConfig;   /*!< Output compare channels configuration */
    ACTION_Type initChOutputEn;                     /*!< Enable/disable Initial channel output */
} PWM_OutputCompareConfigType;

/*******************************************************************************
* PWM_MODE_INPUT_CAPTURE: PWM input capture mode type definition
*******************************************************************************/
/*!
* @brief PWM input capture edge mode enumeration.
*/
typedef enum
{
    PWM_INPUTCAP_SINGLE_EDGE = 0,       /*!< Signle edge capture mode */
    PWM_INPUTCAP_DUAL_EDGE,             /*!< Dual edge capture mode */
} PMW_InputCaptureEdgeModeType;

/*!
* @brief PWM input capture single edge detect enumeration.
*/
typedef enum
{
    PWM_RISING_EDGE_DETECT = 1,     /*!< Rising edge detect */
    PWM_FALLING_EDGE_DETECT,        /*!< Falling Edge detect */
    PWM_BOTH_EDGE_DETECT,           /*!< Rising or falling edge detect */
} PWM_InputSingleEdgeDetectType;

/*!
* @brief PWM input capture dual edge measure enumeration.
*/
typedef enum
{
    PWM_POSITIVE_PLUSE_WIDTH_MEASURE = 0,       /*!< Positive Pluse width measurement */
    PWM_NEGATIVE_PLUSE_WIDTH_MEASURE,           /*!< Negative Pluse width measurement */
    PWM_RISING_EDGE_PERIOD_MEASURE,             /*!< Period measurement between two consecutive rising edges */
    PWM_FALLING_EDGE_PERIOD_MEASURE,            /*!< Period measurement between two consecutive falling edges */
} PWM_InputDualEdgeMeasureType;

/*!
* @brief PWM input capture measurement type for dual edge input capture enumeration.
*/
typedef enum
{
    PWM_INPUTCAP_ONESHOT = 0,       /*!< Dual edge capture with one shot mode */
    PWM_INPUTCAP_CONTINUOUS,            /*!< Dual edge capture with continuous mode */
} PMW_DualOnceModeType;

/*!
* @brief PWM input capture event prescaler enumeration.
*/
typedef enum
{
    PWM_EVENT_PSC_1 = 0,    /*!< 1 event trigger input capture */
    PWM_EVENT_PSC_2,        /*!< 2 event trigger input capture */
    PWM_EVENT_PSC_4,        /*!< 4 event trigger input capture */
    PWM_EVENT_PSC_8,        /*!< 8 event trigger input capture */
} PMW_InputEventPscType;

/*!
* @brief PWM Input capture channel configuration structure.
*/
typedef struct
{
    PWM_ChannelType channel;                    /*!< Channel number */
    PMW_InputCaptureEdgeModeType mode;          /*!< Input channel operation mode */
    /* single mode parameters */
    PWM_InputSingleEdgeDetectType detectType;   /*!< In single mode, channel edge detect type */
    PMW_InputEventPscType eventPsc;             /*!< Event prescaler */
    /* dual mode parameters */
    PWM_InputDualEdgeMeasureType measureType;   /*!< In dual mode, channel measure type */
    PMW_DualOnceModeType onceMode;              /*!< Enable/disable Continuous mode */
    /* General parameters */
    ACTION_Type filterEn;                       /*!< Enable/disable Input capture filter */
    uint8_t filterValue;                        /*!< Filter value */
    ACTION_Type interruptEn;                    /*!< Enable/disable channel interrupt */
} PWM_inputChConfigType;

/*!
* @brief PWM input capture mode configuration structure.
*/
typedef struct
{
    uint8_t channelNum;                     /*!< Number of output compare channels */
    PWM_inputChConfigType *channelConfig;   /*!< Input capture channels configuration */
} PWM_inputCaptureConfigType;

/*******************************************************************************
* PWM_MODE_QUADRATURE_DECODER: PWM quadrature decode mode type definition
*******************************************************************************/
/*!
 * @brief PWM quadrature decode mode enumeration.
 */
typedef enum
{
    PWM_QUAD_PHASE_ENCODE = 0,  /*!< Phase encoding mode */
    PWM_QUAD_COUNT_DIR,         /*!< Counter and direction encoding mode */
} PWM_QuadDecodeModeType;

/*!
 * @brief PWM quadrature phase polarity enumeration.
 */
typedef enum
{
    PWM_QUAD_PHASE_NORMAL = 0,  /*!< Phase input signal is not inverted */
    PWM_QUAD_PHASE_INVERT,      /*!< Phase input signal is inverted */
} PWM_QuadPhasePolarityType;

/*!
 * @brief PWM quadrature decoder phase input configuration structure.
 *
 */
typedef struct
{
    PWM_QuadPhasePolarityType polarity;     /*!< PhaseA Polarity */
    ACTION_Type filterEn;                   /*!< Filter Enable */
    uint8_t filterVal;                      /*!< Filter value */
} PWM_QuadPhaseConfigType;

/*!
 * @brief PWM quadrature configuration structure.
 */
typedef struct
{
    PWM_QuadDecodeModeType mode;                /*!< PWM quadrature decode mode */
    PWM_QuadPhaseConfigType phaseAConfig;       /*!< PhaseA config */
    PWM_QuadPhaseConfigType phaseBConfig;       /*!< PhaseB config */
    ACTION_Type quadEn;                         /*!< Enable quadrature decode mode */
} PWM_QuadDecodeConfigType;

/*******************************************************************************
* PWM general type definition
*******************************************************************************/
/*!
 * @brief PWM clock source enumeration.
 */
typedef enum
{
    PWM_CLK_SOURCE_NONE = 0,    /*!< No clock selected, in effect disables the counter */
    PWM_CLK_SOURCE_APB,         /*!< APB clock */
    PWM_CLK_SOURCE_HSI,         /*!< HSI clock */
    PWM_CLK_SOURCE_MAX,         /*!< Invalid clock */
} PWM_ClkSourceType;

/*!
* @brief PWM operation mode enumeration.
*/
typedef enum
{
    PWM_MODE_MODULATION = 0,        /*!< Pulse width modulation module mode */
    PWM_MODE_INPUT_CAPTURE,         /*!< Input capture mode */
    PWM_MODE_OUTPUT_COMPARE,        /*!< Output compare mode */
    PWM_MODE_QUADRATURE_DECODER,    /*!< Quadrature decoder mode */
    PWM_MODE_GENERAL_TIMER,         /*!< General timer mode */
} PWM_OperateModeType;

/*!
* @brief PWM simply configuration structure.
*/
typedef struct
{
    PWM_ChannelCombineType allChCombineMode;/*!< All Channel combine mode */
    PWM_CountModeType countMode;            /*!< Counting mode */
    PWM_OutputLevelModeType levelMode;      /*!< Output level mode */
    PWM_ClkSourceType clkSource;            /*!< Select clock source */
    uint16_t clkPsc;                        /*!< Clock prescaler */
    uint16_t initValue;                     /*!< Initial counter value */
    uint16_t maxValue;                      /*!< Maximum counter value */
    uint16_t chValue[PWM_CHANNEL_MAX];      /*!< Channel channel value */
    ACTION_Type complementEn;               /*!< Enable/disable complementary output */
    PWM_OutputPolarityActiveType oddPolarity; /*!< Odd channel the polarity of the channel output */
    PWM_OutputPolarityActiveType evenPolarity;/*!< Even channel the polarity of the channel output */
    PWM_OutputLevelType oddInitLevel;       /*!< Odd channel init output level */
    PWM_OutputLevelType evenInitLevel;      /*!< Even channel init output level */
    ACTION_Type initChOutputEn;             /*!< Enable/disable Initial channel output */
    uint8_t deadtime;                       /*!< Dead time value  */
    PWM_DeadtimePscType deadtimePsc;        /*!< Dead time prescaler value */
    ACTION_Type overflowInterrupEn;         /*!< Enable/disable CNTOF interrupt */
    ACTION_Type interruptEn;                /*!< Enable/disable pwm interrupt  */
    DeviceCallback_Type callBack;           /*!< Callback pointer */
} PWM_SimplyConfigType;

/*!
* @brief PWM configuration structure.
*/
typedef struct
{
    PWM_OperateModeType mode;       /*!< Select pwm operation mode */
    void *initModeStruct;           /*!< Pointer to the corresponding initialization structure for different modes */
    PWM_ClkSourceType clkSource;    /*!< Select clock source */
    uint16_t clkPsc;                /*!< Clock prescaler */
    uint16_t initValue;             /*!< Initial counter value */
    uint16_t maxValue;              /*!< Maximum counter value */
    ACTION_Type overflowInterrupEn; /*!< Enable/disable CNTOF interrupt */
    uint8_t cntOverflowFreq;        /*!< CNTOF frequency */
    ACTION_Type interruptEn;        /*!< Enable/disable pwm interrupt  */
    DeviceCallback_Type callBack;   /*!< Callback pointer */
} PWM_ConfigType;

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief PWM initialize.
*
* @param[in] PWMx: pwm module
            - PWM0
            - PWM1
* @param[in] config: pointer to configuration structure
* @return none
*/
void PWM_Init(PWM_Type *PWMx, const PWM_ConfigType *config);

/*!
* @brief PWM De-initialize.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return none
*/
void PWM_DeInit(PWM_Type *PWMx);

/*!
* @brief Init fault control.
*
* Use only in PWM Modulation mode,Called after PWM_Init function
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] config: pointer to configuration structure
* @return none
*/
void PWM_InitFaultControl(PWM_Type *PWMx, const PWM_FaultConfig *config);

/*!
* @brief Init synchronization control function.
*
* Use only in PWM Modulation mode,Called after PWM_Init function
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] config: pointer to configuration structure
* @return none
*/
void PWM_InitSyncControl(PWM_Type *PWMx, const PWM_SyncConfigType *config);

/*!
* @brief PWM simply initialize.
*
* Easy for users to simply configure
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @param[in] config: pointer to configuration structure
* @return none
*/
void PWM_SimplyInit(PWM_Type *PWMx, const PWM_SimplyConfigType *config);

/*!
* @brief Set pwm callback function.
*
* @param[in] PWMx: pwm module
                - PWM0
                - PWM1
* @return none
*/
void PWM_SetCallback(PWM_Type *PWMx, const DeviceCallback_Type func);

#ifdef __cplusplus
}
#endif

#endif /* _AC780X_PWM_H */

/* =============================================  EOF  ============================================== */
