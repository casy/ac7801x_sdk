/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_GPIO_REG_H
#define _AC780X_GPIO_REG_H
/*!
* @file ac780x_gpio_reg.h
*
* @brief GPIO access register inline function definition.
*
*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ===========================================  Includes  =========================================== */
#include "ac780x_gpio.h"
#include "stdint.h"


/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */

/* ======================================  Functions define  ======================================== */
/*!
* @brief Set PMUXx and PFSELx with functionx
*
* @param[in] PMUXx: PMUX register pointer,x can be 0 to 4
* @param[in] PFSELx: PFSEL index, value can be 0 to 9
* @param[in] functionx: Function index, value can be 0 to 3
* @return none
*/
#define PMUX_SetPMUXxPFSELx(PMUXx, PFSELx, functionx) MODIFY_REG32((*(uint32_t *)(PMUXx)), PMUX_PINMUX_Msk(PFSELx), PMUX_PINMUX_Pos(PFSELx), (functionx))

/*!
* @brief Get PMUX value
*
* @param[in] PMUXx: PMUX register pointer,x can be 0 to 4
* @return PMUX value
*/
#define PMUX_GetPMUXxPFSEL(PMUXx) (*(uint32_t *)(PMUXx))

/*!
* @brief Set GPIO Configuration and Mode
*
* @param[in] GPIOx: GPIO type pointer,x can be A.B.C
* @param[in] GPIO_Pin: pin index, value can be 0-15
* @param[in] dir: gpio pin direction
*                     1: output mode
*                     0: input mode
* @return none
*/
#define GPIO_SetGPIOxMODE(GPIOx, GPIO_Pin, dir) MODIFY_REG32((GPIOx)->CR, GPIO_CR_Msk(GPIO_Pin),  GPIO_CR_Pos(GPIO_Pin), (dir))

/*!
* @brief Get GPIO Configuration and Mode value
*
* @param[in] GPIOx: GPIO type pointer,x can be A.B.C
* @return CR value
*/
#define GPIO_GetGPIOxCR(GPIOx) ((GPIOx)->CR)

/*!
* @brief Get GPIO IDR register value
*
* @param[in] GPIOx: GPIO type pointer,x can be A.B.C
* @return IDR register value
*/
#define GPIO_GetGPIOxIDR(GPIOx) ((GPIOx)->IDR)

/*!
* @brief Set GPIO ODR register value
*
* @param[in] GPIOx: GPIO type pointer,x can be A.B.C
* @param[in] GPIO_Pin: pin index, value can be 0 to 15
* @param[in] pinVal: Pin level
*                   -GPIO_LEVEL_LOW
*                   -GPIO_LEVEL_HIGH
* @return none
*/
#define GPIO_SetGPIOxODRx(GPIOx,  GPIO_Pin, pinLevel) MODIFY_REG32((GPIOx)->ODR, GPIO_ODR_Msk(GPIO_Pin),  GPIO_ODR_Pos(GPIO_Pin), (pinLevel)) 

/*!
* @brief Get GPIO ODR register value
*
* @param[in] GPIOx: GPIO type pointer,x can be A.B.C
* @return ODR register value
*/
#define GPIO_GetGPIOxODR(GPIOx)  ((GPIOx)->ODR)

/*!
* @brief Set GPIO PD register value
*
* @param[in] GPIOx: GPIO type pointer,x can be A.B.C
* @param[in] GPIO_Pin: pin index, value can be 0 to 15
* @param[in] PdValue: GPIO pull down select. 1:Select   0:No select
* @return none
*/
#define GPIO_SetGPIOxPD(GPIOx, GPIO_Pin, PdValue) MODIFY_REG32((GPIOx)->PD, GPIO_PD_Msk(GPIO_Pin), GPIO_PD_Pos(GPIO_Pin), (PdValue))

/*!
* @brief Set GPIO PU register value
*
* @param[in] GPIOx: GPIO type pointer,x can be A.B.C
* @param[in] GPIO_Pin: pin index, value can be 0 to 15
* @param[in] PuValue: GPIO pull up select. 1:Select   0:No select
* @return none
*/
#define GPIO_SetGPIOxPU(GPIOx, GPIO_Pin, PuValue) MODIFY_REG32((GPIOx)->PU, GPIO_PU_Msk(GPIO_Pin), GPIO_PU_Pos(GPIO_Pin), (PuValue))

/*!
* @brief Set GPIO BSRR register value
*
* @param[in] GPIOx: GPIO type pointer,x can be A.B.C
* @param[in] GPIO_Pin: pin index,value can be 0 to 15
* @return none
*/
#define GPIO_SetGPIOxBSRRx(GPIOx, GPIO_Pin) SET_BIT32((GPIOx)->BSRR, GPIO_BSRR_BS_Msk(GPIO_Pin))

/*!
* @brief Set GPIO BRR register value
*
* @param[in] GPIOx: GPIO type pointer,x can be A.B.C
* @param[in] GPIO_Pin: pin index, value can be 0 to 15
* @return none
*/
#define GPIO_SetGPIOxBRRx(GPIOx, GPIO_Pin) SET_BIT32((GPIOx)->BRR, GPIO_BRR_BR_Msk(GPIO_Pin))

/*!
* @brief Set GPIO E4E2 register value
*
* @param[in] GPIOx: GPIO type pointer,x can be A.B.C
* @param[in] GPIO_Pin: pin index, value can be 0 to 15
* @param[in] driving: output driver level,value can be 0 to 3
* @return none
*/
#define GPIO_SetGPIOxE4E2x(GPIOx, GPIO_Pin, driving) MODIFY_REG32((GPIOx)->E4_E2, GPIO_E4E2_Msk(GPIO_Pin), GPIO_E4E2_Pos(GPIO_Pin), (driving))


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _AC780X_GPIO_REG_H */

/* =============================================  EOF  ============================================== */
