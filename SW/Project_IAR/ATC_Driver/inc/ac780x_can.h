/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef __AC780X_CAN_H
#define __AC780X_CAN_H
/*!
* @file ac780x_can.h
*
* @brief This file provides CAN integration functions interface.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x.h"
#include "ac780x_gpio.h"

/* ============================================  Define  ============================================ */
/*!
* @brief CAN instance index macro
*/
#define CAN_INDEX(CANx)                 ((uint8_t)(((uint32_t)(CANx) - CAN0_BASE) >> 10) )

/* @brief CAN filter macro */
#define CAN_MAX_FILTER_NUM              (16U)

/* @brief CAN interrupt enable all interrupt macro */
#define CAN_IRQ_ALL_ENABLE_MSK          (CAN_CTRL1_EIE_Msk  | CAN_CTRL1_TSIE_Msk | CAN_CTRL1_TPIE_Msk |\
                                        CAN_CTRL1_RAFIE_Msk | CAN_CTRL1_RFIE_Msk | CAN_CTRL1_ROIE_Msk |\
                                        CAN_CTRL1_RIE_Msk   | CAN_CTRL1_EPIE_Msk | CAN_CTRL1_ALIE_Msk | CAN_CTRL1_BEIE_Msk)

/* @brief CAN transmit buffer full macro */
#define CAN_TRANSMIT_BUFFER_FULL        (3UL)

/* ===========================================  Typedef  ============================================ */
typedef struct _CAN_MSG_INFO
{
    uint32_t    ID;                                /*!< CAN identifier */
    uint32_t    RTS;                               /*!< Receive time stamps */
    uint8_t     ESI;                               /*!< Transmit time-stamp enable or error state indicator */
    uint8_t     DLC;                               /*!< Data length code */
    uint8_t     BRS;                               /*!< Bit rate switch */
    uint8_t     FDF;                               /*!< FD format indicator */
    uint8_t     RTR;                               /*!< Remote transmission request */
    uint8_t     IDE;                               /*!< Identifier extension */
    uint8_t     *DATA;                             /*!< Data */
} CAN_MsgInfoType;

/*!< Bitrate = CAN_CLOCK / ((S_PRESC + 1) * (1 + (S_SEG_1 + 1) + (S_SEG_2 + 1))) */
typedef struct
{
    uint8_t PRESC;                                  /*!< Prescaler */
    uint8_t SEG_1;                                  /*!< Bit Timing Segment 1 */
    uint8_t SEG_2;                                  /*!< Bit Timing Segment 2 */
    uint8_t SJW;                                    /*!< Synchronization Jump Width */
} CAN_BitrateConfigType;

typedef struct
{
    uint8_t index;                                  /*!< Filter index */
    ACTION_Type enable;                             /*!< Enable or disable */
    uint32_t code;                                  /*!< Code data */
    uint32_t mask;                                  /*!< Mask data */
} CAN_FilterControlType;

typedef enum
{
    CAN_BITRATE_1M = 0,
    CAN_BITRATE_800K,
    CAN_BITRATE_500K,
    CAN_BITRATE_250K,
    CAN_BITRATE_125K,
    CAN_BITRATE_100K,
    CAN_BITRATE_50K,
    CAN_BITRATE_20K,
    CAN_BITRATE_10K,
    CAN_BITRATE_5K,
    CAN_BITRATE_NUM,
} CAN_BitrateType;                                  /*!< CAN normal bitrate */

typedef enum
{
    CAN_DBITRATE_50K = 0,
    CAN_DBITRATE_100K,
    CAN_DBITRATE_1M,
    CAN_DBITRATE_2M,
    CAN_DBITRATE_4M,
    CAN_DBITRATE_6M,
    CAN_DBITRATE_8M,
    CAN_DBITRATE_NUM,
} CAN_DBitrateType;                                 /*!< CAN FD data bitrate */

typedef enum
{
    CAN_MODE_NORMAL,                                /*!< Normal mode */
    CAN_MODE_MONITOR,                               /*!< Listen only mode */
    CAN_MODE_LOOPBACK_INTERNAL,                     /*!< Loopback internal mode */
    CAN_MODE_LOOPBACK_EXTERNAL                      /*!< Loopback external mode */
} CAN_ModeType;

typedef enum
{
    CAN_CLKSRC_AHB,                                 /*!< Clcok source from AHB bus */
    CAN_CLKSRC_EXTERNAL_OSC,                        /*!< Clcok source from external OSC */
} CAN_ClkSrcType;

typedef enum
{
    CAN_DLC_12_BYTES = 9UL,
    CAN_DLC_16_BYTES,
    CAN_DLC_20_BYTES,
    CAN_DLC_24_BYTES,
    CAN_DLC_32_BYTES,
    CAN_DLC_48_BYTES,
    CAN_DLC_64_BYTES,
} CAN_DlcType;


typedef enum
{
    CAN_TRANSMIT_PRIMARY = 0,                       /*!< Transmit promary mode */
    CAN_TRANSMIT_SECONDARY,                         /*!< Transmit secondary mode */
} CAN_TransmitBufferType;

typedef enum
{
    CAN_TRANSMIT_ALL = 0,                           /*!< Transmit all message */
    CAN_TRANSMIT_ONE,                               /*!< Transmit one message */
    CAN_TRANSMIT_AMOUNT_MAX,                        /*!< Invalid transmit amount */
} CAN_TransmitAmountType;

typedef enum
{
    CAN_TSMODE_FIFO = 0,                            /*!< Transmit fifo mode */
    CAN_TSMODE_PRIORITY,                            /*!< Transmit priority mode */
} CAN_TransmitModeType;

typedef enum
{
    CAN_RECV_OVER_WRITE = 0,                        /*!< Oldest message will be overwrite */
    CAN_RECV_DISCARD,                               /*!< New message will not be store */
} CAN_OverflowModeType;

typedef enum
{
    CAN_TIME_STAMP_SOF = 0,                         /*!< TIME-STAMPing position SOF */
    CAN_TIME_STAMP_EOF,                             /*!< TIME-STAMPing position EOF */
}CAN_TimeStampPosType;

typedef enum
{
    CAN_CTRL_DATA_FRAME = 0,                        /*!< Data frame */
    CAN_CTRL_REMOTE_FRAME,                          /*!< Remote frame */
}CAN_CtrlFrameType;

typedef struct
{
    uint8_t filterNum;                              /*!< Set filter number */
    uint8_t errorWarningLimit;                      /*!< programmable error warning limit*/
    CAN_ModeType canMode;                           /*!< CAN mode */
    CAN_ClkSrcType clockSrc;                        /*!< CAN clock source */
    CAN_TransmitModeType tsMode;                    /*!< Transmit buffer secondary operation mode */
    CAN_TransmitAmountType tsAmount;                /*!< Transmit secondary: CAN_TRANSMIT_ALL->all frames; CAN_TRANSMIT_ONE->one frame */
    ACTION_Type interruptEn;                        /*!< Interrupt enable */
    ACTION_Type tpss;                               /*!< Transmission primary single shot mode for PTB */
    ACTION_Type tsss;                               /*!< Transmission secondary single shot mode for STB */
    ACTION_Type timeStampEn;                        /*!< Time stamp enable */
    CAN_TimeClockDividerType timeStampClk;          /*!< Time stamp clock */
    CAN_TimeStampPosType timeStampPos;              /*!< Time stamp position  */
    CAN_OverflowModeType rom;                       /*!< Receive buffer overflow mode */
    ACTION_Type selfAckEn;                          /*!< Self-Acknowledge enable(when LBME=1) */
    ACTION_Type fdModeEn;                           /*!< FD enable */
    ACTION_Type fdIsoEn;                            /*!< FD ISO mode */
    ACTION_Type tdcEnable;                          /*!< TDC enable (when data bitrate >= 1Mbps) */
    uint8_t sspOffset;                              /*!< SSP offset */
    uint32_t interruptMask;                         /*!< Interrupt enable mask */
    CAN_BitrateConfigType *normalBitrate;           /*!< Normal bitrate setting */
    CAN_BitrateConfigType *dataBitrate;             /*!< Data bitrate setting */
    CAN_FilterControlType *filterList;              /*!< Filter controller list */
    DeviceCallback_Type callback;                   /*!< CAN callback pointer */
} CAN_ConfigType;

/* ====================================  Functions declaration  ===================================== */
/*!
 * @brief Init CANx (GPIO, clock, bitrate, filter and interrupt setting)
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] config: CAN config
 * @return none
 */
void CAN_Init(CAN_Type *CANx, CAN_ConfigType *config);

/*!
 * @brief Uninitialize CANx
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return none
 */
void CAN_DeInit(CAN_Type *CANx);

/*!
 * @brief Transmit CANx message
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] info: CAN message information
 * @param[in] type: CAN transmit type
                - CAN_TRANSMIT_PRIMARY
                - CAN_TRANSMIT_SECONDARY
 * @return 0: success, -1: busy, -2: timeout, -3: send error, -4: Transmit timeout
 */
int32_t CAN_TransmitMessage(CAN_Type *CANx, const CAN_MsgInfoType *info, CAN_TransmitBufferType type);

/*!
 * @brief set transmit amount for can secondary buffer
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] amount: CAN transmit amount
                - CAN_TRANSMIT_ALL
                - CAN_TRANSMIT_ONE
 * @return none
 */
void CAN_SetTransmitAmount(CAN_Type *CANx, CAN_TransmitAmountType amount);

/*!
* @brief Receive CANx message
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] info: CAN message information
 * @return 0: success, 1: no message
 */
int32_t CAN_ReceiveMessage(CAN_Type *CANx, CAN_MsgInfoType *info);

/*!
 * @brief Set CANx filter when RESET = 1
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] index:CAN filter id
                -: (0-15)
 * @param[in] code: CAN filter code
 * @param[in] mask: CAN filter mask
 * @param[in] state: enable state
                - ENDBLE
                - DISDBLE
 * @return 0: success, 1: index error or reset error
 */
int32_t CAN_SetFilter(CAN_Type *CANx, uint8_t index, uint32_t code, uint32_t mask, ACTION_Type state);

/*!
 * @brief Get CAN error flag
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return error flag
 */
int32_t CAN_GetError(CAN_Type *CANx);

/*!
 * @brief Wait for transmitting done in primary mode; or transmission buffer not full in secondary mode, otherwise timeout
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] type: CAN transmit type
                - CAN_TRANSMIT_PRIMARY
                - CAN_TRANSMIT_SECONDARY
 * @return Transmit idle (flag 0: idle, 1: timeout)
 */
int32_t CAN_WaitTransmissionIdle(CAN_Type *CANx, CAN_TransmitBufferType type);

/*!
 * @brief Wait transmit done
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] type: CAN transmit type
                - CAN_TRANSMIT_PRIMARY
                - CAN_TRANSMIT_SECONDARY
 * @return Transmit flag (0: done, 1: timeout)
 */
int32_t CAN_WaitTransmissionDone(CAN_Type *CANx, CAN_TransmitBufferType type);

/*!
 * @brief Set message information
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] info: CAN message information
 * @param[in] type: CAN transmit type
                - CAN_TRANSMIT_PRIMARY
                - CAN_TRANSMIT_SECONDARY
 * @return none
 */
void CAN_SetMsgInfo(CAN_Type *CANx, const CAN_MsgInfoType *info, CAN_TransmitBufferType type);

/*!
 * @brief Get payload size
 *
 * @param[in] dlcValue: DLC value
 * @return ret : payload size
 */
uint8_t CAN_GetPayloadSize(uint8_t dlcValue);

/*!
 * @brief Set call back function
 *
 * @param[in] CANx: CAN type pointer
 * @param[in] callbackFunc: Event call back function
 */
void CAN_SetCallBack(CAN_Type *CANx, DeviceCallback_Type callbackFunc);

#ifdef __cplusplus
}
#endif

#endif /* __AC780X_CAN_H */

/* =============================================  EOF  ============================================== */
