/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef __AC780X_ECC_SRAM_REG_H
#define __AC780X_ECC_SRAM_REG_H
/*!
* @file ac780x_ecc_sram_reg.h
*
* @brief ECC_SRAM access register inline function definition.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x_ecc_sram.h"

/* ======================================  Functions define  ======================================== */
/*!
* @brief Clear ECC_SRAM 2 bit error interrupt flag
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void ECC_SRAM_ClearInterrupFlag(void)
{
    ECC_SRAM->CTRL |= ECC_SRAM_CTRL_ERR2_STATUS_Msk;
}

/*!
* @brief Get ECC_SRAM 2 bit error interrupt flag
*
* @param[in] none
* @return 2 bit error flag(0: no error, 1: 2 bit error)
*/
__STATIC_INLINE uint32_t ECC_SRAM_GetInterruptFlag(void)
{
    return (READ_BIT32(ECC_SRAM->CTRL, ECC_SRAM_CTRL_ERR2_STATUS_Msk) >> ECC_SRAM_CTRL_ERR2_STATUS_Pos);
}

/*!
* @brief Get ECC_SRAM 1 bit Error correction happened address
*
* @param[in] none
* @return ECC_SRAM 1 bit error address
*/
__STATIC_INLINE uint32_t ECC_SRAM_Get1BitErrAddr(void)
{
    return (READ_BIT32(ECC_SRAM->ERR1_ADDR, ECC_SRAM_ERR1_ADDR_ERR1_ADDR_Msk) >> ECC_SRAM_ERR1_ADDR_ERR1_ADDR_Pos) ;
}

/*!
* @brief Get ECC_SRAM 2 bit error correction happened address
*
* @param[in] none
* @return ECC_SRAM 2 bit error address
*/
__STATIC_INLINE uint32_t ECC_SRAM_Get2BitErrAddr(void)
{
    return (READ_BIT32(ECC_SRAM->ERR2_ADDR, ECC_SRAM_ERR2_ADDR_ERR2_ADDR_Msk) >> ECC_SRAM_ERR2_ADDR_ERR2_ADDR_Pos);
}

/*!
* @brief Get ECC_SRAM error status
*
* @param[in] none
* @return ECC_SRAM error status(0: no error, 1: 2 bit error, 2/3: 1 bit error)
*/
__STATIC_INLINE uint32_t ECC_SRAM_GetErrStatus(void)
{
    return ((ECC_SRAM->CTRL & ECC_SRAM_CTRL_ERR_STATUS_Msk) >> ECC_SRAM_CTRL_ERR_STATUS_Pos);
}

/*!
* @brief ECC_SRAM error status clear
*
* @param[in] none
* @return none
*/
__STATIC_INLINE void ECC_SRAM_ClearErrStatus(void)
{
    ECC_SRAM->CTRL |= ECC_SRAM_CTRL_ERR_STATUS_Msk;
}

/*!
* @brief Set ECC_SRAM 2 bit error interrupt
*
* @param[in] state: Enable state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ECC_SRAM_SetInterrupt(ACTION_Type state)
{
    MODIFY_REG32(ECC_SRAM->CTRL, ECC_SRAM_CTRL_ERR2_IRQEN_Msk, ECC_SRAM_CTRL_ERR2_IRQEN_Pos, state);
}

/*!
* @brief Enable/Disable ECC_SRAM 2 bit error reset
*
* @param[in] state: Enable state
                - ENABLE
                - DISABLE
* @return none
*/
__STATIC_INLINE void ECC_SRAM_Enable2BitErrReset(ACTION_Type state)
{
    MODIFY_REG32(CKGEN->CTRL, CKGEN_RESET_CTRL_ECC2_RST_EN_Msk, CKGEN_RESET_CTRL_ECC2_RST_EN_Pos, state);
}

#ifdef __cplusplus
}
#endif

#endif /* _AC780X_ECC_SRAM_REG_H */

/* =============================================  EOF  ============================================== */
