/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef AC780X_EFLASH_H
#define AC780X_EFLASH_H
/*!
* @file ac780x_eflash.h
*
* @brief This file provides EFLASH integration functions interface.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x.h"

/* ============================================  Define  ============================================ */
#define EFLASH_BASE_ADDRESS                 (0x08000000UL)                      /*!< Based address of eflash user area */
#define EFLASH_SIZE                         (0x00020000UL)                      /*!< Total size for eflash user area */
#define EFLASH_PAGE_SIZE                    (0x00000800UL)                      /*!< eflash page size */
#define EFLASH_END_ADDRESS                  (EFLASH_BASE_ADDRESS+EFLASH_SIZE)   /*!< End address of eflash user area */
#define EFLASH_PAGE_AMOUNT                  (EFLASH_SIZE/EFLASH_PAGE_SIZE)      /*!< eflash page total page amount */

#define OPTION_BASE                         (0x08040000UL)                      /*!< eFlash - Optoion byte base addresses */
#define OPTION_END                          (0x08040030UL)                      /*!< eFlash - Optoion byte end addresses */

#define EFLASH_PAGE_ERASE_TIMEOUT           (0x00A00000UL)                      /*!< eflash page ease time out */
#define EFLASH_MASS_ERASE_TIMEOUT           (0x00A00000UL)                      /*!< eflash mass ease time out */
#define EFLASH_PAGE_PROGRAM_TIMEOUT         (0x00004000UL)                      /*!< eflash page program time out */
/* ===========================================  Typedef  ============================================ */
typedef enum
{
    EFLASH_CMD_IDLE = 0,                /*!< Idle command */
    EFLASH_CMD_PAGERASE,                /*!< Page erase command */
    EFLASH_CMD_MASSRASE,                /*!< Mass erase command. */
    EFLASH_CMD_PAGEPROGRAM,             /*!< Page program command */
    EFLASH_CMD_PAGERASEVERIFY,          /*!< Page erase verify command */
    EFLASH_CMD_MASSRASEVERIFY,          /*!< Mass erase verify command */
    EFLASH_CMD_PROTECTERASE,            /*!< Protect byte page erase command */
    EFLASH_CMD_PROTECTROGRAM,           /*!< Protect byte page program command */
    EFLASH_CMD_INVALID,                 /*!< Invalid command */
}EFLASH_CmdType;                        /*!< eflash command */

typedef enum
{
    EFLASH_STATUS_ACK = 1,              /*!< Acknowledge status */
    EFLASH_STATUS_SUCCESS,              /*!< Success status */
    EFLASH_STATUS_CMD_INVALID,          /*!< Invalid command status */
    EFLASH_STATUS_BUSY,                 /*!< Error status for busy */
    EFLASH_STATUS_READOUT_PROTECT,      /*!< Error status for read protect */
    ELFASH_STATUS_WRITE_PROTECT,        /*!< Error status for write protect */
    EFLASH_STATUS_PARAMETER_ERROR,      /*!< Error status for parameter */
    EFLASH_STATUS_WRER_ERROR,           /*!< Error status for the operation violates the write protection rules */
    EFLASH_STATUS_RDER_ERROR,           /*!< Error status for the operation violates the read protection rules */
    EFLASH_STATUS_PPERER_ERROR,         /*!< Error status for permission error of commands operation */
    EFLASH_STATUS_PPADRER_ERROR,        /*!< Error status for the page program command operation */
    EFLASH_STATUS_ERAER_ERROR,          /*!< Error status for the erase command operation */
    EFLASH_STATUS_VRER_ERROR,           /*!< Error status for verify command operation */
    EFLASH_STATUS_OPTER_ERROR,          /*!< Error status for option byte */
    EFLASH_STATUS_ALLOCMM_ERROR,        /*!< Error status for allocate memory */
    EFLASH_STATUS_TIMEOUT,              /*!< Error status for time */
    EFLASH_STATUS_ADDRESS_ERROR,        /*!< Error status for eflash address */
    EFLASH_STATUS_PVDWARNING_ERROR,     /*!< Error status for pvd warning */
    EFLASH_STATUS_INVALID,              /*!< Invalid status */
} EFLASH_StatusType;                    /*!< eflash status */

/* ====================================  Functions declaration  ===================================== */
/*!
 * @brief Unlock the eflash control
 *
 * @param[in] none
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_UnlockCtrl(void);

/*!
 * @brief Lock the eflash control
 *
 * @param[in] none
 * @return none
 */
void EFLASH_LockCtrl(void);

/*!
 * @brief Waiting for the end of all operation
 *
 * @param[in] timeout: Time out value
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_WaitForLastOperation(uint32_t timeout);

/*!
 * @brief Waiting for the end of the operation
 *
 * @param[in] timeout: Time out value
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_WaitEop(uint32_t timeout);

/*!
 * @brief Read double word from the flash specified address
 *
 * @param[in] eflashAddr: Specified address
 * @return EFLASH STATUS: eflash operation status
 */
uint32_t EFLASH_ReadDWord(uint32_t eflashAddr);

/*!
 * @brief Read a certain amount of data from the flash specified address
 *
 * @param[in] readAddr: Eflash address
 * @param[in] dataBuffer: Data buffer
 * @param[in] numbToRead: Number of DWORD
 * @return none
 */
void EFLASH_Read(uint32_t readAddr, uint32_t *dataBuffer, uint32_t numbToRead);

/*!
 * @brief Mass erase elfash user area
 *
 * @param[in] none
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_MassErase(void);

/*!
 * @brief User page program
 *
 * @param[in] writeAddress: Specified eflash addrees to be programed
 * @param[in] dataBuffer: Point to the data to be programed
 * @param[in] dataLength: Data length to be programed
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_PageProgram(uint32_t writeAddress, uint32_t *dataBuffer, uint32_t dataLength);

/*!
 * @brief Set write protect
 *
 * @param[in] startPageNumber: Page to be changed write protect
 * @param[in] pageLength: write protect page amount
 * @param[in] state: enable state
                -: ENDBLE
                -: DISDBLE
 *
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_SetWriteProtect(uint32_t startPageNumber, uint32_t pageLength ,ACTION_Type state);

/*!
* @brief config hardware watch.
*
* @param[in] state
                - ENABLE: enalbe hardware watchdog
                - DISABLE: disable hardware watchdog
* @return EFLASH STATUS: eflash operation status
*/
EFLASH_StatusType EFLASH_ConfigWdg(ACTION_Type state);

/*!
 * @brief Verify whether the mass erase operation is performed successfully
 *
 * @param[in] none
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_MassEraseVerify(void);

/*!
 * @brief Verify whether the page erase operation is performed successfully
 *
 * @param[in] pageAddress: Specified eflash addrees to be verified
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_PageEraseVerify(uint32_t pageAddress);

/*!
 * @brief Erase specified eflash user area address
 *
 * @param[in] pageAddress: Specified eflash addrees to be erased
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_PageErase(uint32_t pageAddress);

/*!
 * @brief Enable EFLASH read protection
 *
 * This API is used to set(enable) EFLASH read protection. Set(enable) read
 * protection will take effect after reset and the all EFLASH main memory(user
 * pages) will return with 0xAAAAAAAA when read by JTAG/SWD. EFLASH main memory
 * can not be erased or programed by code/JTAG/SWD when in read protection status.
 *
 * @param[in] none
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_EnableReadOut(void);

/*!
 * @brief Disable EFLASH read protection
 *
 * This API is used to clear(disable) EFLASH read protection. Clear(disable) read
 * protection will take effect after reset and the EFLASH main memory(user pages)
 * will be erased to 0xFFFFFFFF automatically.
 *
 * @param[in] none
 * @return EFLASH STATUS: eflash operation status
 */
EFLASH_StatusType EFLASH_DisableReadOut(void);

#ifdef __cplusplus
}
#endif

#endif /* __AC780X_EFLASH_H */

/* =============================================  EOF  ============================================== */
