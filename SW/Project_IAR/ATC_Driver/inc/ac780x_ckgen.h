/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef _AC780X_CKGEN_H
#define _AC780X_CKGEN_H
/*!
* @file ac780x_ckgen.h
*
* @brief This file provides clock generator module integration functions interfaces.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x.h"

/* ============================================  Define  ============================================ */

/* ===========================================  Typedef  ============================================ */

/*!
* @brief clock module index enumeration
*/
typedef enum
{
    /* PERI_CLK_EN_0 */
    CLK_UART0 = 0,
    CLK_UART1,
    CLK_UART2,
    CLK_RESERVE3,
    CLK_RESERVE4,
    CLK_RESERVE5,
    CLK_SPI0,
    CLK_SPI1,
    CLK_I2C0,
    CLK_I2C1,
    CLK_PWDT0,
    CLK_PWM0,
    CLK_PWM1,
    CLK_RESERVE13,
    CLK_RESERVE14,
    CLK_RESERVE15,
    CLK_RESERVE16,
    CLK_RESERVE17,
    CLK_RESERVE18,
    CLK_TIMER,
    CLK_RTC,
    CLK_DMA0,
    CLK_RESERVE22,
    CLK_GPIO,
    CLK_RESERVE24,
    CLK_WDG,
    CLK_CRC,
    CLK_RESERVE27,
    CLK_CAN0,
    CLK_RESERVE29,
    CLK_RESERVE30,
    CLK_RESERVE31,

    /* PERI_CLK_EN_1 */
    CLK_RESERVE32 = 32 + 0,
    CLK_CTU,
    CLK_ADC0,
    CLK_ACMP0,
    CLK_RESERVE36,
    CLK_PWDT1,
    CLK_MODULE_NUM
}CKGEN_ClockType;

#define IS_CKGEN_CLOCK_PARA(CLOCK) (((CLOCK) == CLK_UART0) || \
                                    ((CLOCK) == CLK_UART1) || \
                                    ((CLOCK) == CLK_UART2) || \
                                    ((CLOCK) == CLK_SPI0)  || \
                                    ((CLOCK) == CLK_SPI1)  || \
                                    ((CLOCK) == CLK_I2C0)  || \
                                    ((CLOCK) == CLK_I2C1)  || \
                                    ((CLOCK) == CLK_PWDT0) || \
                                    ((CLOCK) == CLK_PWM0)  || \
                                    ((CLOCK) == CLK_PWM1)  || \
                                    ((CLOCK) == CLK_TIMER) || \
                                    ((CLOCK) == CLK_RTC)   || \
                                    ((CLOCK) == CLK_DMA0)   || \
                                    ((CLOCK) == CLK_GPIO)  || \
                                    ((CLOCK) == CLK_WDG)   || \
                                    ((CLOCK) == CLK_CRC)   || \
                                    ((CLOCK) == CLK_CAN0)  || \
                                    ((CLOCK) == CLK_CTU)   || \
                                    ((CLOCK) == CLK_ADC0)  || \
                                    ((CLOCK) == CLK_ACMP0) || \
                                    ((CLOCK) == CLK_PWDT1))


/*!
* @brief soft reset module index enumeration
*/
typedef enum
{
    /* PERI_SFT_RST1 */
    SRST_UART0 = 0,
    SRST_UART1,
    SRST_UART2,
    SRST_RESERVE3,
    SRST_RESERVE4,
    SRST_RESERVE5,
    SRST_SPI0,
    SRST_SPI1,
    SRST_I2C0,
    SRST_I2C1,
    SRST_PWDT0,
    SRST_PWM0,
    SRST_PWM1,
    SRST_RESERVE13,
    SRST_RESERVE14,
    SRST_RESERVE15,
    SRST_RESERVE16,
    SRST_RESERVE17,
    SRST_RESERVE18,
    SRST_TIMER,
    SRST_RTC,
    SRST_DMA0,
    SRST_RESERVE22,
    SRST_GPIO,
    SRST_RESERVE24,
    SRST_WDG,
    SRST_CRC,
    SRST_RESERVE27,
    SRST_CAN0,
    SRST_RESERVE29,
    SRST_RESERVE30,
    SRST_RESERVE31,

    /* PERI_SFT_RST2 */
    SRST_RESERVE32 = 32 + 0,
    SRST_CTU,
    SRST_ADC0,
    SRST_ACMP0,
    SRST_ANA_REG,
    SRST_PWDT1,
    SRST_MODULE_NUM
} CKGEN_SoftResetType;

#define IS_CKGEN_SOFT_RESET_PARA(RESET) (((RESET) == SRST_UART0) || \
                                    ((RESET) == SRST_UART1) || \
                                    ((RESET) == SRST_UART2) || \
                                    ((RESET) == SRST_SPI0)  || \
                                    ((RESET) == SRST_SPI1)  || \
                                    ((RESET) == SRST_I2C0)  || \
                                    ((RESET) == SRST_I2C1)  || \
                                    ((RESET) == SRST_PWDT0) || \
                                    ((RESET) == SRST_PWM0)  || \
                                    ((RESET) == SRST_PWM1)  || \
                                    ((RESET) == SRST_TIMER) || \
                                    ((RESET) == SRST_RTC)   || \
                                    ((RESET) == SRST_DMA0)  || \
                                    ((RESET) == SRST_GPIO)  || \
                                    ((RESET) == SRST_WDG)   || \
                                    ((RESET) == SRST_CRC)   || \
                                    ((RESET) == SRST_CAN0)  || \
                                    ((RESET) == SRST_CTU)   || \
                                    ((RESET) == SRST_ADC0)  || \
                                    ((RESET) == SRST_ACMP0) || \
                                    ((RESET) == SRST_PWDT1))

/*!
* @brief system clock source enumeration
*/
typedef enum
{
    SYSCLK_SRC_INTERNAL_OSC = 0,
    SYSCLK_SRC_PLL_OUTPUT,
    SYSCLK_SRC_EXTERNAL_OSC
} SYSTEM_ClockSourceType;

#define IS_CLOCK_SOURCE_PARA(SYSCLK) (((SYSCLK) == SYSCLK_SRC_INTERNAL_OSC) || \
                                      ((SYSCLK) == SYSCLK_SRC_PLL_OUTPUT)   || \
                                      ((SYSCLK) == SYSCLK_SRC_EXTERNAL_OSC))

/*!
* @brief PLL Post divider enumeration
*/
enum
{
    PLL_POSDIV_1 = 0,
    PLL_POSDIV_2 = 1,
    PLL_POSDIV_4 = 2,
    PLL_POSDIV_6 = 3,
    PLL_POSDIV_8 = 4,
    PLL_POSDIV_10 = 5,
    PLL_POSDIV_12 = 6,
    PLL_POSDIV_16 = 8,
    PLL_POSDIV_18 = 9,
    PLL_POSDIV_20 = 10,
    PLL_POSDIV_24 = 12,
    PLL_POSDIV_30 = 15
};

#define IS_PLL_POSDIV_PARA(POSDIV) (((POSDIV) == PLL_POSDIV_1) || \
                                    ((POSDIV) == PLL_POSDIV_2) || \
                                    ((POSDIV) == PLL_POSDIV_4) || \
                                    ((POSDIV) == PLL_POSDIV_6) || \
                                    ((POSDIV) == PLL_POSDIV_8) || \
                                    ((POSDIV) == PLL_POSDIV_10)|| \
                                    ((POSDIV) == PLL_POSDIV_12)|| \
                                    ((POSDIV) == PLL_POSDIV_16)|| \
                                    ((POSDIV) == PLL_POSDIV_18)|| \
                                    ((POSDIV) == PLL_POSDIV_20)|| \
                                    ((POSDIV) == PLL_POSDIV_24)|| \
                                    ((POSDIV) == PLL_POSDIV_30))

/*!
* @brief PLL Previous divider enumeration
*/
enum
{
    PLL_PREDIV_1 = 0,
    PLL_PREDIV_2 = 1,
    PLL_PREDIV_4 = 2
};

#define IS_PLL_PREDIV_PARA(PREDIV) (((PREDIV) == PLL_PREDIV_1) || \
                                    ((PREDIV) == PLL_PREDIV_2) || \
                                    ((PREDIV) == PLL_PREDIV_4))

/*!
* @brief system clock divide enumeration
*/
typedef enum
{
    SYSCLK_DIVIDER_1 = 0,
    SYSCLK_DIVIDER_2,
    SYSCLK_DIVIDER_3,
    SYSCLK_DIVIDER_4
} SYSCLK_DividerType;

#define IS_SYSCLK_DIVIDER_PARA(DIVIDER) (((DIVIDER) == SYSCLK_DIVIDER_1) || \
                                         ((DIVIDER) == SYSCLK_DIVIDER_2) || \
                                         ((DIVIDER) == SYSCLK_DIVIDER_3) || \
                                         ((DIVIDER) == SYSCLK_DIVIDER_4))

/*!
* @brief APBCLK clock divider enumeration
*/
typedef enum
{
    APBCLK_DIVIDER_1 = 0,
    APBCLK_DIVIDER_2,
    APBCLK_DIVIDER_3,
    APBCLK_DIVIDER_4
} APBCLK_DividerType;

#define IS_APBCLK_DIVIDER_PARA(DIVIDER) (((DIVIDER) == APBCLK_DIVIDER_1) || \
                                         ((DIVIDER) == APBCLK_DIVIDER_2) || \
                                         ((DIVIDER) == APBCLK_DIVIDER_3) || \
                                         ((DIVIDER) == APBCLK_DIVIDER_4))

/*!
* @brief PLL Reference enumeration
*/
typedef enum
{
    PLL_REF_INTERAL_OSC = 0,
    PLL_REF_EXTERNAL_OSC = 1
} PLL_ReferenceType;

#define IS_PLL_REFERENCE_PARA(REF) (((REF) == PLL_REF_INTERAL_OSC) || \
                                    ((REF) == PLL_REF_EXTERNAL_OSC))

/*!
* @brief CAN clock source enumeration
*/
typedef enum
{
    CAN_CLK_SEL_EXTERNAL_OSC = 0,
    CAN_CLK_SEL_AHB
} CAN_ClockSelectType;

#define IS_CAN_CLOCK_SELECT_PARA(CLOCK) (((CLOCK) == CAN_CLK_SEL_EXTERNAL_OSC) || \
                                         ((CLOCK) == CAN_CLK_SEL_AHB))

/*!
* @brief CAN time clock divider enumeration
*/
typedef enum
{
    CAN_TIME_CLK_DIVIDER_8 = 0,
    CAN_TIME_CLK_DIVIDER_16,
    CAN_TIME_CLK_DIVIDER_24,
    CAN_TIME_CLK_DIVIDER_48
} CAN_TimeClockDividerType;

#define IS_CAN_TIMECLOCK_DIVIDER_PARA(DIVIDER) (((DIVIDER) == CAN_TIME_CLK_DIVIDER_8) || \
                                                ((DIVIDER) == CAN_TIME_CLK_DIVIDER_16) || \
                                                ((DIVIDER) == CAN_TIME_CLK_DIVIDER_24) || \
                                                ((DIVIDER) == CAN_TIME_CLK_DIVIDER_48))

/* ==========================================  Variables  =========================================== */

/* ====================================  Functions declaration  ===================================== */
/*!
* @brief enbale the module clock
*
* @param[in] module: CKGEN_ClockType. value can be
*                   - CLK_UART0
*                   - CLK_UART1
*                   - CLK_UART2
*                   - CLK_RESERVE3
*                   - CLK_RESERVE4
*                   - CLK_RESERVE5
*                   - CLK_SPI0
*                   - CLK_SPI1
*                   - CLK_I2C0
*                   - CLK_I2C1
*                   - CLK_PWDT0
*                   - CLK_PWM0
*                   - CLK_PWM1
*                   - CLK_RESERVE13
*                   - CLK_RESERVE14
*                   - CLK_RESERVE15
*                   - CLK_RESERVE16
*                   - CLK_RESERVE17
*                   - CLK_RESERVE18
*                   - CLK_TIMER
*                   - CLK_RTC
*                   - CLK_DMA0
*                   - CLK_RESERVE22
*                   - CLK_GPIO
*                   - CLK_RESERVE24
*                   - CLK_WDG
*                   - CLK_CRC
*                   - CLK_RESERVE27
*                   - CLK_CAN0
*                   - CLK_RESERVE29
*                   - CLK_RESERVE30
*                   - CLK_RESERVE31

*                   - CLK_RESERVE32
*                   - CLK_CTU
*                   - CLK_ADC0
*                   - CLK_ACMP0
*                   - CLK_RESERVE36
*                   - CLK_PWDT1
* @param[in]  enable: 0:disable, 1: enable
* @return none
*/
void CKGEN_Enable(CKGEN_ClockType module, ACTION_Type enable);
/*!
* @brief do soft reset for the module
*
* @param[in] module: the module to do Soft Reset. value can be
*                   - SRST_UART0
*                   - SRST_UART1
*                   - SRST_UART2
*                   - SRST_RESERVE3
*                   - SRST_RESERVE4
*                   - SRST_RESERVE5
*                   - SRST_SPI0
*                   - SRST_SPI1
*                   - SRST_I2C0
*                   - SRST_I2C1
*                   - SRST_PWDT0
*                   - SRST_PWM0
*                   - SRST_PWM1
*                   - SRST_RESERVE13
*                   - SRST_RESERVE14
*                   - SRST_RESERVE15
*                   - SRST_RESERVE16
*                   - SRST_RESERVE17
*                   - SRST_RESERVE18
*                   - SRST_TIMER
*                   - SRST_RTC
*                   - SRST_DMA0
*                   - SRST_RESERVE22
*                   - SRST_GPIO
*                   - SRST_RESERVE24
*                   - SRST_WDG
*                   - SRST_CRC
*                   - SRST_RESERVE27
*                   - SRST_CAN0
*                   - SRST_RESERVE29

*                   - //PERI_SFT_RST2
*                   - SRST_RESERVE32
*                   - SRST_CTU
*                   - SRST_ADC0
*                   - SRST_ACMP0
*                   - SRST_ANA_REG
*                   - SRST_PWDT1
* @param[in] active: 1: active, 0: inactive
* @return 0 : no error, -1: not support module
*/
void CKGEN_SoftReset(CKGEN_SoftResetType module, ACTION_Type active);
/*!
* @brief set the sysclck clock source
*
* @param[in] clockSource: set system clock source, value can be
*                        - SYSCLK_SRC_INTERNAL_OSC
*                        - SYSCLK_SRC_PLL_OUTPUT
*                        - SYSCLK_SRC_EXTERNAL_OSC
* @return none
*/
void CKGEN_SetSysclkSrc(SYSTEM_ClockSourceType clockSource);
/*!
* @brief set the PLL Previous Divider
*
* @param[in] div: set pll PREDIV, value can be
*                - PLL_PREDIV_1
*                - PLL_PREDIV_2
*                - PLL_PREDIV_4
* @return none
*/
void CKGEN_SetPllPrevDiv(uint8_t div);
/*!
* @brief set the PLL post Divider
*
* @param[in] div: set pll post-divider, value can be
*                - PLL_POSDIV_1
*                - PLL_POSDIV_2
*                - PLL_POSDIV_4
* @return none
*/
void CKGEN_SetPllPostDiv(uint8_t div);
/*!
* @brief set the PLL feedback Divider
*
* @param[in] div: set pll FBKDIV, value can be 0 to 255
* @return none
*/
void CKGEN_SetPllFeedbackDiv(uint8_t div);
/*!
* @brief set the sysclck divider
*
* @param[in] div: system clock divider set, value can be
*                - SYSCLK_DIVIDER_1
*                - SYSCLK_DIVIDER_2
*                - SYSCLK_DIVIDER_3
*                - SYSCLK_DIVIDER_4
* @return none
*/
void CKGEN_SetSysclkDiv(SYSCLK_DividerType div);
/*!
* @brief set the APB clock divider
*
* @param[in] div: apb clock divider set, value can be
*                - APBCLK_DIVIDER_1
*                - APBCLK_DIVIDER_2
*                - APBCLK_DIVIDER_3
*                - APBCLK_DIVIDER_4
* @return none
*/
void CKGEN_SetAPBClockDivider(APBCLK_DividerType div);
/*!
* @brief set the PLL reference
*
* @param[in] ref: set PLL reference clock, value can be
*                - PLL_REF_INTERAL_OSC(8M)
*                - PLL_REF_EXTERNAL_OSC
* @return none
*/
void CKGEN_SetPLLReference(PLL_ReferenceType ref);
/*!
* @brief set the CAN0 clock
*
* @param[in] canIndex: 0:can0
* @param[in] sel: can clock source select, value can be
*                - CAN_CLK_SEL_EXTERNAL_OSC
*                - CAN_CLK_SEL_AHB
* @return none
*/
void CKGEN_SetCANClock(uint8_t canIndex, CAN_ClockSelectType sel);
/*!
* @brief set the CAN time clock divider (for time stamp)
*
* @param[in] canIndex: 0:can0
* @param[in] divider: CAN time clock divider, value can be
*                - 2'h0 : divider by 8
*                - 2'h1 : divider by 16
*                - 2'h2 : divider by 24
*                - 2'h3 : divider by 48
* @return none
*/
void CKGEN_SetCANTimeDivider(uint8_t canIndex, CAN_TimeClockDividerType divider);

#ifdef __cplusplus
}
#endif

#endif /* _AC780X_CKGEN_H */

/* =============================================  EOF  ============================================== */
