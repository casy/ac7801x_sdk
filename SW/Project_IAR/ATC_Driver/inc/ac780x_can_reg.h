/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2020. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
/******************************************************************************
 * @version: V1.0.5
 ******************************************************************************/

#ifndef __AC780X_CAN_REG_H
#define __AC780X_CAN_REG_H
/*!
* @file ac780x_can_reg.h
*
* @brief CAN access register inline function definition.
*
*/
#ifdef __cplusplus
extern "C" {
#endif

/* ===========================================  Includes  =========================================== */
#include "ac780x_can.h"

/* ======================================  Functions define  ======================================== */
/*!
 * @brief Set CANx reset state
 *
 * @param[in] CANx: CAN module
                -: CAN0
 * @param[in] state: enable state
                -: ENDBLE
                -: DISDBLE
 * @return none
 */
__STATIC_INLINE void CAN_SetReset(CAN_Type *CANx, ACTION_Type state)
{
    MODIFY_REG32(CANx->CTRL0, CAN_CTRL0_RESET_Msk, CAN_CTRL0_RESET_Pos, state);
}

/*!
 * @brief Set transceiver standby state
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] state: enable state
                - ENDBLE
                - DISDBLE
 * @return 0: no error, 1: standby error
 */
__STATIC_INLINE int32_t CAN_SetStandby(CAN_Type *CANx, ACTION_Type state)
{
    int32_t err = 0;

    if (state)
    {
        /*!< If there is any transmission, can not entry standby mode */
        if (!(CANx->CTRL0 & (CAN_CTRL0_TPE_Msk | CAN_CTRL0_TSONE_Msk | CAN_CTRL0_TSALL_Msk)))
        {
            CANx->CTRL0 |= CAN_CTRL0_STBY_Msk;
        }
        else
        {
            err = 1;
        }
    }
    else
    {
        CANx->CTRL0 &= (~CAN_CTRL0_STBY_Msk);
    }
    
    return err;
}

/*!
 * @brief Get receive buffer status
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return Receive buffer status
 */
__STATIC_INLINE int32_t CAN_IsMsgInReceiveBuf(CAN_Type *CANx)
{
    return (READ_BIT32(CANx->CTRL0, CAN_CTRL0_RSTAT_Msk) >> CAN_CTRL0_RSTAT_Pos);
}

/*!
 * @brief CAN start transmission
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] type: CAN transmit type
                - CAN_TRANSMIT_PRIMARY
                - CAN_TRANSMIT_SECONDARY
 * @param[in] amount: Transmit Secondary ALL or ONE frames
                - CAN_TRANSMIT_ONE
                - CAN_TRANSMIT_ALL
 * @return 0 : success 1: error
 */
__STATIC_INLINE int32_t CAN_StartTransmission(CAN_Type *CANx, CAN_TransmitBufferType type, CAN_TransmitAmountType amount)
{
    /*!< Can't send message when CAN is in standby mode(return or delay) */
    if (CANx->CTRL0 & CAN_CTRL0_STBY_Msk)
    {
        return 1;
    }

    if (CAN_TRANSMIT_PRIMARY == type)
    {
        CANx->CTRL0 |= CAN_CTRL0_TPE_Msk;
    }
    else
    {
        if (CAN_TRANSMIT_ONE == amount)
        {
            CANx->CTRL0 |= CAN_CTRL0_TSONE_Msk;    /*!< Send one message only */
        }
        else
        {
            CANx->CTRL0 |= CAN_CTRL0_TSALL_Msk;    /*!< Send all messages */
        }
    }

    return 0;
}

/*!
 * @brief CAN Abort transmission
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] type: CAN transmit type
                - CAN_TRANSMIT_PRIMARY
                - CAN_TRANSMIT_SECONDARY
 * @return none
 */
__STATIC_INLINE void CAN_AbortTransmission(CAN_Type *CANx, CAN_TransmitBufferType type)
{
    if (CAN_TRANSMIT_PRIMARY == type)
    {
        CANx->CTRL0 |= CAN_CTRL0_TPA_Msk;
    }
    else
    {
        CANx->CTRL0 |= CAN_CTRL0_TSA_Msk;
    }
}

/*!
 * @brief Check transmission is busy
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] type: CAN transmit type
                - CAN_TRANSMIT_PRIMARY
                - CAN_TRANSMIT_SECONDARY
 * @return Transmit busy flag (0: not busy, 1: busy)
 */
__STATIC_INLINE int32_t CAN_IsTransmitBusy(CAN_Type *CANx, CAN_TransmitBufferType type)
{
    if (CAN_TRANSMIT_PRIMARY == type)
    {
        /*!< Transmitting is not successful */
        return (1 == ((CANx->CTRL0 & CAN_CTRL0_TPE_Msk) >> CAN_CTRL0_TPE_Pos));
    }
    else
    {
        /*!< Transmission buffers are full */
        return (CAN_TRANSMIT_BUFFER_FULL == ((CANx->CTRL0 & CAN_CTRL0_TSSTAT_Msk) >> CAN_CTRL0_TSSTAT_Pos));
    }
}

/*!
 * @brief Check whether is transmitting
 *
 * @param[in] CANx: CAN module
 *              - CAN0
 * @param[in] type: CAN transmit type
 *              - CAN_TRANSMIT_PRIMARY
 *              - CAN_TRANSMIT_SECONDARY
 * @return Transmitting status (0: not transmitting, 1: transmitting)
 */
__STATIC_INLINE int32_t CAN_IsTransmitting(CAN_Type *CANx, CAN_TransmitBufferType type)
{
    if (CAN_TRANSMIT_PRIMARY == type)
    {
        return ((CANx->CTRL0 & CAN_CTRL0_TPE_Msk) >> CAN_CTRL0_TPE_Pos);
    }
    else
    {
        return ((CANx->CTRL0 & (CAN_CTRL0_TSALL_Msk | CAN_CTRL0_TSONE_Msk)) != 0);
    }
}

/*!
 * @brief Check transmission is idle
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] type: CAN transmit type
                - CAN_TRANSMIT_PRIMARY
                - CAN_TRANSMIT_SECONDARY
 * @return Transmit idle flag (0: busy, 1: idle)
 */
__STATIC_INLINE int32_t CAN_IsTransmitIdle(CAN_Type *CANx, CAN_TransmitBufferType type)
{
    if (CAN_TRANSMIT_PRIMARY == type)
    {
        return (0 == (CANx->CTRL0 & CAN_CTRL0_TPE_Msk));        /*!< PTB is empty */
    }
    else
    {
        return (0 == (CANx->CTRL0 & CAN_CTRL0_TSSTAT_Msk));     /*!< STB are empty */
    }
}

/*!
 * @brief Check bus off status
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return Bus off status
 */
__STATIC_INLINE int32_t CAN_IsBusoff(CAN_Type *CANx)
{
    return (CANx->CTRL0 & CAN_CTRL0_BUSOFF_Msk);
}

/*!
 * @brief Set CAN interrupt enable
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] intEn: CAN interrupt enable
 * @return none
 */
__STATIC_INLINE void CAN_SetIntEnable(CAN_Type *CANx, uint32_t intEn)
{
    MODIFY_REG32(CANx->CTRL1, CAN_IRQ_ALL_ENABLE_MSK, 0, intEn);
}

/*!
 * @brief Set CAN error warning limit
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] ewl: error warning limit
            - (0-15)
 * @return none
 */
__STATIC_INLINE void CAN_SetEwl(CAN_Type *CANx, uint8_t ewl)
{
    MODIFY_REG32(CANx->CTRL1, CAN_CTRL1_EWL_Msk, CAN_CTRL1_EWL_Pos, ewl);
}

/*!
 * @brief Set CAN almost full warning limit
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] afwl: almost full warning limit
 * @return none
 */
__STATIC_INLINE void CAN_SetAfwl(CAN_Type *CANx, uint8_t afwl)
{
    MODIFY_REG32(CANx->CTRL1, CAN_CTRL1_AFWL_Msk, CAN_CTRL1_AFWL_Pos, afwl);
}

/*!
 * @brief Get CAN transmission time stamp
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return transmission time stamp
 */
__STATIC_INLINE uint32_t CAN_GetTts(CAN_Type *CANx)
{
    return (CANx->TTS[0]);
}

/*!
 * @brief Enable CAN transmit PTB single shot mode
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] state: enable state
                - ENDBLE
                - DISDBLE
 * @return none
 */
__STATIC_INLINE void CAN_SetTpss(CAN_Type *CANx, ACTION_Type state)
{
    MODIFY_REG32(CANx->CTRL0, CAN_CTRL0_TPSS_Msk, CAN_CTRL0_TPSS_Pos, state);
}

/*!
 * @brief  Enable CAN transmit STB single shot mode
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] state: enable state
                - ENDBLE
                - DISDBLE
 * @return none
 */
__STATIC_INLINE void CAN_SetTsss(CAN_Type *CANx, ACTION_Type state)
{
    MODIFY_REG32(CANx->CTRL0, CAN_CTRL0_TSSS_Msk, CAN_CTRL0_TSSS_Pos, state);
}

/*!
 * @brief get CAN Transmit secondary status
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return Transmit secondary status
 */
__STATIC_INLINE uint32_t CAN_GetTSStat(CAN_Type *CANx)
{
    return ((CANx->CTRL0 & CAN_CTRL0_TSSTAT_Msk) >> CAN_CTRL0_TSSTAT_Pos);
}

/*!
 * @brief get CAN Receiver status
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return Receiver status
 */
__STATIC_INLINE uint32_t CAN_GetRStat(CAN_Type *CANx)
{
    return ((CANx->CTRL0 & CAN_CTRL0_RSTAT_Msk) >> CAN_CTRL0_RSTAT_Pos);
}

/*!
 * @brief get CAN Reception buffer overflow
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return Reception buffer overflow
 */
__STATIC_INLINE uint32_t CAN_GetRov(CAN_Type *CANx)
{
    return ((CANx->CTRL0 & CAN_CTRL0_ROV_Msk) >> CAN_CTRL0_ROV_Pos);
}

/*!
 * @brief get CAN kind of error
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return Kind of error
 */
__STATIC_INLINE uint32_t CAN_GetKoer(CAN_Type *CANx)
{
    return ((CANx->ERRINFO & CAN_ERRINFO_KOER_Msk) >> CAN_ERRINFO_KOER_Pos);
}

/*!
 * @brief Set CAN FD ISO state
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] state: enable state
                - ENDBLE
                - DISDBLE
 * @return none
 */
__STATIC_INLINE void CAN_SetFdIso(CAN_Type *CANx, ACTION_Type state)
{
   MODIFY_REG32(CANx->CTRL0, CAN_CTRL0_FDISO_Msk, CAN_CTRL0_FDISO_Pos, state);
}

/*!
 * @brief Set operation mode
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] tsMode: Transmit secondary: operation mode
                - CAN_TSMODE_FIFO
                - CAN_TSMODE_PRIORITY
 * @return none
 */
__STATIC_INLINE void CAN_SetTSMode(CAN_Type *CANx, CAN_TransmitModeType tsMode)
{
    MODIFY_REG32(CANx->CTRL0, CAN_CTRL0_TSMODE_Msk, CAN_CTRL0_TSMODE_Pos, tsMode);
}

/*!
 * @brief Set CAN Reception buffer overflow
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] rom: 1: new message will not be stored, 0: The oldest message will be overwrite 
                - CAN_RECV_OVER_WRITE
                - CAN_RECV_DISCARD
 * @return none
 */
__STATIC_INLINE void CAN_SetRom(CAN_Type *CANx, CAN_OverflowModeType rom)
{
    MODIFY_REG32(CANx->CTRL0, CAN_CTRL0_ROM_Msk, CAN_CTRL0_ROM_Pos, rom);
}

/*!
 * @brief Enable CAN Self-Acknowledge when LBME = 1
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] state: enable state
                - ENDBLE
                - DISDBLE
 * @return none
 */
__STATIC_INLINE void CAN_SetSack(CAN_Type *CANx, ACTION_Type state)
{
    MODIFY_REG32(CANx->CTRL0, CAN_CTRL0_SACK_Msk, CAN_CTRL0_SACK_Pos, state);
}

/*!
 * @brief Enable CAN TDC
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] state: enable state
                - ENDBLE
                - DISDBLE
 * @return none
 */
__STATIC_INLINE void CAN_SetTdc(CAN_Type *CANx, ACTION_Type state)
{
    MODIFY_REG32(CANx->ERRINFO, CAN_ERRINFO_TDCEN_Msk, CAN_ERRINFO_TDCEN_Pos, state);
}

/*!
 * @brief Set CAN error warning limit
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] offset: SSP offset
 * @return none
 */
__STATIC_INLINE void CAN_SetSspOffet(CAN_Type *CANx, uint8_t offset)
{
    MODIFY_REG32(CANx->ERRINFO, CAN_ERRINFO_SSPOFF_Msk, CAN_ERRINFO_SSPOFF_Pos, offset & 0x7F);
}

/*!
 * @brief Set CAN TIME-stamping Position
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] pos: TIME-stamping Position
                - CAN_TIME_STAMP_SOF
                - CAN_TIME_STAMP_EOF
 * @return none
 */
__STATIC_INLINE void CAN_SetTimePosition(CAN_Type *CANx, CAN_TimeStampPosType pos)
{
    MODIFY_REG32(CANx->ACFCTRL, CAN_ACFCTRL_TIMEPOS_Msk, CAN_ACFCTRL_TIMEPOS_Pos, pos);
}

/*!
 * @brief Enable CAN TIME-stamping
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] state: enable state
                - ENDBLE
                - DISDBLE
 * @return none
 */
__STATIC_INLINE void CAN_EnableTime(CAN_Type *CANx, ACTION_Type state)
{
    MODIFY_REG32(CANx->ACFCTRL, CAN_ACFCTRL_TIMEEN_Msk, CAN_ACFCTRL_TIMEEN_Pos, state);
}

/*!
 * @brief Set CAN acceptance filter enable
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] index: filter id
 * @param[in] state: 1:enable filter, 0: disable filter
                - DISABLE
                - ENABLE
 * @return none
 */
__STATIC_INLINE void CAN_SetAcfEn(CAN_Type *CANx, uint8_t index, ACTION_Type state)
{
    if (state)
    {
        CANx->ACFCTRL |= ((1 << index) << CAN_ACFCTRL_ACFEN_Pos);
    }
    else
    {
        CANx->ACFCTRL &= ~((1 << index) << CAN_ACFCTRL_ACFEN_Pos);
    }
}

/*!
 * @brief Set CAN acceptance filter index
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] index: filter id
 * @return none
 */
__STATIC_INLINE void CAN_SetAcfIndex(CAN_Type *CANx, uint8_t index)
{
    DEVICE_ASSERT(CAN_MAX_FILTER_NUM > index);
    MODIFY_REG32(CANx->ACFCTRL, CAN_ACFCTRL_ACFADR_Msk, CAN_ACFCTRL_ACFADR_Pos, index);
}

/*!
 * @brief Set CAN Filter data code
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] code: Filter data code
 * @return none
 */
__STATIC_INLINE void CAN_SetAcfCode(CAN_Type *CANx, uint32_t code)
{
    CANx->ACFCTRL &= (~CAN_ACFCTRL_SELMASK_Msk);
    CANx->ACF = (code & CAN_ACF_ACODE_Msk);
}

/*!
 * @brief Set CAN Filter data mask + AIDE + AIDEE
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @param[in] mask: Filter data mask + AIDE + AIDEE
 * @return none
 */
__STATIC_INLINE void CAN_SetAcfMask(CAN_Type *CANx, uint32_t mask)
{
    CANx->ACFCTRL |= CAN_ACFCTRL_SELMASK_Msk;
    CANx->ACF = mask & (CAN_ACF_ACODE_Msk | CAN_ACF_AIDE_Msk | CAN_ACF_AIDEE_Msk);
}

/*!
 * @brief Get CAN version
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return CAN version
 */
__STATIC_INLINE uint16_t CAN_GetVersion(CAN_Type *CANx)
{
    return (CANx->VERSION & CAN_VERSION_VERSION_Msk);
}

/*!
 * @brief get receive error count
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return receive error count
 */
__STATIC_INLINE uint8_t CAN_GetRECnt(CAN_Type *CANx)
{
    return ((CANx->ERRINFO & CAN_ERRINFO_RECNT_Msk) >> CAN_ERRINFO_RECNT_Pos);
}

/*!
 * @brief get transmit error count
 *
 * @param[in] CANx: CAN module
                - CAN0
 * @return transmit error count
 */
__STATIC_INLINE uint8_t CAN_GetTECnt(CAN_Type *CANx)
{
    return ((CANx->ERRINFO & CAN_ERRINFO_TECNT_Msk) >> CAN_ERRINFO_TECNT_Pos);
}

#ifdef __cplusplus
}
#endif

#endif /* __AC780X_CAN_REG_H */

/* =============================================  EOF  ============================================== */
