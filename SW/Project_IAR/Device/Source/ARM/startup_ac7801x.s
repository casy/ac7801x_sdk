;/* Copyright Statement:
; *
; * This software/firmware and related documentation ("AutoChips Software") are
; * protected under relevant copyright laws. The information contained herein is
; * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
; * the prior written permission of AutoChips inc. and/or its licensors, any
; * reproduction, modification, use or disclosure of AutoChips Software, and
; * information contained herein, in whole or in part, shall be strictly
; * prohibited.
; *
; * AutoChips Inc. (C) 2020. All rights reserved.
; *
; * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
; * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
; * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
; * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
; * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
; * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
; * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
; * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
; * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
; * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
; * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER''S SOLE RESPONSIBILITY TO
; * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
; * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
; * RELEASES MADE TO RECEIVER''S SPECIFICATION OR TO CONFORM TO A PARTICULAR
; * STANDARD OR OPEN FORUM. RECEIVER''S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS''S
; * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
; * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS''S OPTION, TO REVISE OR REPLACE THE
; * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
; * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
; */
;* File Name          : startup_ac7801x.s
;*
;* @file startup_ac7801x.s
;*
;* @author AutoChips
;*
;* @version 0.0.1
;*
;* @date Jan.02.2019
;* Description        : AC7801x Devices vector table for EWARM
;*                      toolchain.
;*                      This module performs:
;*                      - Set the initial SP
;*                      - Configure the clock system
;*                      - Set the initial PC == __iar_program_start,
;*                      - Set the vector table entries with the exceptions ISR 
;*                        address.
;*                      After Reset the Cortex-M3 processor is in Thread mode,
;*                      priority is Privileged, and the Stack is set to Main.
;********************************************************************************
;
; The modules in this file are included in the libraries, and may be replaced
; by any user-defined modules that define the PUBLIC symbol _program_start or
; a user defined start symbol.
; To override the cstartup defined in the library, simply add your modified
; version to the workbench project.
;
; The vector table is normally located at address 0.
; When debugging in RAM, it can be located in RAM, aligned to at least 2^6.
; The name "__vector_table" has special meaning for C-SPY:
; it is where the SP start value is found, and the NVIC vector
; table register (VTOR) is initialized to this address if != 0.
;
; Cortex-M version
;

        MODULE  ?cstartup

        ;; Forward declaration of sections.
        SECTION CSTACK:DATA:NOROOT(3)

        SECTION .intvec:CODE:NOROOT(2)

        EXTERN  __iar_program_start
        EXTERN  SystemInit        
        PUBLIC  __vector_table
        PUBLIC  __Vectors
        PUBLIC  __Vectors_End
        PUBLIC  __Vectors_Size

        DATA
__vector_table
        DCD     sfe(CSTACK)
		DCD     Reset_Handler              ; Reset Handler
		DCD     NMI_Handler                ; NMI Handler
		DCD     HardFault_Handler          ; Hard Fault Handler
		DCD     0                          ; Reserved
		DCD     0                          ; Reserved
		DCD     0                          ; Reserved
		DCD     0                          ; Reserved
		DCD     0                          ; Reserved
		DCD     0                          ; Reserved
		DCD     0                          ; Reserved
		DCD     SVC_Handler                ; SVCall Handler
		DCD     0                          ; Reserved
		DCD     0                          ; Reserved
		DCD     PendSV_Handler             ; PendSV Handler
		DCD     SysTick_Handler            ; SysTick Handler

		; External Interrupts
		DCD     PWDT0_IRQHandler           ;  0: PWDT0 interrupt
		DCD     PWDT1_IRQHandler           ;  1: PWDT1 interrupt
		DCD     PWM0_IRQHandler            ;  2: PWM0 interrupt
		DCD     PWM1_IRQHandler            ;  3: PWM1 interrupt
		DCD     ACMP0_IRQHandler           ;  4: ACMP0 interrupt
		DCD     UART0_IRQHandler           ;  5: UART0 interrupt
		DCD     UART1_IRQHandler           ;  6: UART1 interrupt
		DCD     UART2_IRQHandler           ;  7: UART2 interrupt
		DCD     WDG_IRQHandler             ;  8: WDG interrupt
		DCD     SPI0_IRQHandler            ;  9: SPI0 interrupt
		DCD     SPI1_IRQHandler            ; 10: SPI1 interrupt
		DCD     I2C0_IRQHandler            ; 11: I2C0 Interrupt
		DCD     I2C1_IRQHandler            ; 12: I2C1 Interrupt
		DCD     DMA0_Channel0_IRQHandler   ; 13: DMA0 channel 0 interrupt
		DCD     DMA0_Channel1_IRQHandler   ; 14: DMA0 channel 1 interrupt
		DCD     DMA0_Channel2_IRQHandler   ; 15: DMA0 channel 2 interrupt
		DCD     DMA0_Channel3_IRQHandler   ; 16: DMA0 channel 3 interrupt
		DCD     TIMER_Channel0_IRQHandler  ; 17: TIMER channel 0 interrupt
		DCD     TIMER_Channel1_IRQHandler  ; 18: TIMER channel 1 interrupt
		DCD     TIMER_Channel2_IRQHandler  ; 19: TIMER channel 2 interrupt
		DCD     TIMER_Channel3_IRQHandler  ; 20: TIMER channel 3 interrupt
		DCD     RTC_IRQHandler             ; 21: RTC Interrupt
		DCD     PVD_IRQHandler             ; 22: PVD Interrupt
		DCD     SPM_IRQHandler             ; 23: SPM interrupt
		DCD     CAN0_Handler               ; 24: CAN0 interrupt
		DCD     ADC0_IRQHandler            ; 25: ADC0 interrupt
		DCD     ECC_SRAM_IRQHandler        ; 26: ECC SRAM interrupt
		DCD     EXTI0_IRQHandler           ; 27: GPIOx PIN0 external interrupt
		DCD     EXTI1_IRQHandler           ; 28: GPIOx PIN1 external interrupt
		DCD     EXTI2_IRQHandler           ; 29: GPIOx PIN2 external interrupt
		DCD     EXTI3_8_IRQHandler         ; 30: GPIOx PIN3~8 external interrupt
		DCD     EXTI9_15_IRQHandler        ; 31: GPIOx PIN9~15 external interrupt
__Vectors_End

__Vectors       EQU   __vector_table
__Vectors_Size  EQU   __Vectors_End - __Vectors
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Default interrupt handlers.
;;
        THUMB

        PUBWEAK Reset_Handler
        SECTION .text:CODE:REORDER:NOROOT(2)
Reset_Handler
        LDR     R0, =SystemInit
        BLX     R0
		LDR     R0, =0x20080014
		LDR     R1, =0xFFFF
		STRH    R1, [R0]

		LDR     R0, =0x20080044
		LDR     R1, =0xFFFF
		STRH    R1, [R0]

		LDR     R0, =0x20080074
		LDR     R1, =0x3FF
		STRH    R1, [R0]
        LDR     R0, =__iar_program_start
        BX      R0
        
        PUBWEAK NMI_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
NMI_Handler
        B NMI_Handler
        
        PUBWEAK HardFault_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
HardFault_Handler
        B HardFault_Handler
        
        PUBWEAK SVC_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
SVC_Handler
        B SVC_Handler
        
        PUBWEAK PendSV_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
PendSV_Handler
        B PendSV_Handler
        
        PUBWEAK SysTick_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
SysTick_Handler
        B SysTick_Handler

		PUBWEAK  PWDT0_IRQHandler           
		PUBWEAK  PWDT1_IRQHandler           
		PUBWEAK  PWM0_IRQHandler            
		PUBWEAK  PWM1_IRQHandler            
		PUBWEAK  ACMP0_IRQHandler           
		PUBWEAK  UART0_IRQHandler           
		PUBWEAK  UART1_IRQHandler           
		PUBWEAK  UART2_IRQHandler           
		PUBWEAK  WDG_IRQHandler             
		PUBWEAK  SPI0_IRQHandler            
		PUBWEAK  SPI1_IRQHandler            
		PUBWEAK  I2C0_IRQHandler            
		PUBWEAK  I2C1_IRQHandler            
		PUBWEAK  DMA0_Channel0_IRQHandler   
		PUBWEAK  DMA0_Channel1_IRQHandler   
		PUBWEAK  DMA0_Channel2_IRQHandler   
		PUBWEAK  DMA0_Channel3_IRQHandler   
		PUBWEAK  TIMER_Channel0_IRQHandler  
		PUBWEAK  TIMER_Channel1_IRQHandler  
		PUBWEAK  TIMER_Channel2_IRQHandler  
		PUBWEAK  TIMER_Channel3_IRQHandler  
		PUBWEAK  RTC_IRQHandler             
		PUBWEAK  PVD_IRQHandler             
		PUBWEAK  SPM_IRQHandler             
		PUBWEAK  CAN0_Handler               
		PUBWEAK  ADC0_IRQHandler            
		PUBWEAK  ECC_SRAM_IRQHandler        
		PUBWEAK  EXTI0_IRQHandler           
		PUBWEAK  EXTI1_IRQHandler           
		PUBWEAK  EXTI2_IRQHandler           
		PUBWEAK  EXTI3_8_IRQHandler         
		PUBWEAK  EXTI9_15_IRQHandler    
        SECTION .text:CODE:REORDER:NOROOT(1)    
Default_Handler
PWDT0_IRQHandler
PWDT1_IRQHandler
PWM0_IRQHandler
PWM1_IRQHandler
ACMP0_IRQHandler
UART0_IRQHandler
UART1_IRQHandler
UART2_IRQHandler
WDG_IRQHandler
SPI0_IRQHandler
SPI1_IRQHandler
I2C0_IRQHandler
I2C1_IRQHandler
DMA0_Channel0_IRQHandler
DMA0_Channel1_IRQHandler
DMA0_Channel2_IRQHandler
DMA0_Channel3_IRQHandler
TIMER_Channel0_IRQHandler
TIMER_Channel1_IRQHandler
TIMER_Channel2_IRQHandler
TIMER_Channel3_IRQHandler
RTC_IRQHandler
PVD_IRQHandler
SPM_IRQHandler
CAN0_Handler
ADC0_IRQHandler
ECC_SRAM_IRQHandler
EXTI0_IRQHandler
EXTI1_IRQHandler
EXTI2_IRQHandler
EXTI3_8_IRQHandler
EXTI9_15_IRQHandler
        B       .

        END
/************************END OF FILE****/


