
/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors,  any
 * reproduction,  modification,  use or disclosure of AutoChips Software,  and
 * information contained herein,  in whole or in part,  shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE,  RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES,  EXPRESS OR IMPLIED,  INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY,  FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, 
 * INCORPORATED IN,  OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE,  AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE,  AT AUTOCHIPS'S OPTION,  TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE,  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */

/*************<start>******************/

/*************<include>****************/
#include "string.h"
#include "sdk_can.h"


/*************<macro>******************/


/*************<enum>*******************/


/*************<union>******************/


/*************<struct>*****************/


/*************<variable>***************/

CAN_MsgInfoType 			g_sendCANMsgInfo;
CAN_MsgInfoType 			g_recvCANMsgInfo;
uint8_t sendDataBuff[64];
uint8_t recvDataBuff[64];

/*************<prototype>**************/
extern void CAN_IRQnCallBack(void *device, uint32_t wpara, uint32_t lpara);


/**
* @prototype Sdk_CAN_Init(void)
*
* @param[in] void
* @return	 void
*
* @brief  	 Initalize CAN module.
*			 初始化CAN模块.
*/
void Sdk_CAN_Init(void)
{
	CAN_ConfigType canConfig;
	CAN_BitrateConfigType canBandrateConfig; 
	memset((void *)&canConfig, 0, sizeof(CAN_ConfigType));
	memset((void *)&canBandrateConfig, 0, sizeof(CAN_BitrateConfigType));
    GPIO_SetFunc(CAN0_TX, GPIO_FUN1);
	GPIO_SetFunc(CAN0_RX, GPIO_FUN1);
    GPIO_SetFunc(CAN0_STB, GPIO_FUN0);
	GPIO_SetDir(CAN0_STB, GPIO_OUT);
	CAN0_TRASCVER_NML;
	
	canBandrateConfig.PRESC 	= 5;
	canBandrateConfig.SEG_1 	= 11;
	canBandrateConfig.SEG_2 	= 2;
	canBandrateConfig.SJW 	    = 2;
	

	canConfig.interruptEn = ENABLE;
    canConfig.interruptMask = CAN_IRQ_ALL_ENABLE_MSK;   
	canConfig.canMode = CAN_MODE_NORMAL;
    canConfig.clockSrc = CAN_CLKSRC_AHB;
    canConfig.callback = CAN_IRQnCallBack; 
    canConfig.normalBitrate = &canBandrateConfig;
	
    g_sendCANMsgInfo.DATA = sendDataBuff;
    g_recvCANMsgInfo.DATA = recvDataBuff;
    
	CAN_Init(CAN0, &canConfig);
}

/**
* @prototype Sdk_CAN_SendMsg(uint32_t ID, CAN_IDE_TypeDef IDE, CAN_RTR_TypeDef RTR, uint8_t *pDataBuf, uint8_t dataLen)
*
* @param[in] ID: send data CAN ID. 
* @param[in] IDE: Choose between CAN_IDE_STD and CAN_IDE_EXT.
* @param[in] RTR: Choose between CAN_RTR_STD and CAN_RTR_RMT.
* @param[in] pDataBuf: The pointer of the data to be sended.
* @param[in] dataLen: Send data length.
* @return	 CAN_SendSts_TypeDef,send fail or success.
*
* @brief  	 Send CAN message.
*			 发送CAN数据.
*/
CAN_SendSts_TypeDef Sdk_CAN_SendMsg(uint32_t ID, CAN_IDE_TypeDef IDE, CAN_RTR_TypeDef RTR, uint8_t *pDataBuf, uint8_t dataLen)
{
	g_sendCANMsgInfo.ID = ID;
	g_sendCANMsgInfo.IDE = IDE;
	g_sendCANMsgInfo.RTR = RTR;
	g_sendCANMsgInfo.DLC = dataLen;
	memcpy(g_sendCANMsgInfo.DATA, pDataBuf, dataLen);
	
    if (!CAN_SendMessage(CAN0, &g_sendCANMsgInfo, CAN_TRANSMIT_SECONDARY))
    {
        return CAN_SEND_MSG_SUCCS;
    }
    else
    {
        return CAN_SEND_MSG_FILED;
    }
}


/*************<end>********************/
