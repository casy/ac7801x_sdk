
/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */
 
/*************<start>******************/


/*************<include>****************/
#include "ac780x.h"
#include "includes.h"

/*************<macro>******************/


/*************<enum>*******************/
typedef enum
{
	KEY_UNDOWN 		 = 0,
	KEY_DOWN		 = 1,
}KEY_Action_TypeDef;


/*************<union>******************/


/*************<struct>*****************/


/*************<variable>***************/
uint8_t 			g_getK6CurtSts = 0,g_getK7CurtSts = 0;
uint8_t 			g_sendCANDataFlag = 0;
uint8_t 			g_sendCANDataBuf[8] = {0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88};
uint32_t 			g_sendCANDataTimeCnt = 0,g_recvStopTimeCnt;

uint8_t 			g_led2BlinkFlag = 0, g_led3BlinkFlag = 0;
uint32_t 			g_led2BlinkTimeCnt = 0,g_led3BlinkTimeCnt = 0;

TaskHandle_t		g_checkCANDataHandle;//任务句柄
TaskHandle_t		g_scanKeyActionHandle;
TaskHandle_t		g_checkLedBlinkHandle;

extern uint8_t 				g_recvedCANDataRdy;

/*************<prototype>**************/
void vTask_Create(void);
void vTask_CheckCANData(void *pParams);
void vTask_ScanKeyAction(void *pParams);
void vTask_CheckLedBlink(void *pParams);


/**
* @prototype main(void)
*
* @param[in] void
* @return	 void
*
* @brief  	 main entry.
*			 main函数.
*/
int main(void)
{
	InitDebug();
	
	Sdk_GPIO_Init();
	Sdk_CAN_Init();
	
	vTask_Create();//创建任务
	
	vTaskStartScheduler();//启动调度,执行任务
	
	
	while(1);//系统正常启动是不会运行到此处
}

/**
* @prototype vTask_Create(void)
*
* @param[in] void
* @return	 void
*
* @brief  	 Create all tasks.
*			 创建所有的任务.
*/
void vTask_Create(void)
{
	xTaskCreate(vTask_ScanKeyAction,		/*任务函数名称*/
				"vTask_ScanKeyAction",		/*任务名*/
				64,						    /*任务栈大小,wrod*/
				NULL,						/*任务参数*/
				4,							/*任务优先级*/
				&g_scanKeyActionHandle		/*任务句柄*/
				);
	
	xTaskCreate(vTask_CheckCANData,			/*任务函数名称*/
				"vTask_CheckCANData",		/*任务名*/
				64,							/*任务栈大小,wrod*/
				NULL,						/*任务参数*/
				3,							/*任务优先级*/
				&g_checkCANDataHandle		/*任务句柄*/
				);
	
	xTaskCreate(vTask_CheckLedBlink,		/*任务函数名称*/
				"vTask_CheckLedBlink",		/*任务名*/
				64,							/*任务栈大小,wrod*/
				NULL,						/*任务参数*/
				2,							/*任务优先级*/
				&g_checkLedBlinkHandle		/*任务句柄*/
				);
}

/**
* @prototype vTask_CheckCANData(void *pParams)
*
* @param[in] pParams:任务创建时传递的参数
* @return	 void
*
* @brief  	 Check CAN data, deal recved data or send data.
*			 检查CAN数据.
*/
void vTask_CheckCANData(void *pParams)
{
	for( ; ;)
	{
		if ((g_sendCANDataFlag) && ((xTaskGetTickCount() - g_sendCANDataTimeCnt) > 99))//是否要发送数据
		{
			g_sendCANDataTimeCnt = xTaskGetTickCount();
			
			Sdk_CAN_SendMsg(0x210, CAN_IDE_STD, CAN_RTR_STD, g_sendCANDataBuf, 8);
		}
		
		if (g_recvedCANDataRdy == TRUE)//是否有数据需要处理
		{
            g_recvedCANDataRdy = FALSE;
			//处理CAN数据
			
			
			if (g_led2BlinkFlag == FALSE)//当前灯没有闪烁,才会去闪烁灯
			{
				g_led2BlinkTimeCnt = xTaskGetTickCount();//获取时钟基准
				g_led2BlinkFlag	   = TRUE;
			}
			g_recvStopTimeCnt  = xTaskGetTickCount();
		}
		else 
		{
			if ((g_led2BlinkFlag) && ((xTaskGetTickCount() - g_recvStopTimeCnt) > 300))//300ms内没有数据,停止闪灯
			{
				g_led2BlinkFlag = 0;
				
				LED2_OFF;
			}
		}
		
		vTaskDelay(50);//50ms扫描一次
	}
}
	
/**
* @prototype vTask_ScanKeyAction(void *pParams)
*
* @param[in] pParams:任务创建时传递的参数
* @return	 void
*
* @brief  	 Scan key aciton.
*			 扫描按键动作.
*/
void vTask_ScanKeyAction(void *pParams)
{
	for ( ; ; )
	{
		/*这里之所以需要记住按键状态,目的是准确识别用按键动作,避免按键一次,执行动作多次*/
		if ((g_getK6CurtSts == KEY_UNDOWN) && (K6_IN == 0))//K6按键按下,只有按键按下然后松开,才能进行下一次按键动作
		{
			g_getK6CurtSts = KEY_DOWN;//记住按键按下状态
			
			g_sendCANDataFlag 	= TRUE;//发送数据
			g_led3BlinkFlag 	= TRUE;
			
			g_led3BlinkTimeCnt 	= xTaskGetTickCount();//获取时钟基准
			g_sendCANDataTimeCnt = g_led3BlinkTimeCnt;
		}
		else if (K6_IN == 1)//读取到高电平,按键松开
		{
			g_getK6CurtSts = KEY_UNDOWN;//记住按键弹起状态
		}
		
		if ((g_getK7CurtSts == KEY_UNDOWN) && (K7_IN == 0))//K7按键按下,只有按键按下然后松开,才能进行下一次按键动作
		{
			g_getK7CurtSts = KEY_DOWN;//记住按键按下状态
			
			g_sendCANDataFlag 	= FALSE;//发送数据
			g_led3BlinkFlag 	= FALSE;
			
			LED3_OFF;
		}
		else if (K7_IN == 1)//读取到高电平,按键松开
		{
			g_getK7CurtSts = KEY_UNDOWN;//记住按键弹起状态
		}
		
		vTaskDelay(10);//10ms扫描一次
	}
}
	
/**
* @prototype vTask_CheckLedBlink(void *pParams)
*
* @param[in] pParams:任务创建时传递的参数
* @return	 void
*
* @brief  	 Check the led weather to be blinking.
*			 检查LED是否需要闪烁.
*/
void vTask_CheckLedBlink(void *pParams)
{
	for ( ; ;)
	{
		if ((g_led2BlinkFlag) && ((xTaskGetTickCount() - g_led2BlinkTimeCnt) > 49))//需要闪烁LED2
		{
			g_led2BlinkTimeCnt = xTaskGetTickCount();
			
			LED2_TOGGLE;
		}
		
		if ((g_led3BlinkFlag) && ((xTaskGetTickCount() - g_led3BlinkTimeCnt) > 49))//需要闪烁LED3
		{
			g_led3BlinkTimeCnt = xTaskGetTickCount();
			
			LED3_TOGGLE;
		}
		
		vTaskDelay(10);//10ms扫描一次
	}
}
	
/*************<end>********************/
